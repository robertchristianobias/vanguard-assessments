/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';

    config.uiColor = '#EEEEEE';
    config.toolbar =
        [
            { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Styles','Format'] },
            { name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Blockquote','-', 'JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'] },
            { name: 'links',       items : [ 'Link', 'Unlink' ] },
            { name: 'insert',      items : [ 'Table', 'Image'] },
            { name: 'colors',      items : [ 'TextColor','BGColor', 'Source'] },
            { name: 'forms',       items : [ 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button']}
        ];
        
    config.extraPlugins = 'forms';
    config.extraPlugins = 'dialog';
    config.extraPlugins = 'dialogui';
    config.extraPlugins = 'fakeobjects';
    config.filebrowserImageBrowseUrl = app_url + 'assets/plugins/ckfinder/ckfinder.html?type=Images';
    config.filebrowserFlashBrowseUrl = app_url + 'assets/plugins/ckfinder/ckfinder.html?type=Flash';
    config.filebrowserUploadUrl      = app_url + 'assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
    config.filebrowserImageUploadUrl = app_url + 'assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
    config.filebrowserFlashUploadUrl = app_url + 'assets/plugins/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';

};

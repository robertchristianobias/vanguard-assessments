<?php // Adds X-Frame-Options to HTTP header, so that page can only be shown in an iframe of the same site.
header('X-Frame-Options: SAMEORIGIN'); // FF 3.6.9+ Chrome 4.1+ IE 8+ Safari 4+ Opera 10.5+
$user = $this->ion_auth->user()->row();
?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo config_item('app_name'); ?> | <?php echo $page_heading; ?></title>

		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="<?php echo assets_url('plugins/bootstrap/css/bootstrap.min.css'); ?>" />
		<link rel="stylesheet" href="<?php echo assets_url('plugins/font-awesome/css/font-awesome.min.css'); ?>" />

		<!-- page specific plugin styles -->

		<!-- text fonts -->
		<link rel="stylesheet" href="<?php echo site_url('themes/backend/aceadmin/css/ace-fonts.min.css'); ?>" />

		<!-- ace styles -->
		<link rel="stylesheet" href="<?php echo site_url('themes/backend/aceadmin/css/ace.min.css'); ?>" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="<?php echo site_url('themes/backend/aceadmin/css/ace-part2.min.css'); ?>" class="ace-main-stylesheet" />
		<![endif]-->
		<link rel="stylesheet" href="<?php echo site_url('themes/backend/aceadmin/css/ace-skins.min.css'); ?>" />
		<link rel="stylesheet" href="<?php echo site_url('themes/backend/aceadmin/css/ace-rtl.min.css'); ?>" />

		<link href="<?php echo assets_url('plugins/alertifyjs/css/alertify.min.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assets_url('plugins/alertifyjs/css/themes/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="<?php echo site_url('themes/backend/aceadmin/css/ace-ie.min.css'); ?>" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<!-- <script src="<?php echo site_url('themes/backend/aceadmin/js/ace-extra.min.js'); ?>"></script> -->

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="../components/html5shiv/dist/html5shiv.min.js"></script>
		<script src="../components/respond/dest/respond.min.js"></script>
		<![endif]-->

		<?php echo $_styles; // loads additional css files ?>

		<!-- Custom CSS and overrides -->
		<link href="<?php echo site_url('themes/backend/aceadmin/css/styles.css'); ?>" rel="stylesheet" type="text/css" />

		<link rel="icon" href="<?php echo assets_url('images/ci.png'); ?>">

	</head>


	<body class="login-layout blur-login">
		
		<?php echo $content; ?>

		<script>
			app_url = '<?php echo site_url(); ?>';
			<?php if (NULL !== config_item('website_url')): ?>
				website_url = '<?php echo config_item('website_url'); ?>';
			<?php endif; ?>
		</script>

		<script src="<?php echo assets_url('plugins/jquery/jquery-2.1.4.min.js'); ?>"></script>
		<script src="<?php echo assets_url('plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
		<script src="<?php echo site_url('themes/backend/aceadmin/js/src/elements.scroller.js'); ?>"></script>
		<script src="<?php echo site_url('themes/backend/aceadmin/js/ace.min.js'); ?>"></script>
		<script src="<?php echo assets_url('plugins/alertifyjs/alertify.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo site_url('themes/backend/aceadmin/js/scripts.js'); ?>"></script>

		<?php echo $_scripts; // loads additional js files ?>

		<?php if (isset($error_message)): ?>
			<script>alertify.error("<?php echo $error_message; ?>");</script>
		<?php endif; ?>

		<?php if (isset($message)): ?>
			<script>alertify.success("<?php echo $message; ?>");</script>
		<?php endif; ?>

		<?php if (NULL != $this->session->flashdata('flash_message')): ?>
			<script>alertify.success("<?php echo $this->session->flashdata('flash_message'); ?>");</script>
		<?php endif; ?>

		<?php if (NULL != $this->session->flashdata('flash_error')): ?>
			<script>alertify.error("<?php echo $this->session->flashdata('flash_error'); ?>");</script>
		<?php endif; ?>

	</body>
</html>
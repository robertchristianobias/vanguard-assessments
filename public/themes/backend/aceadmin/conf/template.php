<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// default template
$template['aceadmin']['template'] = 'backend/aceadmin/template';
$template['aceadmin']['regions'] = array('head', 'styles', 'header', 'content', 'footer', 'scripts');
$template['aceadmin']['parser'] = 'parser';
$template['aceadmin']['parser_method'] = 'parse';
$template['aceadmin']['parse_template'] = FALSE;

// blank
$template['blank']['template'] = 'backend/aceadmin/blank';
$template['blank']['regions'] = array('styles', 'header', 'content', 'footer', 'scripts');
$template['blank']['parser'] = 'parser';
$template['blank']['parser_method'] = 'parse';
$template['blank']['parse_template'] = FALSE;

// modal
$template['modal']['template'] = 'backend/aceadmin/modal';
$template['modal']['regions'] = array('styles', 'content', 'scripts');
$template['modal']['parser'] = 'parser';
$template['modal']['parser_method'] = 'parse';
$template['modal']['parse_template'] = FALSE;

// confirm
$template['confirm']['template'] = 'backend/aceadmin/confirm';
$template['confirm']['regions'] = array('content');
$template['confirm']['parser'] = 'parser';
$template['confirm']['parser_method'] = 'parse';
$template['confirm']['parse_template'] = FALSE;
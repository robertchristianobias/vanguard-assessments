<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// theme config
$config = array(
	'theme_code' => 'aceadmin',
	'theme_name' => 'Ace Admin',
	'theme_author' => 'Mohsen Hosseini <ace.support@live.com>',
	'theme_copyright' => 'Mohsen Hosseini',
	'theme_version' => '1.4.0',
	'theme_layouts' => array(),
	'theme_widget_sections' => array()
);
<?php // Adds X-Frame-Options to HTTP header, so that page can only be shown in an iframe of the same site.
header('X-Frame-Options: SAMEORIGIN'); // FF 3.6.9+ Chrome 4.1+ IE 8+ Safari 4+ Opera 10.5+
$user = $this->ion_auth->user()->row();
?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo config_item('app_name'); ?> | <?php echo $page_heading; ?></title>

		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<!-- bootstrap & fontawesome -->
		<link rel="stylesheet" href="<?php echo assets_url('plugins/bootstrap/css/bootstrap.min.css'); ?>" />
		<link rel="stylesheet" href="<?php echo assets_url('plugins/font-awesome/css/font-awesome.min.css'); ?>" />

		<!-- text fonts -->
		<link rel="stylesheet" href="<?php echo site_url('themes/backend/aceadmin/css/ace-fonts.min.css'); ?>" />

		<!-- ace styles -->
		<link rel="stylesheet" href="<?php echo site_url('themes/backend/aceadmin/css/ace.min.css'); ?>" class="ace-main-stylesheet" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="<?php echo site_url('themes/backend/aceadmin/css/ace-part2.min.css'); ?>" class="ace-main-stylesheet" />
		<![endif]-->
		<link rel="stylesheet" href="<?php echo site_url('themes/backend/aceadmin/css/ace-skins.min.css'); ?>" />
		<link rel="stylesheet" href="<?php echo site_url('themes/backend/aceadmin/css/ace-rtl.min.css'); ?>" />

		<link href="<?php echo assets_url('plugins/alertifyjs/css/alertify.min.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assets_url('plugins/alertifyjs/css/themes/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="<?php echo site_url('themes/backend/aceadmin/css/ace-ie.min.css'); ?>" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<!-- <script src="<?php echo site_url('themes/backend/aceadmin/js/ace-extra.min.js'); ?>"></script> -->

		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->

		<!--[if lte IE 8]>
		<script src="../components/html5shiv/dist/html5shiv.min.js"></script>
		<script src="../components/respond/dest/respond.min.js"></script>
		<![endif]-->

		<?php echo $_styles; // loads additional css files ?>

		<!-- Custom CSS and overrides -->
		<link href="<?php echo site_url('themes/backend/aceadmin/css/styles.css'); ?>" rel="stylesheet" type="text/css" />

		<link rel="icon" href="<?php echo site_url('themes/backend/aceadmin/img/events.ico'); ?>">

	</head>

	<body class="no-skin">
		<!-- #section:basics/navbar.layout -->
		<div id="navbar" class="navbar navbar-default          ace-save-state">
			<div class="navbar-container ace-save-state" id="navbar-container">
				<!-- #section:basics/sidebar.mobile.toggle -->
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>

				<!-- /section:basics/sidebar.mobile.toggle -->
				<div class="navbar-header pull-left">
					<!-- #section:basics/navbar.layout.brand -->
					<a href="<?php echo site_url('dashboard'); ?>" class="navbar-brand">
						<small>
							<i class="fa fa-calendar"></i>
							<?php echo config_item('app_name'); ?>
						</small>
					</a>

					<!-- /section:basics/navbar.layout.brand -->

					<!-- #section:basics/navbar.toggle -->

					<!-- /section:basics/navbar.toggle -->
				</div>

				<!-- #section:basics/navbar.dropdown -->
				<div class="navbar-buttons navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						

						<li class="purple dropdown-modal">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="ace-icon fa fa-bell icon-animated-bell"></i>
								<span class="badge badge-important">8</span>
							</a>

							<ul class="dropdown-menu-right dropdown-navbar navbar-green dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="ace-icon fa fa-exclamation-triangle"></i>
									8 Notifications
								</li>

								<li class="dropdown-content">
									<ul class="dropdown-menu dropdown-navbar navbar-pink">
										<li>
											<a href="#">
												<div class="clearfix">
													<span class="pull-left">
														<i class="btn btn-xs no-hover btn-pink fa fa-comment"></i>
														New Comments
													</span>
													<span class="pull-right badge badge-info">+12</span>
												</div>
											</a>
										</li>

										<li>
											<a href="#">
												<i class="btn btn-xs btn-primary fa fa-user"></i>
												Bob just signed up as an editor ...
											</a>
										</li>

										<li>
											<a href="#">
												<div class="clearfix">
													<span class="pull-left">
														<i class="btn btn-xs no-hover btn-success fa fa-shopping-cart"></i>
														New Orders
													</span>
													<span class="pull-right badge badge-success">+8</span>
												</div>
											</a>
										</li>

										<li>
											<a href="#">
												<div class="clearfix">
													<span class="pull-left">
														<i class="btn btn-xs no-hover btn-info fa fa-twitter"></i>
														Followers
													</span>
													<span class="pull-right badge badge-info">+11</span>
												</div>
											</a>
										</li>
									</ul>
								</li>

								<li class="dropdown-footer">
									<a href="#">
										See all notifications
										<i class="ace-icon fa fa-arrow-right"></i>
									</a>
								</li>
							</ul>
						</li>

						

						<!-- #section:basics/navbar.user_menu -->
						<li class="light-blue dropdown-modal">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="<?php echo site_url($user->photo); ?>" alt="Jason's Photo" />
								<span class="user-info">
									<small>Welcome,</small>
									<?php echo $user->first_name; ?>
								</span>

								<i class="ace-icon fa fa-caret-down"></i>
							</a>

							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="<?php echo site_url('users/password'); ?>" data-toggle="modal" data-target="#modal">
										<i class="ace-icon fa fa-lock"></i>
										Change Password
									</a>
								</li>

								<li>
									<a href="<?php echo site_url('users/profile'); ?>" data-toggle="modal" data-target="#modal">
										<i class="ace-icon fa fa-user"></i>
										Change Profile
									</a>
								</li>

								<li>
									<a href="<?php echo site_url('users/photo'); ?>">
										<i class="ace-icon fa fa-file-image-o"></i>
										Change Photo
									</a>
								</li>

								<li class="divider"></li>

								<li>
									<a href="<?php echo site_url('users/logout'); ?>">
										<i class="ace-icon fa fa-power-off"></i>
										Logout
									</a>
								</li>
							</ul>
						</li>

						<!-- /section:basics/navbar.user_menu -->
					</ul>
				</div>

				<!-- /section:basics/navbar.dropdown -->
			</div><!-- /.navbar-container -->
		</div>

		<!-- /section:basics/navbar.layout -->
		<div class="main-container ace-save-state" id="main-container">
			<script type="text/javascript">
				try{ace.settings.loadState('main-container')}catch(e){}
			</script>

			<!-- #section:basics/sidebar -->
			<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
				<script type="text/javascript">
					try{ace.settings.loadState('sidebar')}catch(e){}
				</script>

				<div class="sidebar-shortcuts" id="sidebar-shortcuts">
					<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
						<button class="btn btn-success">
							<i class="ace-icon fa fa-tags"></i>
						</button>

						<button class="btn btn-info">
							<i class="ace-icon fa fa-sitemap"></i>
						</button>

						<!-- #section:basics/sidebar.layout.shortcuts -->
						<a href="<?php echo site_url('users/users'); ?>" class="btn btn-warning">
							<i class="ace-icon fa fa-users"></i>
						</a>

						<a href="<?php echo site_url('settings/configs'); ?>" class="btn btn-danger">
							<i class="ace-icon fa fa-cogs"></i>
						</a>

						<!-- /section:basics/sidebar.layout.shortcuts -->
					</div>

					<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
						<span class="btn btn-success"></span>

						<span class="btn btn-info"></span>

						<span class="btn btn-warning"></span>

						<span class="btn btn-danger"></span>
					</div>
				</div><!-- /.sidebar-shortcuts -->

				<?php echo $this->app_menu->show(); ?>

				

				<!-- #section:basics/sidebar.layout.minimize -->
				<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
					<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
				</div>

				<!-- /section:basics/sidebar.layout.minimize -->
			</div>

			<!-- /section:basics/sidebar -->
			<div class="main-content">
				<div class="main-content-inner">
					<!-- #section:basics/content.breadcrumbs -->
					<div class="breadcrumbs ace-save-state" id="breadcrumbs">

						<?php echo $this->breadcrumbs->show(); ?>

						<!-- #section:basics/content.searchbox -->
						<div class="nav-search hide" id="nav-search">
							<form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
									<i class="ace-icon fa fa-search nav-search-icon"></i>
								</span>
							</form>
						</div><!-- /.nav-search -->

						<!-- /section:basics/content.searchbox -->
					</div>

					<!-- /section:basics/content.breadcrumbs -->
					<div class="page-content">
						

						<div class="page-header">
							<h1>
								<?php echo $page_heading; ?>
								<small>
									<i class="ace-icon fa fa-angle-double-right"></i>
									<?php echo $page_subhead; ?>
								</small>
							</h1>
						</div><!-- /.page-header -->

						<!-- /section:settings.box -->
						<div class="row">
							<div class="col-xs-12">
								<!-- PAGE CONTENT BEGINS -->

								<?php echo $content; ?>

								<!-- PAGE CONTENT ENDS -->
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->

			<div class="footer">
				<div class="footer-inner">
					<!-- #section:basics/footer -->
					<div class="footer-content">
						<span class="bigger-120">
							<span class="blue bolder"><?php echo config_item('app_name'); ?></span>
							&copy; <?php echo date('Y'); ?>
						</span>

						&nbsp; &nbsp;
						<span class="action-buttons">
							<a href="#">
								<i class="ace-icon fa fa-twitter-square light-blue bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-facebook-square text-primary bigger-150"></i>
							</a>

							<a href="#">
								<i class="ace-icon fa fa-rss-square orange bigger-150"></i>
							</a>
						</span>
					</div>

					<!-- /section:basics/footer -->
				</div>
			</div>

			<a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
				<i class="ace-icon fa fa-angle-double-up icon-only bigger-110"></i>
			</a>
		</div><!-- /.main-container -->

		<div class="modal" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span>
							<span class="sr-only"><?php echo lang('close')?></span>
						</button>
						<h4 class="modal-title" id="myModalLabel"><?php echo lang('loading')?></h4>
					</div>
					<div class="modal-body">
						<div class="text-center">
							<img src="<?php echo assets_url('images/loading3.gif')?>" alt="<?php echo lang('loading')?>" />
							<p><?php echo lang('loading')?></p>
						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="modal" id="modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span>
							<span class="sr-only"><?php echo lang('close')?></span>
						</button>
						<h4 class="modal-title" id="myModalLabel"><?php echo lang('loading')?></h4>
					</div>
					<div class="modal-body">
						<div class="text-center">
							<img src="<?php echo assets_url('images/loading3.gif')?>" alt="<?php echo lang('loading')?>" />
							<p><?php echo lang('loading')?></p>
						</div>
					</div>

				</div>
			</div>
		</div>

		<div class="modal" id="modal_restricted" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span>
							<span class="sr-only"><?php echo lang('close')?></span>
						</button>
						<h4 class="modal-title" id="myModalLabel"><?php echo lang('title_restricted')?></h4>
					</div>
					<div class="modal-body">
						<div class="text-center">
							<div id="message" class="callout callout-danger callout-dismissable">
								<?php echo lang('error_page_restricted')?>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">
							<i class="fa fa-times"></i> <?php echo lang('button_close')?>
						</button>
					</div>
				</div>
			</div>
		</div>

		<script>
			app_url = '<?php echo site_url(); ?>';
			<?php if (NULL !== config_item('website_url')): ?>
				website_url = '<?php echo config_item('website_url'); ?>';
			<?php endif; ?>
		</script>

		<script src="<?php echo assets_url('plugins/jquery/jquery-2.1.4.min.js'); ?>"></script>
		<script src="<?php echo assets_url('plugins/jquery-ui/jquery-ui.min.js'); ?>"></script>
		<script src="<?php echo assets_url('plugins/bootstrap/js/bootstrap.min.js'); ?>"></script>
		<script src="<?php echo site_url('themes/backend/aceadmin/js/src/elements.scroller.js'); ?>"></script>
		<script src="<?php echo site_url('themes/backend/aceadmin/js/ace.min.js'); ?>"></script>
		<script src="<?php echo assets_url('plugins/alertifyjs/alertify.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo site_url('themes/backend/aceadmin/js/scripts.js'); ?>"></script>

		<?php echo $_scripts; // loads additional js files ?>

		<?php if (isset($error_message)): ?>
			<script>alertify.error("<?php echo $error_message; ?>");</script>
		<?php endif; ?>

		<?php if (isset($message)): ?>
			<script>alertify.success("<?php echo $message; ?>");</script>
		<?php endif; ?>

		<?php if (NULL != $this->session->flashdata('flash_message')): ?>
			<script>alertify.success("<?php echo $this->session->flashdata('flash_message'); ?>");</script>
		<?php endif; ?>

		<?php if (NULL != $this->session->flashdata('flash_error')): ?>
			<script>alertify.error("<?php echo $this->session->flashdata('flash_error'); ?>");</script>
		<?php endif; ?>

	</body>
</html>
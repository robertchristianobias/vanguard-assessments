<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// frontend_index
$template['frontend_index']['template'] = 'frontend/frontend/index';
$template['frontend_index']['regions'] = array('head', 'styles', 'header', 'content', 'footer', 'scripts');
$template['frontend_index']['parser'] = 'parser';
$template['frontend_index']['parser_method'] = 'parse';
$template['frontend_index']['parse_template'] = FALSE;

// frontend
$template['frontend']['template'] = 'frontend/frontend/template';
$template['frontend']['regions'] = array('head', 'styles', 'header', 'content', 'footer', 'scripts');
$template['frontend']['parser'] = 'parser';
$template['frontend']['parser_method'] = 'parse';
$template['frontend']['parse_template'] = FALSE;

$template['frontend_modal']['template'] = 'frontend/frontend/modal';
$template['frontend_modal']['regions'] = array('styles', 'content', 'scripts');
$template['frontend_modal']['parser'] = 'parser';
$template['frontend_modal']['parser_method'] = 'parse';
$template['frontend_modal']['parse_template'] = FALSE;
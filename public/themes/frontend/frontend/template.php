<?php // Adds X-Frame-Options to HTTP header, so that page can only be shown in an iframe of the same site.
header('X-Frame-Options: SAMEORIGIN'); // FF 3.6.9+ Chrome 4.1+ IE 8+ Safari 4+ Opera 10.5+
$user = $this->ion_auth->user()->row();
?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo config_item('website_name'); ?> | <?php echo $page_heading; ?></title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<?php echo $head; ?>
		
		<link href="<?php echo assets_url('plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assets_url('plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assets_url('plugins/alertifyjs/css/alertify.min.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assets_url('plugins/alertifyjs/css/themes/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">

		<link href="<?php echo site_url('themes/frontend/frontend/css/frontend.css'); ?>" rel="stylesheet" type="text/css" />

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
				<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<?php echo $_styles; // loads additional css files ?>

		<link rel="icon" href="<?php echo site_url('themes/frontend/frontend/img/events.ico'); ?>">

	</head>

	<body>
		<main>
			<?php echo $content; ?>
			<div id="form_container">
				<div class="row">
					<div class="col-lg-5">
						<div id="left_form">
							<figure><img src="img/registration_bg.svg" alt=""></figure>
							<h2>Questionaire</h2>
							<p>Tation argumentum et usu, dicit viderer evertitur te has. Eu dictas concludaturque usu, facete detracto patrioque an per, lucilius pertinacia eu vel.</p>
							<a href="#0" id="more_info" data-toggle="modal" data-target="#more-info"><i class="pe-7s-info"></i></a>
						</div>
					</div>
					<div class="col-lg-7">

						<div id="wizard_container" class="wizard" novalidate="novalidate">
							<div id="top-wizard">
								<div id="progressbar" class="ui-progressbar ui-widget ui-widget-content ui-corner-all" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0"><div class="ui-progressbar-value ui-widget-header ui-corner-left" style="display: none; width: 0%;"></div></div>
							</div>
							<!-- /top-wizard -->
							<form name="example-1" id="wrapped" method="POST" action="questionare_send.php" class="wizard-form">
								<input id="website" name="website" type="text" value="">
								<!-- Leave for security protection, read docs for details -->
								<div id="middle-wizard" class="wizard-branch wizard-wrapper">
									
									<div class="step wizard-step current" style="">
										<h3 class="main_question wizard-header"><strong>1/3</strong>How do you describe your overall satisfaction?</h3>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group radio_input">
													<label class=""><div class="iradio_square-grey" style="position: relative;"><input type="radio" value="Satisfied" name="satisfaction" class="icheck required" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>I am very satisfied</label>
												</div>
												<div class="form-group radio_input">
													<label class=""><div class="iradio_square-grey" style="position: relative;"><input type="radio" value="Somewhat satisfied" name="satisfaction" class="icheck required" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>I am somewhat satisfied</label>
												</div>
												<div class="form-group radio_input">
													<label class=""><div class="iradio_square-grey" style="position: relative;"><input type="radio" value="Neutral" name="satisfaction" class="icheck required" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>Neutral</label>
												</div>
												<div class="form-group radio_input">
													<label class=""><div class="iradio_square-grey" style="position: relative;"><input type="radio" value="Somewhat dissatisfied" name="satisfaction" class="icheck required" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>I am somewhat dissatisfied</label>
												</div>
												<div class="form-group radio_input">
													<label class=""><div class="iradio_square-grey" style="position: relative;"><input type="radio" value="Very dissatisfied" name="satisfaction" class="icheck required" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>I am very dissatisfied</label>
												</div>
											</div>
										</div>
										<!-- /row -->
									</div>
									<!-- /step -->
									
									<div class="step wizard-step" style="display: none;">
										<h3 class="main_question"><strong>2/3</strong>How do you describe your carrer or profile?</h3>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group radio_input">
													<label><div class="icheckbox_square-grey" style="position: relative;"><input type="checkbox" value="Professional" name="question_2[]" class="icheck required" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>Professional</label>
												</div>
												<div class="form-group radio_input">
													<label><div class="icheckbox_square-grey" style="position: relative;"><input type="checkbox" value="Graduated" name="question_2[]" class="icheck required" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>Graduated</label>
												</div>
												<div class="form-group radio_input">
													<label><div class="icheckbox_square-grey" style="position: relative;"><input type="checkbox" value="Not graduated" name="question_2[]" class="icheck required" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>Not graduated</label>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group radio_input">
													<label><div class="icheckbox_square-grey" style="position: relative;"><input type="checkbox" value="Emphatic" name="question_2[]" class="icheck required" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>Emphatic</label>
												</div>
												<div class="form-group radio_input">
													<label><div class="icheckbox_square-grey" style="position: relative;"><input type="checkbox" value="Motivated" name="question_2[]" class="icheck required" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>Motivated</label>
												</div>
												<div class="form-group radio_input">
													<label><div class="icheckbox_square-grey" style="position: relative;"><input type="checkbox" value="Passionate" name="question_2[]" class="icheck required" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>Passionate</label>
												</div>
											</div>
										</div>
										<!-- /row -->
									</div>
									<!-- /step -->

									<div class="submit step wizard-step" disabled="disabled" style="display: none;">
										<h3 class="main_question"><strong>3/3</strong>Please fill with your details</h3>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<input type="text" name="firstname" class="form-control required" placeholder="First name">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<input type="text" name="lastname" class="form-control required" placeholder="Last name">
												</div>
											</div>
										</div>
										<!-- /row -->

										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<input type="email" name="email" class="form-control required" placeholder="Your Email">
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<input type="text" name="telephone" class="form-control" placeholder="Your Telephone">
												</div>
											</div>
										</div>
										<!-- /row -->
										<br>
										<div class="row">
											<div class="col-md-12">
												<div class="form-group terms">
													<div class="icheckbox_square-grey" style="position: relative;"><input name="terms" type="checkbox" class="icheck required" value="yes" style="position: absolute; opacity: 0;"><ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"></ins></div>
													<label>Please accept <a href="#" data-toggle="modal" data-target="#terms-txt">terms and conditions</a> ?</label>
												</div>
											</div>
										</div>
									</div>
									<!-- /step-->
								</div>
								<!-- /middle-wizard -->
								<div id="bottom-wizard">
									<button type="button" name="backward" class="backward" disabled="disabled">Backward </button>
									<button type="button" name="forward" class="forward">Forward</button>
									<button type="submit" name="process" class="submit" disabled="disabled">Submit</button>
								</div>
								<!-- /bottom-wizard -->
							</form>
						</div>
						<!-- /Wizard container -->
					</div>
				</div><!-- /Row -->
			</div><!-- /Form_container -->
		</main>
		
		
		<div class="main-container" id="top">
			
			<section id="content" class="container">
				<div class="row">
					<div class="col-sm-12">
						<div id="breadcrumbs">
							<?php echo $this->breadcrumbs->show(); ?>
						</div>
					</div>

					<?php if ($page_layout == 'right_sidebar'): ?>
						<div class="col-sm-9">
							<?php echo $content; ?>
						</div>
						<div class="col-sm-3">
							<?php include_once('sidebar.php'); ?>
						</div>
					<?php elseif ($page_layout == 'left_sidebar'): ?>
						<div class="col-sm-3">
							<?php include_once('sidebar.php'); ?>
						</div>
						<div class="col-sm-9">
							<?php echo $content; ?>
						</div>
					<?php elseif ($page_layout == 'full_width'): ?>
						<div class="col-sm-12">
							<?php echo $content; ?>
						</div>
					<?php elseif ($page_layout == 'narrow_width'): ?>
						<div class="col-sm-offset-2 col-sm-8">
							<?php echo $content; ?>
						</div>
					<?php endif; ?>
				</div>

			</section>
			
			<footer id="footer" class="section">
				<div class="container">
					<div class="row">
						<div class="col-sm-3">
							<?php frontend_widgets('footer1'); ?>
						</div>
						<div class="col-sm-3">
							<?php frontend_widgets('footer2'); ?>
						</div>
						<div class="col-sm-3">
							<?php frontend_widgets('footer3'); ?>
						</div>
						<div class="col-sm-3">
							<?php frontend_widgets('footer4'); ?>
						</div>
					</div>
				</div>
			</footer>

		</div>

		<?php if (NULL !== config_item('website_url')): ?>
        <script>var website_url = "<?php echo site_url(); ?>";</script>
		<?php endif; ?>

		<script src="<?php echo assets_url('plugins/jquery/jquery-2.1.4.min.js'); ?>"></script>
		<script src="<?php echo assets_url('plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo assets_url('plugins/slimScroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo assets_url('plugins/fastclick/fastclick.min.js'); ?>"></script>
		<script src="<?php echo assets_url('plugins/alertifyjs/alertify.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo site_url('themes/frontend/frontend/js/scripts.js'); ?>" type="text/javascript"></script>

		<?php echo $_scripts; // loads additional js files ?>

		<script type="text/javascript">
		jQuery(function($) {
			///make navbar compact when scrolling down
			var isCompact = false;
			$(window)
			.on('scroll.scroll_nav', function() {
				var scroll = $(window).scrollTop();
				var h = $(window).height();
				var body_sH = document.body.scrollHeight;
				if(scroll > parseInt(h / 4) || (scroll > 0 && body_sH >= h && h + scroll >= body_sH - 1)) {//|| for smaller pages, when reached end of page
					if(!isCompact) $('.navbar').addClass('navbar-compact');
					isCompact = true;
				}
				else {
					$('.navbar').removeClass('navbar-compact');
					isCompact = false;
				}
			}).triggerHandler('scroll.scroll_nav');
		});
		</script>
	</body>
</html>
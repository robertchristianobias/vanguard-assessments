<?php // Adds X-Frame-Options to HTTP header, so that page can only be shown in an iframe of the same site.
header('X-Frame-Options: SAMEORIGIN'); // FF 3.6.9+ Chrome 4.1+ IE 8+ Safari 4+ Opera 10.5+
$user = $this->ion_auth->user()->row();
?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $page_heading; ?> | <?php echo config_item('website_name'); ?></title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<?php echo $head; ?>
		
		<link href="<?php echo assets_url('plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assets_url('plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assets_url('plugins/alertifyjs/css/alertify.min.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assets_url('plugins/alertifyjs/css/themes/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">

		<link href="<?php echo site_url('themes/frontend/frontend/css/frontend.css'); ?>" rel="stylesheet" type="text/css" />

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
				<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<?php echo $_styles; // loads additional css files ?>

		<link rel="icon" href="<?php echo site_url('themes/frontend/frontend/img/events.ico'); ?>">

	</head>
	<body>
		<main>
			<div id="form_container">
				<div class="row">
					<div class="col-md-4">
						<div id="left_form">
							<img src="<?php echo site_url('themes/frontend/frontend/img/logo.jpg'); ?>" class="img-responsive" />
							<?php if ( $user ) { ?>
								<?php if ( isset($user->first_name) ) { ?>
									<div class="form-horizontal">
										<div class="form-group">
											<label class="control-label col-sm-3" for="name">Name: </label>
											<div class="col-sm-9">
												<p class="form-control-static"><?php echo $user->first_name . ' ' . $user->last_name; ?></p>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-sm-3" for="email">Email: </label>
											<div class="col-sm-9">
												<p class="form-control-static"><?php echo $user->email; ?></p>
											</div>
										</div>
										<div class="form-group">
											<label class="control-label col-sm-3" for="contact_no">Phone No: </label>
											<div class="col-sm-9">
												<p class="form-control-static"><?php echo $user->phone; ?></p>
											</div>
										</div>
									</div>
								<?php } ?>
							<?php } ?>
						</div>
					</div>
					<div class="col-md-8">
						<?php echo $content; ?>
					</div>
				</div>
			</div>
			
			
		</main>
		<footer id="home" class="clearfix">
			<p>© <?php echo date('Y') . ' ' . config_item('website_name'); ?></p>
			<ul>
				<li><a href="#" class="animated_link">Terms and conditions</a></li>
				<li><a href="#" class="animated_link">Contacts</a></li>
			</ul>
		</footer>

		<div class="modal" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span>
							<span class="sr-only">Close</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">Loading...</h4>
					</div>
					<div class="modal-body">
						<div class="text-center">
							<img src="<?php echo site_url('assets/images/loading3.gif'); ?>" alt="Loading..." />
							<p>Loading...</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<?php if (NULL !== config_item('website_url')): ?>
        <script>var website_url = "<?php echo site_url(); ?>";</script>
		<?php endif; ?>

		<script src="<?php echo assets_url('plugins/jquery/jquery-2.1.4.min.js'); ?>"></script>
		<script src="<?php echo assets_url('plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo assets_url('plugins/alertifyjs/alertify.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo site_url('themes/frontend/frontend/js/scripts.js'); ?>" type="text/javascript"></script>

		<?php echo $_scripts; // loads additional js files ?>

	</body>
</html>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// assessment_index
$template['assessment_index']['template'] = 'frontend/assessment/index';
$template['assessment_index']['regions'] = array('head', 'styles', 'header', 'content', 'footer', 'scripts');
$template['assessment_index']['parser'] = 'parser';
$template['assessment_index']['parser_method'] = 'parse';
$template['assessment_index']['parse_template'] = FALSE;

// assessment
$template['assessment']['template'] = 'frontend/assessment/template';
$template['assessment']['regions'] = array('head', 'styles', 'header', 'content', 'footer', 'scripts');
$template['assessment']['parser'] = 'parser';
$template['assessment']['parser_method'] = 'parse';
$template['assessment']['parse_template'] = FALSE;

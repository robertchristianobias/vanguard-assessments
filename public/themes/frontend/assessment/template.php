<?php // Adds X-Frame-Options to HTTP header, so that page can only be shown in an iframe of the same site.
header('X-Frame-Options: SAMEORIGIN'); // FF 3.6.9+ Chrome 4.1+ IE 8+ Safari 4+ Opera 10.5+
//$user = $this->ion_auth->user()->row();
?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo $microsite->event_name; ?> | <?php echo $page_heading; ?></title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<?php if ($microsite->event_name) : ?>
		<meta property="og:title" content="<?php echo $microsite->event_name; ?>" />
		<?php endif; ?>

		<?php if ($microsite->microsite_hero_image) : ?>
		<meta property="og:image" content="<?php echo site_url($microsite->microsite_hero_image); ?>" />
		<?php endif; ?>

		<?php if ($microsite->event_description) : ?>
		<meta property="og:description" content="<?php echo $microsite->event_description ?>" />
		<?php endif; ?>

		<?php echo $head; ?>

		<link href="<?php echo assets_url('plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assets_url('plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assets_url('plugins/alertifyjs/css/alertify.min.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assets_url('plugins/alertifyjs/css/themes/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo site_url('themes/frontend/assessment/css/ace-fonts.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo site_url('themes/frontend/assessment/css/ace.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo site_url('themes/frontend/assessment/css/frontend.css'); ?>" rel="stylesheet" type="text/css" />

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
				<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<?php echo $_styles; // loads additional css files ?>

		<link rel="icon" href="<?php echo site_url('themes/frontend/assessment/img/events.ico'); ?>">

		<style>
		.jumbotron.has-background {
			background: transparent url("<?php echo site_url('themes/frontend/assessment/img/home-bg.jpg'); ?>") top center no-repeat;
			/* background source: https://pixabay.com/en/apple-steve-jobs-quotes-scrabble-758334/ */
		}
		.section-contact {
			background: #222 url("<?php echo site_url('themes/frontend/assessment/img/map-contact.jpg'); ?>") center no-repeat;
		}
		</style>

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="<?php echo site_url('themes/frontend/assessment/css/ace-part2.css'); ?>" class="ace-main-stylesheet" />
			<link rel="stylesheet" href="<?php echo site_url('themes/frontend/assessment/css/ace-ie.css'); ?>" />
		<![endif]-->


		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
		<!--[if lte IE 8]>
		<script src="<?php echo site_url('themes/frontend/assessment/components/html5shiv/dist/html5shiv.min.js'); ?>"></script>
		<script src="<?php echo site_url('themes/frontend/assessment/components/respond/dest/respond.min.js'); ?>"></script>
		<![endif]-->
	</head>

	<body class="no-skin pos-rel" data-spy="scroll" data-target="#menu">
		<div id="navbar" class="navbar navbar-default navbar-fixed-top">
			<div class="navbar-container container" id="navbar-container">

				<!-- <button data-target="#menu" data-toggle="collapse" type="button" class="pull-right navbar-toggle collapsed">
					<span class="sr-only">Toggle menu</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button> -->

				<div class="navbar-header navbar-left">
                    <a href="<?php echo $microsite->microsite_logo_link; ?>" class="navbar-brand" target="_blank">
						<img src="<?php echo site_url($microsite->microsite_logo_image); ?>" />
					</a>
				</div>

				<div class="navbar-header navbar-right social-nav" role="navigation">
                    <ul class="nav nav-pills">
                        <li role="presentation">
                            <?php if($microsite->event_facebook) { ?>
                                <span class="social-icons">
                                    <a href="<?php echo $microsite->event_facebook; ?>" target="_blank">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                </span>
                            <?php } ?>
                        </li>
                        <li role="presentation">
                            <?php if($microsite->event_twitter) { ?>
                                <span class="social-icons">
                                    <a href="<?php echo $microsite->event_twitter; ?>" target="_blank">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                </span>
                            <?php } ?>
                        </li>
						<li role="presentation">
                            <?php if($microsite->event_instagram) { ?>
                                <span class="social-icons">
                                    <a href="<?php echo $microsite->event_instagram; ?>" target="_blank">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                </span>
                            <?php } ?>
                        </li>
                        <li role="presentation">
                            <?php if($microsite->event_website) { ?>
                                <span class="social-icons">
                                    <a href="<?php echo $microsite->event_website; ?>" target="_blank">
                                        <i class="fa fa-globe"></i>
                                    </a>
                                </span>
                            <?php } ?>
                        </li>

                    </ul>

                </div>

			</div><!-- /.navbar-container -->
		</div>

		<div id="hero_image" style="background-image: url(<?php echo site_url($microsite->microsite_hero_image); ?>)">
	        <div class="inner">
				<?php
					$last = $this->uri->total_segments();
					$record_num = $this->uri->segment($last - 1);
				?>
				<?php if($record_num  == "register") : ?>
	            <h1><?php //// echo $microsite->event_name; ?></h1>
				<?php endif; ?>
	        </div>
	    </div>

		<div class="main-container" id="top">

			<section id="content" class="container">

				<?php echo $content; ?>

			</section>


			<footer id="footer" class="section">
				<div class="container">
					<div class="col-md-offset-2 col-md-8">
						<ul class="nav nav-pills nav-stacked text-center">
							<li role="presentation"><a href="<?php echo site_url('event/terms_of_service/'.$microsite->microsite_slug); ?>" data-toggle="modal" data-target="#modal">Terms of Service</a></li>
                            <li role="presentation"><a href="http://www.google.com/" target="_blank">&copy; 2016 Google Inc.</a></li>
						</ul>
					</div>
				</div>
			</footer>

		</div>

        <div class="modal" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">
							<span aria-hidden="true">&times;</span>
							<span class="sr-only">Close</span>
						</button>
						<h4 class="modal-title" id="myModalLabel">Loading...</h4>
					</div>
					<div class="modal-body">
						<div class="text-center">
							<img src="<?php echo site_url('assets/images/loading3.gif'); ?>" alt="Loading..." />
							<p>Loading...</p>
						</div>
					</div>

				</div>
			</div>
		</div>

		<script src="<?php echo assets_url('plugins/jquery/jquery-2.1.4.min.js'); ?>"></script>
		<script src="<?php echo assets_url('plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo assets_url('plugins/slimScroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo assets_url('plugins/fastclick/fastclick.min.js'); ?>"></script>
		<script src="<?php echo assets_url('plugins/alertifyjs/alertify.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo site_url('themes/frontend/assessment/js/scripts.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo site_url('themes/frontend/starter/js/social-plugins.js'); ?>" type="text/javascript"></script>
        
        <!-- Add to CMS -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-87071919-1', 'auto');
            ga('send', 'pageview');

        </script>
		<?php echo $_scripts; // loads additional js files ?>

	</body>
</html>

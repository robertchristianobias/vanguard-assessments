/**
 * @package		rcmediaph
 * @version		1.0
 * @author 		JP Llapitan <john.llapitan@google.com>
 * @copyright 	Copyright (c) 2014-2015, JP Llapitan
 * @link		john.llapitan@google.com
 */
$(function() {

    $(".social-plugin").click(function(e){
        e.preventDefault();

        var name        = $(this).attr("id");
        var channel     = $(this).attr("title");
        var url         = $(this).attr("data-url");

        var title       = $("head").find('meta[property*="og:title"]').attr('content') || $("head").find('title').html();
        var caption     = $("head").find('meta[property*="og:description"]').attr('content');
        var image       = $("head").find('meta[property*="og:image"]').attr('content');

        switch (name)
        {
            case "facebook" :
                send_tracking(name, 'share', url);
                window.open('https://www.facebook.com/sharer/sharer.php?u=' + encodeURIComponent(url), 'facebook', 'top=0,left=0,menu=false,width=550,height=500');
                break;

            case "twitter" :
                send_tracking(name, 'share', url);
                window.open('https://twitter.com/intent/tweet?url=' + encodeURIComponent(url) + '&text=' + title, 'twitter', 'top=0,left=0,menu=false,width=550,height=450');
                break;

            case "googleplus" :
                send_tracking(name, 'share', url);
                window.open('https://plus.google.com/share?url=' + encodeURIComponent(url), 'googleplus', 'top=0,left=0,menu=false,width=750,height=500');
                break;

            case "linkedin" :
                send_tracking(name, 'share', url);
                window.open('https://www.linkedin.com/cws/share?url=' + encodeURIComponent(url) + '&_ts=' + Math.random(), 'linkedin', 'top=0,left=0,menu=false,width=550,height=500');
                break;

            case "pinterest" :
                send_tracking(name, 'share', url);
                window.open('https://www.pinterest.com/pin/create/button/?url=' + encodeURIComponent(url) + '&media=' + image + '&description=' + name, 'pinterest', 'top=0,left=0,menu=false,width=550,height=500');
                break;

            default:
                alertify.error('Widget not found.', 10);
                break;
        }

    });

    function send_tracking(channel, event, url)
    {
        $.getJSON(site_url + 'social_plugins/collect?channel=' + channel + '&event=' + event + '&url=' + encodeURI(url) + '&_t=' + (Math.random()*999), function(data, status){
            console.log(data.message);
        });
    }

});

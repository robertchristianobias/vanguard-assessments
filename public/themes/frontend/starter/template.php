<?php // Adds X-Frame-Options to HTTP header, so that page can only be shown in an iframe of the same site.
header('X-Frame-Options: SAMEORIGIN'); // FF 3.6.9+ Chrome 4.1+ IE 8+ Safari 4+ Opera 10.5+
$user = $this->ion_auth->user()->row();
?><!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta charset="utf-8" />
		<title><?php echo config_item('website_name'); ?> | <?php echo $page_heading; ?></title>

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

		<?php echo $head; ?>
		
		<link href="<?php echo assets_url('plugins/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assets_url('plugins/font-awesome/css/font-awesome.min.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assets_url('plugins/alertifyjs/css/alertify.min.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo assets_url('plugins/alertifyjs/css/themes/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo site_url('themes/frontend/starter/css/ace-fonts.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo site_url('themes/frontend/starter/css/ace.css'); ?>" rel="stylesheet" type="text/css" />
		<link href="<?php echo site_url('themes/frontend/starter/css/frontend.css'); ?>" rel="stylesheet" type="text/css" />

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
				<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

		<?php echo $_styles; // loads additional css files ?>

		<link rel="icon" href="<?php echo site_url('themes/frontend/starter/img/events.ico'); ?>">

		<style>
		.jumbotron.has-background {
			background: transparent url("<?php echo site_url('themes/frontend/starter/img/home-bg.jpg'); ?>") top center no-repeat;
			/* background source: https://pixabay.com/en/apple-steve-jobs-quotes-scrabble-758334/ */
		}
		.section-contact {
			background: #222 url("<?php echo site_url('themes/frontend/starter/img/map-contact.jpg'); ?>") center no-repeat;
		}
		</style>

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="<?php echo site_url('themes/frontend/starter/css/ace-part2.css'); ?>" class="ace-main-stylesheet" />
			<link rel="stylesheet" href="<?php echo site_url('themes/frontend/starter/css/ace-ie.css'); ?>" />
		<![endif]-->


		<!-- HTML5shiv and Respond.js for IE8 to support HTML5 elements and media queries -->
		<!--[if lte IE 8]>
		<script src="<?php echo site_url('themes/frontend/starter/components/html5shiv/dist/html5shiv.min.js'); ?>"></script>
		<script src="<?php echo site_url('themes/frontend/starter/components/respond/dest/respond.min.js'); ?>"></script>
		<![endif]-->
	</head>

	<body class="no-skin pos-rel" data-spy="scroll" data-target="#menu">
		<div id="navbar" class="navbar navbar-default navbar-fixed-top">
			<div class="navbar-container container" id="navbar-container">
				
				<button data-target="#menu" data-toggle="collapse" type="button" class="pull-right navbar-toggle collapsed">
					<span class="sr-only">Toggle menu</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>

				<div class="navbar-header navbar-left">
					<a href="<?php echo site_url(); ?>" class="navbar-brand">
						<small>
							<i class="fa fa-leaf"></i>
							<span class="light-green"><?php echo config_item('website_name'); ?></span>
						</small>
					</a>
				</div>

				

				<div class="navbar-header navbar-right " role="navigation">
					<div id="menu" class="collapse navbar-collapse">
						<?php echo frontend_navigation(1); ?>
					</div>				
				</div>
			</div><!-- /.navbar-container -->
		</div>
		
		<div class="main-container" id="top">
			
			<section id="content" class="container">
				<div class="row">
					<div class="col-sm-12">
						<div id="breadcrumbs">
							<?php echo $this->breadcrumbs->show(); ?>
						</div>
					</div>

					<?php if ($page_layout == 'right_sidebar'): ?>
						<div class="col-sm-9">
							<?php echo $content; ?>
						</div>
						<div class="col-sm-3">
							<?php include_once('sidebar.php'); ?>
						</div>
					<?php elseif ($page_layout == 'left_sidebar'): ?>
						<div class="col-sm-3">
							<?php include_once('sidebar.php'); ?>
						</div>
						<div class="col-sm-9">
							<?php echo $content; ?>
						</div>
					<?php elseif ($page_layout == 'full_width'): ?>
						<div class="col-sm-12">
							<?php echo $content; ?>
						</div>
					<?php elseif ($page_layout == 'narrow_width'): ?>
						<div class="col-sm-offset-2 col-sm-8">
							<?php echo $content; ?>
						</div>
					<?php endif; ?>
				</div>

			</section>
			
			
			<footer id="footer" class="section">
				<div class="container">
					<div class="row">
						<div class="col-sm-3">
							<?php frontend_widgets('footer1'); ?>
						</div>
						<div class="col-sm-3">
							<?php frontend_widgets('footer2'); ?>
						</div>
						<div class="col-sm-3">
							<?php frontend_widgets('footer3'); ?>
						</div>
						<div class="col-sm-3">
							<?php frontend_widgets('footer4'); ?>
						</div>
					</div>
				</div>
			</footer>

		</div>

		<script src="<?php echo assets_url('plugins/jquery/jquery-2.1.4.min.js'); ?>"></script>
		<script src="<?php echo assets_url('plugins/bootstrap/js/bootstrap.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo assets_url('plugins/slimScroll/jquery.slimscroll.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo assets_url('plugins/fastclick/fastclick.min.js'); ?>"></script>
		<script src="<?php echo assets_url('plugins/alertifyjs/alertify.min.js'); ?>" type="text/javascript"></script>
		<script src="<?php echo site_url('themes/frontend/starter/js/scripts.js'); ?>" type="text/javascript"></script>

		<?php echo $_scripts; // loads additional js files ?>

		<script type="text/javascript">
		jQuery(function($) {
			///make navbar compact when scrolling down
			var isCompact = false;
			$(window)
			.on('scroll.scroll_nav', function() {
				var scroll = $(window).scrollTop();
				var h = $(window).height();
				var body_sH = document.body.scrollHeight;
				if(scroll > parseInt(h / 4) || (scroll > 0 && body_sH >= h && h + scroll >= body_sH - 1)) {//|| for smaller pages, when reached end of page
					if(!isCompact) $('.navbar').addClass('navbar-compact');
					isCompact = true;
				}
				else {
					$('.navbar').removeClass('navbar-compact');
					isCompact = false;
				}
			}).triggerHandler('scroll.scroll_nav');
		});
		</script>
	</body>
</html>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// theme config
$config = array(
	'theme_code' => 'starter',
	'theme_name' => 'Starter',
	'theme_author' => 'Robert Christian Obias <rchristian_obias@yahoo.com>',
	'theme_copyright' => 'Google.',
	'theme_version' => '1.0',
	'theme_layouts' => array(
		'right_sidebar' => 'Right Sidebar',
		'left_sidebar' => 'Left Sidebar',
		'full_width' => 'Full Width',
		'narrow_width' => 'Narrow Width'
	),
	'theme_widget_sections' => array(
		'sidebar' => 'Sidebar',
		'footer1' => 'Footer 1',
		'footer2' => 'Footer 2',
		'footer3' => 'Footer 3',
		'footer4' => 'Footer 4'
	)
);
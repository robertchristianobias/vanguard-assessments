<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// starter_index
$template['starter_index']['template'] = 'frontend/starter/index';
$template['starter_index']['regions'] = array('head', 'styles', 'header', 'content', 'footer', 'scripts');
$template['starter_index']['parser'] = 'parser';
$template['starter_index']['parser_method'] = 'parse';
$template['starter_index']['parse_template'] = FALSE;

// starter
$template['starter']['template'] = 'frontend/starter/template';
$template['starter']['regions'] = array('head', 'styles', 'header', 'content', 'footer', 'scripts');
$template['starter']['parser'] = 'parser';
$template['starter']['parser_method'] = 'parse';
$template['starter']['parse_template'] = FALSE;
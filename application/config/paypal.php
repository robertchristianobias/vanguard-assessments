<?php  
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------
// Paypal IPN Class
// ------------------------------------------------------------------------
// Use PayPal on Sandbox or Live
$config['sandbox']               = TRUE; // FALSE for live environment

$config['live_url']              = 'https://www.paypal.com/cgi-bin/webscr';
$config['live_business']         = 'ignitephilippinesinc@gmail.com';
$config['live_api_user']         = 'ignitephilippinesinc_api1.gmail.com';
$config['live_api_pass']         = '9EQ2CJSZSLENRN6Y';
$config['live_api_signature']    = 'An5ns1Kso7MWUdW4ErQKJJJ4qi4-AxEreUfhDsna1Y-kD3Z3XPoMBJXQ';

$config['sandbox_url']           = 'https://www.sandbox.paypal.com/cgi-bin/webscr';
$config['sandbox_business']      = 'robert_mechant@yahoo.com';
$config['sandbox_api_user']      = 'robert_mechant_api1.yahoo.com';
$config['sandbox_api_pass']      = '4JLASLLEPLGN8M8M';
$config['sandbox_api_signature'] = 'AFcWxV21C7fd0v3bYYYRCpSSRl31At6IEihnLfB4zl.HPKZDuDDRtOUc';

// If (and where) to log ipn to file
$config['paypal_lib_ipn_log_file'] = BASEPATH . 'logs/paypal_ipn.log';
$config['paypal_lib_ipn_log']      = TRUE;

// Where are the buttons located at 
$config['paypal_lib_button_path'] = 'buttons';

// What is the default currency?
$config['paypal_lib_currency_code'] = 'PHP'; 
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Common Helper Functions
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2014-2015, Robert Christian Obias
 * @link		rchristian_obias@yahoo.com
 */

if (!function_exists('frontend_widgets'))
{
	function frontend_widgets($section)
	{
		$CI = & get_instance();
		$CI->load->model('website/pages_model');
		$CI->load->model('website/widgets_model');

		$widgets = $CI->widgets_model
			->where('widget_deleted', 0)
			->where('widget_status', 'Active')
			->where('widget_section', $section)
			->order_by('widget_order')
			->find_all();

		if ($widgets)
		{
			foreach ($widgets as $widget)
			{
				echo '<div class="clearfix">';
				echo ($widget->widget_name) ? '<h2 class="widget-title">' . $widget->widget_name . '</h2>' : '';
				eval($widget->widget_function . ';');
				echo '</div>';
			}
		}
	}
}

if (!function_exists('frontend_page_widget'))
{
	function frontend_page_widget($id)
	{
		$CI = & get_instance();
		$CI->load->model('website/pages_model');
		$page = $CI->pages_model->find($id);

		echo $page->page_content;
	}
}

if (!function_exists('frontend_posts_widget'))
{
	function frontend_posts_widget($category_id)
	{
		$CI = & get_instance();
		$CI->load->model('website/posts_model');
		$posts = $CI->posts_model
			->join('post_categories', 'post_category_post_id = post_id', 'LEFT')
			->join('categories', 'category_id = post_category_category_id')
			->join('users', 'id = post_created_by', 'LEFT')
			->where('category_id', $category_id)
			->order_by('post_posted_on', 'desc')
			->limit(10)
			->find_all();

		$html = '';
		if ($posts)
		{
			$html .= '<ul class="category-posts">';
			foreach ($posts as $post)
			{
				$html .= '<li><a href="' . site_url('post/' . $post->post_slug) . '">' . $post->post_title . '</a></li>';
			}
			$html .= '</ul>';
		}

		echo $html;
	}
}

if (!function_exists('frontend_navigation_widget'))
{
	function frontend_navigation_widget($id)
	{
		$CI = & get_instance();
		$CI->load->model('website/navigations_model');
		echo $CI->navigations_model->get_navigation_widget($id);
	}
}

if (!function_exists('frontend_banner_widget'))
{
	function frontend_banner_widget($id)
	{
		$CI = & get_instance();
		$CI->load->model('website/banners_model');
		echo $CI->banners_model->get_frontend_banners($id);
	}
}

if (!function_exists('frontend_partial_widget'))
{
	function frontend_partial_widget($id)
	{
		$CI = & get_instance();
		$CI->load->model('website/partials_model');
		$page = $CI->partials_model->find($id);

		echo $page->partial_content;
	}
}

if (!function_exists('frontend_social_plugin_widget'))
{
	function frontend_social_plugin_widget()
	{
		$CI = & get_instance();
		$CI->load->model('settings/configs_model');

		$configs = $CI->configs_model
			->where('config_deleted', 0)
			->where_in('config_name', array('share_buttons', 'share_buttons_orientation'))
			->find_all();

		$orientation = "";
		$output = '';

		// return none if no record
		if (! $configs) return FALSE;

		foreach ($configs as $key => $value)
		{
			if ($buttons = json_decode($value->config_value))
			{
				foreach ($buttons as $k => $plugin)
				{
					$output .= ($plugin->status ? '<a id="' . $plugin->id . '" title="' . $plugin->name
							. '" data-title="' . $plugin->name . '" data-url="' . current_url()
							. '" class="btn btn-default social-plugin"><i class="'. $plugin->icon .'"></i></a>' : '');
				}
			}
			else
			{
				$orientation = $value->config_value;
			}
		}

		$output = '<div class="btn-group social-plugins orientation-' . $orientation . '">' . $output . '</div>';
		$output .= '<script>var site_url = "' . site_url() . '"; </script>';
	//	$output .= '<script src="' . site_url('themes/frontend/starter/js/social-plugins.js') .'" type="text/javascript"></script>';

		return $output;
	}
}

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span>
		<span class="sr-only"><?php echo lang('button_close')?></span>
	</button>
	<h4 class="modal-title" id="myModalLabel"><?php echo $page_heading?></h4>
</div>

<div class="modal-body">

	<div class="form-horizontal">

		<div class="form-group">
			<label class="col-sm-3 control-label" for="region_name"><?php echo lang('region_name')?>:</label>
			<div class="col-sm-8">
				<?php echo form_input(array('id'=>'region_name', 'name'=>'region_name', 'value'=>set_value('region_name', isset($record->region_name) ? $record->region_name : ''), 'class'=>'form-control'));?>
				<div id="error-region_name"></div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="region_code"><?php echo lang('region_code')?>:</label>
			<div class="col-sm-8">
				<?php echo form_input(array('id'=>'region_code', 'name'=>'region_code', 'value'=>set_value('region_code', isset($record->region_code) ? $record->region_code : ''), 'class'=>'form-control'));?>
				<div id="error-region_code"></div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="region_short_name"><?php echo lang('region_short_name')?>:</label>
			<div class="col-sm-8">
				<?php echo form_input(array('id'=>'region_short_name', 'name'=>'region_short_name', 'value'=>set_value('region_short_name', isset($record->region_short_name) ? $record->region_short_name : ''), 'class'=>'form-control'));?>
				<div id="error-region_short_name"></div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="region_group"><?php echo lang('region_group')?>:</label>
			<div class="col-sm-8">
				<?php echo form_input(array('id'=>'region_group', 'name'=>'region_group', 'value'=>set_value('region_group', isset($record->region_group) ? $record->region_group : ''), 'class'=>'form-control'));?>
				<div id="error-region_group"></div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="region_country"><?php echo lang('region_country')?>:</label>
			<div class="col-sm-8">
				<?php echo form_dropdown('region_country', $countries, set_value('region_country', (isset($record->region_country)) ? $record->region_country : ''), 'id="region_country" class="form-control"'); ?>
				<div id="error-region_country"></div>
			</div>
		</div>

	</div>

</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
		<i class="fa fa-times"></i> <?php echo lang('button_close')?>
	</button>
	<?php if ($action == 'add'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_add')?>
		</button>
	<?php elseif ($action == 'edit'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_update')?>
		</button>
	<?php else: ?>
		<script>$(".modal-body :input").attr("disabled", true);</script>
	<?php endif; ?>
</div>
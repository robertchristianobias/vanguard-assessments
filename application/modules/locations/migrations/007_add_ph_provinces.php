<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link		http://www.google.com
 */
class Migration_Add_ph_provinces extends CI_Migration 
{
	private $_table = 'provinces';

	function __construct()
	{
		parent::__construct();

		$this->load->model('migrations_model');
	}
	
	public function up()
	{
		// add the initial values
		$file = fopen(APPPATH . "modules/locations/models/ph_provinces.csv","r");

		$data = array();
		while(! feof($file))
		{
			
			$province = fgetcsv($file);
			if ($province)
			{
				$province = array_map("utf8_encode", $province); // handles the encoding
				$data[] = array(
					'province_code' 		=> $province[0],
					'province_name' 		=> $province[1],
					'province_region' 		=> $province[2], 
					'province_country' 		=> $province[3],
				);
			}
		}
		$this->db->insert_batch($this->_table, $data);
	}

	public function down()
	{

	}
}
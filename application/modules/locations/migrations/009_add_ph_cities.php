<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link		http://www.google.com
 */
class Migration_Add_ph_cities extends CI_Migration 
{
	private $_table = 'cities';

	function __construct()
	{
		parent::__construct();

		$this->load->model('migrations_model');
	}
	
	public function up()
	{
		// add the initial values
		$file = fopen(APPPATH . "modules/locations/models/ph_cities.csv","r");

		$data = array();
		while(! feof($file))
		{
			
			$city = fgetcsv($file);
			if ($city)
			{
				$city = array_map("utf8_encode", $city); // handles the encoding
				$data[] = array(
					'city_name' 		=> $city[0],
					'city_type' 		=> $city[1],
					'city_province' 	=> $city[2], 
					'city_country' 		=> $city[3],
				);
			}
		}
		$this->db->insert_batch($this->_table, $data);
	}

	public function down()
	{

	}
}
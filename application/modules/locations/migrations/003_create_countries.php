<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link		http://www.google.com
 */
class Migration_Create_countries extends CI_Migration 
{
	private $_table = 'countries';

	private $_permissions = array(
		array('Countries Link', 'locations.countries.link'),
		array('Countries List', 'locations.countries.list'),
		array('View Country', 'locations.countries.view'),
		array('Add Country', 'locations.countries.add'),
		array('Edit Country', 'locations.countries.edit'),
		array('Delete Country', 'locations.countries.delete'),
	);

	private $_menus = array(
		array(
			'menu_parent'		=> 'locations', // none if parent or single menu
			'menu_text' 		=> 'Countries', 
			'menu_link' 		=> 'locations/countries', 
			'menu_perm' 		=> 'locations.countries.link', 
			'menu_icon' 		=> 'fa fa-flag', 
			'menu_order' 		=> 2, 
			'menu_active' 		=> 1
		),
	);

	function __construct()
	{
		parent::__construct();

		$this->load->model('migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'country_id' 			=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'auto_increment' => TRUE, 'null' => FALSE),
			'country_name'			=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			'country_code2'			=> array('type' => 'CHAR', 'constraint' => 2, 'null' => FALSE),
			'country_code3'			=> array('type' => 'CHAR', 'constraint' => 3, 'null' => FALSE),
			'country_continent'		=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => TRUE),

			'country_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'country_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'country_modified_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'country_modified_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'country_deleted' 		=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'country_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('country_id', TRUE);
		$this->dbforge->add_key('country_name');
		$this->dbforge->add_key('country_code2');
		$this->dbforge->add_key('country_code3');
		$this->dbforge->add_key('country_continent');

		$this->dbforge->add_key('country_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

		// // add the module permissions
		$this->migrations_model->add_permissions($this->_permissions);

		// // add the module menu
		$this->migrations_model->add_menus($this->_menus);

		// add the initial values
		$file = fopen(APPPATH . "modules/locations/models/countries.csv","r");

		$data = array();
		while(! feof($file))
		{
			$country = fgetcsv($file);
			if ($country)
			{
				$country = array_map("utf8_encode", $country); // handles the encoding
				$data[] = array(
					'country_name' => $country[3],
					'country_code2' => $country[0], 
					'country_code3' => $country[1], 
					'country_continent' => $country[2], 
				);
			}
		}
		$this->db->insert_batch($this->_table, $data);
	}

	public function down()
	{
		// drop the table
		$this->dbforge->drop_table($this->_table);

		// // delete the permissions
		$this->migrations_model->delete_permissions($this->_permissions);

		// // delete the menu
		$this->migrations_model->delete_menus($this->_menus);
	}
}
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span>
		<span class="sr-only"><?php echo lang('button_close')?></span>
	</button>
	<h4 class="modal-title" id="myModalLabel"><?php echo $page_heading?> (<?php echo $record->image_width; ?>x<?php echo $record->image_height; ?>)</h4>
</div>

<div class="modal-body">
	
	<div class="full-image">
		<img src="<?php echo site_url($record->image_file); ?>" class="img-responsive" />
	</div>

</div>
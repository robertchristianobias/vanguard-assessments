<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span>
		<span class="sr-only"><?php echo lang('button_close')?></span>
	</button>
	<h4 class="modal-title" id="myModalLabel"><?php echo $page_heading?></h4>
</div>

<div class="modal-body">

	<div class="form-horizontal">

		<div class="col-xs-12">

			<div class="form-group">

				<?php echo form_open(site_url('files/images/upload'), array('class'=>'dropzone', 'id'=>'dropzone'));?>
				<div class="fallback">
					<input name="file" type="file" class="hide" />
				</div>
				<?php echo form_close();?>

			</div>

		</div>

	</div>
	<div class="clearfix"></div>
</div>

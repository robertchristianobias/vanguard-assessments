<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Upload_folders Class
 *
 * @package		rcmediaph
 * @version		1.1
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2014-2016, Robert Christian Obias
 * @link		rchristian_obias@yahoo.com
 */
class Upload_folders {

	/**
	* Constructor
	*
	* @access	public
	*
	*/
	public function __construct()
	{
		log_message('debug', "Upload_folders Class Initialized");
	}
	
	// --------------------------------------------------------------------

	/**
	 * get
	 *
	 * Returns the current upload folder
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */	
	function get()
	{
		$upload_path = 'assets/uploads/';

		$year = date("Y");   
		$month = date("m");   
		
		$year_folder = FCPATH . $upload_path . $year;   
		$month_folder = FCPATH . $upload_path . $year . "/" . $month;

		if (file_exists($year_folder))
		{
			if (file_exists($month_folder) == FALSE)
			{
				mkdir($month_folder, 0777);
			}
		}
		else
		{
			mkdir($year_folder, 0777);
			mkdir($month_folder, 0777);
		}

		return $upload_path . $year . "/" . $month;
	}
}

/* End of file Upload_folders.php */
/* Location: ./application/modules/files/libraries/Upload_folders.php */
<span class="btn-actions">
	<a href="<?php echo site_url('users/add')?>" data-toggle="modal" data-target="#modal" class="btn btn-sm btn-success btn-add" id="btn_add"><span class="fa fa-plus"></span> <?php echo lang('button_add')?></a>
</span>
	
<table class="table table-striped table-bordered table-hover dt-responsive" id="datatables">
	<thead>
		<tr>
			<th class="all"><?php echo lang('index_th_id')?></th>
			<th class="all"><?php echo lang('index_th_firstname')?></th>
			<th class="min-desktop"><?php echo lang('index_th_lastname')?></th>
			<th class="min-tablet"><?php echo lang('index_th_email')?></th>
			<th class="min-tablet"><?php echo lang('index_th_status')?></th>
			<th class="min-desktop"><?php echo lang('index_th_action')?></th>
		</tr>
	</thead>
</table>
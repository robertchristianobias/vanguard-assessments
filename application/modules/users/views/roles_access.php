<?php echo form_open(current_url());?>

	<h3><?php echo strtoupper($group->name); ?></h3>
	
	<?php foreach ($grants as $module => $permissions):?>
		<h4 class="module_name"><?php echo strtoupper($module); ?>
			<small class="pull-right">
				<a href="javascript:;" class="label label-success select_all" data-module="<?php echo $module; ?>">Select All</a> <a href="javascript:;" class="label label-warning select_none" data-module="<?php echo $module; ?>">Select None</a>
			</small>
		</h4>
		<?php if ($permissions): ?>
			<div class="row">
				<?php foreach ($permissions as $grant): ?>
					<div class="col-sm-6 col-md-4 col-lg-3">
						<div class="checkbox">
							<label>
								<input data-group-id="<?php echo $group->id; ?>" data-permission-id="<?php echo $grant['id']; ?>" data-permission-code="<?php echo $grant['code']; ?>" class="access <?php echo $module; ?>" name="permission[]" type="checkbox" value="1"<?php echo ($grant['access'] == 'allow') ? ' checked' : '' ?> /> <?php echo $grant['name']; ?>
							</label>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	<?php endforeach; ?>

		
<?php echo form_close();?>
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1 top-margin6">
						<div class="login-container">
							<div class="center">
								<h1>
									<i class="ace-icon fa fa-leaf white"></i>
									<span class="white"><?php echo config_item('app_name'); ?></span>
								</h1>
								<h4 class="blue hide" id="id-company-text">&copy; Company Name</h4>
							</div>

							<div class="space-6"></div>

							<div class="position-relative">
								<div id="login-box" class="login-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header blue lighter bigger">
												<i class="ace-icon fa fa-lock"></i>
												Login
											</h4>

											<div class="space-6"></div>

											<?php $return = ($this->input->get('return')) ? '?return=' . urlencode($this->input->get('return')) : ''; ?>
											<?php echo form_open(current_url() . $return);?>
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<?php echo form_input(array('name'=>'identity', 'value'=>set_value('identity'), 'class'=>'form-control', 'placeholder' => 'Email', 'autocomplete'=>'off'));?>
															<i class="ace-icon fa fa-envelope"></i>
														</span>
														<?php echo form_error('identity'); ?>
													</label>
													

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input name="password" type="password" class="form-control" placeholder="Password" autocomplete="off"/>
															<i class="ace-icon fa fa-key"></i>
														</span>
														<?php echo form_error('password'); ?>
													</label>
													

													<div class="space"></div>

													<div class="clearfix">
														<label class="inline">
															<input name="remember" type="checkbox" class="ace">
															<span class="lbl"> Remember Me</span>
														</label>

														<button id="submit" type="submit" class="pull-right btn btn-sm btn-primary" data-loading-text="<?php echo lang('processing')?>">
															<i class="ace-icon fa fa-lock"></i>
															<span class="bigger-110">Login</span>
														</button>
													</div>

													<div class="space-4"></div>
												</fieldset>
												<?php echo form_hidden('submit', 1); ?>
												<?php echo form_hidden('return', ($this->input->get('return') ? $this->input->get('return') : '')); ?>
											</form>

											<div class="social-or-login center">
												<span class="bigger-110"><a href="forgot_password">I forgot my password</a></span>
											</div>

										</div><!-- /.widget-main -->

									</div><!-- /.widget-body -->
								</div><!-- /.login-box -->

								
								
							</div><!-- /.position-relative -->

							
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->
/**
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2014-2015, Robert Christian Obias
 * @link		rchristian_obias@yahoo.com
 */
$(function() {
	Dropzone.autoDiscover = false;
	$("#dropzone").dropzone({
		maxFiles: 1,
	}).on("complete", function(file) {
		console.log(file);
		if (file.xhr.responseText){
			var error = file.xhr.responseText;
			alert(error.replace(/(<([^>]+)>)/ig,""))
		}
	});
});
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link		http://www.google.com
 */
class Migration_Edit_users_01 extends CI_Migration 
{
	private $_table = 'users';

	public function __construct() 
	{
		parent::__construct();

		$this->load->model('migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'gender'     => array('type' => 'VARCHAR', 'constraint' => 20, 'null' => TRUE, 'after' => 'photo'),
			'bday'		 => array('type' => 'DATE', 'null' => TRUE, 'after' => 'gender'),
			'job_title'  => array('type' => 'VARCHAR', 'constraint' => 100, 'null' => TRUE, 'after' => 'bday'),
			'industry'   => array('type' => 'VARCHAR', 'constraint' => 100, 'null' => TRUE, 'after' => 'job_title'),
		);
		
		$this->dbforge->add_column($this->_table, $fields);
	}

	public function down()
	{
		$this->dbforge->drop_column($this->_table, 'gender');
		$this->dbforge->drop_column($this->_table, 'bday');
		$this->dbforge->drop_column($this->_table, 'job_title');
		$this->dbforge->drop_column($this->_table, 'industry');
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Dashboard Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2014-2015, Robert Christian Obias
 * @link		rchristian_obias@yahoo.com
 */
class Dashboard extends CI_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	function __construct()
	{
		parent::__construct();

		$this->load->language('dashboard');

		// $this->output->enable_profiler(TRUE);
	}
	
	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function index()
	{
		// cache this page for 2 mins
		// $this->output->cache(2);
		
		// check if database is already installed
		// comment out these lines after installation
		// $this->load->dbutil();
		// if (! $this->db->table_exists('users'))
		// {
		// 	redirect('migrate');
		// }

		// $permission = $this->acl->restrict('dashboard.dashboard.list', 'return');
		// if (!$permission) show_404();
		
		$data['page_heading'] = lang('index_heading');
		$data['page_subhead'] = lang('index_subhead');

		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('dashboard'));

		// session breadcrumb
		$this->session->set_userdata('redirect', current_url());
		
		$this->template->write_view('content', 'dashboard_index', $data);
		$this->template->render();
	}
}

/* End of file dashboard.php */
/* Location: ./application/modules/dashboard/controllers/dashboard.php */
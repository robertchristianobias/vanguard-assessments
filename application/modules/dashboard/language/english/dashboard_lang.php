<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Dashboard Language File (English)
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2014-2015, Robert Christian Obias
 * @link		rchristian_obias@yahoo.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Dashboard';

// Labels


// Buttons


// Index Function
$lang['index_heading']				= 'Dashboard';
$lang['index_subhead']				= 'It all starts here';
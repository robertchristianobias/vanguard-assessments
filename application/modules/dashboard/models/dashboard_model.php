<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Dashboard_model Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2014-2015, Robert Christian Obias
 * @link		rchristian_obias@yahoo.com
 */
class Dashboard_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

}

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span>
		<span class="sr-only">Close</span>
	</button>
	<h4 class="modal-title" id="myModalLabel"><?php echo lang('button_login'); ?></h4>
</div>
<div class="modal-body">
    <form name="" method="POST" action="" id="user-form">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label"><?php echo lang('event_email'); ?></label>
                    <?php echo form_input('event_email', set_value('event_email'), 'class="form-control" id="event_email" autocomplete="off"'); ?>
                    <div id="error-event_email"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label"><?php echo lang('event_password'); ?></label>
                    <?php echo form_password('event_password', set_value('event_password'), 'class="form-control" id="event_password" autocomplete="off"'); ?>
                    <div id="error-event_password"></div>  
                </div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
		<i class="fa fa-times"></i> <?php echo lang('button_close')?>
	</button>
    <button id="submit" class="btn btn-success" type="submit">
        <i class="fa fa-save"></i> <?php echo lang('button_login'); ?>
    </button>
</div>
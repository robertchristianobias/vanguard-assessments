// IIFE - Immediately Invoked Function Expression
(function(yourcode) {

    // The global jQuery object is passed as a parameter
    yourcode(window.jQuery, window, document);

}(function($, window, document) {

    var ajax = function(data, endpoint) {
        return $.ajax({
                url:  endpoint,
                type: 'POST',
                data: data
            });
    };
    
    // The $ is now locally scoped 
    // Listen for the jQuery ready event on the document
    $(function() {
        
        var submit    = $('#submit');
        
        
        submit.on('click', function() {
            
            var data = $('#user-form').serializeArray();
                data.push({
                    name: 'event_captcha', 
                    value: $('#g-recaptcha-response').val()
                });
                
            ajax(data, app_url + '/process_user_register').done(function(data) {
                var response = $.parseJSON(data);
                
                if(response.success === false) {
                    if (response.errors) {
                        for (var form_name in response.errors) {
                            $('#error-' + form_name).html(response.errors[form_name]);
                        }
                    }
                    alertify.error(response.message);
                } else {
                    
                    $('#modal').modal('hide');
                    
                    alertify.success(response.message);
                }
            });
        });
    });
        
    console.log('DOM is not ready');
   
}));
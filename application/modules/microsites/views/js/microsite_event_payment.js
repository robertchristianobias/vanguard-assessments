/**
 * @package	Robert Christian Obias
 * @version	1.0
 * @author 	Robert Christian Obias <robert.obias@google.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link	http://www.google.com
 */

$(document).ready(function() {
    
    $('#event-images').lightSlider({
        item:      1,
        gallery:   true,
        loop:      true,
        slideMove: 1,
        easing:    'cubic-bezier(0.25, 0, 0.25, 1)',
        speed: 600,
        responsive : [
            {
                breakpoint: 800,
                settings: {
                    item: 1,
                    slideMove: 1
                }
            },
            {
                breakpoint:480,
                settings: {
                    item: 1,
                    slideMove: 1
                }
            }
        ]
    });
    
//    $('.payment-method').click(function() {
//        var payment_id = $(this).val();
//        var form       = $('#payment-form');
//        
//        $.post(form.attr('action'), {
//            payment_id: payment_id
//        }, function(result) {
//                $('.payment-method-wrapper-' + payment_id).html(result);
//            }
//        );
//
//        return false;
//        
//    });
    
});
/**
 * @package	Robert Christian Obias
 * @version	1.0
 * @author 	Robert Christian Obias <robert.obias@google.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link	http://www.google.com
 */

$(document).ready(function() {
    
    $('.reserved').change(function() {
        // redirect to seat 
    });
    
  
     
    $('.ticket_section').change(function() {
        
        var selected = $(this).find('option:selected').val();
        var seat_selection  = $(this).closest('.seat-selection');
        
        $.post(app_url + '/process_ticket_section', {
            section_id: selected
        }, function(result) {
            seat_selection.find('.ticket_quantity_wrapper').html(result);
            //$('.t_quality').html(result);
        });
        
        if(selected != '') {
            seat_selection.find('.btn-add-to-cart').fadeIn('slow');
        } else {
            seat_selection.find('.btn-add-to-cart').fadeOut('slow');
        }
    });
    
    $('.ticket_quantity').change(function() {
        var ticket_quantity = $(this).val();
        
        $.post(app_url + '/process_ticket_quantity', {
            ticket_quantity: ticket_quantity
        }, function(result) {
            
        });
    });
    
    $('#event-images').lightSlider({
        item: 1,
        gallery: true,
        loop: true,
        slideMove: 1,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed: 600,
        responsive : [
            {
                breakpoint: 800,
                settings: {
                    item:3,
                    slideMove:1,
                    slideMargin:6,
                }
            },
            {
                breakpoint:480,
                settings: {
                    item:2,
                    slideMove:1
                }
            }
        ]
    });
    
    $('.btn-add-to-cart').click(function() {
        
        var form            = $('#ticket-form');
        var seat_selection  = $(this).closest('.seat-selection');
        var ticket_section  = seat_selection.find('.ticket_section').val();
        var ticket_quantity = seat_selection.find('.ticket_quantity').val();
                
        var btn = $(this);
            btn.val('Loading... ');

        $.post(form.attr('action'), {
            ticket_section:     ticket_section,
            ticket_quantity:    ticket_quantity,
        }, function(result) {
            
            btn.val('Get Ticket');
            var response = $.parseJSON(result);
            
            if(response.success === true)
            {
                window.location.replace(response.redirect); // Redirect to register page
            }
            else
            {
                // displays individual error messages
                if (response.errors) {
                    for (var form_name in response.errors) {
                        seat_selection.find('.error-'+form_name).html(response.errors[form_name]);
                    }
                }
                
                alertify.error(response.message);
            }
            
        });

        return false;
        
    });
    
    // Submit
    $('.btn-register').click(function() {
        
        var form                  = $('#ticket-form');
        var seat_selection        = $(this).closest('.seat-selection');
        var ticket_section        = $(this).closest('.seat-selection').find('.ticket_section').val();
        var ticket_quantity       = $(this).closest('.seat-selection').find('.ticket_quantity').val();
        var ticket_ticket_type_id = $(this).closest('.seat-selection').find('.ticket_ticket_type_id').val();
                
        var btn = $(this);
            btn.val('Loading... ');

        $.post(form.attr('action'), {
            ticket_section:          ticket_section,
            ticket_quantity:         ticket_quantity,
            ticket_ticket_type_id:   ticket_ticket_type_id,
        }, function(result) {
            
            btn.val('Register');
            var response = $.parseJSON(result);
            
            if(response.success === true)
            {
                window.location.replace(response.redirect + form.data('slug') + '/register'); // Redirect to register page
            }
            else
            {
                // displays individual error messages
                if (response.errors) {
                    for (var form_name in response.errors) {
                        seat_selection.find('.error-'+form_name).html(response.errors[form_name]);
                    }
                }
                
                alertify.error(response.message);
            }
        });

        return false;
        
    });
    
});
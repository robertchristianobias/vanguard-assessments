// IIFE - Immediately Invoked Function Expression
(function(yourcode) {

    // The global jQuery object is passed as a parameter
    yourcode(window.jQuery, window, document);

}(function($, window, document) {

    // The $ is now locally scoped 
    // Listen for the jQuery ready event on the document
    $(function() {
        
        var submit    = $('#submit');
        var user_form = $('#user-form');
        
        submit.on('click', function() {
            
            submit.html('Loading');
            ajax(user_form.serializeArray(), app_url + 'microsite/process_user_login').done(function(data) {
                var response = $.parseJSON(data);
                
                if(response.success === false) {
                    if (response.errors) {
                        for (var form_name in response.errors) {
                            $('#error-' + form_name).html(response.errors[form_name]);
                        }
                    }
                    alertify.error(response.message);
                } else {
                    $('#modal').modal('hide');
                    
                    alertify.success(response.message);
                    window.location.replace(app_url); // Redirect to register page
                }
                
                submit.html('Login');
            });
        });
    });
    
    var ajax = function(data, endpoint) {
        return $.ajax({
            url:  endpoint,
            type: 'POST',
            data: data
        });
    };
    
    console.log('DOM is not ready');
   
}));
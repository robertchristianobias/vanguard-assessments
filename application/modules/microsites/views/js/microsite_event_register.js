/**
 * @package	Robert Christian Obias
 * @version	1.0
 * @author 	Robert Christian Obias <robert.obias@google.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link	http://www.google.com
 */

$(document).ready(function() {
    
    $('body').on('hidden.bs.modal', '.modal', function() {
        $(this).removeData('bs.modal');
    });
        
    $('#event-images').lightSlider({
        item: 1,
        gallery: true,
        loop: true,
        slideMove: 1,
        easing: 'cubic-bezier(0.25, 0, 0.25, 1)',
        speed: 600,
        responsive : [
            {
                breakpoint: 800,
                settings: {
                    item: 3,
                    slideMove: 1,
                    slideMargin: 6,
                }
            },
            {
                breakpoint:480,
                settings: {
                    item: 2,
                    slideMove: 1
                }
            }
        ]
    });
    
    $('.use-my-info').click(function() {
        
        var form_data = $('#register-form').serializeArray();
        //var form_data = JSON.stringify(form_data);
        var checked   = $(this);
        
        if(checked.is(':checked')) {
            $.each(form_data, function(index, field) {
                alert(field['name'].val());
            });
            checked.closest('.register-form-container').find('.ticket_field').val('ABC');
        } else {
            checked.closest('.register-form-container').find('.ticket_field').val('');
        }
    });
    
    $('.btn-submit').click(function() {
       
        var form = $('#register-form');
        var btn  = $(this);
        
        btn.val('Please wait...');
        $.post(form.attr('action'), form.serializeArray(), 
            function(result) {
                btn.val('Submit');
                var response = $.parseJSON(result);
                
                if (response.success === false)
                {
                    alertify.error(response.message);
                    
                    if (response.errors) {
                        $.each(response.errors, function(index, value) {
                            for(var form_name in value) {
                                console.log(form_name);
                                if(typeof value[form_name] !== 'undefined') {
                                    $('.error-' + form_name + '_' + value.ticket_id).html(value[form_name]);
                                }
                            }
                        });
                    }
                }
                else
                {
                    alertify.success(response.message);
                    window.location.replace(app_url + '/payment');
                }
            }
        );
        
        return false;
    });
});
<?php    
    $cart = $this->cart->contents();
?>
<?php if($cart) { ?>
    <form name="" method="POST" action="" id="cart-form">
        <div class="row">
            <div class="col-md-6">
                <h3>Selected Tickets</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table width="100%" class="table table-bordered">
                    <thead>
                        <tr>
                            <th class="col-md-1 text-center"></th>
                            <th class="col-md-6">Section</th>
                            <th class="col-md-1 text-center">Quantity</th>
                            <th class="col-md-2 text-center">Price</th>
                            <th class="col-md-2">Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $i = 1; ?>
                        <?php foreach($cart as $v) { ?>
                            <?php
                                $currency = $v['options']['currency'] . ' ';
                            ?>
                            <?php echo form_hidden('cart['.$i.'][rowid]', $v['rowid']); ?>
                            <tr>
                                <td class="text-center">
                                    <a href="<?php echo site_url('microsite/'.$microsite->microsite_slug.'/delete_cart_item/'.$v['rowid']); ?>" data-toggle="modal" data-target="#modal" tooltip-toggle="tooltip" data-placement="top" title="" class="btn btn-xs btn-danger" data-original-title="Delete"><span class="fa fa-trash-o"></span></a>
                                </td>
                                <td>
                                    <?php echo $v['name']; ?> - <?php echo $v['options']['section']; ?>
                                </td>
                                <td class="text-center">
                                    <?php echo $v['qty']; ?>
                                </td>
                                <td>
                                    <?php echo $currency . $this->cart->format_number($v['price']); ?></td>
                                <td><?php echo $currency . $this->cart->format_number($v['subtotal']); ?></td>
                            </tr>
                            <?php $i++; ?>
                            <?php } ?>
                            <tr>
                                <td class="text-right" colspan="4"><b>Total</b></td>
                                <td><?php echo $currency . $this->cart->format_number($this->cart->total()); ?></td>
                            </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <a href="<?php echo site_url('microsite/'.$microsite->microsite_slug); ?>" class="btn btn-success">Continue Adding Tickets</a>
            </div>
            <div class="col-md-6 text-right">
                <a href="<?php echo site_url('microsite/'.$microsite->microsite_slug.'/register'); ?>" class="btn btn-danger">Checkout</a>
            </div>
        </div>
    </form>
<?php } else { ?>
    <div class="row">
        <div class="col-md-6">
            <a href="<?php echo site_url('microsite/'.$microsite->microsite_slug); ?>" class="btn btn-success">Continue Adding Tickets</a>
        </div>
    </div>
<?php } ?>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span>
		<span class="sr-only">Close</span>
	</button>
	<h4 class="modal-title" id="myModalLabel"><?php echo lang('button_register'); ?></h4>
</div>
<div class="modal-body">
    <form name="" method="POST" action="" id="user-form">
        <div class="row">
            <div class="form-group">
                <label class="control-label col-md-12"><?php echo lang('event_name'); ?></label>
            </div>
            <div class="form-group clearfix">
                <div class="col-md-4">
                    <?php echo form_input('event_firstname', set_value('event_firstname'), 'class="form-control" placeholder="First Name" autocomplete="off"'); ?>
                    <div id="error-event_firstname"></div>
                </div>
                <div class="col-md-4">
                    <?php echo form_input('event_middlename', set_value('event_middlename'), 'class="form-control" placeholder="Middle Name" autocomplete="off"'); ?>
                    <div id="error-event_middlename"></div>
                </div>
                <div class="col-md-4">
                    <?php echo form_input('event_lastname', set_value('event_lastname'), 'class="form-control" placeholder="Last Name" autocomplete="off"'); ?>
                    <div id="error-event_lastname"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label"><?php echo lang('event_email'); ?></label>
                    <?php echo form_input('event_email', set_value('event_email'), 'class="form-control" id="event_email" autocomplete="off"'); ?>
                    <div id="error-event_email"></div>  
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label"><?php echo lang('event_password'); ?></label>
                    <?php echo form_password('event_password', set_value('event_password'), 'class="form-control" id="event_password" autocomplete="off"'); ?>
                    <div id="error-event_password"></div>  
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label"><?php echo lang('event_confirm_password'); ?></label>
                    <?php echo form_password('event_confirm_password', set_value('event_confirm_password'), 'class="form-control" id="event_confirm_password"'); ?>
                    <div id="error-event_confirm_password"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <input type="checkbox" name="event_agree_terms_and_conditions" value="1" /> I have agreed to the <a href="<?php echo site_url('terms_and_conditions'); ?>" target="_blank">Terms and Conditions</a>
                <div id="error-event_agree_terms_and_conditions"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <input type="checkbox" name="event_agree_privacy_policy" value="1" /> I have agreed to the <a href="<?php echo site_url('privacy_policy'); ?>">Privacy Policy</a>
                <div id="error-event_agree_privacy_policy"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <p><?php echo $this->recaptcha->getWidget(); ?></p>
                <div id="error-event_captcha"></div>
            </div>
        </div>
    </form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
		<i class="fa fa-times"></i> <?php echo lang('button_close')?>
	</button>
    <button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
        <i class="fa fa-save"></i> <?php echo lang('button_register'); ?>
    </button>
</div>
<?php echo $this->recaptcha->getScriptTag(); ?>
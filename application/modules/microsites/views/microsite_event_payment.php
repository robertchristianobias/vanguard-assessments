<div class="row">
    <div class="col-md-12">
        <h2><?php echo lang('ticket_payment'); ?></h2>
        
        <form name="" method="POST" action="<?php echo site_url('microsites/process_payment'); ?>" id="payment-form">
            <?php if($microsite->event_has_payment_module == 1) { ?>
                <p>Select payment</p>
                <ul class="list-unstyled">
                    <?php foreach($payment_methods as $method => $payment) : ?>
                        <li>
                            <h4><?php echo $method; ?></h4>
                            <ul>
                                <?php foreach($payment as $account) : ?>
                                    <li>
                                        <?php echo $account->payment_name ?>
                                        <?php if ($account->payment_type == 'Bank Deposit') : ?>
                                            <p>Account Name: <?php echo $account->setting->payment_bank_account_name ?></p>
                                            <p>Account Number: <?php echo $account->setting->payment_bank_account_number ?></p>
                                            <p>Branch: <?php echo $account->setting->payment_bank_branch ?></p>
                                            <p>Instruction: <?php echo $account->payment_instruction ?></p>
                                        <?php else: ?>
                                            <form name="" method="" action=""></form>
                                        <?php endif ?>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php } else { ?>
                <p><?php echo lang('no_payment_module'); ?></p>
            <?php } ?>
        </form>
        
    </div>
</div>

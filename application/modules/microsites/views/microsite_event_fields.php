<form name="" method="POST" action="<?php echo site_url('microsites/process_transaction'); ?>" class="" id="register-form">
    <input type="hidden" name="event_id" value="<?php echo $event_id; ?>" />
    <?php foreach($this->cart->contents() as $v) { ?>
        <div class="row">
            <div class="col-md-12">
                <h4><?php echo $v['name'] . ' - ' . $v['options']['section']; ?></h4>
            </div>
        </div>
        
        <div class="register-form-container">
            <?php foreach($fields as $key=>$field) { ?>
                <?php
                    switch($field->microsite_field_name)
                    {
                        case 'name':
                            ?>
                            <div class="form-group row">
                                <label class="col-md-1">Name: </label>
                                <div class="col-md-3">
                                    <?php echo form_input('ticket[firstname]['.$v['id'].']', set_value('ticket[firstname]['.$v['id'].']', isset($user->first_name) ? $user->first_name : ''), 'placeholder="First Name" class="form-control ticket_field"'); ?>
                                    <div class="error-ticket_firstname_<?php echo $v['id']; ?>"></div>
                                </div>
                                <div class="col-md-3">
                                    
                                    <?php echo form_input('ticket[lastname]['.$v['id'].']', set_value('ticket[lastname]['.$v['id'].']', isset($user->last_name) ? $user->last_name : ''), 'placeholder="Last Name" class="form-control ticket_field"'); ?>
                                    <div class="error-ticket_lastname_<?php echo $v['id']; ?>"></div>
                                </div>
                            </div>
                            <?php
                        break;
                        case 'email':
                            ?>
                            <div class="form-group row">
                                <label class="col-md-1">Email: </label>
                                <div class="col-md-3">
                                    <?php echo form_input('ticket[email]['.$v['id'].']', set_value('ticket[email]['.$v['id'].']', isset($user->email) ? $user->email : ''), 'placeholder="Email" class="form-control ticket_field"'); ?>
                                    <div class="error-ticket_email_<?php echo $v['id']; ?>"></div>
                                </div>
                            </div>
                            <?php
                        break;
                        case 'contact_number':
                            ?>
                            <div class="form-group row">
                                <label class="col-md-1">Contact: </label>
                                <div class="col-md-3">
                                    <?php echo form_input('ticket[phone]['.$v['id'].']', set_value('ticket[phone]['.$v['id'].']', isset($user->phone) ? $user->photo : ''), 'placeholder="Phone" class="form-control ticket_field"'); ?>
                                    <div class="error-ticket_phone_<?php echo $v['id']; ?>"></div>
                                </div>
                            </div>
                            <?php
                        break;
                        case 'company':
                            ?>
                            <div class="form-group row">
                                <label class="col-md-1">Company: </label>
                                <div class="col-md-3">
                                    <?php echo form_input('ticket[company]['.$v['id'].']', set_value('ticket[company]['.$v['id'].']', isset($user->company) ? $user->company : ''), 'placeholder="Company" class="form-control ticket_field"'); ?>
                                    <div class="error-ticket_company_<?php echo $v['id']; ?>"></div>
                                </div>
                            </div>
                            <?php
                        break;
                        case 'age':
                            ?>
                            <div class="form-group row">
                                <label class="col-md-1">Age: </label>
                                <div class="col-md-2">
                                    <?php echo form_input('ticket[age]['.$v['id'].']', set_value('ticket[age]['.$v['id'].']', isset($user->age) ? $user->age : ''), 'placeholder="Age" class="form-control ticket_field"'); ?>
                                    <div class="error-ticket_age_<?php echo $v['id']; ?>"></div>
                                </div>
                            </div>
                            <?php
                        break;
                        case 'gender':
                            ?>
                            <div class="form-group row">
                                <label class="col-md-1">Gender: </label>
                                <div class="col-md-2">
                                    <?php
                                        $age_arr = create_dropdown('array', 'Male,Female');
                                        
                                        echo form_dropdown('ticket[gender]['.$v['id'].']', $age_arr, 'Male', 'class="form-control ticket_field"');
                                    ?>
                                    <div class="error-ticket_gender_<?php echo $v['id']; ?>"></div>
                                </div>
                            </div>
                            <?php
                        break;
                    }
                ?>
            <?php } ?>
        </div>
        <!--
            <?php for($i=1;$i<=$v['qty'];$i++) { ?>
                <div class="register-form-container">
                    <div class="form-group row">
                        <label class="col-md-1">Name: </label>
                        <div class="col-md-3">
                            <?php echo form_input('ticket[firstname]['.$v['id'].']['.$i.']', set_value('ticket[firstname]['.$v['id'].'][]'), 'placeholder="First Name" class="form-control ticket_field"'); ?>
                            <div class="error-ticket_firstname_<?php echo $v['id'] . '_' . $i; ?>"></div>
                        </div>
                        <div class="col-md-3">
                            <?php echo form_input('ticket[middlename]['.$v['id'].']['.$i.']', set_value('ticket[middlename]['.$v['id'].'][]'), 'placeholder="Middle Name" class="form-control ticket_field"'); ?>
                            <div class="error-ticket_middlename_<?php echo $v['id'] . '_' . $i; ?>"></div>
                        </div>
                        <div class="col-md-3">
                            <?php echo form_input('ticket[lastname]['.$v['id'].']['.$i.']', set_value('ticket[lastname]['.$v['id'].'][]'), 'placeholder="Last Name" class="form-control ticket_field"'); ?>
                            <div class="error-ticket_lastname_<?php echo $v['id'] . '_' . $i; ?>"></div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-1">Email: </label>
                        <div class="col-md-3">
                            <?php echo form_input('ticket[email]['.$v['id'].']['.$i.']', set_value('ticket[email]['.$v['id'].'][]'), 'placeholder="Email" class="form-control ticket_field"'); ?>
                            <div class="error-ticket_email_<?php echo $v['id'] . '_' . $i; ?>"></div>
                        </div>
                        <label class="col-md-1">Contact: </label>
                        <div class="col-md-3">
                            <?php echo form_input('ticket[phone]['.$v['id'].']['.$i.']', set_value('ticket[phone]['.$v['id'].'][]'), 'placeholder="Phone" class="form-control ticket_field"'); ?>
                            <div class="error-ticket_phone_<?php echo $v['id'] . '_' . $i; ?>"></div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        -->
        <hr />
    <?php } ?>
    <div class="form-group row">
        <div class="col-md-3">
            <input type="submit" name="submit" value="Submit" class="btn btn-success btn-submit" />
        </div>
    </div>
</form>
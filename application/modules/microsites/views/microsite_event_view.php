<div class="row">
    <div class="col-md-12">
        <h2>Tickets</h2>
        <form name="" method="POST" action="<?php echo site_url('microsites/process_add_to_cart'); ?>" id="ticket-form">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th class="col-md-3">Name</th>
                        <th class="col-md-3">Section</th>
                        <th class="col-md-2">Price</th>
                        <th class="col-md-4">Seat Selection</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if($ticket_types) { ?>
                        <?php foreach($ticket_types as $val) { ?>
                            <?php
                            
                                //$sections = $this->sections_model->get_sections_dropdown($val->ticket_type_id);
                                $ticket_type_class = ($val->ticket_type_type == 'Reserved' ? 'reserved' : 'not-reserved');
                                
                                $section = $sections[$val->ticket_type_id];
                                
                                if($section)
                                {
                                    $section_arr = array('');
                                    foreach($section as $v)
                                    {
                                        $section_arr[$v['section_id']] = $v['section_name'];
                                    }
                                }
                            ?>
                            <tr class="seat-selection">
                                <td>
                                    <p>
                                        <?php echo $val->ticket_type_name; ?> <br />
                                        <i>(<?php echo $val->ticket_type_type; ?>)</i>
                                    </p>
                                </td>
                                <td>
                                    <?php echo form_dropdown('ticket_section', $section_arr, '', 'class="form-control ticket_section '.$ticket_type_class.'"'); ?>
                                    <span class="error-ticket_section"></span>
                                </td>
                                <td>
                                    <?php echo $val->ticket_type_currency . ' ' . number_format($val->ticket_type_amount, 2); ?>
                                </td>
                                <td>
                                    <?php if($val->ticket_type_type == 'Standing' OR $val->ticket_type_type == 'Guaranteed') { ?>
                                        <div class="col-md-6">
                                            <span class="ticket_quantity_wrapper"></span>
                                        </div>
                                        <input type="submit" name="add_to_cart" value="<?php echo lang('get_ticket'); ?>" class="btn btn-success btn-add-to-cart" />
                                    <?php } else { ?>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                </tbody>
            </table>
        </form>
    </div>
</div>
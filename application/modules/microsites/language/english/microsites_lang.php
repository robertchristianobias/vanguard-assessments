<?php

$lang['event_name'] = 'Name';
$lang['event_firstname'] = 'First Name';
$lang['event_middlename'] = 'Middle Name';
$lang['event_lastname'] = 'Last Name';
$lang['event_username'] = 'Username';
$lang['event_password'] = 'Password';
$lang['event_confirm_password'] = 'Confirm Password';
$lang['event_email'] = 'Email';
$lang['event_company'] = 'Company';
$lang['event_phone'] = 'Phone';
$lang['event_agree_terms_and_conditions'] = 'Agree to Terms and Conditions';
$lang['event_agree_privacy_policy'] = 'Agree to Privacy Policy';
$lang['event_captcha']  = 'Captcha';

$lang['ticket_section'] = 'Ticket Section';
$lang['ticket_payment'] = 'Payment';

$lang['no_ticket'] = 'No ticket selected';
$lang['no_payment_module'] = 'Payment module is not enabled for this event.';
$lang['no_available_seats'] = 'No available seats for this section';

// Buttons
$lang['button_register'] = 'Register';
$lang['button_login'] = 'Login';
$lang['delete_heading'] = 'Remove ticket';
$lang['delete_confirm'] = 'Are you sure you want to remove the ticket?';

$lang['register_success'] = 'Successfully registered the user';
$lang['login_required'] = 'You need to login before you can purchase a ticket.';
$lang['login_success'] = 'Successfully logged-in';
$lang['login_failed'] = 'Failed to login';
$lang['remove_item'] = 'Successfuly remove the ticket';
$lang['event_captcha_failed'] = 'Failed to verify captcha. Please try again.';
$lang['add_to_cart_success'] = 'Successfully added to cart';
$lang['transaction_error']  = 'Cannot process data. Please try again.';
$lang['ticket_section_not_exist'] = 'Ticket section does\'nt exist.';

$lang['get_ticket'] = 'Get Ticket';


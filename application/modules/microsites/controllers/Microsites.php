<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Microsites Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <robert.obias@google.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link		http://www.google.com
 */
class Microsites extends MX_Controller {
    
    public function __construct() {
        
        parent::__construct();
        
        $this->load->model('events/events_model');
        $this->load->model('events/tickets_model');
        $this->load->model('events/ticket_types_model');
        $this->load->model('events/microsite_fields_model');
        $this->load->model('events/sections_model');
        $this->load->model('events/transactions_model');
        $this->load->model('events/orders_model');
        $this->load->model('microsites_model');
        
        $this->load->library('cart');
        $this->load->language('microsites');
    }
    
    // --------------------------------------------------------------------
	/**
	 * index
	 *
     * 
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robert.obias@google.com>
	 */
    public function index()
    {
        show_404();
    }
    
    // --------------------------------------------------------------------
	/**
	 * view
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robert.obias@google.com>
	 */
    public function view($slug)
    {
        //$slug = (string) $this->uri->segment('2');
        
        $microsite = $this->microsites_model->get_microsite($slug);
        $ticket_types = $this->ticket_types_model->get_ticket_types($microsite->event_id);
        $template = url_title($microsite->microsite_theme_name, 'dash', TRUE);
        $event_images = $this->events_model->get_event_images($microsite->event_id);
        
        $sections = $this->sections_model->get_sections();
        
        $data = array(
            'microsite' => $microsite,
            'ticket_types' => $ticket_types,
            'template' => $template,
            'event_images' => $event_images, 
            'sections'  => $sections,
        );
        
        $this->template->set_template($template.'_index');
        
        $this->template->add_js('themes/frontend/'.$template.'/components/chained/jquery.chained.remote.js');
        $this->template->add_css('themes/frontend/'.$template.'/components/lightslider/css/lightslider.min.css');
		$this->template->add_js('themes/frontend/'.$template.'/components/lightslider/js/lightslider.min.js');

        $this->template->add_css(module_css('microsites', 'microsite_event_view'), 'embed');
        $this->template->add_js(module_js('microsites', 'microsite_event_view'), 'embed');
        $this->template->write_view('content', 'microsite_event_view', $data);
		$this->template->render();
    }
    
    // --------------------------------------------------------------------
	/**
	 * cart
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robert.obias@google.com>
	 */
    public function cart($slug)
    {
        if( !$this->ion_auth->logged_in())
        {
            redirect(site_url('microsite/'.$slug));
        }
        
        $microsite    = $this->microsites_model->get_microsite($slug);
        $ticket_types = $this->ticket_types_model->find_all_by(array('ticket_type_event_id' => $microsite->event_id));
        $template     = url_title($microsite->microsite_theme_name, 'dash', TRUE);
        $event_images = $this->events_model->get_event_images($microsite->event_id);
        
        if($this->input->post('update_cart'))
        {
            $data = array();
            foreach($this->input->post('cart') as $key=>$val)
            {
                $data[] = array(
                    'rowid' => $val['rowid'],
                    'qty'   => $val['qty']
                );
            }

            $success = $this->cart->update($data);
            if($success)
            {
                redirect(site_url($slug.'/cart'));
            }
        }
        
        $data = array(
            'microsite'    => $microsite,
            'ticket_types' => $ticket_types,
            'template'     => $template,
            'event_images' => $event_images
        );
        
        $this->template->set_template($template.'_index');
        
        $this->template->add_css('themes/frontend/'.$template.'/components/lightslider/css/lightslider.min.css');
		$this->template->add_js('themes/frontend/'.$template.'/components/lightslider/js/lightslider.min.js');
        
        $this->template->add_css(module_css('microsites', 'microsite_event_cart'), 'embed');
        $this->template->add_js(module_js('microsites', 'microsite_event_cart'), 'embed');
        $this->template->write_view('content', 'microsite_event_cart', $data);
		$this->template->render();
    }
    

    // --------------------------------------------------------------------

	/**
	 * register
	 *
	 * @access	public
	 * @param	string $slug 
	 * @author 	Robert Christian Obias <robert.obias@google.com>
	 */
    public function register($slug)
    {
        if( !$this->ion_auth->logged_in())
        {
            redirect(site_url('microsite/'.$slug));
        }
        
        $microsite = $this->microsites_model->get_microsite($slug);
        $event_images = $this->events_model->get_event_images($microsite->event_id);
        $fields = $this->microsite_fields_model->get_microsite_fields($microsite->event_id);
        $template = url_title($microsite->microsite_theme_name, 'dash', TRUE);
        
        $data = array(
            'microsite' => $microsite,
            'event_images' => $event_images,
            'fields' => $this->_fields($fields, $microsite->event_id),
            'template' => $template,
        );
        
        $this->template->set_template($template.'_index');
        
        $this->template->add_css('themes/frontend/'.$template.'/components/lightslider/css/lightslider.min.css');
		$this->template->add_js('themes/frontend/'.$template.'/components/lightslider/js/lightslider.min.js');
        
        $this->template->add_css(module_css('microsites', 'microsite_event_register'), 'embed');
        $this->template->add_js(module_js('microsites', 'microsite_event_register'), 'embed');
        $this->template->write_view('content', 'microsite_event_register', $data);
		$this->template->render();
        
    }
    
    // --------------------------------------------------------------------
	/**
	 * Payment
     * 
	 *
	 * @access	public
	 * @param	$slug (string)
	 * @author 	Robert Christian Obias <robert.obias@google.com>
	 */
    public function payment($slug)
    {
        $this->load->model('payments/payments_model');
        $this->load->model('payments/payment_banks_model');
		$this->load->model('payments/payment_paypal_model');

        $microsite  = $this->microsites_model->get_microsite($slug);
        $event_images = $this->events_model->get_event_images($microsite->event_id);
       // $fields = $this->microsite_fields_model->get_microsite_fields($microsite->event_id);
        $template = url_title($microsite->microsite_theme_name, 'dash', TRUE);

        $data = array(
            'microsite'         => $microsite,
            'templte'           => $template,
            'event_images'      => $event_images,
            'template'          => $template,
            'payment_methods'   => $this->payments_model->get_payment_methods()
        );
        
        $this->template->set_template($template.'_index');
        
        $this->template->add_css('themes/frontend/'.$template.'/components/lightslider/css/lightslider.min.css');
        $this->template->add_js('themes/frontend/'.$template.'/components/lightslider/js/lightslider.min.js');
        
        //$this->template->add_css(module_css('microsites', 'microsite_event_register'), 'embed');
        $this->template->add_js(module_js('microsites', 'microsite_event_payment'), 'embed');
        $this->template->write_view('content', 'microsite_event_payment', $data);
		$this->template->render();
    }
    
    // --------------------------------------------------------------------
	/**
	 * process_add_to_cart
     * 
     * TODO
     * Check if section has available sets/capacity
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robert.obias@google.com>
	 */
    public function process_add_to_cart()
    {
        // Check if ajax request
        is_ajax();
       
        if($this->input->post())
        {
            // Check if user is logged-in
            $user = $this->ion_auth->user()->row();
            
            if($user)
            {
                // Validate ticket
                $this->form_validation->set_rules('ticket_section',  lang('ticket_section'),  'required');
                $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

                if ($this->form_validation->run($this) == FALSE)
                {
                    $response['success'] = FALSE;
                    $response['message'] = lang('validation_error');
                    $response['errors']  = array(
                        'ticket_section' => form_error('ticket_section'),
                    );
                }
                else
                {
                    // get ticket type section
                    $section = $this->sections_model->select('section_id, section_name, section_capacity, section_available, ticket_type_name')
                                                    ->select('ticket_type_id, ticket_type_event_id, ticket_type_name, ticket_type_currency, ticket_type_amount')
                                                    ->select('ticket_type_capacity, ticket_type_available')
                                                    ->select('microsite_slug')
                                                    ->select('event_id')
                                                    ->join('ticket_types', 'section_ticket_type_id=ticket_type_id', 'LEFT')
                                                    ->join('events', 'event_id=ticket_type_event_id', 'LEFT')
                                                    ->join('microsites', 'microsite_event_id=event_id', 'LEFT')
                                                    ->where('section_deleted', 0)
                                                    ->find($this->input->post('ticket_section'));

                    if($section)
                    {
                        // check if we have enough available seats for a particular section
                        if($section->section_available <= 0)
                        {
                            $response = array(
                                'success' => FALSE,
                                'message' => lang('no_available_seats')
                            );
                        }
                        else // Yay, we have available seats
                        {
                            // Add data to cart
                            $cart = array(
                                'id' => $section->section_id,
                                'qty' => (int) $this->input->post('ticket_quantity'),
                                'price' => $section->ticket_type_amount,
                                'name' => $section->ticket_type_name, 
                                'options' => array(
                                    'currency' => $section->ticket_type_currency,
                                    'section' => $section->section_name, 
                                    'event_id' => $section->event_id,
                                    'ticket_type_id' => $section->ticket_type_id
                                )
                            );
                            
                            $this->cart->insert($cart);
                            
                            // Add to orders table
                            $cart_contents = $this->cart->contents();
                            
                            if($cart_contents)
                            {
                                // Start Transaction
                                $this->db->trans_start();
                                
                                foreach($cart_contents as $content)
                                {
                                    $order = array(
                                        'order_user_id' => $user->id,
                                        'order_event_id' => $section->event_id,
                                        'order_ticket_type_id' => $section->ticket_type_id, 
                                        'order_section_id' => $section->section_id
                                    );
                                    
                                    $this->orders_model->insert($order);
                                }
                                
                                // Update section available
                                $section_data = array(
                                    'section_available' => $section->section_available - $cart['qty']
                                );

                                $this->sections_model->update($section->section_id, $section_data);

                                // Update ticket type available
                                $ticket_type_data = array(
                                    'ticket_type_available' => $section->ticket_type_available - $cart['qty']
                                );

                                $this->ticket_types_model->update($section->ticket_type_id, $ticket_type_data);
                                
                                $this->db->trans_complete();
                                
                                if ($this->db->trans_status() === FALSE)
                                {
                                    // Destroy cart
                                    $this->cart->destroy();
                                    
                                    // Rollback DB
                                    $this->db->trans_rollback();
                                    $response = array(
                                        'success' => FALSE,
                                        'message' => lang('transaction_error')
                                    );
                                }
                                else
                                {
                                    $this->db->trans_commit();
                                    $response = array(
                                        'success'  => TRUE,
                                        'redirect' => site_url('microsite/'.$section->microsite_slug.'/cart'),
                                        'message'  => lang('add_to_cart_success')
                                    );
                                }
                            }
                        }
                    }
                    else
                    {
                        $response = array(
                            'success' => FALSE,
                            'message' => lang('ticket_section_not_exist')
                        );
                    }
                }
            }
            else
            {
                $response['success'] = FALSE;
                $response['message'] = lang('login_required');
            }
            
            echo json_encode($response);
        }
    }
    
    public function process_ticket_quantity()
    {
        is_ajax();
        
        echo '<pre>';
        print_r($_POST);
    }
    
    // --------------------------------------------------------------------
	/**
	 * process_transactions
     * 
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robert.obias@google.com>
	 */
    public function process_transaction()
    {
        is_ajax();
        
        $cart   = $this->cart->contents();
        $ticket = $this->input->post('ticket');
        $first_name = $this->input->post('ticket[first_name]');
        
        if($cart)
        {
            foreach($cart as $v)
            {
                $this->form_validation->set_rules('ticket[firstname]['.$v['id'].']',  lang('event_firstname'),  'required');
                $this->form_validation->set_rules('ticket[lastname]['.$v['id'].']',   lang('event_lastname'),   'required');
                $this->form_validation->set_rules('ticket[email]['.$v['id'].']',      lang('event_email'),       'required|valid_email');
                $this->form_validation->set_rules('ticket[phone]['.$v['id'].']',      lang('event_phone'),       'required');
                $this->form_validation->set_rules('ticket[company]['.$v['id'].']',    lang('event_company'),     'required');
            }
            
            $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
            
            if ($this->form_validation->run($this) == FALSE)
            {
                $response['success'] = FALSE;
                $response['message'] = lang('validation_error');

                $errors = array();
                foreach($cart as $v)
                {
                    $errors[] = array(
                        'ticket_id'         => $v['id'],
                        'ticket_firstname'  => form_error('ticket[firstname]['.$v['id'].']'),
                        'ticket_lastname'   => form_error('ticket[lastname]['.$v['id'].']'),
                        'ticket_email'      => form_error('ticket[email]['.$v['id'].']'),
                        'ticket_phone'      => form_error('ticket[phone]['.$v['id'].']'), 
                        'ticket_company'    => form_error('ticket[company]['.$v['id'].']')
                    );
                }
                
                $response['errors']  = $errors;
            }
            else
            {
                foreach($cart as $key=>$v)
                {
                    
                    // Add to tickets
                    $data = array(
                        'ticket_type_id' => $v['options']['ticket_type_id'],
                        'ticket_event_id' => $v['options']['event_id'],
                        'ticket_firstname' => $ticket['firstname'][$v['id']],
                        'ticket_lastname' => $ticket['lastname'][$v['id']],
                        'ticket_mobile' => $ticket['phone'][$v['id']],
                        'ticket_email' => $ticket['email'][$v['id']],
                        'ticket_company' => $ticket['company'][$v['id']],
                        'ticket_registered_on' => date('Y-m-d'),
                        'ticket_status' => 'Registered',
                    );
                    
                    $ticket_id[]  = $this->tickets_model->insert($data);
                                        
                    // Add to transactions
                    $data = array(
                        'transaction_event_id'  => (int) $this->input->post('event_id'),
                        'transaction_ticket_id' => $ticket_id['0'],
                        'transaction_currency'  => 'PHP',
                        'transaction_amount'    => $this->cart->total(),
                        'transaction_status'    => 'Pending'
                    );
                    
                    $this->transactions_model->insert($data);

                }
                
                $response['success'] = TRUE;
                $response['message'] = 'Successfully added';
            }
            
            echo json_encode($response);
        }
    }
    
    // --------------------------------------------------------------------
	/**
	 * user register
	 *
     * 
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robert.obias@google.com>
	 */
    public function user_register()
    {
        is_ajax();
        
        $this->config->load('recaptcha');
        $this->load->library('recaptcha');
        
        $data = array();
        
        $this->template->set_template('modal');
		
		//$this->template->add_css(module_css('events', 'events_add'), 'embed');
		$this->template->add_js(module_js('microsites', 'microsite_event_user_register'), 'embed');
        
		$this->template->write_view('content', 'microsite_event_user_register', $data);
		$this->template->render();
    }
    
    // --------------------------------------------------------------------
	/**
	 * process user register
	 *
     * 
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robert.obias@google.com>
	 */
    public function process_user_register()
    {
        is_ajax();
        
        $this->form_validation->set_rules('event_firstname', lang('event_firstname'), 'required');
        $this->form_validation->set_rules('event_middlename', lang('event_middlename'), 'required');
        $this->form_validation->set_rules('event_lastname', lang('event_lastname'), 'required');
        $this->form_validation->set_rules('event_email', lang('event_email'), 'required|valid_email');
        $this->form_validation->set_rules('event_password', lang('event_password'), 'required|matches[event_confirm_password]');
        $this->form_validation->set_rules('event_confirm_password', lang('event_confirm_password'), 'required');
        $this->form_validation->set_rules('event_agree_terms_and_conditions', lang('event_agree_terms_and_conditions'), 'required');
        $this->form_validation->set_rules('event_agree_privacy_policy', lang('event_agree_privacy_policy'), 'required');
        $this->form_validation->set_rules('event_captcha', lang('event_captcha'), 'required');
        
        $this->form_validation->set_message('required', '%s is required');
        $this->form_validation->set_message('matches', 'Password must match');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
        
        if ($this->form_validation->run($this) == FALSE)
		{
            $response = array(
                'success' => FALSE,
                'message' => lang('validation_error'),
                'errors'  => array(
                    'event_firstname' => form_error('event_firstname'),
                    'event_middlename' => form_error('event_middlename'),
                    'event_lastname' => form_error('event_lastname'),
                    'event_email' => form_error('event_email'),
                    'event_password' => form_error('event_password'),
                    'event_confirm_password' => form_error('event_confirm_password'), 
                    'event_agree_terms_and_conditions' => form_error('event_agree_terms_and_conditions'), 
                    'event_agree_privacy_policy' => form_error('event_agree_privacy_policy'),
                    'event_captcha' => form_error('event_captcha')
                )
            );
		}
        else
        {
            $event_email = $this->input->post('event_email');
            $event_password = $this->input->post('event_password');
            $event_captcha = $this->input->post('event_captcha');
            $response = $this->recaptcha->verifyResponse($event_captcha);
            
            if (isset($response['success']) and $response['success'] === TRUE) 
            {
                
                $additional = array(
                    'first_name' => $this->input->post('event_firstname'),
                    'last_name' => $this->input->post('event_lastname')
                );

                if ($this->ion_auth->register($event_email, $event_password, $event_email, $additional))
                {
                    $response = array(
                        'success' => TRUE,
                        'message' => lang('register_success')
                    );
                }
                
            }
            else
            {
                $response = array(
                    'success' => FALSE,
                    'message' => lang('event_captcha_failed')
                );
            }
            
            
        }
        
        echo json_encode($response);
    }
    
    // --------------------------------------------------------------------
	/**
	 * user login
	 *
     * 
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robert.obias@google.com>
	 */
    public function user_login()
    {
        is_ajax();
        
        $data = array();
        
        $this->template->set_template('modal');
		$this->template->add_js(module_js('microsites', 'microsite_event_user_login'), 'embed');
		$this->template->write_view('content', 'microsite_event_user_login', $data);
		$this->template->render();
    }
    
    // --------------------------------------------------------------------
	/**
	 * logout
	 *
     * 
	 * @access	public
	 * @param	$slug string
	 * @author 	Robert Christian Obias <robert.obias@google.com>
	 */
    public function logout($slug)
    {
        $data['title'] = "Logout";
        
		//log the user out
		$logout = $this->ion_auth->logout();

		//redirect them to the login page
		$this->session->set_flashdata('message', $this->ion_auth->messages());
        
		redirect('microsite/'.$slug, 'refresh');
    }
    
    // --------------------------------------------------------------------
	/**
	 * process user login
	 *
     * 
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robert.obias@google.com>
	 */
    public function process_user_login()
    {
        is_ajax();

        $tables = $this->config->item('tables','ion_auth');

        //validate form input
        $this->form_validation->set_rules('event_email',    lang('event_email'), 'required|valid_email');
        $this->form_validation->set_rules('event_password', lang('event_password'), 'required');

        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

        if($this->form_validation->run($this) == FALSE)
        {
            $response['success'] = FALSE;
            $response['message'] = lang('validation_error');
            $response['errors']  = array(
                'event_email'    => form_error('event_email'),
                'event_password' => form_error('event_password'),
            );
        }
        else
        {
            $event_email = $this->input->post('event_email');
            $event_password = $this->input->post('event_password');

            if($this->ion_auth->login($event_email, $event_password, TRUE))
            {
                $response['success']  = TRUE;
                $response['message']  = lang('login_success');
                $response['redirect'] = site_url();
            }
            else
            {
                $response['success'] = FALSE;
                $response['message'] = lang('login_failed');
            }
        }
        
        echo json_encode($response);
    }
    
    // --------------------------------------------------------------------
	/**
	 * delete cart item
	 *
     * 
	 * @access	public
	 * @param	$row_id (int)
	 * @author 	Robert Christian Obias <robert.obias@google.com>
	 */
    public function delete_cart_item($row_id)
	{
        is_ajax();
        
		$data['page_heading'] = lang('delete_heading');
		$data['page_confirm'] = lang('delete_confirm');
		$data['page_button'] = lang('button_delete');

		if ($this->input->post())
		{
            $cart_item = $this->cart->get_item($row_id);
            $section_id = $cart_item['id'];
            $ticket_type_id = $cart_item['options']['ticket_type_id'];
            
            $section = $this->sections_model->get_section($section_id);
            $ticket_type = $this->ticket_types_model->get_ticket_type($ticket_type_id);
            
            // Re-add to section and ticket types available fields
            // Update section
            $section_data = array(
                'section_available' => $section->section_available + $cart_item['qty']
            );
            $this->sections_model->update($section_id, $section_data);
            
            // Update ticket type
            $ticket_type_data = array(
                'ticket_type_available' => $ticket_type->ticket_type_available + $cart_item['qty']
            );
            $this->ticket_types_model->update($ticket_type_id, $ticket_type_data);
            
            // Remove from cart
            $this->cart->remove($row_id);
            
    		echo json_encode(array('success' => true, 'message' => lang('remove_item'))); exit;
		}

		$this->load->view('../../../views/confirm', $data);
	}
    
    public function process_payment()
    {
        echo 'abc';
    }
    
    // --------------------------------------------------------------------
	/**
	 * process ticket section
	 *
     * 
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robert.obias@google.com>
	 */
    public function process_ticket_section()
    {
        $section_id = (int) $this->input->post('section_id');
        $section = $this->sections_model->get_section($section_id);
        $ticket_type = $this->ticket_types_model->get_ticket_type($section->section_ticket_type_id);
        
        $ticket_max = $section->section_available;
        if($section->section_available >= $ticket_type->ticket_type_max_qty)
        {
            $ticket_max = $ticket_type->ticket_type_max_qty;
        }
        
        $selection = array();
        for($i=$ticket_type->ticket_type_min_qty;$i<=$ticket_max;$i++)
        {
            $selection[$i] = $i;
        }
        
        echo form_dropdown('ticket_quantity', $selection, '1', 'class="form-control input-number ticket_quantity"');
    }
   
    
    // --------------------------------------------------------------------

	/**
	 * _fields
	 *
	 * @access	private
	 * @param	$fields array()
	 * @author 	Robert Christian Obias <robert.obias@google.com>
	 */
    private function _fields($fields = array(), $event_id)
    {
        // get current user data
        $user = $this->ion_auth->user()->row();

        $data = array( 
            'fields'    => $fields,
            'event_id'  => $event_id, 
            'user'      => $user
        );
        
        return $this->load->view('microsite_event_fields', $data, TRUE);
    }
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');

$slug = 'microsite/(:any)';
//$route['user_register']         = 'microsites/user_register';
//$route['process_user_register'] = 'microsites/process_user_register';
//$route['user_login']            = 'microsites/user_login';
//$route['process_user_login']    = 'microsites/process_user_login';

//$route['(:any)/cart'] = 'microsites/cart/$1';
//$route['(:any)/register'] = 'microsites/register/$1';
//$route['(:any)/payment'] = 'microsites/payment/$1';

$route[$slug.'/user_register'] = 'microsites/user_register';
$route[$slug.'/user_login'] = 'microsites/user_login';
$route[$slug.'/logout'] = 'microsites/logout/$1';
$route[$slug.'/process_user_register'] = 'microsites/process_user_register';
$route[$slug.'/process_user_login'] = 'microsites/process_user_login';
$route[$slug.'/cart'] = 'microsites/cart/$1';
$route[$slug.'/delete_cart_item/(:any)'] = 'microsites/delete_cart_item/$2';
$route[$slug.'/register'] = 'microsites/register/$1';
$route[$slug.'/payment']  = 'microsites/payment/$1';
$route[$slug.'/process_ticket_section'] = 'microsites/process_ticket_section';
$route[$slug.'/process_ticket_quantity'] = 'microsites/process_ticket_quantity';
$route[$slug] = 'microsites/view/$1';

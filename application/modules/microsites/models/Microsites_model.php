<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Microsites_model Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Google Admin <rcmediaph@google.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link		http://www.google.com
 */
class Microsites_model extends BF_Model {

	protected $table_name			= 'microsites';
	protected $key					= 'microsite_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'microsite_created_on';
	protected $created_by_field		= 'microsite_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'microsite_modified_on';
	protected $modified_by_field	= 'microsite_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'microsite_deleted';
	protected $deleted_by_field		= 'microsite_deleted_by';
    
    // --------------------------------------------------------------------

	/**
	 * get_microsite
	 *
	 * @access	public
	 * @param	string $key
	 * @author Robert Christian Obias <robert.obias@google.com>
	 */
    public function get_microsite($slug)
    {
        // get the microsite dropdown
		if (! $microsite = $this->cache->get(site_url() . 'microsite_' . $slug))
		{
			$microsite = $this->microsites_model->join('events', 'event_id = microsite_event_id', 'LEFT')
                                                ->join('microsite_themes', 'microsites.microsite_theme_id = microsite_themes.microsite_theme_id', 'LEFT')
                                                ->join('event_venues', 'event_venues.event_venue_id = events.event_venue_id', 'LEFT')
                                                ->where('microsite_deleted', 0)
                                                ->find_by(array('microsite_slug' => $slug));
            
			
			$this->cache->save(site_url() . 'microsite_' . $slug, $microsite, 300); // TTL in seconds
		}

		return $microsite;
        
    }

}
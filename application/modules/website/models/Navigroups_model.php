<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Navigroups_model Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link		http://www.google.com
 */
class Navigroups_model extends BF_Model 
{

	protected $table_name			= 'navigroups';
	protected $key					= 'navigroup_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'navigroup_created_on';
	protected $created_by_field		= 'navigroup_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'navigroup_modified_on';
	protected $modified_by_field	= 'navigroup_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'navigroup_deleted';
	protected $deleted_by_field		= 'navigroup_deleted_by';

	// --------------------------------------------------------------------

	/**
	 * get_datatables
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	// public function get_datatables()
	// {
	// 	$fields = array(
	// 		'navigroup_id', 
	// 		'navigroup_name',

	// 		'navigroup_created_on', 
	// 		'concat(creator.first_name, " ", creator.last_name)', 
	// 		'navigroup_modified_on', 
	// 		'concat(modifier.first_name, " ", modifier.last_name)'
	// 	);

	// 	return $this->join('users as creator', 'creator.id = navigroup_created_by', 'LEFT')
	// 				->join('users as modifier', 'modifier.id = navigroup_modified_by', 'LEFT')
	// 				->datatables($fields);
	// }
}
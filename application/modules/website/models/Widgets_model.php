<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Widgets_model Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link		http://www.google.com
 */
class Widgets_model extends BF_Model 
{

	protected $table_name			= 'widgets';
	protected $key					= 'widget_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'widget_created_on';
	protected $created_by_field		= 'widget_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'widget_modified_on';
	protected $modified_by_field	= 'widget_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'widget_deleted';
	protected $deleted_by_field		= 'widget_deleted_by';

	// --------------------------------------------------------------------

	/**
	 * get_datatables
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function get_datatables()
	{
		$fields = array(
			'widget_id',
			'widget_name',
			'widget_section',
			'widget_content',
			'widget_order',
			'widget_status',
			
			'widget_created_on', 
			'concat(creator.first_name, " ", creator.last_name)', 
			'widget_modified_on', 
			'concat(modifier.first_name, " ", modifier.last_name)'
		);

		return $this->join('users as creator', 'creator.id = widget_created_by', 'LEFT')
					->join('users as modifier', 'modifier.id = widget_modified_by', 'LEFT')
					->datatables($fields);
	}
}
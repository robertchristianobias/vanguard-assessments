<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Post_categories_model Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2015, Google.
 * @link		http://www.google.com
 */
class Post_categories_model extends BF_Model 
{

	protected $table_name			= 'post_categories';
	protected $key					= 'post_category_category_id';

	protected $log_user				= FALSE;
	protected $set_created			= FALSE;
	protected $set_modified			= FALSE;
	protected $soft_deletes			= FALSE;

	// --------------------------------------------------------------------

	/**
	 * get_current_categories
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function get_current_categories($id)
	{
		$result = $this
			->join('categories', 'category_id = post_category_category_id', 'LEFT')
			->where('post_category_post_id', $id)
			->format_dropdown('category_id', 'category_name');

		return $result;
	}
}
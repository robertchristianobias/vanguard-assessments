<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Banners_model Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link		http://www.google.com
 */
class Banners_model extends BF_Model 
{

	protected $table_name			= 'banners';
	protected $key					= 'banner_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'banner_created_on';
	protected $created_by_field		= 'banner_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'banner_modified_on';
	protected $modified_by_field	= 'banner_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'banner_deleted';
	protected $deleted_by_field		= 'banner_deleted_by';

	// --------------------------------------------------------------------

	/**
	 * get_datatables
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function get_datatables()
	{
		$fields = array(
			'banner_id', 
			'banner_banner_group_id',
			'banner_thumb',
			'banner_image',
			'banner_caption',
			'banner_link',
			'banner_target',
			'banner_order',
			'banner_status',

			'banner_created_on', 
			'concat(creator.first_name, " ", creator.last_name)', 
			'banner_modified_on', 
			'concat(modifier.first_name, " ", modifier.last_name)'
		);

		return $this->join('users as creator', 'creator.id = banner_created_by', 'LEFT')
					->join('users as modifier', 'modifier.id = banner_modified_by', 'LEFT')
					->datatables($fields);
	}

	// --------------------------------------------------------------------

	/**
	 * get_frontend_banners
	 *
	 * @access	public
	 * @param	integer $banner_group_id
	 * @return 	string $html
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function get_frontend_banners($banner_group_id)
	{
		$banners = $this
			->where_in('banner_banner_group_id', $banner_group_id)
			->where('banner_deleted', 0)
			->order_by('banner_order', 'asc')
			->find_all();

		if ($banners)
		{
			$indicators = '';
			$banner_items = '';
			$active = TRUE;
			foreach ($banners as $banner)
			{
				$class = ($active) ? 'active' : '';
				$link  = is_url($banner->banner_link) ? $banner->banner_link : site_url($banner->banner_link);
				$indicators .= '<li data-target="#carousel-example-generic" data-slide-to="' . $banner->banner_order . '" class="' . $class . '"></li>' . PHP_EOL;
				$banner_items .= '<div class="item ' . $class . '">';
				$banner_items .= ($banner->banner_link) ? '<a href="' . $link . '" target="' . $banner->banner_target . '">' : '';
				$banner_items .= '<img src="' . config_item('website_url') . $banner->banner_image . '" alt="">';
				$banner_items .= ($banner->banner_link) ? '</a>' : '';
				$banner_items .= '<div class="carousel-caption">' . $banner->banner_caption . '</div></div>' . PHP_EOL;
				$active = FALSE;
			}

			$html = '<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">' . PHP_EOL;
			$html .= '<ol class="carousel-indicators">' . PHP_EOL;
			$html .= $indicators;
			$html .= '</ol>' . PHP_EOL;
			$html .= '<div class="carousel-inner" role="listbox">' . PHP_EOL;
			$html .= $banner_items;
			$html .= '</div>' . PHP_EOL;
			$html .= '<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">' . PHP_EOL;
			$html .= '<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>' . PHP_EOL;
			$html .= '<span class="sr-only">Previous</span>' . PHP_EOL;
			$html .= '</a>' . PHP_EOL;
			$html .= '<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">' . PHP_EOL;
			$html .= '<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>' . PHP_EOL;
			$html .= '<span class="sr-only">Next</span>' . PHP_EOL;
			$html .= '</a>' . PHP_EOL;
			$html .= '</div>' . PHP_EOL;

			return $html;
		}
		else
		{
			return FALSE;
		}
	}
}
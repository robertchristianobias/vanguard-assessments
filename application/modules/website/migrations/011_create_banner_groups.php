<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link		http://www.google.com
 */
class Migration_Create_banner_groups extends CI_Migration 
{
	private $_table = 'banner_groups';

	private $_permissions = array(
		array('Banner Groups Link', 'website.banner_groups.link'),
		array('Banner Groups List', 'website.banner_groups.list'),
		array('View Banner Group', 'website.banner_groups.view'),
		array('Add Banner Group', 'website.banner_groups.add'),
		array('Edit Banner Group', 'website.banner_groups.edit'),
		array('Delete Banner Group', 'website.banner_groups.delete'),
	);

	function __construct()
	{
		parent::__construct();

		$this->load->model('migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'banner_group_id' 			=> array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE, 'null' => FALSE),
			'banner_group_name'			=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => FALSE),

			'banner_group_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'banner_group_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'banner_group_modified_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'banner_group_modified_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'banner_group_deleted' 		=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'banner_group_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('banner_group_id', TRUE);
		$this->dbforge->add_key('banner_group_name');

		$this->dbforge->add_key('banner_group_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

		// add the module permissions
		$this->migrations_model->add_permissions($this->_permissions);

		// add the initial values
		$data = array(
			array('banner_group_name'  => 'Main'),
		);
		$this->db->insert_batch($this->_table, $data);
	}

	public function down()
	{
		// drop the table
		$this->dbforge->drop_table($this->_table);

		// delete the permissions
		$this->migrations_model->delete_permissions($this->_permissions);
	}
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link		http://www.google.com
 */
class Migration_Edit_configs_01 extends CI_Migration
{
	var $_table = 'configs';

	function __construct()
	{
		parent::__construct();
	}
	
	public function up()
	{
		$this->db->set('config_values', '"starter,legion,frontend"', FALSE);
		$this->db->where('config_name', 'website_theme');
		$this->db->update($this->_table);
	}

	public function down()
	{
		$this->db->set('config_values', 'starter', FALSE);
		$this->db->where('config_name', 'website_theme');
		$this->db->update($this->_table);
	}
}
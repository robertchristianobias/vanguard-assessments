<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Migration Class
*
* @package	rcmediaph
* @version	1.0
* @author Robert Christian Obias <rchristian_obias@yahoo.com>
* @copyright Copyright (c) 2016, Google.
* @link	http://www.google.com
*/
class Migration_Edit_navigations_01 extends CI_Migration 
{
	private $_table = 'navigations';

	public function __construct() 
	{
		parent::__construct();
	}

	public function up() 
	{
		$fields = array(
		'navigation_attribute' => array('type'  => 'VARCHAR', 'constraint' => 255, 'null' => TRUE),
		);
		$this->dbforge->add_column($this->_table, $fields);
		$this->db->query('ALTER TABLE ' . $this->_table . ' ADD INDEX `navigation_attribute` (`navigation_attribute`)');
	}

	public function down()
	{
		$this->dbforge->drop_column($this->_table, 'navigation_attribute');
	}
}
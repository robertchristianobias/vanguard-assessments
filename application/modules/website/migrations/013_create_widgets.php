<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link		http://www.google.com
 */
class Migration_Create_widgets extends CI_Migration 
{
	private $_table = 'widgets';

	private $_permissions = array(
		array('Widgets Link', 'website.widgets.link'),
		array('Widgets List', 'website.widgets.list'),
		array('View Widget', 'website.widgets.view'),
		array('Add Widget', 'website.widgets.add'),
		array('Edit Widget', 'website.widgets.edit'),
		array('Delete Widget', 'website.widgets.delete'),
	);

	private $_menus = array(
		array(
			'menu_parent'		=> 'website', // 'none' if parent menu or single menu; or menu_link of parent
			'menu_text' 		=> 'Widgets', 
			'menu_link' 		=> 'website/widgets', 
			'menu_perm' 		=> 'website.widgets.link', 
			'menu_icon' 		=> 'fa fa-clone', 
			'menu_order' 		=> 7, 
			'menu_active' 		=> 1
		),
	);

	function __construct()
	{
		parent::__construct();

		$this->load->model('migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'widget_id' 			=> array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE, 'null' => FALSE),
			'widget_type'			=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => FALSE),
			'widget_name'			=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => FALSE),
			'widget_section'		=> array('type' => 'VARCHAR', 'constraint' => 20, 'null' => FALSE),
			'widget_function'		=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => FALSE),
			'widget_source_table'	=> array('type' => 'VARCHAR', 'constraint' => 100, 'null' => TRUE),
			'widget_source_id'		=> array('type' => 'INT', 'unsigned' => TRUE, 'default' => 0),
			'widget_order'			=> array('type' => 'TINYINT', 'constraint' => 3, 'unsigned' => TRUE, 'default' => 0),
			'widget_status'			=> array('type' => 'SET("Active","Disabled")', 'null' => FALSE),

			'widget_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'widget_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'widget_modified_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'widget_modified_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'widget_deleted' 		=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'widget_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('widget_id', TRUE);
		$this->dbforge->add_key('widget_type');
		$this->dbforge->add_key('widget_name');
		$this->dbforge->add_key('widget_section');
		$this->dbforge->add_key('widget_function');
		$this->dbforge->add_key('widget_source_table');
		$this->dbforge->add_key('widget_source_id');
		$this->dbforge->add_key('widget_order');
		$this->dbforge->add_key('widget_status');
		

		$this->dbforge->add_key('widget_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

		// add the module permissions
		$this->migrations_model->add_permissions($this->_permissions);

		// add the module menu
		$this->migrations_model->add_menus($this->_menus);

		// add the initial values
		$data = array(
			array('widget_type'  => 'Partial', 'widget_name' => 'Sample', 'widget_section' => 'sidebar', 'widget_function' => 'frontend_partial_widget(1)', 'widget_source_table' => 'partials', 'widget_source_id' => 1, 'widget_order' => 1, 'widget_status' => 'Active', 'widget_created_by' => 1, 'widget_created_on' => date('Y-m-d H:i:s'))
		);
		$this->db->insert_batch($this->_table, $data);
	}

	public function down()
	{
		// drop the table
		$this->dbforge->drop_table($this->_table);

		// delete the permissions
		$this->migrations_model->delete_permissions($this->_permissions);

		// delete the menu
		$this->migrations_model->delete_menus($this->_menus);
	}
}
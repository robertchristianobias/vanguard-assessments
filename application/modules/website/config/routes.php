<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['website/navigations/(:num)'] = 'website/navigations/index/$1';
$route['website/banners/(:num)'] = 'website/banners/index/$1';
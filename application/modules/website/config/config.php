<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// load the theme's config
require FCPATH . 'themes/frontend/' . config_item('website_theme') . '/conf/config.php';
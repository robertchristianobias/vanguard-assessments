/**
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link		http://www.google.com
 */

$(function() {

	// handles the post action
	$('#post').click(function(e){
		// change the button to loading state
		var btn = $(this);
		btn.button('loading');

		// prevents a submit button from submitting a form
		e.preventDefault();

		// submits the data to the backend
		$.post(post_url, {
			partial_title: $('#partial_title').val(),
			partial_content: tinyMCE.activeEditor.getContent(),
			partial_status: $('.partial_status:checked').val(),
		},
		function(data, status){
			// handles the returned data
			var o = jQuery.parseJSON(data);
			if (o.success === false) {
				// shows the error message
				alertify.error(o.message);

				// displays individual error messages
				if (o.errors) {
					for (var form_name in o.errors) {
						$('#error-' + form_name).html(o.errors[form_name]);
					}
				}
			} else {
				if (o.action == 'add') {
					window.location.replace(app_url + 'website/partials/form/edit/' + o.id);
				} else {
					// shows the success message
					alertify.success(o.message); 
				}
			}
			// reset the button
			btn.button('reset');
		});
	});

	// initialize tinymce
	tinymce.init({
		selector: "#partial_content",
		theme: "modern",
		statusbar: true,
		menubar: true,
		relative_urls: false,
		remove_script_host : false,
		convert_urls : true,
		plugins: [
			'advlist autolink lists link image charmap print preview hr anchor pagebreak',
			'searchreplace wordcount visualblocks visualchars code',
			'insertdatetime media nonbreaking save table contextmenu directionality',
			'emoticons template paste textcolor colorpicker textpattern'
		],
		toolbar1: 'insertfile undo redo | styleselect forecolor backcolor | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link emoticons mybutton',
		image_advtab: true,
		setup: function (editor) {
		editor.addButton('mybutton', {
			text: '',
				icon: 'image',
				onclick: function () {
					$('#modal').modal({
						remote: app_url + 'files/images/rte/mce'
					})
				}
			});
		},
		content_css: app_url + 'themes/backend/aceadmin/css/tinymce.css'
	});
});
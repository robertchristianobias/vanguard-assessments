/**
 * @package     rcmediaph
 * @version     1.0
 * @author      Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright   Copyright (c) 2016, Google.
 * @link        http://www.google.com
 */
$(function() {
	$( "#sortable" ).sortable({
		update: function( event, ui ) {
			var banner_ids = new Array();
			$("#sortable li").each(function(key, val){
				banner_ids.push($(this).attr('data-id'));
			});
			
			// submit
			$.post(app_url + 'website/banners/reorder', {
				banner_ids: banner_ids,
			},
			function(data, status){
				// handles the returned data
				var o = jQuery.parseJSON(data);
				if (o.success === false) {
					// shows the error message
					alertify.error(o.message);

				} else {
					// // shows the success message
					// alertify.success(o.message); 
				}
			});
		}
	});
    $( "#sortable" ).disableSelection();
});
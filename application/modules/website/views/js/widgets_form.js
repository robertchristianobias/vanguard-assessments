/**
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link		http://www.google.com
 */

$(function() {

	// console.log($(this).children().children().children())

	// handles the submit action
	$('#submit').click(function(e){
		// change the button to loading state
		var btn = $(this);
		btn.button('loading');

		// prevents a submit button from submitting a form
		e.preventDefault();

		// submits the data to the backend
		$.post(ajax_url, {
			widget_type: $('#widget_type').val(),
			widget_name: $('#widget_name').val(),
			widget_source_id: $('#widget_source_id').val(),
			widget_status: $('#widget_status').val(),
		},
		function(data, status){
			// handles the returned data
			var o = jQuery.parseJSON(data);
			if (o.success === false) {
				// shows the error message
				alertify.error(o.message);

				// displays individual error messages
				if (o.errors) {
					for (var form_name in o.errors) {
						$('#error-' + form_name).html(o.errors[form_name]);
					}
				}
			} else {

				// adds the title and widget id
				var title = ($('#widget_name').val()) ? $('#widget_name').val() : 'No Title';
				$('#' + widget_temp_id + ' .list-group-item-text').text($('#widget_type').val() + ': ' + title);
				$('#' + widget_temp_id).attr('data-widget-id', o.widget_id);

				// submits the order
				var widget_ids = new Array();
				$.each($('#' + widget_temp_id).parent().children('.box'), function(i,e) {
					widget_ids.push($(this).attr('data-widget-id'));
				});
				console.log(widget_ids)

				// submit
				$.post(app_url + 'website/widgets/reorder', { widget_ids: widget_ids } );

				// closes the modal
				$('#modal').modal('hide'); 

				// // restores the modal content to loading state
				// restore_modal(); 

				// // shows the success message
				// alertify.success(o.message); 

				// window.location.reload(true);
			}
		});
	});

	// disables the enter key
	$('form input').keydown(function(event){
		if(event.keyCode == 13) {
			event.preventDefault();
			return false;
		}
	});

});
/**
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2015, Google.
 * @link		http://www.google.com
 */

$(function() {

	// handles the post action
	$('#post').click(function(e){
		// change the button to loading state
		var btn = $(this);
		btn.button('loading');

		// prevents a submit button from submitting a form
		e.preventDefault();

		// get checked categories
		var checkedCategories = $('.post_categories:checked').map(function() {
			return this.value;
		}).get();

		// submits the data to the backend
		$.post(post_url, {
			post_title: $('#post_title').val(),
			post_content: tinyMCE.activeEditor.getContent(),
			post_categories: checkedCategories,
			post_posted_on: $('#post_posted_on').val(),
			post_layout: $('#post_layout').val(),
			post_status: $('.post_status:checked').val(),
		},
		function(data, status){
			// handles the returned data
			var o = jQuery.parseJSON(data);
			if (o.success === false) {
				
				// shows the error message
				alertify.error(o.message);

				// displays individual error messages
				if (o.errors) {
					for (var form_name in o.errors) {
						$('#error-' + form_name).html(o.errors[form_name]);
					}
				}
			} else {
				if (o.action == 'add') {
					window.location.replace(app_url + 'website/posts/form/edit/' + o.id);
				} else {
					// shows the success message
					alertify.success(o.message); 
				}
			}
			// reset the button
			btn.button('reset');

		});
	});

	// initialize tinymce
	tinymce.init({
		selector: "#post_content",
		theme: "modern",
		statusbar: true,
		menubar: true,
		relative_urls: false,
		remove_script_host : false,
		convert_urls : true,
		plugins: [
			'advlist autolink lists link image charmap print preview hr anchor pagebreak',
			'searchreplace wordcount visualblocks visualchars code',
			'insertdatetime media nonbreaking save table contextmenu directionality',
			'emoticons template paste textcolor colorpicker textpattern'
		],
		toolbar1: 'insertfile undo redo | styleselect forecolor backcolor | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link emoticons mybutton',
		image_advtab: true,
		setup: function (editor) {
		editor.addButton('mybutton', {
			text: '',
				icon: 'image',
				onclick: function () {
					$('#modal').modal({
						remote: app_url + 'files/images/rte/mce'
					})
				}
			});
		},
		content_css: app_url + 'themes/backend/aceadmin/css/tinymce.css'
	});

	$("#dtBox").DateTimePicker({
		dateTimeFormat: "yyyy-mm-dd HH:mm:ss",
		minuteInterval: 5,
		secondsInterval: 5,
	});
});
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span>
		<span class="sr-only"><?php echo lang('button_close')?></span>
	</button>
	<h4 class="modal-title" id="myModalLabel"><?php echo $page_heading?></h4>
</div>

<div class="modal-body">

	<div id="form" class="form-horizontal">

		<div class="form-group">
			<label class="col-sm-2 control-label" for="banner_thumb"><?php echo lang('banner_image')?>:</label>
			<div class="col-sm-10">
				<a href="javascript:;" id="banner_image_link" class="banner_image" data-target="banner_thumb"><img id="preview_image_thumb" src="<?php echo (isset($record->banner_thumb) && $record->banner_thumb) ? site_url($record->banner_thumb) : site_url('assets/images/transparent.png'); ?>" height="100" /></a>
			</div>
			<div class="col-sm-offset-2 col-sm-9">
				<div id="error-banner_image"></div>
			</div>
		</div>


		<div class="form-group hide">
			<label class="col-sm-2 control-label" for="banner_thumb"><?php echo lang('banner_thumb')?>:</label>
			<div class="col-sm-10">
				<?php echo form_input(array('id'=>'banner_thumb', 'name'=>'banner_thumb', 'value'=>set_value('banner_thumb', isset($record->banner_thumb) ? $record->banner_thumb : ''), 'class'=>'form-control'));?>
				<div id="error-banner_thumb"></div>
			</div>
		</div>

		<div class="form-group hide">
			<label class="col-sm-2 control-label" for="banner_image"><?php echo lang('banner_image')?>:</label>
			<div class="col-sm-10">
				<?php echo form_input(array('id'=>'banner_image', 'name'=>'banner_image', 'value'=>set_value('banner_image', isset($record->banner_image) ? $record->banner_image : ''), 'class'=>'form-control'));?>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label" for="banner_caption"><?php echo lang('banner_caption')?>:</label>
			<div class="col-sm-10">
				<?php echo form_input(array('id'=>'banner_caption', 'name'=>'banner_caption', 'value'=>set_value('banner_caption', isset($record->banner_caption) ? $record->banner_caption : ''), 'class'=>'form-control'));?>
				<div id="error-banner_caption"></div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label" for="banner_link"><?php echo lang('banner_link')?>:</label>
			<div class="col-sm-6">
				<?php echo form_input(array('id'=>'banner_link', 'name'=>'banner_link', 'value'=>set_value('banner_link', isset($record->banner_link) ? $record->banner_link : ''), 'class'=>'form-control'));?>
				<div id="error-banner_link"></div>
			</div>
			<div class="col-sm-4">
				<?php $options = array('_top' => 'Same Window', '_blank' => 'New Window'); ?>
				<?php echo form_dropdown('banner_target', $options, set_value('banner_target', (isset($record->banner_target)) ? $record->banner_target : ''), 'id="banner_target" class="form-control"'); ?>
				<div id="error-banner_target"></div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-2 control-label" for="banner_status"><?php echo lang('banner_status')?>:</label>
			<div class="col-sm-4">
				<?php $options = create_dropdown('array', 'Active,Disabled'); ?>
				<?php echo form_dropdown('banner_status', $options, set_value('banner_status', (isset($record->banner_status)) ? $record->banner_status : ''), 'id="banner_status" class="form-control"'); ?>
				<div id="error-banner_status"></div>
			</div>
		</div>

	</div>

	<div id="image" class="hide">
		<div class="nav-tabs-custom bottom-margin">
			<ul class="nav nav-tabs">
				
				<li class="active"><a href="#tab_1" data-toggle="tab">Upload Image</a></li>
				<li><a href="#tab_2" data-toggle="tab">Add Existing Image</a></li>
				<li><a href="javascript:;" class="go-back">Go Back</a></li>
			</ul>
			<div class="tab-content" data-target="">
				
				<div class="tab-pane active" id="tab_1">
					<div class="form-horizontal top-margin3">

						<div class="col-sm-4">

							<div class="form-group">

								<?php echo form_open(site_url('files/images/upload'), array('class'=>'dropzone', 'id'=>'dropzone'));?>
								<div class="fallback">
									<input name="file" type="file" class="hide" />
								</div>
								<?php echo form_close();?>

							</div>

						</div>

						<div class="col-sm-6 text-center">
							<div id="image_sizes"></div>
						</div>

					</div>

					<div class="clearfix"></div>
				</div>

				<div class="tab-pane" id="tab_2">
					<table class="table table-striped table-bordered table-hover dt-responsive" id="dt-images">
						<thead>
							<tr>
								<th class="all"><?php echo lang('index_id')?></th>
								<th class="min-desktop"><?php echo lang('index_width'); ?></th>
								<th class="min-desktop"><?php echo lang('index_height'); ?></th>
								<th class="min-desktop"><?php echo lang('index_name'); ?></th>
								<th class="min-desktop"><?php echo lang('index_file'); ?></th>
								<th class="min-desktop"><?php echo lang('index_thumb'); ?></th>

								<th class="none"><?php echo lang('index_created_on')?></th>
								<th class="none"><?php echo lang('index_created_by')?></th>
								<th class="none"><?php echo lang('index_modified_on')?></th>
								<th class="none"><?php echo lang('index_modified_by')?></th>
								<th class="min-tablet"><?php echo lang('index_action')?></th>
							</tr>
						</thead>
					</table>
					<div id="thumbnails" class="row text-center"></div>
				</div>

				
			</div>
		</div>
	</div>

</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
		<i class="fa fa-times"></i> <?php echo lang('button_close')?>
	</button>
	<?php if ($action == 'add'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_add')?>
		</button>
	<?php elseif ($action == 'edit'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_update')?>
		</button>
	<?php else: ?>
		<script>$(".modal-body :input").attr("disabled", true);</script>
	<?php endif; ?>
</div>
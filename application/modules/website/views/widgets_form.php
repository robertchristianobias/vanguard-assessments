<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span>
		<span class="sr-only"><?php echo lang('button_close')?></span>
	</button>
	<h4 class="modal-title" id="myModalLabel"><?php echo $page_heading?></h4>
</div>

<div class="modal-body">

	<div class="form-horizontal">

		<div class="form-group hide">
			<label class="col-sm-3 control-label" for="widget_type"><?php echo lang('widget_type')?>:</label>
			<div class="col-sm-8">
				<?php echo form_input(array('id'=>'widget_type', 'name'=>'widget_type', 'value'=>set_value('widget_type', $widget_type), 'class'=>'form-control'));?>
				<div id="error-widget_type"></div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="widget_name"><?php echo lang('widget_name')?>:</label>
			<div class="col-sm-8">
				<?php echo form_input(array('id'=>'widget_name', 'name'=>'widget_name', 'value'=>set_value('widget_name', isset($record->widget_name) ? $record->widget_name : ''), 'class'=>'form-control'));?>
				<div id="error-widget_name"></div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="widget_source_id"><?php echo lang('widget_source_id')?>:</label>
			<div class="col-sm-8">
				<?php echo form_dropdown('widget_source_id', $widget_sources, set_value('widget_source_id', (isset($record->widget_source_id)) ? $record->widget_source_id : ''), 'id="widget_source_id" class="form-control"'); ?>
				<div id="error-widget_source_id"></div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="widget_status"><?php echo lang('widget_status')?>:</label>
			<div class="col-sm-8">
				<?php $options = create_dropdown('array', 'Active,Disabled'); ?>
				<?php echo form_dropdown('widget_status', $options, set_value('widget_status', (isset($record->widget_status)) ? $record->widget_status : ''), 'id="widget_status" class="form-control"'); ?>
				<div id="error-widget_status"></div>
			</div>
		</div>
		
	</div>

</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
		<i class="fa fa-times"></i> <?php echo lang('button_close')?>
	</button>
	<?php if ($action == 'add'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_add')?>
		</button>
	<?php elseif ($action == 'edit'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_update')?>
		</button>
	<?php else: ?>
		<script>$(".modal-body :input").attr("disabled", true);</script>
	<?php endif; ?>
</div>
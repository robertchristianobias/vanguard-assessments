<div class="nav-tabs-custom bottom-margin">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#tab_page" data-toggle="tab"><span class="fa fa-file-text"></span> Content</a></li>
		
		
		<li class="pull-right">
			
		</li>
		<?php if (isset($record->page_id) AND isset($record->page_metatag_id)): ?>
			<li><a href="<?php echo site_url('metatags/form/website/pages/' . $record->page_id); ?>" data-toggle="modal" data-target="#modal" class="btn btn-info"><span class="fa fa-cog"></span> Meta Tags</a></li>
		<?php endif; ?>
	</ul>
	<div class="tab-content">
		<div class="tab-pane active" id="tab_page">
			<div class="row">

				<div class="col-sm-9">

					<div class="form-group">
						<label class="control-label" for="page_title"><?php echo lang('page_title'); ?>:</label>	
						<?php echo form_input(array('id'=>'page_title', 'name'=>'page_title', 'value'=>set_value('page_title', isset($record->page_title) ? $record->page_title : '', FALSE), 'class'=>'form-control meta-title'));?>
						<div id="error-page_title"></div>
					</div>

					<div class="form-group">
						<label class="control-label" for="page_content"><?php echo lang('page_content'); ?>:</label>
						<?php echo form_textarea(array('id'=>'page_content', 'name'=>'page_content', 'rows'=>'15', 'value'=>set_value('page_content', isset($record->page_content) ? $record->page_content : '', FALSE), 'class'=>'form-control meta-description')); ?>
						<div id="error-page_content"></div>
					</div>

				</div>

				<div class="col-sm-3">
					<div class="form-group">
						<label class="control-label" for="page_parent_id"><?php echo lang('page_parent_id')?>:</label>
						<?php echo form_dropdown('page_parent_id', $pages, set_value('page_parent_id', (isset($record->page_parent_id)) ? $record->page_parent_id : ''), 'id="page_parent_id" class="form-control"'); ?>
						<div id="error-page_parent_id"></div>
					</div>

					<div class="form-group">
						<label class="control-label bottom-margin" for="page_status"><?php echo lang('page_status'); ?>:</label>
						<div class="radio top-margin">
							<label class="radio-inline">
								<input class="page_status" name="page_status" type="radio" value="Posted" <?php echo set_radio('page_status', 'Posted', (isset($record->page_status) && $record->page_status == 'Posted') ? TRUE : FALSE); ?> /> Posted
							</label>
					
							<label class="radio-inline">
								<input class="page_status" name="page_status" type="radio" value="Draft" <?php echo set_radio('page_status', 'Draft', ($action == 'add' OR isset($record->page_status) && $record->page_status == 'Draft') ? TRUE : FALSE); ?> /> Draft
							</label>
						</div>
						<div id="error-page_status"></div>
					</div>

					<div class="form-group">
						<label class="control-label" for="page_layout"><?php echo lang('page_layout')?>:</label>
						<?php echo form_dropdown('page_layout', config_item('theme_layouts'), set_value('page_layout', (isset($record->page_layout)) ? $record->page_layout : ''), 'id="page_layout" class="form-control"'); ?>
						<div id="error-page_layout"></div>
					</div>

					<div class="top-margin5">
						<?php if ($action == 'add'): ?>
							<button id="post" class="btn btn-default btn-lg btn-block" type="submit" data-loading-text="<?php echo lang('processing')?>">
								<i class="fa fa-plus"></i> <?php echo lang('button_add')?>
							</button>
						<?php elseif ($action == 'edit'): ?>
							<button id="post" class="btn btn-default btn-lg btn-block" type="submit" data-loading-text="<?php echo lang('processing')?>">
								<i class="fa fa-save"></i> <?php echo lang('button_update')?>
							</button>
						<?php endif; ?>
					</div>

				</div>

			</div>
		</div>
		<div class="tab-pane" id="tab_seo">
			
		</div>
	</div>
</div>
<script>var post_url = '<?php echo current_url() ?>';</script>
<div class="nav-tabs-custom bottom-margin">
	<ul class="nav nav-tabs">
		<li class="active"><a href="#tab_general" data-toggle="tab"><span class="fa fa-cog"></span> General</a></li>
	</ul>
	<div class="tab-content">

		<div class="tab-pane active" id="tab_general">

			<div class="form-horizontal">

				<?php if ($configs): ?>
					<?php foreach ($configs as $config): ?>
						<div class="form-group">
							<label class="col-sm-2 control-label" for="<?php echo $config->config_name; ?>"><?php echo $config->config_label; ?>:</label>
							<div class="col-sm-4">
								<?php if ($config->config_type == 'input' OR $config->config_type == 'text'): ?>
									<?php echo form_input(array('id'=>$config->config_name, 'name'=>$config->config_name, 'value'=>set_value($config->config_name, $config->config_value), 'class'=>'form-control')); ?>
								<?php elseif ($config->config_type == 'textarea'): ?>
									<?php echo form_textarea(array('id'=>$config->config_name, 'name'=>$config->config_name, 'rows'=>'10', 'value'=>set_value($config->config_name, isset($config->config_value) ? $config->config_value : '', TRUE), 'class'=>'form-control')); ?>
								<?php elseif ($config->config_type == 'dropdown'): ?>
									<?php $options = create_dropdown('array', $config->config_values); ?>
									<?php echo form_dropdown($config->config_name, $options, set_value($config->config_name, (isset($config->config_value)) ? $config->config_value: ''), 'id="' . $config->config_name . '" class="form-control"'); ?>
								<?php endif; ?>
								<div class="help-text"><?php echo $config->config_notes; ?></div>
								<div id="error-<?php echo $config->config_name; ?>"></div>
							</div>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>

			</div>
			
		</div>

	</div>
</div>

<div class="clearfix form-actions">
	<button id="submit" class="btn btn-info" type="button" data-loading-text="<?php echo lang('processing')?>">
		<i class="ace-icon fa fa-save bigger-110"></i>
		Save Changes
	</button>
</div>


<script>var post_url = '<?php echo current_url() ?>';</script>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Widgets Language File (English)
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link		http://www.google.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Website';

// Labels
$lang['widget_section']				= 'Section';
$lang['widget_content']				= 'Content';
$lang['widget_order']				= 'Order';
$lang['widget_status']				= 'Status';
$lang['widget_name']				= 'Title';
$lang['widget_source_id']			= 'Source';
$lang['widget_type']				= 'Type';

// Buttons
$lang['button_add']					= 'Add Widget';
$lang['button_update']				= 'Update Widget';
$lang['button_delete']				= 'Delete Widget';
$lang['button_edit_this']			= 'Edit This';


// Index Function
$lang['index_heading']				= 'Widgets';
$lang['index_subhead']				= 'Manage your frontend widgets here';
$lang['index_id']					= 'ID';
$lang['index_section']				= 'Section';
$lang['index_content']				= 'Content';
$lang['index_order']				= 'Order';
$lang['index_status']				= 'Status';
$lang['index_name']					= 'Name';

$lang['index_created_on']			= 'Created On';
$lang['index_created_by']			= 'Created By';
$lang['index_modified_on']			= 'Modified On';
$lang['index_modified_by']			= 'Modified By';
$lang['index_status']				= 'Status';
$lang['index_action']				= 'Action';

// View Function
$lang['view_heading']				= 'View Widget';

// Add Function
$lang['add_heading']				= 'Add Widget';
$lang['add_success']				= 'Widget has been successfully added';

// Edit Function
$lang['edit_heading']				= 'Edit Widget';
$lang['edit_success']				= 'Widget has been successfully updated';

// Delete Function
$lang['delete_heading']				= 'Delete Widget';
$lang['delete_confirm']				= 'Are you sure you want to delete this widget?';
$lang['delete_success']				= 'Widget has been successfully deleted';

// Reorder Function
$lang['reorder_heading']			= 'Reorder Widgets';
$lang['reorder_success']			= 'Widget order has been successfully updated';
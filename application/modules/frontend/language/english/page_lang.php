<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Contacts Language File (English)
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link		http://www.google.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Page';

$lang['success'] 	= 'Email successfully sent';
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// Labels
$lang['ticket_fullname']					= 'Full Name';
$lang['ticket_firstname']					= 'First Name';
$lang['ticket_middlename']					= 'Middle Name';
$lang['ticket_middle_initial']				= 'Middle Initial';
$lang['ticket_lastname']					= 'Last Name';
$lang['ticket_nickname']					= 'Nick Name';
$lang['ticket_email']						= 'Email Address';
$lang['ticket_mobile']						= 'Mobile Number';
$lang['ticket_phone']						= 'Phone Number';
$lang['ticket_gender']						= 'Gender';
$lang['ticket_birthday']					= 'Birthday';
$lang['ticket_mstatus']						= 'Marital Status';
$lang['ticket_is_pwd']						= 'Person With Disabilities';
$lang['ticket_is_senior']					= 'Senior Citizen';

$lang['ticket_address']						= 'Address';
$lang['ticket_city']						= 'City';
$lang['ticket_zipcode']						= 'Zip Code';
$lang['ticket_country']						= 'Country';

$lang['ticket_company']						= 'Company or School';
$lang['ticket_work_address']				= 'Work or School Address';
$lang['ticket_occupation']					= 'Occupation or Course';
$lang['ticket_work_phone']					= 'Work or School Phone Number';

$lang['ticket_medical_conditions']			= 'Medical Conditions';
$lang['ticket_emergency_contact']			= 'Full Name';
$lang['ticket_emergency_number']			= 'Phone/Mobile';
$lang['ticket_emergency_relationship']		= 'Relationship';

$lang['ticket_newsletter']					= 'I want to subscribe to the Newsletter.';
$lang['ticket_terms']						= 'Terms and Conditions';
$lang['captcha']               = 'Captcha';

$lang['coupon_code']    = 'Coupon Code';
$lang['invalid_coupon'] = 'Invalid coupon code.';
$lang['valid_coupon']   = 'Coupon claimed.';
$lang['used_coupon']    = 'Coupon is already in used.';

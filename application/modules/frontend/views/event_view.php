<style>
	<?php echo $microsite->microsite_custom_css ?>
</style>
<?php if($microsite->microsite_slug == 'true-life-2016') { ?>
<style>
    h1, h2, h3, h4, h5, h6 {
        font-family: "Open Sans";
    }
</style>
<?php } ?>
<div class="col-md-offset-1 col-md-10">
    <div class="row">
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading"><h3>Description</h3></div>
                <div class="panel-body">
                    <?php echo $microsite->event_description; ?>
                </div>
            </div>

            <?php if($microsite->event_organizer) { ?>
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>Organizer</h3></div>
                    <div class="panel-body">
                        <?php echo $microsite->event_organizer; ?>
                    </div>
                </div>
            <?php } ?>

            <?php if($microsite->event_sponsors) { ?>
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>Sponsor</h3></div>
                    <div class="panel-body">
                        <?php echo $microsite->event_sponsors; ?>
                    </div>
                </div>
            <?php } ?>

            <?php if($microsite->event_contacts) { ?>
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>Contacts</h3></div>
                    <div class="panel-body">
                        <?php echo $microsite->event_contacts; ?>
                    </div>
                </div>
            <?php } ?>

            <?php if($microsite->event_rules) { ?>
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>Rules</h3></div>
                    <div class="panel-body">
                         <?php  echo $microsite->event_rules; ?>
                    </div>
                </div>
            <?php } ?>

        </div>
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Where and When</h3>
                    <p>
                        <?php echo $microsite->event_venue_name . ' ' . $microsite->event_venue_address; ?> 
                    </p>
                    <p>
                        <?php echo date('F d, Y gA', strtotime($microsite->event_starts_on)); ?> onwards
                    </p>
                    <br />
                    
                    <!-- TODO -->
                    <?php if($microsite->microsite_slug == 'legion-run-manila-2017') { ?>
                        <h3>What you get</h3>
                        <ul>
                            <li>A free beer</li>
                            <li>A medal</li>
                            <li>An exclusive "I Am Legion" t-shirt that only participants get</li>
                            <li>A few scrapes, bruises, mud in places it shouldn't be</li>
                            <li>Genuine Pride</li>
                        </ul>
                    <?php } ?>
                </div>
            </div>

            <?php if($ticket_types) { ?>
                <div class="panel panel-default">
                    <div class="panel-heading"><h3><h3>Ticket</h3></h3></div>
                    <div class="panel-body">
                        <form name="" method="POST" action="">
                            <?php foreach($ticket_types as $val) { ?>
                                <div class="ticket_type">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p>
                                                <b><?php echo $val->ticket_type_name; ?></b>
                                                <br />
                                                <?php echo $val->ticket_type_currency . ' ' . number_format($val->ticket_type_amount, 2); ?>
                                            </p>
                                        </div>
                                        <?php if($microsite->microsite_slug != 'true-life-2016') { ?>
                                        <div class="col-md-6">
                                            <?php 
                                                for($i=1;$i<=10;$i++)
                                                {
                                                    $no_of_tickets[$i] = $i;
                                                }
                                                
                                                echo form_dropdown('ticket_qty', $no_of_tickets, '1', 'class="form-control" id="ticket_qty"'); 
                                            ?>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <input type="submit" name="get_ticket" value="Get Ticket" data-ticket-id="<?php echo $val->ticket_type_id; ?>" class="get_ticket btn btn-danger btn-block" />
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </form>
                    </div>
                </div>
			<?php } ?>

    		<section id="sidebar">
                <?php  echo frontend_social_plugin_widget() ?>
        	</section>
        </div>
    </div>
</div>

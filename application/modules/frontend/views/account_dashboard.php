<h2><?php echo $page_heading;?></h2>

<?php if ($events): ?>
	<div class="row" id="events">
		<?php foreach ($events as $event): ?>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<div class="thumbnail">
					<a href="<?php echo site_url('account/event/' . $event->event_code); ?>">
						<img src="<?php echo site_url($event->event_photo); ?>" />
						<div class="caption text-center">
							<h4><?php echo $event->event_name; ?></h4>
						</div>
					</a>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>
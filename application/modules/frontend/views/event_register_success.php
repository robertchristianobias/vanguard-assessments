<style>
	<?php echo $microsite->microsite_custom_css ?>
</style>
<?php if($microsite->microsite_slug == 'true-life-2016') { ?>
<style>
    h1, h2, h3, h4, h5, h6 {
        font-family: "Open Sans";
    }
</style>
<?php } ?>
<div class="text-center">

	<div class="clearfix">
		<img src="<?php echo site_url('assets/images/check.jpg') ?>" class="col-xs-2 col-xs-offset-5" border="0" />
	</div>
	<br />
	<h1>Registration Completed</h1>
	<br />
	<h4>We've sent you a confirmation email.  See you at the event.</h4>

</div>
<br />
<div class="text-center">
	<a href="<?php echo site_url("event/" . $microsite->microsite_slug) ?>" class="btn btn-danger"><i class="fa fa-chevron-left"></i> Back to Registration</a>
</div>

<?php #pr($microsite); ?>
<style>
	<?php echo $microsite->microsite_custom_css ?>
</style>
<?php if($microsite->microsite_slug == 'true-life-2016') { ?>
<style>
    h1, h2, h3, h4, h5, h6 {
        font-family: "Open Sans";
    }
</style>
<?php } ?>
<div class="text-center">
	<h1>Registration</h1>

	<?php if ($microsite->microsite_header != ''): ?>
		<?php echo $microsite->microsite_header; ?>
	<?php endif; ?>
	<hr />
</div>

<?php if($microsite->microsite_slug != 'true-life-2016') { ?>
<form method="POST" name="paypal_form" action="" class="form-horizontal" id="form-register">
    <input type="hidden" name="event_id" value="<?php echo $microsite->event_id; ?>" />
<?php } else { ?>
    <form method="POST" name="" action="<?php echo site_url('event/register/'.$microsite->microsite_slug); ?>" class="form-horizontal" id="form-register">
<?php } ?>
	<div class="col-md-offset-1 col-md-10">
		<div class="row">
			<div class="col-md-8">
				<?php if($this->uri->segment('3') != 'true-life-2016') { // Make this dynamic ?>
					<div class="panel panel-default">
						<div class="panel-heading"><h3><?php echo lang('coupon_code'); ?></h3></div>
						<div class="panel-body">
							<div class="form-group">
								<div class="col-md-8">
									<?php echo form_input(array('id'=>'coupon_code', 'name'=>'coupon_code', 'value'=>set_value('coupon_code'), 'class'=>'form-control ', 'placeholder'=>lang('coupon_code')));?>
									<p>If you have a coupon code, please enter above and click on the Claim button.</p>
									<div id="error-coupon_code" class="text-left"></div>
								</div>
								<div class="col-md-3">
									<button name="submit_coupon_code" id="submit_coupon_code" class="btn btn-sm btn-danger btn-block">
										<span class="fa fa-send"></span> Claim
									</button>
								</div>
							</div>
						</div>
					</div>
				<?php } ?>
                
                <?php if($microsite->microsite_slug == 'legion-run-manila-2017') { // Make this dynamic ?>
                    <?php
                
                    $label = '';
                    if($this->session->userdata('ticket_qty') > 1)
                    {
                        $label = '<small><i>(This registrant will receive the payment receipt)</i>';
                    }
                ?>
                
                
                <?php for($i=1;$i<=$this->session->userdata('ticket_qty');$i++) { ?>
				<div class="panel panel-default">

					<div class="panel-heading">
                        <h3>
                            Ticket #<?php echo $i; ?> <?php echo ($i == 1) ? $label : ''; ?></small>
                        </h3>
                    </div>

					<ul class="list-group">

						<li class="list-group-item">

							<h3>Personal Information</h3>

							<?php if (in_array('ticket_fullname', $fields)): ?>
								<div class="form-group">                        
									<div class="col-md-12">
										<?php echo form_input(array('class'=>'form-control ticket_fullname['.$i.']', 'name'=>'ticket_fullname['.$i.']', 'value'=>set_value('ticket_fullname['.$i.']'), 'placeholder'=>lang('ticket_fullname')));?>
										<div class="error-ticket_fullname_<?php echo $i; ?>"></div>
									</div>
								</div>
							<?php endif; ?>
                            
                            <?php if (in_array('ticket_firstname', $fields) OR in_array('ticket_middlename', $fields) OR in_array('ticket_lastname', $fields)): ?>
                                <div class="form-group">
                                    <?php if (in_array('ticket_firstname', $fields)): ?>
                                        <div class="col-sm-4">
                                            <?php echo form_input(array('class'=>'form-control ticket_firstname_'.$i, 'name'=>'ticket[firstname]['.$i.']', 'value'=>set_value('ticket[firstname]['.$i.']'), 'placeholder'=>lang('ticket_firstname')));?>
                                            <div class="error-ticket_firstname_<?php echo $i; ?>"></div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (in_array('ticket_middlename', $fields)): ?>
                                        <div class="col-sm-4 col-no-padding-left">
                                            <?php echo form_input(array('class'=>'form-control ticket_middlename_'.$i, 'name'=>'ticket[middlename]['.$i.']', 'value'=>set_value('ticket[middlename]['.$i.']'), 'placeholder'=>lang('ticket_middlename')));?>
                                            <div class="error-ticket_middlename_<?php echo $i; ?>"></div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (in_array('ticket_lastname', $fields)): ?>
                                        <div class="col-sm-4 col-no-padding-left">
                                            <?php echo form_input(array('class'=>'form-control ticket_lastname_'.$i, 'name'=>'ticket[lastname]['.$i.']', 'value'=>set_value('ticket[lastname]['.$i.']'), 'placeholder'=>lang('ticket_lastname')));?>
                                            <div class="error-ticket_lastname_<?php echo $i; ?>"></div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>

							<?php if (in_array('ticket_middle_initial', $fields) OR in_array('ticket_nickname', $fields)): ?>
								<div class="form-group">

									<?php if (in_array('ticket_middle_initial', $fields)): ?>
										<div class="col-sm-4">
											<?php echo form_input(array('class'=>'form-control ticket_middle_initial_'.$i, 'name'=>'ticket[middle_initial]['.$i.']', 'value'=>set_value('ticket[middle_initial]['.$i.']'), 'placeholder'=>lang('ticket_middle_initial')));?>
											<div class="error-ticket_middle_initial_<?php echo $i; ?>"></div>
										</div>
									<?php endif; ?>

									<?php if (in_array('ticket_nickname', $fields)): ?>
										<div class="col-sm-4 col-no-padding-left">
											<?php echo form_input(array('class'=>'form-control ticket_nickname_'.$i, 'name'=>'ticket[nickname]['.$i.']', 'value'=>set_value('ticket[nickname]['.$i.']'), 'placeholder'=>lang('ticket_nickname')));?>
											<div class="error-ticket_nickname_<?php echo $i; ?>"></div>
										</div>
									<?php endif; ?>

								</div>
							<?php endif; ?>
                            
                            <?php if (in_array('ticket_email', $fields) OR in_array('ticket_mobile', $fields) OR in_array('ticket_phone', $fields)): ?>
                                <div class="form-group">

                                    <?php if (in_array('ticket_email', $fields)): ?>
                                        <div class="col-sm-4">
                                            <?php echo form_input(array('class'=>'form-control ticket_email_'.$i, 'name'=>'ticket[email]['.$i.']', 'value'=>set_value('ticket[email]['.$i.']'), 'placeholder'=>lang('ticket_email')));?>
                                            <div class="error-ticket_email_<?php echo $i; ?>"></div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (in_array('ticket_mobile', $fields)): ?>
                                        <div class="col-sm-4 col-no-padding-left">
                                            <?php echo form_input(array('name'=>'ticket[mobile]['.$i.']', 'value'=>set_value('ticket[mobile]['.$i.']'), 'class'=>'form-control ticket_mobile_'.$i, 'placeholder'=>lang('ticket_mobile')));?>
                                            <div class="error-ticket_mobile_<?php echo $i; ?>"></div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (in_array('ticket_phone', $fields)): ?>
                                        <div class="col-sm-4 col-no-padding-left">
                                            <?php echo form_input(array('class'=>'form-control ticket_phone_'.$i, 'name'=>'ticket[phone]['.$i.']', 'value'=>set_value('ticket[phone]['.$i.']'), 'placeholder'=>lang('ticket_phone')));?>
                                            <div class="error-ticket_phone_<?php echo $i; ?>"></div>
                                        </div>
                                    <?php endif; ?>

                                </div>
                            <?php endif; ?>
                            
                            <?php if (in_array('ticket_gender', $fields) OR in_array('ticket_mstatus', $fields) OR in_array('ticket_birthday', $fields)): ?>
                                <div class="form-group">

                                    <?php if (in_array('ticket_gender', $fields)): ?>
                                        <div class="col-sm-4">
                                            <div class="row">
                                                <?php $genders = create_dropdown('array', ',Male,Female,Unknown'); ?>
                                                <label class="col-sm-4 control-label" for="ticket_gender"><?php echo lang('ticket_gender')?>:</label>
                                                <div class="col-sm-8">
                                                    <?php echo form_dropdown('ticket[gender]['.$i.']', $genders, set_value('ticket[gender]['.$i.']', ''), 'class="form-control ticket_gender_'.$i.'" placeholder="' . lang('ticket_gender') .'"'); ?>
                                                </div>
                                                <div class="error-ticket_gender_<?php echo $i; ?>"></div>
                                            </div>
                                        </div>
                                    <?php endif; ?>


                                    <?php if (in_array('ticket_mstatus', $fields)): ?>
                                        <div class="col-sm-5 col-no-padding-left">
                                            <div class="row">
                                                <?php $mstatuses = create_dropdown('array', ',Single,Married,Divorced,Separated,Widowed,Other'); ?>
                                                <label class="col-sm-5 control-label" for="ticket_mstatus"><?php echo lang('ticket_mstatus')?>:</label>
                                                <div class="col-sm-7">
                                                    <?php echo form_dropdown('ticket[mstatus]['.$i.']', $mstatuses, set_value('ticket[mstatus]['.$i.']', ''), 'class="form-control input-block ticket_status_'.$i.'" placeholder="' . lang('ticket_mstatus') .'"'); ?>
                                                </div>
                                                <div class="error-ticket_mstatus_<?php echo $i; ?>"></div>
                                            </div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (in_array('ticket_birthday', $fields)): ?>
                                        <div class="col-sm-3 col-no-padding-left">
                                            <?php echo form_input(array('name'=>'ticket[birthday]['.$i.']', 'value'=>set_value('ticket[birthday]['.$i.']', ''), 'class'=>'ticket_birthday form-control ticket_birthday_'.$i, 'placeholder'=>lang('ticket_birthday')));?>
                                            <div class="error-ticket_birthday_<?php echo $i; ?>"></div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                            
                            <?php if (in_array('ticket_address', $fields)): ?>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <?php echo form_input(array('name'=>'ticket[address]['.$i.']', 'value'=>set_value('ticket_address'), 'class'=>'form-control ', 'placeholder'=>lang('ticket_address')));?>
                                        <div class="error-ticket_address_<?php echo $i; ?>"></div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            
                            <?php if (in_array('ticket_city', $fields) OR in_array('ticket_zipcode', $fields) OR in_array('ticket_country', $fields)): ?>
                                <div class="form-group">

                                    <?php if (in_array('ticket_city', $fields)): ?>
                                        <div class="col-sm-5">
                                            <?php echo form_input(array('name'=>'ticket[city]['.$i.']', 'value'=>set_value('ticket[city]['.$i.']'), 'class'=>'form-control ticket_city_'.$i, 'placeholder'=>lang('ticket_city')));?>
                                            <div class="error-ticket_city_<?php echo $i; ?>"></div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (in_array('ticket_zipcode', $fields)): ?>
                                        <div class="col-sm-2 col-no-padding-left">
                                            <?php echo form_input(array('name'=>'ticket[zipcode]['.$i.']', 'value'=>set_value('ticket[zipcode]['.$i.']'), 'class'=>'form-control ticket_zipcode_'.$i, 'placeholder'=>lang('ticket_zipcode')));?>
                                            <div class="error-ticket_zipcode_<?php echo $i; ?>"></div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (in_array('ticket_country', $fields)): ?>
                                        <div class="col-sm-5 col-no-padding-left">
                                            <?php echo form_dropdown('ticket[country]['.$i.']', $countries, set_value('ticket[country]['.$i.']', 'Philippines'), 'class="form-control ticket_country_'.$i.'" placeholder="' . lang('ticket_country') .'"'); ?>
                                            <div class="error-ticket_country_<?php echo $i; ?>"></div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
						</li>
						<li class="list-group-item">

							<h3>Work/School Information</h3>
                            
                            <?php if (in_array('ticket_company', $fields) OR in_array('ticket_work_address', $fields)): ?>
                                <div class="form-group">
                                    <?php if (in_array('ticket_company', $fields)): ?>
                                        <div class="col-sm-6">
                                            <?php echo form_input(array('name'=>'ticket[company]['.$i.']', 'value'=>set_value('ticket[company]['.$i.']'), 'class'=>'form-control ticket_company_'.$i, 'placeholder'=>lang('ticket_company')));?>
                                            <div class="error-ticket_company_<?php echo $i; ?>"></div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (in_array('ticket_work_address', $fields)): ?>
                                        <div class="col-sm-6 col-no-padding-left">
                                            <?php echo form_input(array('name'=>'ticket[work_address]['.$i.']', 'value'=>set_value('ticket[work_address]['.$i.']'), 'class'=>'form-control ticket_work_address_'.$i, 'placeholder'=>lang('ticket_work_address')));?>
                                            <div class="error-ticket_work_address_<?php echo $i; ?>"></div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>
                            
                            <?php if (in_array('ticket_occupation', $fields) OR in_array('ticket_work_phone', $fields)): ?>
                                <div class="form-group">
                                    <?php if (in_array('ticket_occupation', $fields)): ?>
                                        <div class="col-sm-6">
                                            <?php echo form_input(array('name'=>'ticket[occupation]['.$i.']', 'value'=>set_value('ticket[occupation]['.$i.']'), 'class'=>'form-control ticket_occupation_'.$i, 'placeholder'=>lang('ticket_occupation')));?>
                                            <div class="error-ticket_occupation_<?php echo $i; ?>"></div>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (in_array('ticket_work_phone', $fields)): ?>
                                        <div class="col-sm-6 col-no-padding-left">
                                            <?php echo form_input(array('name'=>'ticket[work_phone]['.$i.']', 'value'=>set_value('ticket[work_phone]['.$i.']'), 'class'=>'form-control ticket_work_phone_'.$i, 'placeholder'=>lang('ticket_work_phone')));?>
                                            <div class="error-ticket_work_phone_<?php echo $i; ?>"></div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            <?php endif; ?>

						</li>

						<li class="list-group-item">

							<?php if (in_array('ticket_is_pwd', $fields) OR in_array('ticket_medical_conditions', $fields) OR in_array('ticket_is_senior', $fields) OR in_array('ticket_emergency_contact', $fields)): ?>

								<h3>Medical/Emergency Information</h3>
								
								<div class="form-group">

									<?php if (in_array('ticket_medical_conditions', $fields)): ?>
										<div class="col-sm-12">
											<?php echo form_textarea(array('name'=>'ticket[medical_conditions]['.$i.']', 'rows'=>'4', 'value'=>set_value('ticket[medical_conditions]['.$i.']'), 'class'=>'form-control ticket_medical_conditions_'.$i, 'placeholder'=>'Medical Conditions')); ?>
											<div class="error-ticket_medical_conditions_<?php echo $i; ?>"></div>
										</div>
									<?php endif; ?>

									<?php if (in_array('ticket_is_senior', $fields)): ?>
										<div class="col-sm-4">
											<div class="checkbox">
												<label>
													<input class="ticket_is_senior_<?php echo $i; ?>" name="ticket[is_senior][<?php echo $i; ?>]" type="checkbox" value="1" <?php echo set_checkbox('ticket[is_senior]['.$i.']', 1, FALSE); ?> /> <?php echo lang('ticket_is_senior')?>
												</label>
											</div>
										</div>
										<div class="error-ticket_is_senior_<?php echo $i; ?>"></div>
									<?php endif; ?>

									<?php if (in_array('ticket_is_pwd', $fields)): ?>
										<div class="col-sm-4">
											<div class="checkbox">
												<label>
													<input class="ticket_is_pwd_<?php echo $i; ?>" name="ticket[is_pwd][<?php echo $i; ?>]" type="checkbox" value="1" <?php echo set_checkbox('ticket[is_pwd]['.$i.']', 1, FALSE); ?> /> <?php echo lang('ticket_is_pwd')?>
												</label>
											</div>
										</div>
										<div class="error-ticket_is_pwd_<?php echo $i; ?>"></div>
									<?php endif; ?>

								</div>

								<?php if (in_array('ticket_emergency_contact', $fields) OR in_array('ticket_emergency_relationship', $fields) OR in_array('ticket_emergency_number', $fields)): ?>
									<div class="form-group">
                                        
                                        <?php if (in_array('ticket_emergency_contact', $fields)): ?>
                                            <div class="col-sm-12"><strong>Contact person in case of emergency</strong></div>
                                            <div class="col-sm-4">
                                                <?php echo form_input(array('name'=>'ticket[emergency_contact]['.$i.']', 'value'=>set_value('ticket[emergency_contact]['.$i.']'), 'class'=>'form-control ticket_emergency_contact_'.$i, 'placeholder'=>lang('ticket_emergency_contact')));?>
                                                <div class="error-ticket_emergency_contact_<?php echo $i; ?>"></div>
                                            </div>
                                        <?php endif; ?>
                                        
                                        <?php if (in_array('ticket_emergency_relationship', $fields)): ?>
                                            <div class="col-sm-4 col-no-padding-left">
                                                <?php echo form_input(array('name'=>'ticket[emergency_relationship]['.$i.']', 'value'=>set_value('ticket[emergency_relationship]['.$i.']'), 'class'=>'form-control ticket_emergency_relationship_'.$i, 'placeholder'=>lang('ticket_emergency_relationship')));?>
                                                <div class="error-ticket_emergency_relationship_<?php echo $i; ?>"></div>
                                            </div>
                                        <?php endif; ?>
                                        
                                        <?php if (in_array('ticket_emergency_number', $fields)): ?>
                                            <div class="col-sm-4 col-no-padding-left">
                                                <?php echo form_input(array('name'=>'ticket[emergency_number]['.$i.']', 'value'=>set_value('ticket[emergency_number]['.$i.']'), 'class'=>'form-control ticket_emergency_number_'.$i, 'placeholder'=>lang('ticket_emergency_number')));?>
                                                <div class="error-ticket_emergency_number_<?php echo $i; ?>"></div>
                                            </div>
                                        <?php endif; ?>

									</div>
								<?php endif; ?>

							<?php endif; ?>

						</li>

						<li class="list-group-item">

							<?php if (in_array('ticket_custom_text_1', $fields) OR in_array('ticket_custom_select_1', $fields)): ?>

								<h3>Other Information</h3>
								<div class="form-group">

									<?php if (in_array('ticket_custom_text_1', $fields)): ?>
										<div class="col-sm-6">
											<?php echo form_input(array('name'=>'ticket[custom_text_1]['.$i.']', 'value'=>set_value('ticket[custom_text_1]['.$i.']'), 'class'=>'form-control ticket_custom_text_1_'.$i, 'placeholder'=>$ticket_custom_text_1));?>
											<?php if ($this->uri->segment('3') == 'legion-run-manila-2016'): // TODO: ADD THIS TO CMS ?>
												<p>If you are the first one to register in your group, please create your group code then share it with your group members.</p>
											<?php endif;  ?>
											<div class="error-ticket_custom_text_1_<?php echo $i; ?>"></div>
                                            <?php if($microsite->microsite_slug != 'true-life-2016') { ?>
                                            
                                                <?php #if($total_tickets > 100) { // Make this dynamic ?>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input class="ticket_bag_check" data-bag-check="100" name="ticket[bag_check][<?php echo $i; ?>]" type="checkbox" value="Yes" <?php echo set_checkbox('ticket[bag_check]['.$i.']', 1, FALSE); ?> /> Include Bag Check (additional Php100.00)
                                                        </label>
                                                    </div>
                                                <?php #} ?>
                                            
                                            <?php } ?>
										</div>
                                        
									<?php endif; ?>

									<?php if (in_array('ticket_custom_select_1', $fields)): ?>
										<div class="col-sm-6">
											<?php $options = create_dropdown('array', $ticket_custom_select_options_1); // TODO: cms for this dropdown options ?>
											<div class="row">
												<label class="col-sm-4 control-label" for="ticket_custom_select_1"><?php echo $ticket_custom_select_1; ?>:</label>
												<div class="col-sm-8">
													<?php echo form_dropdown('ticket[custom_select_1]['.$i.']', $options, set_value('ticket[custom_select_1]['.$i.']', ''), 'class="form-control input-block custom_select_1_'.$i.'" placeholder="' . $ticket_custom_select_1 .'"'); ?>
													<div class="error-ticket_custom_select_1_<?php echo $i; ?>"></div>
												</div>
											</div>
										</div>
									<?php endif; ?>

								</div>

								<?php if (in_array('ticket_custom_text_2', $fields) OR in_array('ticket_custom_select_2', $fields)): ?>

									<div class="form-group">

										<?php if (in_array('ticket_custom_text_2', $fields)): ?>
											<div class="col-sm-6">
												<?php echo form_input(array('id'=>'ticket_custom_text_2', 'name'=>'ticket_custom_text_2', 'value'=>set_value('ticket_custom_text_2'), 'class'=>'form-control ', 'placeholder'=>$ticket_custom_text_2));?>
												<div id="error-ticket_custom_text_2"><?php echo form_error('ticket_custom_text_2') ?></div>
											</div>
										<?php endif; ?>

										<?php if (in_array('ticket_custom_select_2', $fields)): ?>
											<div class="col-sm-6">
												<?php $options = create_dropdown('array', $ticket_custom_select_options_2); // TODO: cms for this dropdown options ?>
												<div class="row">
													<label class="col-sm-4 control-label" for="ticket_custom_select_2"><?php echo $ticket_custom_select_2; ?>:</label>
													<div class="col-sm-8">
														<?php echo form_dropdown('ticket_custom_select_2', $options, set_value('ticket_custom_select_2'), 'id="ticket_custom_select_2" class="form-control  input-block" placeholder="' . $ticket_custom_select_2 .'"'); ?>
														<div id="error-ticket_custom_select_2"><?php echo form_error('ticket_custom_select_2') ?></div>
													</div>
												</div>
											</div>
										<?php endif; ?>

									</div>
								<?php endif; ?>

								<?php if (in_array('ticket_custom_text_3', $fields) OR in_array('ticket_custom_select_3', $fields)): ?>
									<div class="form-group">

										<?php if (in_array('ticket_custom_text_3', $fields)): ?>
											<div class="col-sm-6">
												<?php echo form_input(array('id'=>'ticket_custom_text_3', 'name'=>'ticket_custom_text_3', 'value'=>set_value('ticket_custom_text_3'), 'class'=>'form-control ', 'placeholder'=>$ticket_custom_text_3));?>
												<div id="error-ticket_custom_text_3"><?php echo form_error('ticket_custom_text_3') ?></div>
											</div>
										<?php endif; ?>

										<?php if (in_array('ticket_custom_select_3', $fields)): ?>
											<div class="col-sm-6">
												<?php $options = create_dropdown('array', $ticket_custom_select_options_3); // TODO: cms for this dropdown options ?>
												<div class="row">
													<label class="col-sm-4 control-label" for="ticket_custom_select_3"><?php echo $ticket_custom_select_3; ?>:</label>
													<div class="col-sm-8">
														<?php echo form_dropdown('ticket_custom_select_3', $options, set_value('ticket_custom_select_3'), 'id="ticket_custom_select_3" class="form-control  input-block" placeholder="' . $ticket_custom_select_3 .'"'); ?>
														<div id="error-ticket_custom_select_3"><?php echo form_error('ticket_custom_select_3') ?></div>
													</div>
												</div>
											</div>
										<?php endif; ?>

									</div>
								<?php endif; ?>

								<?php if (in_array('ticket_custom_text_4', $fields) OR in_array('ticket_custom_select_4', $fields)): ?>
								<div class="form-group">

									<?php if (in_array('ticket_custom_text_4', $fields)): ?>
										<div class="col-sm-6">
											<?php echo form_input(array('id'=>'ticket_custom_text_4', 'name'=>'ticket_custom_text_4', 'value'=>set_value('ticket_custom_text_4'), 'class'=>'form-control ', 'placeholder'=>$ticket_custom_text_4));?>

											<div id="error-ticket_custom_text_4"><?php echo form_error('ticket_custom_text_4') ?></div>
										</div>
									<?php endif; ?>

									<?php if (in_array('ticket_custom_select_4', $fields)): ?>
										<div class="col-sm-6">
											<?php $options = create_dropdown('array', $ticket_custom_select_options_4); // TODO: cms for this dropdown options ?>
											<div class="row">
												<label class="col-sm-4 control-label" for="ticket_custom_select_4"><?php echo $ticket_custom_select_4; ?>:</label>
												<div class="col-sm-8">
													<?php echo form_dropdown('ticket_custom_select_4', $options, set_value('ticket_custom_select_4'), 'id="ticket_custom_select_4" class="form-control  input-block" placeholder="' . $ticket_custom_select_4 .'"'); ?>
													<div id="error-ticket_custom_select_4"><?php echo form_error('ticket_custom_select_4') ?></div>
												</div>
											</div>
										</div>
									<?php endif; ?>

								</div>
								<?php endif; ?>

								<?php if (in_array('ticket_custom_text_5', $fields) OR in_array('ticket_custom_select_5', $fields)): ?>
									<div class="form-group">

										<?php if (in_array('ticket_custom_text_5', $fields)): ?>
											<div class="col-sm-6">
												<?php echo form_input(array('id'=>'ticket_custom_text_5', 'name'=>'ticket_custom_text_5', 'value'=>set_value('ticket_custom_text_5'), 'class'=>'form-control ', 'placeholder'=>$ticket_custom_text_5));?>
												<div id="error-ticket_custom_text_5"><?php echo form_error('ticket_custom_text_5') ?></div>
											</div>
										<?php endif; ?>

										<?php if (in_array('ticket_custom_select_5', $fields)): ?>
											<div class="col-sm-6">
												<?php $options = create_dropdown('array', $ticket_custom_select_options_5); // TODO: cms for this dropdown options ?>
												<div class="row">
													<label class="col-sm-4 control-label" for="ticket_custom_select_5"><?php echo $ticket_custom_select_5; ?>:</label>
													<div class="col-sm-8">
														<?php echo form_dropdown('ticket_custom_select_5', $options, set_value('ticket_custom_select_5'), 'id="ticket_custom_select_5" class="form-control  input-block" placeholder="' . $ticket_custom_select_5 .'"'); ?>
														<div id="error-ticket_custom_select_5"><?php echo form_error('ticket_custom_select_5') ?></div>
													</div>
												</div>
											</div>
										<?php endif; ?>

									</div>
								<?php endif; ?>

								<?php if (in_array('ticket_custom_text_6', $fields) OR in_array('ticket_custom_select_6', $fields)): ?>
									<div class="form-group">

										<?php if (in_array('ticket_custom_text_6', $fields)): ?>
											<div class="col-sm-6">
												<?php echo form_input(array('id'=>'ticket_custom_text_6', 'name'=>'ticket_custom_text_6', 'value'=>set_value('ticket_custom_text_6'), 'class'=>'form-control ', 'placeholder'=>$ticket_custom_text_6));?>
												<div id="error-ticket_custom_text_6"><?php echo form_error('ticket_custom_text_6') ?></div>
											</div>
										<?php endif; ?>

										<?php if (in_array('ticket_custom_select_6', $fields)): ?>
											<div class="col-sm-6">
												<?php $options = create_dropdown('array', $ticket_custom_select_options_6); // TODO: cms for this dropdown options ?>
												<div class="row">
													<label class="col-sm-4 control-label" for="ticket_custom_select_6"><?php echo $ticket_custom_select_6; ?>:</label>
													<div class="col-sm-8">
														<?php echo form_dropdown('ticket_custom_select_6', $options, set_value('ticket_custom_select_6'), 'id="ticket_custom_select_6" class="form-control  input-block" placeholder="' . $ticket_custom_select_6 .'"'); ?>
														<div id="error-ticket_custom_select_6"><?php echo form_error('ticket_custom_select_6') ?></div>
													</div>
												</div>
											</div>
										<?php endif; ?>

									</div>
								<?php endif; ?>

							<?php endif; ?>

						</li>

						<li class="list-group-item">

							<h3>Terms and Conditions</h3>

							<div class="form-group">
								<?php if (in_array('ticket_terms', $fields)): ?>
									<div class="col-sm-12">
										<div class="checkbox">
											<label>
												<input class="ticket_terms_<?php echo $i; ?>" name="ticket[terms][<?php echo $i; ?>]" type="checkbox" value="1" <?php echo set_checkbox('ticket[terms]['.$i.']', 1, FALSE); ?> /> <strong>I agree and accept the <a href="<?php echo site_url("event/terms/" . $microsite->microsite_slug) ?>" data-toggle="modal" data-target="#modal">Terms and Conditions</a>. </strong>
											</label>
										</div>
										<div class="error-ticket_terms_<?php echo $i; ?>"></div><br />
									</div>
								<?php endif; ?>

								<?php if (in_array('ticket_newsletter', $fields)): ?>
									<div class="col-sm-12">
										<div class="checkbox">
											<label>
												<input class="ticket_newsletter_<?php echo $i; ?>" name="ticket[newsletter][<?php echo $i; ?>]" type="checkbox" value="1" <?php echo set_checkbox('ticket[newsletter]['.$i.']', 1, FALSE); ?> /> <?php echo lang('ticket_newsletter')?>
											</label>
										</div>
									</div>
									<div class="error-ticket_newsletter_<?php echo $i; ?>"></div>
								<?php endif; ?>
							</div>
						</li>
					</ul>
				</div>
                <?php } ?>
                <?php } else { // true life ?> 
                    <div class="panel panel-default">

					<div class="panel-heading"><h3>Ticket</h3></div>

					<ul class="list-group">

						<li class="list-group-item">

							<h3>Personal Information</h3>

							<?php if (in_array('ticket_fullname', $fields)): ?>
								<div class="form-group">                        
									<div class="col-md-12">
										<?php echo form_input(array('id'=>'ticket_fullname', 'name'=>'ticket_fullname', 'value'=>set_value('ticket_fullname'), 'class'=>'form-control ', 'placeholder'=>lang('ticket_fullname')));?>
										<div id="error-ticket_fullname"><?php echo (form_error('ticket_fullname') ? form_error('ticket_fullname') . '.' : '') ?></div>
									</div>
								</div>
							<?php endif; ?>

							<div class="form-group">

								<?php if (in_array('ticket_firstname', $fields)): ?>
									<div class="col-sm-4">
										<?php echo form_input(array('id'=>'ticket_firstname', 'name'=>'ticket_firstname', 'value'=>set_value('ticket_firstname'), 'class'=>'form-control ', 'placeholder'=>lang('ticket_firstname')));?>
										<div id="error-ticket_firstname"><?php echo (form_error('ticket_firstname') ? form_error('ticket_firstname') . '.': '') ?></div>
									</div>
								<?php endif; ?>

								<?php if (in_array('ticket_middlename', $fields)): ?>
									<div class="col-sm-4 col-no-padding-left">
										<?php echo form_input(array('id'=>'ticket_middlename', 'name'=>'ticket_middlename', 'value'=>set_value('ticket_middlename'), 'class'=>'form-control ', 'placeholder'=>lang('ticket_middlename')));?>
										<div id="error-ticket_middlename"><?php echo form_error('ticket_middlename') ?></div>
									</div>
								<?php endif; ?>

								<?php if (in_array('ticket_lastname', $fields)): ?>
									<div class="col-sm-4 col-no-padding-left">
										<?php echo form_input(array('id'=>'ticket_lastname', 'name'=>'ticket_lastname', 'value'=>set_value('ticket_lastname'), 'class'=>'form-control ', 'placeholder'=>lang('ticket_lastname')));?>
										<div id="error-ticket_lastname"><?php echo (form_error('ticket_lastname') ? form_error('ticket_lastname') . '.': '') ?></div>
									</div>
								<?php endif; ?>

							</div>

							<?php if (in_array('ticket_middle_initial', $fields) OR in_array('ticket_nickname', $fields)): ?>
								<div class="form-group">

									<?php if (in_array('ticket_middle_initial', $fields)): ?>
										<div class="col-sm-4">
											<?php echo form_input(array('id'=>'ticket_middle_initial', 'name'=>'ticket_middle_initial', 'value'=>set_value('ticket_middle_initial'), 'class'=>'form-control ', 'placeholder'=>lang('ticket_middle_initial')));?>
											<div id="error-ticket_middle_initial"><?php echo form_error('ticket_middle_initial') ?></div>
										</div>
									<?php endif; ?>

									<?php if (in_array('ticket_nickname', $fields)): ?>
										<div class="col-sm-4 col-no-padding-left">
											<?php echo form_input(array('id'=>'ticket_nickname', 'name'=>'ticket_nickname', 'value'=>set_value('ticket_nickname'), 'class'=>'form-control ', 'placeholder'=>lang('ticket_nickname')));?>
											<div id="error-ticket_nickname"><?php echo form_error('ticket_nickname') ?></div>
										</div>
									<?php endif; ?>

								</div>
							<?php endif; ?>

							<div class="form-group">

								<?php if (in_array('ticket_email', $fields)): ?>
									<div class="col-sm-4">
										<?php echo form_input(array('id'=>'ticket_email', 'name'=>'ticket_email', 'value'=>set_value('ticket_email'), 'class'=>'form-control ', 'placeholder'=>lang('ticket_email')));?>
										<div id="error-ticket_email"><?php echo (form_error('ticket_email') ? form_error('ticket_email') .'.' : '') ?></div>
									</div>
								<?php endif; ?>

								<?php if (in_array('ticket_mobile', $fields)): ?>
									<div class="col-sm-4 col-no-padding-left">
										<?php echo form_input(array('id'=>'ticket_mobile', 'name'=>'ticket_mobile', 'value'=>set_value('ticket_mobile'), 'class'=>'form-control ', 'placeholder'=>lang('ticket_mobile')));?>
										<div id="error-ticket_mobile"><?php echo (form_error('ticket_mobile') ? form_error('ticket_mobile') .'.' : '') ?></div>
									</div>
								<?php endif; ?>

								<?php if (in_array('ticket_phone', $fields)): ?>
									<div class="col-sm-4 col-no-padding-left">
										<?php echo form_input(array('id'=>'ticket_phone', 'name'=>'ticket_phone', 'value'=>set_value('ticket_phone'), 'class'=>'form-control ', 'placeholder'=>lang('ticket_phone')));?>
										<div id="error-ticket_phone"><?php echo form_error('ticket_phone') ?></div>
									</div>
								<?php endif; ?>

							</div>

							<div class="form-group">

								<?php if (in_array('ticket_gender', $fields)): ?>
									<div class="col-sm-4">
										<div class="row">
											<?php $genders = create_dropdown('array', ',Male,Female,Unknown'); ?>
											<label class="col-sm-4 control-label" for="ticket_gender"><?php echo lang('ticket_gender')?>:</label>
											<div class="col-sm-8">
												<?php echo form_dropdown('ticket_gender', $genders, set_value('ticket_gender'), 'id="ticket_gender" class="form-control " placeholder="' . lang('ticket_gender') .'"'); ?>
											</div>
											<div id="error-ticket_gender"><?php echo form_error('ticket_gender') ?></div>
										</div>
									</div>
								<?php endif; ?>


								<?php if (in_array('ticket_mstatus', $fields)): ?>
									<div class="col-sm-5 col-no-padding-left">
										<div class="row">
											<?php $mstatuses = create_dropdown('array', ',Single,Married,Divorced,Separated,Widowed,Other'); ?>
											<label class="col-sm-5 control-label" for="ticket_mstatus"><?php echo lang('ticket_mstatus')?>:</label>
											<div class="col-sm-7">
												<?php echo form_dropdown('ticket_mstatus', $mstatuses, set_value('ticket_mstatus'), 'id="ticket_mstatus" class="form-control  input-block" placeholder="' . lang('ticket_mstatus') .'"'); ?>
											</div>
											<div id="error-ticket_mstatus"><?php echo form_error('ticket_mstatus') ?></div>
										</div>
									</div>
								<?php endif; ?>

								<?php if (in_array('ticket_birthday', $fields)): ?>
									<div class="col-sm-3 col-no-padding-left">
										<?php echo form_input(array('id'=>'ticket_birthday', 'name'=>'ticket_birthday', 'value'=>set_value('ticket_birthday'), 'class'=>'form-control ', 'placeholder'=>lang('ticket_birthday')));?>
										<div id="error-ticket_birthday"><?php echo form_error('ticket_birthday') ?></div>
									</div>
								<?php endif; ?>
							</div>

							<div class="form-group">

								<?php if (in_array('ticket_address', $fields)): ?>
									<div class="col-sm-12">
										<?php echo form_input(array('id'=>'ticket_address', 'name'=>'ticket_address', 'value'=>set_value('ticket_address'), 'class'=>'form-control ', 'placeholder'=>lang('ticket_address')));?>
										<div id="error-ticket_address"><?php echo form_error('ticket_address') ?></div>
									</div>
								<?php endif; ?>
							</div>

							<div class="form-group">

								<?php if (in_array('ticket_city', $fields)): ?>
									<div class="col-sm-5">
										<?php echo form_input(array('id'=>'ticket_city', 'name'=>'ticket_city', 'value'=>set_value('ticket_city'), 'class'=>'form-control ', 'placeholder'=>lang('ticket_city')));?>
										<div id="error-ticket_city"><?php echo form_error('ticket_city') ?></div>
									</div>
								<?php endif; ?>

								<?php if (in_array('ticket_zipcode', $fields)): ?>
									<div class="col-sm-2 col-no-padding-left">
										<?php echo form_input(array('id'=>'ticket_zipcode', 'name'=>'ticket_zipcode', 'value'=>set_value('ticket_zipcode'), 'class'=>'form-control ', 'placeholder'=>lang('ticket_zipcode')));?>
										<div id="error-ticket_zipcode"><?php echo form_error('ticket_zipcode') ?></div>
									</div>
								<?php endif; ?>

								<?php if (in_array('ticket_country', $fields)): ?>
									<div class="col-sm-5 col-no-padding-left">
										<?php echo form_dropdown('ticket_country', $countries, set_value('ticket_country', 'Philippines'), 'id="ticket_country" class="form-control " placeholder="' . lang('ticket_country') .'"'); ?>
										<div id="error-ticket_country"><?php echo form_error('ticket_country') ?></div>
									</div>
								<?php endif; ?>

							</div>

						</li>

						<li class="list-group-item">

							<h3>Work/School Information</h3>

							<div class="form-group">

								<?php if (in_array('ticket_company', $fields)): ?>
									<div class="col-sm-6">
										<?php echo form_input(array('id'=>'ticket_company', 'name'=>'ticket_company', 'value'=>set_value('ticket_company'), 'class'=>'form-control ', 'placeholder'=>lang('ticket_company')));?>
										<div id="error-ticket_company"><?php echo form_error('ticket_company') ?></div>
									</div>
								<?php endif; ?>

								<?php if (in_array('ticket_work_address', $fields)): ?>
									<div class="col-sm-6 col-no-padding-left">
										<?php echo form_input(array('id'=>'ticket_work_address', 'name'=>'ticket_work_address', 'value'=>set_value('ticket_work_address'), 'class'=>'form-control ', 'placeholder'=>lang('ticket_work_address')));?>
										<div id="error-ticket_work_address"><?php echo form_error('ticket_work_address') ?></div>
									</div>
								<?php endif; ?>

							</div>

							<div class="form-group">

								<?php if (in_array('ticket_occupation', $fields)): ?>
									<div class="col-sm-6">
										<?php echo form_input(array('id'=>'ticket_occupation', 'name'=>'ticket_occupation', 'value'=>set_value('ticket_occupation'), 'class'=>'form-control ', 'placeholder'=>lang('ticket_occupation')));?>
										<div id="error-ticket_occupation"><?php echo form_error('ticket_occupation') ?></div>
									</div>
								<?php endif; ?>

								<?php if (in_array('ticket_work_phone', $fields)): ?>
									<div class="col-sm-6 col-no-padding-left">
										<?php echo form_input(array('id'=>'ticket_work_phone', 'name'=>'ticket_work_phone', 'value'=>set_value('ticket_work_phone'), 'class'=>'form-control ', 'placeholder'=>lang('ticket_work_phone')));?>
										<div id="error-ticket_work_phone"><?php echo form_error('ticket_work_phone') ?></div>
									</div>
								<?php endif; ?>

							</div>

						</li>

						<li class="list-group-item">

							<?php if (in_array('ticket_is_pwd', $fields) OR in_array('ticket_medical_conditions', $fields) OR in_array('ticket_is_senior', $fields) OR in_array('ticket_emergency_contact', $fields)): ?>

								<h3>Medical/Emergency Information</h3>
								
								<div class="form-group">

									<?php if (in_array('ticket_medical_conditions', $fields)): ?>
										<div class="col-sm-12">
											<?php echo form_textarea(array('id'=>'ticket_medical_conditions', 'name'=>'ticket_medical_conditions', 'rows'=>'4', 'value'=>set_value('ticket_medical_conditions'), 'class'=>'form-control', 'placeholder'=>'Medical Conditions')); ?>
											<div id="error-ticket_medical_conditions"><?php echo form_error('ticket_medical_conditions') ?></div>
										</div>
									<?php endif; ?>

									<?php if (in_array('ticket_is_senior', $fields)): ?>
										<div class="col-sm-4">
											<div class="checkbox">
												<label>
													<input id="ticket_is_senior" name="ticket_is_senior" type="checkbox" value="1" <?php echo set_checkbox('ticket_is_senior', 1, FALSE); ?> /> <?php echo lang('ticket_is_senior')?>
												</label>
											</div>
										</div>
										<div id="error-ticket_is_senior"><?php echo form_error('ticket_is_senior') ?></div>
									<?php endif; ?>

									<?php if (in_array('ticket_is_pwd', $fields)): ?>
										<div class="col-sm-4">
											<div class="checkbox">
												<label>
													<input id="ticket_is_pwd" name="ticket_is_pwd" type="checkbox" value="1" <?php echo set_checkbox('ticket_is_pwd', 1, FALSE); ?> /> <?php echo lang('ticket_is_pwd')?>
												</label>
											</div>
										</div>
										<div id="error-ticket_is_pwd"><?php echo form_error('ticket_is_pwd') ?></div>
									<?php endif; ?>

								</div>

								<?php if (in_array('ticket_emergency_contact', $fields)): ?>
									<div class="form-group">

										<div class="col-sm-12"><strong>Contact person in case of emergency</strong></div>
										<div class="col-sm-4">
											<?php echo form_input(array('id'=>'ticket_emergency_contact', 'name'=>'ticket_emergency_contact', 'value'=>set_value('ticket_emergency_contact'), 'class'=>'form-control ', 'placeholder'=>lang('ticket_emergency_contact')));?>
											<div id="error-ticket_emergency_contact"><?php echo form_error('ticket_emergency_contact') ?></div>
										</div>
										<div class="col-sm-4 col-no-padding-left">
											<?php echo form_input(array('id'=>'ticket_emergency_relationship', 'name'=>'ticket_emergency_relationship', 'value'=>set_value('ticket_emergency_relationship'), 'class'=>'form-control ', 'placeholder'=>lang('ticket_emergency_relationship')));?>
											<div id="error-ticket_emergency_relationship"><?php echo form_error('ticket_emergency_relationship') ?></div>
										</div>
										<div class="col-sm-4 col-no-padding-left">
											<?php echo form_input(array('id'=>'ticket_emergency_number', 'name'=>'ticket_emergency_number', 'value'=>set_value('ticket_emergency_number'), 'class'=>'form-control ', 'placeholder'=>lang('ticket_emergency_number')));?>
											<div id="error-ticket_emergency_number"><?php echo form_error('ticket_emergency_number') ?></div>
										</div>

									</div>
								<?php endif; ?>

							<?php endif; ?>

						</li>

						<li class="list-group-item">

							<?php if (in_array('ticket_custom_text_1', $fields) OR in_array('ticket_custom_select_1', $fields)): ?>

								<h3>Other Information</h3>
								<div class="form-group">

									<?php if (in_array('ticket_custom_text_1', $fields)): ?>
										<div class="col-sm-6">
											<?php echo form_input(array('id'=>'ticket_custom_text_1', 'name'=>'ticket_custom_text_1', 'value'=>set_value('ticket_custom_text_1'), 'class'=>'form-control ', 'placeholder'=>$ticket_custom_text_1));?>
											<?php if ($this->uri->segment('3') == 'legion-run-manila-2016'): // TODO: ADD THIS TO CMS ?>
												<p>If you are the first one to register in your group, please create your group code then share it with your group members.</p>
											<?php endif;  ?>
											<div id="error-ticket_custom_text_1"><?php echo form_error('ticket_custom_text_1') ?></div>
										</div>
									<?php endif; ?>

									<?php if (in_array('ticket_custom_select_1', $fields)): ?>
										<div class="col-sm-6">
											<?php $options = create_dropdown('array', $ticket_custom_select_options_1); // TODO: cms for this dropdown options ?>
											<div class="row">
												<label class="col-sm-4 control-label" for="ticket_custom_select_1"><?php echo $ticket_custom_select_1; ?>:</label>
												<div class="col-sm-8">
													<?php echo form_dropdown('ticket_custom_select_1', $options, set_value('ticket_custom_select_1'), 'id="ticket_custom_select_1" class="form-control  input-block" placeholder="' . $ticket_custom_select_1 .'"'); ?>
													<div id="error-ticket_custom_select_1"><?php echo form_error('ticket_custom_select_1') ?></div>
												</div>
											</div>
										</div>
									<?php endif; ?>

								</div>

								<?php if (in_array('ticket_custom_text_2', $fields) OR in_array('ticket_custom_select_2', $fields)): ?>

									<div class="form-group">

										<?php if (in_array('ticket_custom_text_2', $fields)): ?>
											<div class="col-sm-6">
												<?php echo form_input(array('id'=>'ticket_custom_text_2', 'name'=>'ticket_custom_text_2', 'value'=>set_value('ticket_custom_text_2'), 'class'=>'form-control ', 'placeholder'=>$ticket_custom_text_2));?>
												<div id="error-ticket_custom_text_2"><?php echo form_error('ticket_custom_text_2') ?></div>
											</div>
										<?php endif; ?>

										<?php if (in_array('ticket_custom_select_2', $fields)): ?>
											<div class="col-sm-6">
												<?php $options = create_dropdown('array', $ticket_custom_select_options_2); // TODO: cms for this dropdown options ?>
												<div class="row">
													<label class="col-sm-4 control-label" for="ticket_custom_select_2"><?php echo $ticket_custom_select_2; ?>:</label>
													<div class="col-sm-8">
														<?php echo form_dropdown('ticket_custom_select_2', $options, set_value('ticket_custom_select_2'), 'id="ticket_custom_select_2" class="form-control  input-block" placeholder="' . $ticket_custom_select_2 .'"'); ?>
														<div id="error-ticket_custom_select_2"><?php echo form_error('ticket_custom_select_2') ?></div>
													</div>
												</div>
											</div>
										<?php endif; ?>

									</div>
								<?php endif; ?>

								<?php if (in_array('ticket_custom_text_3', $fields) OR in_array('ticket_custom_select_3', $fields)): ?>
									<div class="form-group">

										<?php if (in_array('ticket_custom_text_3', $fields)): ?>
											<div class="col-sm-6">
												<?php echo form_input(array('id'=>'ticket_custom_text_3', 'name'=>'ticket_custom_text_3', 'value'=>set_value('ticket_custom_text_3'), 'class'=>'form-control ', 'placeholder'=>$ticket_custom_text_3));?>
												<div id="error-ticket_custom_text_3"><?php echo form_error('ticket_custom_text_3') ?></div>
											</div>
										<?php endif; ?>

										<?php if (in_array('ticket_custom_select_3', $fields)): ?>
											<div class="col-sm-6">
												<?php $options = create_dropdown('array', $ticket_custom_select_options_3); // TODO: cms for this dropdown options ?>
												<div class="row">
													<label class="col-sm-4 control-label" for="ticket_custom_select_3"><?php echo $ticket_custom_select_3; ?>:</label>
													<div class="col-sm-8">
														<?php echo form_dropdown('ticket_custom_select_3', $options, set_value('ticket_custom_select_3'), 'id="ticket_custom_select_3" class="form-control  input-block" placeholder="' . $ticket_custom_select_3 .'"'); ?>
														<div id="error-ticket_custom_select_3"><?php echo form_error('ticket_custom_select_3') ?></div>
													</div>
												</div>
											</div>
										<?php endif; ?>

									</div>
								<?php endif; ?>

								<?php if (in_array('ticket_custom_text_4', $fields) OR in_array('ticket_custom_select_4', $fields)): ?>
								<div class="form-group">

									<?php if (in_array('ticket_custom_text_4', $fields)): ?>
										<div class="col-sm-6">
											<?php echo form_input(array('id'=>'ticket_custom_text_4', 'name'=>'ticket_custom_text_4', 'value'=>set_value('ticket_custom_text_4'), 'class'=>'form-control ', 'placeholder'=>$ticket_custom_text_4));?>

											<div id="error-ticket_custom_text_4"><?php echo form_error('ticket_custom_text_4') ?></div>
										</div>
									<?php endif; ?>

									<?php if (in_array('ticket_custom_select_4', $fields)): ?>
										<div class="col-sm-6">
											<?php $options = create_dropdown('array', $ticket_custom_select_options_4); // TODO: cms for this dropdown options ?>
											<div class="row">
												<label class="col-sm-4 control-label" for="ticket_custom_select_4"><?php echo $ticket_custom_select_4; ?>:</label>
												<div class="col-sm-8">
													<?php echo form_dropdown('ticket_custom_select_4', $options, set_value('ticket_custom_select_4'), 'id="ticket_custom_select_4" class="form-control  input-block" placeholder="' . $ticket_custom_select_4 .'"'); ?>
													<div id="error-ticket_custom_select_4"><?php echo form_error('ticket_custom_select_4') ?></div>
												</div>
											</div>
										</div>
									<?php endif; ?>

								</div>
								<?php endif; ?>

								<?php if (in_array('ticket_custom_text_5', $fields) OR in_array('ticket_custom_select_5', $fields)): ?>
									<div class="form-group">

										<?php if (in_array('ticket_custom_text_5', $fields)): ?>
											<div class="col-sm-6">
												<?php echo form_input(array('id'=>'ticket_custom_text_5', 'name'=>'ticket_custom_text_5', 'value'=>set_value('ticket_custom_text_5'), 'class'=>'form-control ', 'placeholder'=>$ticket_custom_text_5));?>
												<div id="error-ticket_custom_text_5"><?php echo form_error('ticket_custom_text_5') ?></div>
											</div>
										<?php endif; ?>

										<?php if (in_array('ticket_custom_select_5', $fields)): ?>
											<div class="col-sm-6">
												<?php $options = create_dropdown('array', $ticket_custom_select_options_5); // TODO: cms for this dropdown options ?>
												<div class="row">
													<label class="col-sm-4 control-label" for="ticket_custom_select_5"><?php echo $ticket_custom_select_5; ?>:</label>
													<div class="col-sm-8">
														<?php echo form_dropdown('ticket_custom_select_5', $options, set_value('ticket_custom_select_5'), 'id="ticket_custom_select_5" class="form-control  input-block" placeholder="' . $ticket_custom_select_5 .'"'); ?>
														<div id="error-ticket_custom_select_5"><?php echo form_error('ticket_custom_select_5') ?></div>
													</div>
												</div>
											</div>
										<?php endif; ?>

									</div>
								<?php endif; ?>

								<?php if (in_array('ticket_custom_text_6', $fields) OR in_array('ticket_custom_select_6', $fields)): ?>
									<div class="form-group">

										<?php if (in_array('ticket_custom_text_6', $fields)): ?>
											<div class="col-sm-6">
												<?php echo form_input(array('id'=>'ticket_custom_text_6', 'name'=>'ticket_custom_text_6', 'value'=>set_value('ticket_custom_text_6'), 'class'=>'form-control ', 'placeholder'=>$ticket_custom_text_6));?>
												<div id="error-ticket_custom_text_6"><?php echo form_error('ticket_custom_text_6') ?></div>
											</div>
										<?php endif; ?>

										<?php if (in_array('ticket_custom_select_6', $fields)): ?>
											<div class="col-sm-6">
												<?php $options = create_dropdown('array', $ticket_custom_select_options_6); // TODO: cms for this dropdown options ?>
												<div class="row">
													<label class="col-sm-4 control-label" for="ticket_custom_select_6"><?php echo $ticket_custom_select_6; ?>:</label>
													<div class="col-sm-8">
														<?php echo form_dropdown('ticket_custom_select_6', $options, set_value('ticket_custom_select_6'), 'id="ticket_custom_select_6" class="form-control  input-block" placeholder="' . $ticket_custom_select_6 .'"'); ?>
														<div id="error-ticket_custom_select_6"><?php echo form_error('ticket_custom_select_6') ?></div>
													</div>
												</div>
											</div>
										<?php endif; ?>

									</div>
								<?php endif; ?>

							<?php endif; ?>

						</li>

						<li class="list-group-item">

							<h3>Terms and Conditions</h3>

							<div class="form-group">
								<?php if (in_array('ticket_terms', $fields)): ?>
									<div class="col-sm-12">
										<div class="checkbox">
											<label>
												<input id="ticket_terms" name="ticket_terms" type="checkbox" value="1" <?php echo set_checkbox('ticket_terms', 1, FALSE); ?> /> <strong>I agree and accept the <a href="<?php echo site_url("event/terms/" . $microsite->microsite_slug) ?>" data-toggle="modal" data-target="#modal">Terms and Conditions</a>. </strong>
											</label>
										</div>
										<div id="error-ticket_terms"><?php echo (form_error('ticket_terms') ? form_error('ticket_terms') . '.': '') ?></div><br />
									</div>
								<?php endif; ?>

								<?php if (in_array('ticket_newsletter', $fields)): ?>
									<div class="col-sm-12">
										<div class="checkbox">
											<label>
												<input id="ticket_newsletter" name="ticket_newsletter" type="checkbox" value="1" <?php echo set_checkbox('ticket_newsletter', 1, FALSE); ?> /> <?php echo lang('ticket_newsletter')?>
											</label>
										</div>
									</div>
									<div id="error-ticket_newsletter"><?php echo form_error('ticket_newsletter') ?></div>
								<?php endif; ?>
							</div>
						</li>
					</ul>
				</div>
                <?php } ?>

                
                
                <?php if($this->uri->segment('3') != 'true-life-2016') { ?>
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3>Are you human</h3></div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <?php 
                                        #echo $this->recaptcha->getWidget();
                                        #echo $this->recaptcha->getScriptTag();
                                    ?>
                                    <div id="error-captcha"></div>
                                </div>

                            </div>
                        </div>
                    </div>
                <?php } ?>
			</div>

			<div class="col-md-4">
				<div id="panel-description" class="panel panel-default">
					<div class="panel-body">
						<h3>Where and When</h3>
						<p>
							<?php echo $microsite->event_venue_name . ' ' . $microsite->event_venue_address; ?> 
						</p>
						<p>
							<?php echo date('F d, Y gA', strtotime($microsite->event_starts_on)); ?> onwards
						</p>
						<br />
						
						<!-- TODO -->
						<?php if($microsite->microsite_slug == 'legion-run-manila-2017') { ?>
							<h3>What you get</h3>
							<ul>
								<li>A free beer</li>
								<li>A medal</li>
								<li>An exclusive "I Am Legion" t-shirt that only participants get</li>
								<li>A few scrapes, bruises, mud in places it shouldn't be</li>
								<li>Genuine Pride</li>
							</ul>
						<?php } ?>
					</div>
				</div>

				<div id="ticket-summary">
					<div id="affixed-element" class="a-elem">
						<div class="panel panel-default ">
							<div class="panel-heading"><h3>Ticket Summary</h3></div>
							<table class="table table-bordered">
								<tr>
									<td class="col-md-5x"><b>Ticket Amount:</b> </td>
									<td class="col-md-7x align-right">Php. <?php echo number_format($this->session->userdata('ticket_type_amount'), 2); ?></td>
								</tr>
                                <tr>
									<td class="col-md-5x"><b>Quantity:</b> </td>
									<td class="col-md-7x align-right"><?php echo $this->session->userdata('ticket_qty'); ?></td>
								</tr>
                                <?php if($microsite->microsite_slug == 'legion-run-manila-2017') { ?>
                                    <tr>
                                        <td class="col-md-5x"><b>Discount:</b> </td>
                                        <td class="col-md-7x align-right">Php. <span id="ticket_discount" data-ticket-discount="<?php echo $this->session->userdata('ticket_discount'); ?>"><?php echo $this->session->userdata('ticket_discount'); ?></span></td>
                                    </tr>
                                    <?php #if($total_tickets > 100) { ?>
                                    <tr>
                                        <td class="col-md-5x"><b>Bag Check:</b> </td>
                                        <td class="col-md-7x align-right">Php. <span id="ticket_bag_check">0.00</span></td>
                                    </tr>
                                    <?php #} ?>
                                    
                                    <tr>
                                        <td class="col-md-5x"><b>Total:</b> </td>
                                        <td class="col-md-7x align-right">Php. <span id="ticket_total"><?php echo $this->session->userdata('ticket_total'); ?></span></td>
                                    </tr>
                                <?php } else { ?>
                                    <tr>
                                        <td class="col-md-5x"><b>Total:</b> </td>
                                        <td class="col-md-7x align-right">Php. <span id="ticket_total"><?php echo number_format($this->session->userdata('ticket_type_amount'), 2); ?></span></td>
                                    </tr>
                                <?php } ?>
							</table>
						</div>
						<?php if($microsite->event_has_payment_module == '1') { ?>
							<!-- add payment modes here --> 
                            <div class="action-buttons">
                                <?php if($microsite->microsite_slug != 'true-life-2016') { ?>
                                    <input name="pay" id="pay" value="Pay with Paypal" class="btn btn-danger btn-block" type="submit">
                                <?php } else { ?>
                                    <input name="submit" id="sub" value="Submit" class="btn btn-danger btn-block" type="submit">
                                <?php } ?>
                            </div>
						<?php } else { ?>
                        
							<!-- Change this -->
							<p>Payment options are currently not available. To submit your registration, please claim your coupon.</p>
                            <div class="action-buttons">
                                <input name="submit" id="sub" value="Submit" class="btn btn-danger btn-block" type="submit">
                            </div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<?php if ($microsite->microsite_footer) { ?>
			<hr />
			<?php echo $microsite->microsite_footer; ?>
		<?php } ?>
	</div>
</form>
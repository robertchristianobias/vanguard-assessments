<style>
    td {
        border: 1px solid #ccc;
        padding: 10px;
    }
</style>
<?php

 $table = 
    '<table width="100%">
        <tr>
            <td width="40%">
                <b>Reference No</b>
            </td>
            <td>
                <b>' . $registrant_info->ticket_reference_no . ' </b>
            </td>
        </tr>';
 
        if($registrant_info->ticket_payment_transaction_id) {
            $table .=
                    '<tr> 
                        <td width="40%"><b>Transaction ID</b>: </td>
                        <td>' . $registrant_info->ticket_payment_transaction_id . '</td>
                    </tr>';
        }
        
        if($first_registrant == $registrant_info->ticket_id) {
        $table .=
                    '<tr> 
                        <td width="40%"><b>Return to Payments</b>: </td>
                        <td>Click <a href="'.site_url('event/order/' . $microsite->microsite_slug . '/' . $registrant_info->ticket_reference_no).'">here</a></td>
                    </tr>';
        }
        if ($coupon_code) { 
        $table .= 
                    '<tr>
                        <td width="40%"><b>Coupon Code:</b> </td>
                        <td><h3>' . $coupon_code . '</h3></td>
                    </tr>';
        }
        
        if ($registrant_info->ticket_bag_check) { 
        $table .= 
                    '<tr>
                        <td width="40%"><b>Bag Check</b>: </td>
                        <td>' . $registrant_info->ticket_bag_check . '</td>
                    </tr>';
        }
        
        $table .= 
                    '<tr>
                        <td width="40%"><b>Name</b>: </td>
                        <td>' . implode(" ", array($registrant_info->ticket_firstname, $registrant_info->ticket_middlename, $registrant_info->ticket_lastname)) . '</td>
                    </tr>
                    <tr>
                        <td width="40%"><b>Email Address</b>: </td>
                        <td>' . $registrant_info->ticket_email . '</td>
                    </tr>
                    <tr>
                        <td width="40%"><b>Mobile Number</b>: </td>
                        <td> ' . $registrant_info->ticket_mobile . ' </td>
                    </tr>';
        
        if ($registrant_info->ticket_phone) { 
        $table .= 
                    '<tr>
                        <td width="40%"><b>Phone Number</b>: </td>
                        <td>' . $registrant_info->ticket_phone . '</td>
                    </tr>';
        }
        if ($registrant_info->ticket_gender) { 
        $table .= 
                    '<tr>
                        <td width="40%"><b>Gender</b>: </td>
                        <td>' . $registrant_info->ticket_gender . ' </td>
                    </tr>';
        } 
        if ($registrant_info->ticket_birthday != '0000-00-00') { 
        $table .= 
                    '<tr>
                        <td width="40%"><b>Birthday</b>: </td>
                        <td>' . $registrant_info->ticket_birthday . '</td>
                    </tr>';
        } 
        if ($registrant_info->ticket_mstatus) { 
        $table .= 
                    '<tr>
                        <td width="40%"><b>Marital Status</b>: </td>
                        <td>' . $registrant_info->ticket_mstatus . ' </td>
                    </tr>';
        } 
        if ($registrant_info->ticket_address || $registrant_info->ticket_city || $registrant_info->ticket_zipcode || $registrant_info->ticket_country) { 
        $table .= 
                    '<tr>
                        <td width="40%"><b>Address</b>: </td>
                        <td>' . $registrant_info->ticket_address . ($registrant_info->ticket_city ? $registrant_info->ticket_city . ', ' : '') . ($registrant_info->ticket_zipcode ? $registrant_info->ticket_zipcode . ', ' : '') . ($registrant_info->ticket_country ? $registrant_info->ticket_country : '') . '</td>
                    </tr>';
        } 
        if ($registrant_info->ticket_company) { 
        $table .= 
            '<tr>
                <td width="40%"><b>Company</b>: </td>
                <td>' . $registrant_info->ticket_company . ' </td>
            </tr>';
        } 

        if ($registrant_info->ticket_occupation) { 
        $table .= ' <tr>
                        <td width="40%"><b>Occupation</b>: </td>
                        <td>' . $registrant_info->ticket_occupation . ' </td>
                    </tr>';
        } 
        if ($registrant_info->ticket_work_phone) { 
        $table .= '
                    <tr>
                        <td width="40%"><b>Work Phone</b>: </td>
                        <td>' . $registrant_info->ticket_work_phone . ' </td>
                    </tr>';
        } 
        if ($registrant_info->ticket_medical_conditions) { 
        $table .= 
                    '<tr>
                        <td width="40%"><b>Medical Conditions</b>: </td>
                        <td>' . $registrant_info->ticket_medical_conditions . ' </td>
                    </tr>';
        } 
        if ($registrant_info->ticket_emergency_contact) { 
        $table .= '
                    <tr>
                        <td width="40%"><b>Contact person in case of emergency</b>: </td>
                        <td>' . $registrant_info->ticket_emergency_contact . ' </td>
                    </tr>';
        } 
        if ($registrant_info->ticket_emergency_number) { 
        $table .= '
                    <tr>
                        <td width="40%"><b>Emergency Contact</b>: </td>
                        <td>' . $registrant_info->ticket_emergency_number . ' </td>
                    </tr>';
        } 
        if ($registrant_info->ticket_custom_select_1) { 
        $table .= '
                    <tr>
                        <td width="40%"><b>T-shirt Size</b>: </td>
                        <td>' . $registrant_info->ticket_custom_select_1 . ' </td>
                    </tr>';
        } 

        if ($registrant_info->ticket_custom_text_4) { 
        $table .= '
                    <tr>
                        <td width="40%"><b>Group Code</b>: </td>
                        <td>' . $registrant_info->ticket_custom_text_4 . ' </td>
                    </tr>';
        } 

        if ($registrant_info->ticket_newsletter) { 
        $table .= '
                    <tr>
                        <td width="40%"><b>Subscribed to Newsletter</b>: </td>
                        <td>' . $registrant_info->ticket_newsletter . ' </td>
                    </tr>';
        } 
    $table .= '</table>';
    
    $cancelled = 
    '<table width="100%">
        <tr>
            <td width="40%">
                <b>Reference No</b>
            </td>
            <td>
                <b>' . $registrant_info->ticket_reference_no . ' </b>
            </td>
        </tr>';
        
        if($first_registrant == $registrant_info->ticket_id) {
        $cancelled .=
                    '<tr> 
                        <td width="40%"><b>Return to Payments</b>: </td>
                        <td>You cancelled your order. Click <a href="'.site_url('event/order/' . $microsite->microsite_slug . '/' . $registrant_info->ticket_id).'">here</a> to make payment. </td>
                    </tr>';
        }
        if ($coupon_code) {
        $cancelled .= 
                    '<tr>
                        <td width="40%"><b>Coupon Code:</b> </td>
                        <td><h3>' . $coupon_code . '</h3></td>
                    </tr>';
        }
        
        if ($registrant_info->ticket_bag_check) { 
        $cancelled .= 
                    '<tr>
                        <td width="40%"><b>Bag Check</b>: </td>
                        <td>' . $registrant_info->ticket_bag_check . '</td>
                    </tr>';
        }
        
        $cancelled .= 
                    '<tr>
                        <td width="40%"><b>Name</b>: </td>
                        <td>' . implode(" ", array($registrant_info->ticket_firstname, $registrant_info->ticket_middlename, $registrant_info->ticket_lastname)) . '</td>
                    </tr>
                    <tr>
                        <td width="40%"><b>Email Address</b>: </td>
                        <td>' . $registrant_info->ticket_email . '</td>
                    </tr>
                    <tr>
                        <td width="40%"><b>Mobile Number</b>: </td>
                        <td> ' . $registrant_info->ticket_mobile . ' </td>
                    </tr>';
        
        if ($registrant_info->ticket_phone) { 
        $cancelled .= 
                    '<tr>
                        <td width="40%"><b>Phone Number</b>: </td>
                        <td>' . $registrant_info->ticket_phone . '</td>
                    </tr>';
        } 
        if ($registrant_info->ticket_gender) { 
        $cancelled .= 
                    '<tr>
                        <td width="40%"><b>Gender</b>: </td>
                        <td>' . $registrant_info->ticket_gender . ' </td>
                    </tr>';
        } 
        if ($registrant_info->ticket_birthday != '0000-00-00') { 
        $cancelled .= 
                    '<tr>
                        <td width="40%"><b>Birthday</b>: </td>
                        <td>' . $registrant_info->ticket_birthday . '</td>
                    </tr>';
        } 
        if ($registrant_info->ticket_mstatus) { 
        $cancelled .= 
                    '<tr>
                        <td width="40%"><b>Marital Status</b>: </td>
                        <td>' . $registrant_info->ticket_mstatus . ' </td>
                    </tr>';
        } 
        if ($registrant_info->ticket_address || $registrant_info->ticket_city || $registrant_info->ticket_zipcode || $registrant_info->ticket_country) { 
        $cancelled .= 
                    '<tr>
                        <td width="40%"><b>Address</b>: </td>
                        <td>' . $registrant_info->ticket_address . ($registrant_info->ticket_city ? $registrant_info->ticket_city . ', ' : '') . ($registrant_info->ticket_zipcode ? $registrant_info->ticket_zipcode . ', ' : '') . ($registrant_info->ticket_country ? $registrant_info->ticket_country : '') . '</td>
                    </tr>';
        } 
        if ($registrant_info->ticket_company) { 
        $cancelled .= 
            '<tr>
                <td width="40%"><b>Company</b>: </td>
                <td>' . $registrant_info->ticket_company . ' </td>
            </tr>';
        } 

        if ($registrant_info->ticket_occupation) { 
        $cancelled .= ' <tr>
                        <td width="40%"><b>Occupation</b>: </td>
                        <td>' . $registrant_info->ticket_occupation . ' </td>
                    </tr>';
        } 
        if ($registrant_info->ticket_work_phone) { 
        $cancelled .= '
                    <tr>
                        <td width="40%"><b>Work Phone</b>: </td>
                        <td>' . $registrant_info->ticket_work_phone . ' </td>
                    </tr>';
        } 
        if ($registrant_info->ticket_medical_conditions) { 
        $cancelled .= 
                    '<tr>
                        <td width="40%"><b>Medical Conditions</b>: </td>
                        <td>' . $registrant_info->ticket_medical_conditions . ' </td>
                    </tr>';
        } 
        if ($registrant_info->ticket_emergency_contact) { 
        $cancelled .= '
                    <tr>
                        <td width="40%"><b>Contact person in case of emergency</b>: </td>
                        <td>' . $registrant_info->ticket_emergency_contact . ' </td>
                    </tr>';
        } 
        if ($registrant_info->ticket_emergency_number) { 
        $cancelled .= '
                    <tr>
                        <td width="40%"><b>Emergency Contact</b>: </td>
                        <td>' . $registrant_info->ticket_emergency_number . ' </td>
                    </tr>';
        } 
        if ($registrant_info->ticket_custom_select_1) { 
        $cancelled .= '
                    <tr>
                        <td width="40%"><b>T-shirt Size</b>: </td>
                        <td>' . $registrant_info->ticket_custom_select_1 . ' </td>
                    </tr>';
        } 

        if ($registrant_info->ticket_custom_text_4) { 
        $cancelled .= '
                    <tr>
                        <td width="40%"><b>Group Code</b>: </td>
                        <td>' . $registrant_info->ticket_custom_text_4 . ' </td>
                    </tr>';
        } 

        if ($registrant_info->ticket_newsletter) { 
        $cancelled .= '
                    <tr>
                        <td width="40%"><b>Subscribed to Newsletter</b>: </td>
                        <td>' . $registrant_info->ticket_newsletter . ' </td>
                    </tr>';
        } 
        $cancelled .= '</table>';
?>

<table width="100%" cellpadding="4" cellspacing="0" border="0" style="border: 1px solid #ccc;">
    <tr>
        <td colspan="2" style="background: #000;">
            <a href="<?php echo site_url('event/' . $microsite->microsite_slug); ?>">
                <img src="<?php echo site_url($microsite->microsite_logo_image); ?>" />
            </a>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <?php
                if($status == 'cancelled')
                {
                    echo $cancelled;
                    //echo str_replace("{{registrant_info}}", $table, nl2br($microsite->event_reg_email_content));
                }
                else
                {
                    echo str_replace("{{registrant_info}}", $table, nl2br($microsite->event_reg_email_content));
                }
               
            ?>
        </td>
    </tr>
</table>

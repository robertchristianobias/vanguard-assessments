<style>
	<?php echo $microsite->microsite_custom_css ?>
	#hero_image h1 {
		visibility: hidden;
	}
</style>

<div class="col-sm-offset-2 col-sm-8">

	<h1><?php echo $header ?></h1>
	<br />
	<div><?php echo $content ?></div>
	<br />

	<div class="text-center">
		<a href="<?php echo site_url("event/register/" . $microsite->microsite_slug) ?>" class="btn btn-primary"><i class="fa fa-chevron-left"></i> Back to Registration</a>
	</div>

</div>

<?php #pr($microsite); ?>

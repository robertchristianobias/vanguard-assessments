<div class="row">

	<div class="col-sm-offset-3 col-sm-6">

		<h2 class="form-signin-heading text-center"><?php echo lang('reset_password_heading');?></h2>

		<div class="login-box-body">

			<p class="login-box-msg text-center">Please enter your new password</p>

			<div id="infoMessage"><?php echo $message;?></div>

			<?php echo form_open('account/reset_password/' . $code, 'class="form-signin"');?>

				<p>
					<label for="new_password"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?></label> <br />
					<?php echo form_input($new_password);?>
					<?php echo form_error('new'); ?>
				</p>

				<p>
					<?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm');?> <br />
					<?php echo form_input($new_password_confirm);?>
					<?php echo form_error('new_confirm'); ?>
				</p>

				<?php echo form_input($user_id);?>
				<?php echo form_hidden($csrf); ?>

				<p><?php echo form_submit('submit', lang('reset_password_submit_btn'), 'class="btn btn-primary"');?></p>

			<?php echo form_close();?>

		</div>
	</div>

</div>
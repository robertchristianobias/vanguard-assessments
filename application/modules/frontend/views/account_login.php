<div class="jumbotron">
	<div class="container">
		<div class="text-center">

			<h2 class="form-signin-heading white">Login</h2>
			<div class="login-box-body">

				<?php echo form_open("account/login", 'class="form-signin"');?>

					<div class="form-group has-feedback">
						<input name="identity" type="text" class="form-control" placeholder="Email"/>
						<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
						<?php echo form_error('identity'); ?>
					</div>
					<div class="form-group has-feedback">
						<input name="password" type="password" class="form-control" placeholder="Password"/>
						<span class="glyphicon glyphicon-lock form-control-feedback"></span>
						<?php echo form_error('password'); ?>
					</div>
					<div class="row">
						<div class="col-xs-7 text-left">		
							<div class="checkbox">
								<label>
									<input name="remember" type="checkbox"> Remember Me
								</label>
							</div>
						</div>
						<div class="col-xs-5">
							<button type="submit" class="btn btn-primary btn-block btn-flat"><span class="fa fa-sign-in"></span> Login</button>
						</div>
					</div>
					<?php echo form_hidden('submit', 1); ?>
					<br />
					<a href="forgot_password">I forgot my password</a> <br />
				</form>
			</div>

		</div>
	</div>
</div>
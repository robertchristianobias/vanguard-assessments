		<div class="jumbotron">
			<div class="container">

				<div class="text-center">
					<div class="row">
					 <div class="col-md-6 col-md-offset-3">
						<h3 class="white">
							<i class="fa fa-wrench icon-animated-wrench orange2 bigger-150"></i>
							&nbsp;
							We are almost ready
						</h3>
						<h2 class="white"><span class="bigger-150"><span class="light-green"><?php echo config_item('website_name'); ?></span> is coming soon</span></h2>

					<br />
					<br />
					<br />


						<div class="hide">

							<div class="row">
								<div class="col-xs-6 col-sm-3">
									<div class="timer" id="timer-day" data-type="day">
										<div class="timer-value">14</div>
										<div class="timer-text"><span class="label label-warning arrowed-in arrowed-in-right">Days</span></div>
									</div>
								</div>
								<div class="col-xs-6 col-sm-3">
									<div class="timer" id="timer-hour" data-type="hour">
										<div class="timer-value">9</div>
										<div class="timer-text"><span class="label label-purple arrowed-in arrowed-in-right">Hours</span></div>
									</div>
								</div>

								<div class="visible-xs clearfix"></div>
								<div class="visible-xs space space-12"></div>

								<div class="col-xs-6 col-sm-3">
									<div class="timer" id="timer-minute" data-type="minute">
										<div class="timer-value">26</div>
										<div class="timer-text"><span class="label label-success arrowed-in arrowed-in-right">Minutes</span></div>
									</div>
								</div>
								<div class="col-xs-6 col-sm-3">
									<div class="timer" id="timer-second" data-type="second">
										<div class="timer-value">11</div>
										<div class="timer-text"><span class="label label-primary arrowed-in arrowed-in-right">Seconds</span></div>
									</div>
								</div>
							</div>

						</div>

					<br />
					<br />
					<br />
						<div class="text-center white bigger-125">If you want us to handle your event registration, please let us know.</div>
						<div class="space-6"></div>
						<form method="post">
							<div class="input-group input-group-lg">
								<input type="email" name="email" placeholder="Type your email address" class="form-control input-transparent" required>
								<span class="input-group-btn">
									<button class="btn btn-white btn-primary btn-lg no-border" type="submit">Send</button>
								</span>
							</div>
							<?php echo form_error("email", '<span class="text-danger text-center">', '</span>'); ?>
						</form>




					 </div>
					</div>

					<br />
					<br />
					<br />


					<div class="text-center social-tools">
						<span class="action-buttons">
							<a href="javascript:;">
								<i class="ace-icon fa fa-twitter-square white fa-3x"></i>
							</a>

							<a href="https://www.facebook.com/Google/" target="_blank">
								<i class="ace-icon fa fa-facebook-square white fa-3x"></i>
							</a>

							<a href="javascript:;">
								<i class="ace-icon fa fa-rss-square white fa-3x"></i>
							</a>
						</span>
					</div>

				</div>
			</div>
		</div>

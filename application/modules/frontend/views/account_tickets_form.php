<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span>
		<span class="sr-only"><?php echo lang('button_close')?></span>
	</button>
	<h4 class="modal-title" id="myModalLabel">Update Ticket</h4>
</div>

<div class="modal-body">
    <form name="" method="POST" action="" id="tickets-form">
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="ticket_status">Ticket Status:</label>
                <div class="col-sm-5">
                    <?php $status = create_dropdown('array', ',Registered,Paid,Cancelled,Refunded,Pending,Complimentary,Failed Payment'); ?>
                    <?php echo form_dropdown('ticket_status', $status, isset($record->ticket_status) ? $record->ticket_status : '', 'class="form-control" id="ticket_status"'); ?>
                    <div id="error-ticket_status"></div>
                </div>
            </div>
			
        </div>
    </form>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
		<i class="fa fa-times"></i> <?php echo lang('button_close')?>
	</button>
	<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
        <i class="fa fa-save"></i> Save
    </button>
</div>

<div id="dtBox"></div>

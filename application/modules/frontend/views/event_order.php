<style>
	<?php echo $microsite->microsite_custom_css ?>
</style>

<div class="text-center">
	<h1>Order</h1>

	<?php if ($microsite->microsite_header != ''): ?>
		<?php echo $microsite->microsite_header; ?>
	<?php endif; ?>
	<hr />
</div>

<div class="col-md-offset-1 col-md-10">
    <div class="row">
        <div class="col-md-8">
            <?php if($order) { ?>
                <form method="POST" name="paypal_form" action="" class="form-horizontal" id="form-register">
                    <div class="panel panel-default">
                        <div class="panel-heading"><h3>Pending Order</h3></div>
                        <div class="panel-body">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <table class="table table-bordered">
                                        <tr>
                                            <td><b>Ticket Amount:</b></td>
                                            <td>Php <?php echo number_format($order->order_ticket_amount, 2); ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Quantity:</b></td>
                                            <td><?php echo $order->order_ticket_qty; ?></td>
                                        </tr>
                                        <tr>
                                            <td><b>Ticket Discount:</b></td>
                                            <td>Php <?php echo number_format($order->order_ticket_discount, 2); ?></td>
                                        </tr>
                                        <?php if($order->order_ticket_addon) { ?>
                                        <tr>
                                            <td><b>Bag Check</b></td>
                                            <td>Php <?php echo number_format($order->order_ticket_addon, 2); ?></td>
                                        </tr>
                                        <?php } ?>
                                        <tr>
                                            <td><b>Ticket Total:</b></td>
                                            <td>Php <?php echo number_format($order->order_ticket_total, 2); ?></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-3">
                                    <input type="submit" name="submit" class="btn btn-sm btn-danger btn-block" id="submit" value="Pay with Paypal" />
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            <?php } else { ?>
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>Pending Order</h3></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <p>There are no currently pending order.</p>
                                <div class="text-center">
                                    <a href="<?php echo site_url("event/" . $microsite->microsite_slug) ?>" class="btn btn-danger"><i class="fa fa-chevron-left"></i> Back to Registration</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>

        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-body">
                    <h3>Where and When</h3>
                    <p>
                        <?php echo $microsite->event_venue_name . ' ' . $microsite->event_venue_address; ?> 
                    </p>
                    <p>
                        <?php echo date('F d, Y gA', strtotime($microsite->event_starts_on)); ?> onwards
                    </p>
                    <br />

                    <!-- TODO -->
                    <?php if($microsite->microsite_slug == 'legion-run-manila-2017') { ?>
                        <h3>What you get</h3>
                        <ul>
                            <li>A free beer</li>
                            <li>A medal</li>
                            <li>An exclusive "I Am Legion" t-shirt that only participants get</li>
                            <li>A few scrapes, bruises, mud in places it shouldn't be</li>
                            <li>Genuine Pride</li>
                        </ul>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <?php if ($microsite->microsite_footer) { ?>
        <hr />
        <?php echo $microsite->microsite_footer; ?>
    <?php } ?>
</div>


<style>
	<?php echo $microsite->microsite_custom_css ?>
</style>

<div class="text-center">
	<h1>Order</h1>

	<?php if ($microsite->microsite_header != ''): ?>
		<?php echo $microsite->microsite_header; ?>
	<?php endif; ?>
	<hr />
</div>

<form method="POST" name="" action="https://google.com" class="form-horizontal" id="form-register">
    
	<div class="col-md-offset-1 col-md-10">
		<div class="row">
			<div class="col-md-8">
				
                <div class="panel panel-default">
                    <div class="panel-heading"><h3>Order Cancelled</h3></div>
                    <div class="panel-body">
                        <div class="form-group">
                            <div class="col-md-12">
                                <p>Order cancelled. Please check your email for details</p>
                                <div class="text-center">
                                    <a href="<?php echo site_url("event/" . $microsite->microsite_slug) ?>" class="btn btn-danger"><i class="fa fa-chevron-left"></i> Back to Registration</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>

			<div class="col-md-4">
				<div class="panel panel-default">
					<div class="panel-body">
						<h3>Where and When</h3>
						<p>
							<?php echo $microsite->event_venue_name . ' ' . $microsite->event_venue_address; ?> 
						</p>
						<p>
							<?php echo date('F d, Y gA', strtotime($microsite->event_starts_on)); ?> onwards
						</p>
						<br />
						
						<!-- TODO -->
						<?php if($microsite->microsite_slug == 'legion-run-manila-2017') { ?>
							<h3>What you get</h3>
							<ul>
								<li>A free beer</li>
								<li>A medal</li>
								<li>An exclusive "I Am Legion" t-shirt that only participants get</li>
								<li>A few scrapes, bruises, mud in places it shouldn't be</li>
								<li>Genuine Pride</li>
							</ul>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
		<?php if ($microsite->microsite_footer) { ?>
			<hr />
			<?php echo $microsite->microsite_footer; ?>
		<?php } ?>
	</div>
</form>

				<div class="page-header">
					<h1 id="page-title"><?php echo $page_heading; ?></h1>
				</div>
				<div class="content">
					<?php echo parse_content($record->page_content); ?>
				</div>

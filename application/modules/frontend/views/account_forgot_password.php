<div class="row">

	<div class="col-sm-offset-3 col-sm-6">

		<h2 class="form-signin-heading text-center"><?php echo lang('forgot_password_heading');?></h2>

		<div class="login-box-body">

			

			<?php echo form_open("account/forgot_password", 'class="form-signin"'); ?>

				<p class="login-box-msg"><?php echo sprintf(lang('forgot_password_subheading'), $identity_label);?></p>

				<p>
					<label for="email"><?php echo sprintf(lang('forgot_password_email_label'), $identity_label);?></label> <br />
					<?php echo form_input($email);?>
					<?php echo form_error('email'); ?>
				</p>

				<p><?php echo form_submit('submit', lang('forgot_password_submit_btn'), 'class="btn btn-primary"');?></p>

			</form>

		</div>

	</div>

</div>
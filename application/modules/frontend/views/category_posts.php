				<?php if ($posts): ?>
					<div class="text-center">
						<?php echo $this->pagination->create_links(); ?>
					</div>
					<?php foreach ($posts as $post): ?>
						<div class="post-content">
							<h2 class="post-title"><a href="<?php echo site_url('post/' . $post->post_slug); ?>"><?php echo $post->post_title; ?></a></h2>
							<div class="post-info">
								Posted by <strong><?php echo $post->first_name; ?> <?php echo $post->last_name; ?></strong>
								on <strong><?php echo $post->post_posted_on; ?></strong>
							</div>
							<div class="post-body">
								<?php echo word_limiter(strip_tags($post->post_content), 100); ?>
							</div>
						</div>
					<?php endforeach; ?>
					
				<?php else: ?>
					<div class="alert alert-warning">Posts not found within this category</div>
				<?php endif; ?>
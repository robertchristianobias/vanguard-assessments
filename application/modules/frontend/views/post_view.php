			<div class="page-header">
				<h1 id="page-title"><?php echo $page_heading; ?></h1>
				<div class="post-info">
					Posted by <strong><?php echo $post->first_name; ?> <?php echo $post->last_name; ?></strong>
					on <strong><?php echo $post->post_posted_on; ?></strong>
				</div>
			</div>
			<div class="content">
				<div class="post-content">
					<div class="post-body">
						<?php echo parse_content($post->post_content); ?>
					</div>
				</div>
			</div>

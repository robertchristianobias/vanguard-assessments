$(function() {
    
    $(".ticket_bag_check").change(function() {
        
        var bag_check = $('.ticket_bag_check:checked');
        var action;
        if(this.checked) {
            action = 'add';
        } else {
            action = 'substract';
        }
        
        $.post(event_url + '/process_addon', {
            bag_check: bag_check.length,
            action: action
        }, function(data, status) {
            var response = $.parseJSON(data);
            
            $('#ticket_total').html(response.ticket_total);
            $('#ticket_bag_check').html(response.bag_check_total);
        });
        
    });
    
//    if($('.ticket_bag_check').click(function() {
//        var bag_check = $('.ticket_bag_check:checked');
//
//        // Update ticket total
//        $.post(event_url + '/process_addon', {
//            bag_check: bag_check.length
//        }, function(data, status) {
//            var response = $.parseJSON(data);
//            
//            $('#ticket_total').html(response.ticket_total);
//            $('#bag_check_wrapper').html('abc');
//        });
//        
//    }));

    $(window).on('load scroll resize', function(e){
        affixTop();
    });    
    
    $(".ticket_birthday").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'yy-mm-dd',
        yearRange: "-100:+0",
    });
    
    $('#submit_coupon_code').click(function() {

        var btn = $(this);
        
        btn.html('Please wait...');
        $.post(event_url+'/process_event_coupon', {
            slug: slug,
            coupon_code: $('#coupon_code').val()
        }, function(data, status){
            
            var response = $.parseJSON(data);
            
            if(response.success == false) {
                
                $('#error-coupon_code').html(response.error);
                alertify.error(response.message);
                
                $('#ticket_discount').html(response.ticket_discount);
                $('#ticket_total').html(response.ticket_total);
                $('#ticket_discount').data('ticket-discount', response.ticket_discount);
                $('#paypal_ticket_discount').val('0.00');
                //$('#paypal_amount').val(response.ticket_total);
                
            } else {
                
                // Add response to the sidebar
                $('#ticket_amount').html(response.ticket_amount);
                $('#ticket_discount').html(response.ticket_discount);
                $('#ticket_total').html(response.ticket_total);
                $('#ticket_discount').data('ticket-discount', response.ticket_discount);
                $('#paypal_ticket_discount').val(response.ticket_discount);
                //$('#paypal_amount').val(response.ticket_total);
                
                if(response.ticket_discount != '') {
                    btn.html('<span class="fa fa-send"></span> Claim');
                }
                
                alertify.success(response.message);
            }
            
            btn.html('<span class="fa fa-send"></span> Claim');
        });
        
        return false;
    });
    
});


// handles the submit action
$('#pay').click(function(e) {
    
    
    
    var form       = $('#form-register');
    var btn        = $(this);
    var form_data  = form.serializeArray();
        form_data.push({
            name:  'captcha',
            value: $('#g-recaptcha-response').val()
        });
    
    btn.val('Loading...');
    
    $.post(event_url + '/process_register', form_data, 
        function(result) {

            btn.val('Pay with Paypal');
            
            var response = $.parseJSON(result);
            
            if (response.success === false)
            {
                alertify.error(response.message);

                if (response.errors) {
                    $.each(response.errors, function(index, value) {
                        for(var form_name in value) {
                            console.log(form_name);
                            if(typeof value[form_name] !== 'undefined') {
                                $('.error-' + form_name + '_' + value.ticket_no).html(value[form_name]);
                            }
                        }
                    });
                }

                if(response.captcha) {
                    $('#error-captcha').html(response.captcha);
                }
                
                grecaptcha.reset();
            }
            else
            {
                //$('#form-register').submit();
                
                alertify.success(response.message);
                $.blockUI({ message: '<h4>Redirecting to Paypal...</h4>' });
                
                window.location.replace(response.redirect);
                return true;
                
                
            }
        }
    );
    
    return false;
   
});


// handles the submit action
$('#sub').click(function(e) {
    
    var ticket_coupon = $('#ticket_coupon').data('ticket-coupon');
    
    $.blockUI({ message: '<h4>Processing...</h4>' });

});

var navbar          = $('#navbar-container');
var ticket_summary  = $('#ticket-summary');
var panel_desc      = $('#panel-description');
var reg_form        = $('#form-register');

// custom ticket summary affix
function affixTop() {
    var wndw = $(window);

    if (wndw.width() > 990) {
        if (parseInt(wndw.scrollTop()) > parseInt(reg_form.position().top + panel_desc.height() - 70)) {
            
            ticket_summary.addClass('affix').css({
                "top"   : parseInt(navbar.height()+20)+'px',
                "width" : parseInt(panel_desc.width())+'px'
            });

        } else {
            ticket_summary.removeClass('affix').removeAttr("style");    
        }            
    } else {
        ticket_summary.removeClass('affix').removeAttr("style");
    }
}
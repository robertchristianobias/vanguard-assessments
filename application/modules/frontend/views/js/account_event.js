$(function() {
    
    // renders the datatables (datatables.net)
    var oTable = $('#dt-account-events').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": website_url + "account/ticketsdt/" + event_code,
        "lengthMenu": [[10, 20, 50, 100, 300, -1], [10, 20, 50, 100, 300, "All"]],
        "pagingType": "full_numbers",
        "language": {
            "paginate": {
                "previous": 'Prev',
                "next": 'Next',
            }
        },
        "bAutoWidth": false,
        "aaSorting": [[ 0, "desc" ]],
        "aoColumnDefs": [
            {
                "aTargets": [0],
                "sClass": "col-md-3",
            },
            {
                "aTargets": [2],
                "sClass": "col-md-4",
            },
            {
                "aTargets": [9,10, 14],
                "sClass": "col-md-1 text-center",
            },
            {
                "aTargets": [11],
                "sClass": "col-md-1 text-center",
            },
            {
                "aTargets": [24],
                "bSortable": false,
                "mRender": function (data, type, full) {
                    html = '<a href="'+ app_url  + 'account/form/edit/'+full[24]+'"  tooltip-toggle="tooltip" data-toggle="modal" data-target="#modal" data-placement="top" title="Edit" class="btn btn-xs btn-warning"><span class="fa fa-pencil"></span></a> ';

                    return html;
                },
                "sClass": "col-md-2 text-center",
            },

        ]
    });

    $('#export').click(function(e){
        var link    = $(this);
        var href    = link.attr('href').split('?');
        var keyword = $('input[type="search"]').val();

        link.attr('href', href[0] + '?q=' + keyword);
    });

});
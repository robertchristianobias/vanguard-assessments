$(function() {
    
    $('.get_ticket').click(function() {
        
        $(this).val('Processing...')
        $.post(event_url+'/process_get_ticket', {
            ticket_id:  $(this).data('ticket-id'),
            ticket_qty: $(this).closest('.ticket_type').find('#ticket_qty').val(),
            slug: slug
        }, function(data, status) {
            
            var response = $.parseJSON(data);
            
            $(this).val('Get Ticket');
            
            window.location.replace(response.redirect);
        });
        
        return false;
    });
    
});

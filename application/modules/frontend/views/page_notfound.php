<section class="container">

	<div class="jumbotron text-center">

		<h1>Page Not Found</h1>

		<p>Sorry, the page you are trying to access does not exist.</p>
		<p>Please use the navigation menu above or go to the <a href="<?php echo site_url(); ?>">homepage</a></p>

	</div>

</section>
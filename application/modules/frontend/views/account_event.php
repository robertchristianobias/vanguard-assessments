<h2><?php echo $page_heading;?></h2>

<div class="row">
    <div class="col-md-12">
        <div class="pull-right">
            <a href="<?php echo site_url('account/export/'.$event->event_code); ?>" class="btn btn-success" id="export">Export to CSV</a>
        </div>
    </div>
</div>

<table class="table table-striped table-bordered table-hover dt-responsive" id="dt-account-events">
    <thead>
        <tr>
            <th class="all col-md-2"><?php echo lang('index_ticket_reference_no')?></th>
            <th class="all col-md-4"><?php echo lang('index_ticket_name'); ?></th>
            
            <!--<th class="all"><?php echo lang('index_ticket_id')?></th>-->
            
            <th class="min-tablet col-md-4"><?php echo lang('index_ticket_email'); ?></th>

            <th class="none"><?php echo lang('index_ticket_phone'); ?></th>
            <th class="none"><?php echo lang('index_ticket_firstname'); ?></th>
            <th class="none"><?php echo lang('index_ticket_middlename'); ?></th>
            <th class="none"><?php echo lang('index_ticket_lastname'); ?></th>
            <th class="none"><?php echo lang('index_ticket_mobile'); ?></th>
            <th class="none"><?php echo lang('index_ticket_gender'); ?></th>
            <th class="min-desktop"><?php echo lang('index_ticket_group_code'); ?></th>
            <th class="min-desktop"><?php echo lang('index_ticket_coupon_code'); ?></th>
            
            <th class="min-desktop"><?php echo lang('index_ticket_payment_method'); ?></th>
            <th class="min-desktop"><?php echo lang('index_ticket_payment_transaction_id'); ?></th>
            
            <th class="min-desktop"><?php echo lang('index_ticket_status'); ?></th>
            
            <th class="none"><?php echo lang('index_ticket_mstatus'); ?></th>
            <th class="none"><?php echo lang('index_ticket_is_pwd'); ?></th>
            <th class="none"><?php echo lang('index_ticket_is_senior'); ?></th>
            <th class="none"><?php echo lang('index_ticket_company'); ?></th>
            <th class="none"><?php echo lang('index_ticket_occupation'); ?></th>
            <th class="none"><?php echo lang('index_ticket_address'); ?></th>
            <th class="none"><?php echo lang('index_ticket_city'); ?></th>
            <th class="none"><?php echo lang('index_ticket_zipcode'); ?></th>
            <th class="none"><?php echo lang('index_ticket_country'); ?></th>
            <th class="none"><?php echo lang('index_ticket_registered_on'); ?></th>
            <th class="all"><?php echo lang('index_action')?></th>
        </tr>
    </thead>
</table>

<script>
    var event_code = "<?php echo $event->event_code; ?>";
    var website_url = "<?php echo site_url(); ?>";
</script>
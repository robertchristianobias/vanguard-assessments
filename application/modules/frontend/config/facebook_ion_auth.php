<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Settings.
| -------------------------------------------------------------------------
*/

// Your app id
$config['app_id'] 		= '483095405227826';

// Your app secret key
$config['app_secret'] 	= 'bf59e493695760e35c59578b2198539f';

// custom permissions check
// http://developers.facebook.com/docs/reference/login/#permissions
$config['scope'] 		= 'email'; 

// url to redirect back from facebook
$config['redirect_uri'] = site_url('account/facebook_login'); 

// default password of registrations through fb
$config['fb_password']	= '@facebook^does&not*have%pass123';
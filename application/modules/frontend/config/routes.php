<?php defined('BASEPATH') OR exit('No direct script access allowed');

$route['event/terms_of_service/(:any)'] = 'frontend/event/terms_of_service/$1'; // Change
$route['event/process_register'] = 'frontend/event/process_register';
$route['event/process_event_coupon'] = 'frontend/event/process_event_coupon';
$route['event/process_get_ticket']  = 'frontend/event/process_get_ticket';
$route['event/process_addon']  = 'frontend/event/process_addon';
$route['event/test_email'] = 'frontend/event/test_email';
$route['event/order/(:any)/(:any)'] = 'frontend/event/order/$1/$2';
$route['event/success/(:any)'] = 'frontend/event/success/$1';
$route['event/cancel/(:any)'] = 'frontend/event/cancel/$1';
$route['event/ipn'] = 'frontend/event/ipn';

$route['event/(:any)'] = 'frontend/event/view/$1';
$route['event/register/(:any)'] = 'frontend/event/register/$1';
$route['event/register/(:any)/(:any)'] = 'frontend/event/register/$1/$2';

$route['event/terms/(:any)'] = 'frontend/event/terms/$1';
$route['event/privacy/(:any)'] = 'frontend/event/privacy/$1';

$route['category/(:any)'] = 'frontend/category/posts/$1';
$route['category/(:any)/(:any)'] = 'frontend/category/posts/$1/$2';
$route['category/(:any)/(:any)/(:any)'] = 'frontend/category/posts/$1/$2/$3';
$route['post/(:any)'] = 'frontend/post/view/$1';
$route['account/form/(:any)/(:any)'] = 'frontend/account/form/$1/$2';

$route['account/export/(:any)'] = 'frontend/account/export/$1';
$route['account/(:any)'] = 'frontend/account/$1';
$route['account/reset_password/(:any)'] = 'frontend/account/reset_password/$1';
$route['account/activate/(:num)/(:any)'] = 'frontend/account/activate/$1/$2';
$route['account/event/(:any)'] = 'frontend/account/event/$1';
$route['account/ticketsdt/(:any)'] = 'frontend/account/ticketsdt/$1';

// $route['([a-z0-9\-]+)'] = 'frontend/page/view/$1';
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Page Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2014-2015, Robert Christian Obias
 * @link		
 */
class Page extends CI_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->dbutil();
		if (! $this->db->table_exists('pages'))
		{
			redirect('dashboard');
		}

		// check for dependencies
		if (! $this->db->table_exists('metatags'))
		{
			show_error('This page requires the Metatags module');
		}

		$this->load->driver('cache', $this->config->item('cache_drivers'));
		$this->load->model('website/navigations_model');
		$this->load->model('website/pages_model');
		$this->load->language('page');
	}
	
	// --------------------------------------------------------------------

	/**
	 * _remap
	 *
	 * @access	public
	 * @param	string $method
	 * @param	array $params
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function _remap($method, $params = array())
	{
		$this->$method($params);
	}

	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	private function index()
	{
		if ($page = $this->pages_model->find_by(array('page_uri' => 'home', 'page_status' => 'Posted', 'page_deleted' => 0)))
		{
			// page title
			$data['page_heading'] = $page->page_title;
			$data['record'] = $page;
		}
		else
		{
			redirect('notfound', 'refresh');
		}

		// template
		$this->template->set_template(config_item('website_theme').'_index');

		if ($this->input->post())
		{
			$this->form_validation->set_rules('email', 'Email Address', 'required|valid_email');

			$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

			if ($this->form_validation->run($this) !== FALSE)
			{
				$this->load->library('email');

				$config['mailtype'] = 'html';
				$this->email->initialize($config);
				$this->email->from($this->input->post("email"));
				$this->email->to("rchristian_obias@yahoo.com,evert.miranda@google.com");
				$this->email->subject("Event Registration Inquiry");
				$this->email->message(nl2br("New event registration inquiry from " . $this->input->post("email")));
				$this->email->send();

				// alert success status
				$this->template->add_js('alertify.success("' . lang('success') . '");', 'embed');
			}
		}
		
		// page layout
		$data['page_layout'] = $page->page_layout;

		// meta tags
		$this->load->model('metatags/metatags_model');
		$metatags = $this->metatags_model->get_metatags($page->page_metatag_id);
		
		$this->template->add_css(module_css('frontend', 'page_index'), 'embed');
		$this->template->add_js(module_js('frontend', 'page_index'), 'embed');
		$this->template->write('head', $metatags);
		$this->template->write_view('content', 'page_index', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * view
	 *
	 * @access	public
	 * @param	string $slug
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	private function view($params)
	{
		if (ENVIRONMENT == 'production')
		{
			$this->output->cache(5);
		}

		// should have at least one param
		if (count($params) === 0) redirect('notfound', 'refresh');

		// combine the slugs
		$uri = implode('/', $params);

		// if homepage
		if ($uri == 'home')
		{
			redirect('', 'refresh');
		}

		// get the page content
		if ($page = $this->pages_model->find_by(array('page_uri' => $uri, 'page_status' => 'Posted', 'page_deleted' => 0)))
		{
			// page title
			$data['page_heading'] = $page->page_title;
			$data['record'] = $page;
		}
		else
		{
			redirect('notfound', 'refresh');
		}

		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$crumbs = $this->pages_model->get_page_crumbs($page->page_id);
		if ($crumbs)
		{
			foreach ($crumbs as $crumb)
			{
				$this->breadcrumbs->push($crumb['name'], site_url($crumb['uri']));
			}
		}

		// page layout
		$data['page_layout'] = $page->page_layout;

		// template
		$this->template->set_template(config_item('website_theme'));

		// meta tags
		if (isset($page->page_metatag_id) && ($page->page_metatag_id))
		{
			$this->load->model('metatags/metatags_model');
			$metatags = $this->metatags_model->get_metatags($page->page_metatag_id);
			$this->template->write('head', $metatags);
		}

		$this->template->add_css(module_css('frontend', 'page_view'), 'embed');
		$this->template->add_js(module_js('frontend', 'page_view'), 'embed');
		$this->template->write_view('content', 'page_view', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * notfound
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function notfound($params = FALSE)
	{
		// page title
		$data['page_heading'] = lang('not_found_heading');

		// page layout
		$data['page_layout'] = 'full_width';

		// template
		$this->template->set_template(config_item('website_theme'));
		$this->template->write_view('content', 'page_notfound', $data);
		$this->template->render();
	}
}

/* End of file Page.php */
/* Location: ./application/modules/page/controllers/Page.php */
<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Event Class
 *
 * @package		rcmediaph
 * @version		1.1
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @author      Robert Christian Obias <robert.obias@google.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link		https://www.google.com
 */

//install from composer
//experimental
use Omnipay\Omnipay;

class Event extends CI_Controller
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
    public $paypal_url;
    public $paypal_email;
    public $paypal_api_user;
    public $paypal_api_pass;
    public $paypal_api_signature;
    public $paypal_test_mode = FALSE;
    
    function __construct()
	{
		parent::__construct();

		$this->load->model('microsites/microsites_model');
		$this->load->model('events/tickets_model');
        $this->load->model('events/ticket_types_model');
        $this->load->model('events/coupons_model');
        $this->load->model('events/orders_model');
        $this->load->model('payments/payments_model');
        $this->load->model('payments/payment_banks_model');
        $this->load->model('payments/payment_paypal_model');

        $this->load->library('recaptcha');
        $this->load->library('user_agent');
		$this->load->language('event');
        
        $this->config->load('paypal');
        
        $this->paypal_url   = $this->config->item('live_url');
        $this->paypal_email = $this->config->item('live_business');
        $this->paypal_api_user = $this->config->item('live_api_user');
        $this->paypal_api_pass = $this->config->item('live_api_pass');
        $this->paypal_api_signature = $this->config->item('live_api_signature');
        
        if($this->config->item('sandbox') == TRUE)
        {
            $this->paypal_url   = $this->config->item('sandbox_url');
            $this->paypal_email = $this->config->item('sandbox_business');
            $this->paypal_api_user      = $this->config->item('sandbox_api_user');
            $this->paypal_api_pass      = $this->config->item('sandbox_api_pass');
            $this->paypal_api_signature = $this->config->item('sandbox_api_signature');
            $this->paypal_test_mode = TRUE;
        }

	}

	// --------------------------------------------------------------------

	/**
	 * view
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
     *          Robert Christian Obias <robert.obias@google.com>
	 */
	public function view($slug)
	{

        // unset user data
        $userdata = array(
                        'coupon_id', 'coupon_code', 'coupon_discount_type',
                        'coupon_discount', 'coupon_used', 'coupon_discount_total', 'ticket_discount', 
                        'ticket_amount', 'ticket_type_amount', 'addon_ticket_total',
        );

        $this->session->unset_userdata($userdata);

		$microsite = $this->microsites_model->get_microsite($slug);
        $ticket_types = $this->ticket_types_model->get_ticket_types($microsite->event_id);


		if (! $microsite) show_404();

		$data['microsite']    = $microsite;
        $data['ticket_types'] = $ticket_types;
		$data['page_heading'] = 'Home';

		$this->template->set_template($microsite->microsite_theme_code);
        $this->template->add_js("var event_url= '".site_url('event')."'; ", 'embed');
        $this->template->add_js("var slug= '".$slug."'; ", 'embed');
		$this->template->add_css(module_css('frontend', 'event_view'), 'embed');
		$this->template->add_js(module_js('frontend', 'event_view'), 'embed');

		$this->template->write_view('content', 'event_view', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * register
	 *
	 * @access	public
	 * @param	string $slug
	 * @param	string $status
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 * @author 	JP Llapitan <john.llapitan@google.com>
     * @author Robert Christian Obias <robert.obias@google.com>
	 */
	public function register($slug, $status=NULL)
	{
        
		$microsite = $this->microsites_model->get_microsite($slug);
        $referrer  = pathinfo($this->agent->referrer());

		if (! $microsite) show_404();

		$data['microsite'] = $microsite;
		$data['page_heading'] = 'Register';

		// TODO: get these fields from DB
		if ($slug == 'legion-run-manila-2017')
		{
			$data['fields'] = array(
				// 'ticket_fullname',
				'ticket_firstname',
				'ticket_middlename',
				// 'ticket_middle_initial',
				'ticket_lastname',
				// 'ticket_nickname',
				'ticket_email',
				'ticket_mobile',
				'ticket_phone',
				'ticket_gender',
				'ticket_birthday',
				'ticket_mstatus',
				'ticket_address',
				'ticket_city',
				'ticket_zipcode',
				'ticket_country',
				'ticket_company',
				'ticket_occupation',
				'ticket_work_phone',

				'ticket_medical_conditions',
				//'ticket_is_pwd',
				//'ticket_is_senior',
				'ticket_emergency_contact',
				'ticket_emergency_number',
				'ticket_emergency_relationship',

				'ticket_custom_select_1',
				'ticket_newsletter',
				'ticket_terms',
                'ticket_custom_text_1',
                'g-recaptcha-response'
			);

			// TODO: remove these when CMS is ready
			$data['ticket_custom_select_1']         = 'T-shirt Size';
			$data['ticket_custom_select_options_1'] = ',XS,S,M,L,XL,XXL';
            $data['ticket_custom_text_1']           = 'Group Code';
            //$data['paypal_url']   = $this->paypal_url;
            //$data['paypal_email'] = $paypal_email;
		}
		else if ($slug == 'true-life-2016')
		{
			$data['fields'] = array(
				// 'ticket_fullname',
				'ticket_firstname',
				// 'ticket_middlename',
				'ticket_middle_initial',
				'ticket_lastname',
				'ticket_nickname',
				'ticket_email',
				'ticket_mobile',
				'ticket_phone',
				'ticket_gender',
				'ticket_birthday',
				'ticket_mstatus',
				'ticket_address',
				'ticket_city',
				'ticket_zipcode',
				'ticket_country',

				'ticket_company',
				'ticket_work_address',
				'ticket_occupation',
				'ticket_work_phone',

				// 'ticket_medical_conditions',
				// 'ticket_is_pwd',
				// 'ticket_is_senior',
				// 'ticket_emergency_contact',
				// 'ticket_emergency_number',

				// 'ticket_tshirt_size',
				// 'ticket_newsletter',
                'ticket_terms',

				'ticket_custom_text_1',
				'ticket_custom_text_2',
				'ticket_custom_text_3',
				'ticket_custom_select_1',
				'ticket_custom_select_2',
				'ticket_custom_select_3',
				'ticket_custom_text_4',
				'ticket_custom_text_5',
				'ticket_custom_text_6'
			);

			// TODO: remove these when CMS is ready
			$data['ticket_custom_text_1'] = 'Religion';
			$data['ticket_custom_text_2'] = 'Name of Church';
			$data['ticket_custom_text_3'] = 'Since When';
			$data['ticket_custom_text_4'] = 'D-Group Leader';
			$data['ticket_custom_text_5'] = 'Who invited you to this encounter?';
			$data['ticket_custom_text_6'] = 'Contact number of person who invited you';
			$data['ticket_custom_select_1'] = 'CCF Member?';
			$data['ticket_custom_select_2'] = 'Is this your first time to attend CCF';
			$data['ticket_custom_select_3'] = 'CCF activities you are attending';
			$data['ticket_custom_select_options_1'] = 'CCF Member,Guest';
			$data['ticket_custom_select_options_2'] = ',Yes,No';
			$data['ticket_custom_select_options_3'] = ',Worship Service,Discipleship Group';
		}

		// TODO: add post processing here
    
            if ($this->input->post('submit'))
            {
                if ($order_id = $this->_save($data))
                {
                    $this->session->set_flashdata('order_id', $order_id);
                    redirect(site_url('event/register/' . $slug . '/success'), 'refresh');
                }
            }

            $data['slug']       = 'legion-run-manila-2016';
            $data['payments']   = $this->payments_model->get_payment_methods(json_decode($microsite->event_payment_id));


		// get the countries
		$this->load->model('locations/countries_model');
		$data['countries'] = $this->countries_model->format_dropdown('country_name', 'country_name', TRUE);
        
        // Display bag check if registrants reach 100
        $data['total_tickets'] = $this->tickets_model->count_by(array(
                                                    'ticket_deleted'      => '0',
                                                    'ticket_event_id'     => $microsite->microsite_event_id,
                                                    'ticket_coupon_id !=' => '1' // Presslaunch
                                                ));
        
		if ($status == 'success')
		{
            // Destroy coupon session
            $this->session->unset_userdata('coupon_id');
            $this->session->unset_userdata('coupon_code');
            $this->session->unset_userdata('coupon_discount_type');
            $this->session->unset_userdata('coupon_discount');
            $this->session->unset_userdata('coupon_used');

			$this->template->set_template($microsite->microsite_theme_code);
			$this->template->add_css('assets/plugins/jquery-ui/jquery-ui.min.css');
			$this->template->add_js('assets/plugins/jquery-ui/jquery-ui.min.js');
			$this->template->add_js('themes/frontend/starter/js/social-plugins.js');
			$this->template->add_css(module_css('frontend', 'event_register_success'), 'embed');
			$this->template->add_js(module_js('frontend', 'event_register_success'), 'embed');
			$this->template->write_view('content', 'event_register_success', $data);
			$this->template->render();

		}
		else
		{

            if($slug == 'legion-run-manila-2017') // Change this
            {
                if($referrer['basename'] != 'legion-run-manila-2017') // Change or remove this
                {
                    redirect(site_url('event/' . $slug));
                }
            }

			$this->template->set_template($microsite->microsite_theme_code);
			$this->template->add_css('assets/plugins/jquery-ui/jquery-ui.min.css');
			$this->template->add_js('assets/plugins/jquery-ui/jquery-ui.min.js');
			$this->template->add_js('assets/plugins/blockUI/jquery.blockUI.js');
			$this->template->add_js('themes/frontend/starter/js/social-plugins.js');

            $this->template->add_js("var event_url= '".site_url('event')."'; ", 'embed');
            $this->template->add_js("var slug= '".$slug."'; ", 'embed');

			$this->template->add_css(module_css('frontend', 'event_register'), 'embed');
			$this->template->add_js(module_js('frontend', 'event_register'), 'embed');
			$this->template->write_view('content', 'event_register', $data);
			$this->template->render();
		}
	}

	/**
	 * register
	 *
	 * @access	public
	 * @param	string $slug
	 * @author 	JP Llapitan <john.llapitan@google.com>
	 */
	public function _save($data)
	{
		$microsite = $data["microsite"];

		$reference_no = strtotime('now');

        // Check if coupon was claimed
		foreach ($data['fields'] as $key => $value) {

			if (strpos($value, 'email') !== FALSE)
			{
				$this->form_validation->set_rules($value, lang($value), 'required|valid_email');
			}
			else if (strpos($value, 'firstname') !== FALSE OR strpos($value, 'lastname') !== FALSE OR strpos($value, 'mobile') !== FALSE)
			{
				$this->form_validation->set_rules($value, lang($value), 'required');
			}
			else if (strpos($value, 'terms') !== FALSE)
			{
				$this->form_validation->set_rules($value, lang($value), 'required');
				//$this->form_validation->set_message('required', 'You must agree to the terms and conditions before registering');
			}
			else
			{
				// $this->form_validation->set_rules($value, lang($value), 'required');
			}
		}

        $this->form_validation->set_message('required', '%s is required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        #$recaptcha = $this->input->post('g-recaptcha-response');
        #$response  = $this->recaptcha->verifyResponse($recaptcha);


		if($this->form_validation->run($this) == FALSE)
		{
			return FALSE;

		} else {

            $data = array(
                    'ticket_id'             => uniqid(),
                    'ticket_event_id' 		=> $data['microsite']->event_id,
                    'ticket_type_id' 		=> $data['microsite']->event_id,
                    'ticket_coupon_id'      => ($this->session->userdata('coupon_id') != '') ? $this->session->userdata('coupon_id') : '', // Ticket Coupon ID
                    // 'ticket_schedule_id' => $data['microsite']->schedule_id,
                    'ticket_schedule_id' 	=> NULL,

                    'ticket_reference_no' 	=> $reference_no,

                    'ticket_fullname' 		=> $this->input->post('ticket_fullname') ? $this->input->post('ticket_fullname') : '',
                    'ticket_firstname' 		=> $this->input->post('ticket_firstname') ? $this->input->post('ticket_firstname') : '',
                    'ticket_middlename' 	=> $this->input->post('ticket_middlename') ? $this->input->post('ticket_middlename') : '',
                    'ticket_middle_initial' => $this->input->post('ticket_middle_initial') ? $this->input->post('ticket_middle_initial') : '',
                    'ticket_lastname' 		=> $this->input->post('ticket_lastname') ? $this->input->post('ticket_lastname') : '',
                    'ticket_nickname' 		=> $this->input->post('ticket_nickname') ? $this->input->post('ticket_nickname') : '',
                    'ticket_email' 			=> $this->input->post('ticket_email') ? $this->input->post('ticket_email') : '',
                    'ticket_mobile' 		=> $this->input->post('ticket_mobile') ? $this->input->post('ticket_mobile') : '',
                    'ticket_phone' 			=> $this->input->post('ticket_phone') ? $this->input->post('ticket_phone') : '',
                    'ticket_gender' 		=> $this->input->post('ticket_gender') ? $this->input->post('ticket_gender') : '',
                    'ticket_birthday' 		=> $this->input->post('ticket_birthday') ? $this->input->post('ticket_birthday') : '',
                    'ticket_mstatus' 		=> $this->input->post('ticket_mstatus') ? $this->input->post('ticket_mstatus') : '',
                    'ticket_address' 		=> $this->input->post('ticket_address') ? $this->input->post('ticket_address') : '',
                    'ticket_city' 			=> $this->input->post('ticket_city') ? $this->input->post('ticket_city') : '',
                    'ticket_zipcode' 		=> $this->input->post('ticket_zipcode') ? $this->input->post('ticket_zipcode') : '',
                    'ticket_country' 		=> $this->input->post('ticket_country') ? $this->input->post('ticket_country') : '',

                    'ticket_company' 		=> $this->input->post('ticket_company') ? $this->input->post('ticket_company') : '',
                    'ticket_work_address' 	=> $this->input->post('ticket_work_address') ? $this->input->post('ticket_work_address') : '',
                    'ticket_occupation' 	=> $this->input->post('ticket_occupation') ? $this->input->post('ticket_occupation') : '',
                    'ticket_work_phone' 	=> $this->input->post('ticket_work_phone') ? $this->input->post('ticket_work_phone') : '',

                    'ticket_medical_conditions'		=> $this->input->post('ticket_medical_conditions') ? $this->input->post('ticket_medical_conditions') : '',
                    'ticket_is_pwd' 				=> $this->input->post('ticket_is_pwd') ? $this->input->post('ticket_is_pwd') : '',
                    'ticket_is_senior' 				=> $this->input->post('ticket_is_senior') ? $this->input->post('ticket_is_senior') : '',
                    'ticket_emergency_contact' 		=> $this->input->post('ticket_emergency_contact') ? $this->input->post('ticket_emergency_contact') : '',
                    'ticket_emergency_number' 		=> $this->input->post('ticket_emergency_number') ? $this->input->post('ticket_emergency_number') : '',
                    'ticket_emergency_relationship' => $this->input->post('ticket_emergency_relationship') ? $this->input->post('ticket_emergency_relationship') : '',
                    'ticket_newsletter' 		=> $this->input->post('ticket_newsletter') ? $this->input->post('ticket_newsletter') : '',
                   //'ticket_terms'              => $this->input->post('ticket_terms') ? $this->input->post('ticket_terms') : '',


                    // 'ticket_custom_select_4'	=> $this->input->post('ticket_tshirt_size') ? $this->input->post('ticket_tshirt_size') : '',
                     #'ticket_custom_text_7' 		=> $this->input->post('ticket_newsletter') ? $this->input->post('ticket_newsletter') : '',
                     #'ticket_custom_text_8' 		=> $this->input->post('ticket_terms') ? $this->input->post('ticket_terms') : '',

                    'ticket_custom_text_1' 		=> $this->input->post('ticket_custom_text_1') ? $this->input->post('ticket_custom_text_1') : '',
                    'ticket_custom_text_2' 		=> $this->input->post('ticket_custom_text_2') ? $this->input->post('ticket_custom_text_2') : '',
                    'ticket_custom_text_3' 		=> $this->input->post('ticket_custom_text_3') ? $this->input->post('ticket_custom_text_3') : '',
                    'ticket_custom_text_4' 		=> $this->input->post('ticket_custom_text_4') ? $this->input->post('ticket_custom_text_4') : '',
                    'ticket_custom_text_5' 		=> $this->input->post('ticket_custom_text_5') ? $this->input->post('ticket_custom_text_5') : '',
                    'ticket_custom_text_6' 		=> $this->input->post('ticket_custom_text_6') ? $this->input->post('ticket_custom_text_6') : '',

                    'ticket_custom_select_1' 	=> $this->input->post('ticket_custom_select_1') ? $this->input->post('ticket_custom_select_1') : '',
                    'ticket_custom_select_2' 	=> $this->input->post('ticket_custom_select_2') ? $this->input->post('ticket_custom_select_2') : '',
                    'ticket_custom_select_3' 	=> $this->input->post('ticket_custom_select_3') ? $this->input->post('ticket_custom_select_3') : '',
                    'ticket_custom_select_4' 	=> $this->input->post('ticket_custom_select_4') ? $this->input->post('ticket_custom_select_4') : '',
                    'ticket_custom_select_5' 	=> $this->input->post('ticket_custom_select_5') ? $this->input->post('ticket_custom_select_5') : '',
                    'ticket_custom_select_6' 	=> $this->input->post('ticket_custom_select_6') ? $this->input->post('ticket_custom_select_6') : '',
                    'ticket_status'             => 'Complimentary' // Change when going public
                );



                // insert record
                $this->tickets_model->insert($data);

                if($this->session->userdata('coupon_id'))
                {
                    // Update coupon used
                    $this->coupons_model->update(array(
                        'coupon_id' => $this->session->userdata('coupon_id')
                    ), array(
                        'coupon_used' => $this->session->userdata('coupon_used') + 1,
                    ));
                }

                // send email confirmation
                // $this->_email($microsite->event_reg_email_from, $microsite->event_name, $this->input->post('ticket_email'), $this->input->post('ticket_firstname'), $microsite->event_reg_email_subject);

                $this->load->library('email');

                //$this->email->initialize($config);
                $this->email->from($microsite->event_reg_email_from, $microsite->event_name);
                $this->email->to($this->input->post('ticket_email'));
                // $this->email->cc('another@another-example.com');
                $this->email->bcc($microsite->event_reg_email_from);

                $this->email->subject($microsite->event_reg_email_subject . ' - Reference No: ' . $reference_no);

                $this->email->message(

                        $this->_email(
                            $microsite->microsite_slug, // microsite slug
                            $microsite->event_reg_email_content, // microsite email template
                            $data, // user information
                            ($this->input->post('coupon_code') ? $this->input->post('coupon_code') : '') // coupon code
                        )
                );
                

                $this->email->send();
                
               
                return TRUE;

		}

		return FALSE;
	}

	/**
	 * terms
	 *
	 * @access	public
	 * @param	string $slug
	 * @author 	JP Llapitan <john.llapitan@google.com>
	 */
	public function terms($slug)
	{
		$microsite = $this->microsites_model->get_microsite($slug);

		if (! $microsite) show_404();

		$data['microsite'] = $microsite;

		$data['header'] = 'Terms and Conditions';
		$data['content'] = $microsite->microsite_terms_and_conditions;

		$data['page_heading'] = 'Terms and Conditions';

		//$this->template->set_template($microsite->microsite_theme_code);

        $this->template->set_template('modal');

		//$this->template->add_css('assets/plugins/jquery-ui/jquery-ui.min.css');
		//$this->template->add_js('assets/plugins/jquery-ui/jquery-ui.min.js');
		$this->template->add_css(module_css('frontend', 'event_static_page'), 'embed');
		$this->template->add_js(module_js('frontend', 'event_static_page'), 'embed');
        $this->template->write_view('content', 'event_terms', $data);
		//$this->template->write_view('content', 'event_static_page', $data);
		$this->template->render();
	}


    // TODO
    // Change
    public function terms_of_service($slug)
	{
		$microsite = $this->microsites_model->get_microsite($slug);

		if (! $microsite) show_404();

		$data['microsite'] = $microsite;

		$data['header'] = 'Terms of Service';
		$data['content'] = $microsite->microsite_terms_of_service;

		$data['page_heading'] = 'Terms of Service';

		//$this->template->set_template($microsite->microsite_theme_code);

        $this->template->set_template('modal');

		//$this->template->add_css('assets/plugins/jquery-ui/jquery-ui.min.css');
		//$this->template->add_js('assets/plugins/jquery-ui/jquery-ui.min.js');
		$this->template->add_css(module_css('frontend', 'event_static_page'), 'embed');
		$this->template->add_js(module_js('frontend', 'event_static_page'), 'embed');
        $this->template->write_view('content', 'event_terms_of_service', $data);
		//$this->template->write_view('content', 'event_static_page', $data);
		$this->template->render();
	}



	/**
	 * privacy
	 *
	 * @access	public
	 * @param	string $slug
	 * @author 	JP Llapitan <john.llapitan@google.com>
	 */
	public function privacy($slug)
	{
		$microsite = $this->microsites_model->get_microsite($slug);

		if (! $microsite) show_404();

		$data['microsite'] = $microsite;

		$data['header'] = 'Privacy Policy';
		$data['content'] = $microsite->microsite_privacy_policy;

		$data['page_heading'] = 'Privacy Policy';

		$this->template->set_template($microsite->microsite_theme_code);
		$this->template->add_css('assets/plugins/jquery-ui/jquery-ui.min.css');
		$this->template->add_js('assets/plugins/jquery-ui/jquery-ui.min.js');
		$this->template->add_css(module_css('frontend', 'event_static_page'), 'embed');
		$this->template->add_js(module_js('frontend', 'event_static_page'), 'embed');
		$this->template->write_view('content', 'event_static_page', $data);
		$this->template->render();
	}

    /**
	 * process_event_coupon
	 *
	 * @access	public
	 * @param	none
	 * @author  Robert Christian Obias <robert.obias@google.com>
	 */
    public function process_event_coupon()
    {
    	is_ajax();

        $ticket_amount = $this->session->userdata('ticket_type_amount');
        $ticket_qty    = $this->session->userdata('ticket_qty');
        $ticket_total  = ($this->session->userdata('addon_ticket_total')) ? $this->session->userdata('addon_ticket_total') : $ticket_amount * $ticket_qty;
        $bag_check     = $this->session->userdata('bag_check');

        $this->form_validation->set_rules('coupon_code', lang('coupon_code'), 'required');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

        if ($this->form_validation->run($this) == FALSE)
        {
            $response = array(
                'success'         => FALSE,
                'message'         => 'Please correct or provide the necessary information.',
                'error'           => form_error('coupon_code'),
                'ticket_qty'      => $ticket_qty,
                'ticket_discount' => '0.00',
                'ticket_total'    => number_format($ticket_total, 2),
            );

		    // Recheck this
            // Unset userdata
            //$userdata = array(
            //    'ticket_total', 'ticket_discount'
            //);
            //$this->session->unset_userdata($userdata);

            $order_data = array(
                'coupon_id'            => '',
                'ticket_discount'      => '0.00',
                'ticket_qty'           => $ticket_qty,
                'ticket_amount'        => number_format($ticket_amount, 2),
                'ticket_total'         => number_format($ticket_total, 2)
            );
		}
        else
        {

            $now = date('Y-m-d');

            // Check if coupon exist for the event
            $microsite = $this->microsites_model->select('microsite_event_id')
                                                ->where('microsite_deleted', 0)
                                                ->find_by(array('microsite_slug' => $this->input->post('slug')));

												// Check if coupon exist in the coupon
		   	$coupon = $this->coupons_model->where("DATE(NOW()) BETWEEN DATE(coupon_start_date) AND DATE(coupon_end_date)")
										  ->find_by(
											   array(
												   'coupon_event_id' => $microsite->microsite_event_id,
												   'coupon_code'     => strtoupper($this->input->post('coupon_code')),
											   ));

			if($coupon) // Event has a valid coupon
		  	{
                
                $remaining_coupon =  $coupon->coupon_limit - $coupon->coupon_used;

			  	// Check if coupon has enough limit
			  	if($remaining_coupon > $ticket_qty)
			  	{
                    
				  	$coupon_discount = $coupon->coupon_discount;
				   	if($coupon->coupon_discount_type == 'Percentage')
				   	{
					   	$coupon_discount = $ticket_amount * ($coupon->coupon_discount / 100);
				   	}
                                        
                    $ticket_discount = $ticket_qty * $coupon_discount;
                    $ticket_total    = $ticket_total - $ticket_discount;
                    
                    if($ticket_total < 0) // Prevent negative total
                    {
                        $ticket_total = 0;
                    }
                    
                    if($this->session->userdata('bag_check') != '0')
                    {
                        $ticket_total = $ticket_total + $this->session->userdata('bag_check');
                    }
          
                    $response = array(
                        'ticket_qty'      => $ticket_qty,
                        'ticket_discount' => number_format($ticket_discount, 2),
                        'ticket_total'    => number_format($ticket_total, 2),
                        'success'         => TRUE,
                        'message'         => lang('valid_coupon')
                    );

                    //$response['coupon_discount_total'] = $coupon_discount_total;
				   	//$response['ticket_amount']         = number_format($this->session->userdata('ticket_type_amount'), 2);
				   	//$response['ticket_coupon']         = number_format($coupon_discount, 2);
				   	//$response['ticket_total']          = number_format($ticket_total - $coupon_discount_total, 2);

				   	//$response['success']    = TRUE;
				   	//$response['message']    = lang('valid_coupon');

                    // Add to session for processing
                    $order_data = array(
                        'coupon_id'             => $coupon->coupon_id,
                        'coupon_code'           => $coupon->coupon_code,
                        'coupon_discount_type'  => $coupon->coupon_discount_type,
                        'coupon_discount'       => $coupon->coupon_discount,
                        'coupon_used'           => $coupon->coupon_used,
                        'coupon_discount_total' => $ticket_discount,
                        'ticket_qty'            => $ticket_qty,
                        'ticket_discount'       => number_format($ticket_discount, 2),
                        'ticket_total'          => number_format($ticket_total, 2)
                    );
			   	}
				else
				{
                    $response = array(
                        'success'         => FALSE,
                        'message'         => lang('invalid_coupon'),
                        'ticket_qty'      => $ticket_qty,
                        'ticket_discount' => '0.00',
                        'ticket_total'    => number_format($ticket_total, 2)
                    );

					#$response['success']      = FALSE;
				  	#$response['message']      = lang('invalid_coupon');
				  	#$response['ticket_total'] = number_format($ticket_total, 2);

                    // Add to session for processing
                    $order_data = array(
                        'coupon_id'            => '',
                        //'coupon_code'          => $coupon->coupon_code,
                        //'coupon_discount_type' => $coupon->coupon_discount_type,
                        //'coupon_discount'      => $coupon->coupon_discount,
                        //'coupon_used'          => $coupon->coupon_used,
                        'ticket_amount'       => number_format($ticket_amount, 2),
                        'ticket_discount'     => '0.00',
                        'ticket_total'        => number_format($ticket_total, 2)
                    );
			  	}


        }
        else
        {
            $response['success']         = FALSE;
            $response['message']         = lang('invalid_coupon');
            $response['ticket_discount'] = '0.00';
            $response['ticket_total']    = number_format($ticket_total, 2);

            // Add to session for processing
            $order_data = array(
                'coupon_id'            => '',
                'ticket_discount'      => '0.00',
                'ticket_qty'           => $ticket_qty,
                'ticket_amount'        => number_format($ticket_amount, 2),
                'ticket_total'         => number_format($ticket_total, 2)
            );

            $this->session->unset_userdata('co'
                . 'upon_code');
        }
	  }

	  $this->session->set_userdata($order_data);

	  echo json_encode($response);

    }

    /**
	 * process_get_ticket
	 *
	 * @access	public
	 * @param	none
	 * @author  Robert Christian Obias <robert.obias@google.com>
	 */
    public function process_get_ticket()
    {
        is_ajax();

        // Add to session data ticket types
        // Change to add to cart

        $ticket_id  = (int) $this->input->post('ticket_id');
        $ticket_qty = (int) $this->input->post('ticket_qty');
        $type       = $this->ticket_types_model->find($ticket_id);

        $this->session->set_userdata(array(
            'ticket_qty'              => $ticket_qty,
            'ticket_type_id'          => $type->ticket_type_id,
            'ticket_type_name'        => $type->ticket_type_name,
            'ticket_type_description' => $type->ticket_type_description,
            'ticket_type_currency'    => $type->ticket_type_currency,
            'ticket_type_amount'      => $type->ticket_type_amount,
            'ticket_discount'         => '0.00',
            'ticket_total'            => number_format($type->ticket_type_amount * $ticket_qty, 2),
        ));

        $response['success'] = TRUE;
        $response['redirect'] = site_url('event/register/'.$this->input->post('slug'));

        echo json_encode($response);

    }

    /**
	 * _email
	 *
	 * @access	public
	 * @param	string
	 * @param	string
	 * @param	array
	 * @param	string
	 * @author  JP Llapitan <robert.obias@google.com>
	 */
	private function _email($microsite_slug, $email_template, $registrant_info=FALSE, $coupon_code=FALSE, $first_registrant = FALSE, $status=FALSE)
	{
		$microsite       = $this->microsites_model->get_microsite($microsite_slug);
		$registrant_info = (object)$registrant_info;
        
        $email_data = array(
            'microsite'        => $microsite,
            'registrant_info'  => $registrant_info,
            'email_template'   => $email_template,
            'first_registrant' => $first_registrant,
            'coupon_code'      => $coupon_code, 
            'status'           => $status
        );
        
        $email_tpl = $this->load->view('event_email', $email_data, TRUE);

		
		return $email_tpl;
	}

    // Experimental

    public function test_email()
    {
        
        $ticket_id = array('5831645fa3475', '5831645f893f8');
        
        foreach($ticket_id as $val)
        {
            $this->generate_pdf($val);
        }
        
//        $gateway = Omnipay::create('PayPal_Express');
//        $gateway->setUsername('robert_mechant_api1.yahoo.com');
//        $gateway->setPassword('4JLASLLEPLGN8M8M');
//        $gateway->setSignature('AFcWxV21C7fd0v3bYYYRCpSSRl31At6IEihnLfB4zl.HPKZDuDDRtOUc');
//        $gateway->setTestMode(true);
//        
//        echo 'start';
//        $params = array(
//            'amount'        => '1500.00',
//            'currency'      => 'PHP',
//            'description'   => 'test purchase',
//            'transactionId' => 'aasdsd',
//            'transactionReference' => 'dfsssd',
//            'returnUrl'            => site_url('event/success/legion-run-manila-2017'),
//            'cancelUrl'            => site_url('event/cancel/legion-run-manila-2017')
//         );
//
//        echo 'end';
//        $response = $gateway->purchase($params)->send();
//        if ($response->isRedirect()) {
//            $response->redirect();
//        } else {
//            echo "bugger";
//            var_dump($response);
//            exit;
//        }
    }


    public function process_register()
    {
        $post_data  = $this->input->post();
        $ticket_qty = $this->session->userdata('ticket_qty');
        
        if($post_data)
        {
            for($i=1;$i<=$ticket_qty;$i++)
            {
                // Check if fields are included in the array
                // Make this dynamic
                if (isset($post_data['ticket']['email']))
                {
                    $this->form_validation->set_rules('ticket[email]['.$i.']', lang('ticket_email'), 'required|valid_email');
                }

                if (isset($post_data['ticket']['firstname']))
                {
                    $this->form_validation->set_rules('ticket[firstname]['.$i.']', lang('ticket_firstname'), 'required');
                }

                if (isset($post_data['ticket']['lastname']))
                {
                    $this->form_validation->set_rules('ticket[lastname]['.$i.']', lang('ticket_lastname'), 'required');
                }

                if (isset($post_data['ticket']['mobile']))
                {
                    $this->form_validation->set_rules('ticket[mobile]['.$i.']', lang('ticket_mobile'), 'required');
                }

                if (!isset($post_data['ticket']['terms'][$i]))
                {
                    $this->form_validation->set_rules('ticket[terms]['.$i.']', lang('ticket_terms'), 'required');
                }
            }
            
            
            
            if($this->input->post('slug') != 'true-life-2016')  // make dynamic
            {
                if (!$post_data['captcha'])
                {
                    $this->form_validation->set_rules('captcha', lang('captcha'), 'required');
                }
            }

            $this->form_validation->set_message('required', '%s is required');
            $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

            $recaptcha         = $this->input->post('captcha');
            $captcha_response  = $this->recaptcha->verifyResponse($recaptcha);

            if($this->form_validation->run($this) == FALSE)
            {
                $response['success'] = FALSE;
                $response['message'] = lang('validation_error');
                for($i=1;$i<=$ticket_qty;$i++)
                {
                    $errors[] = array(
                        'ticket_no'         => $i,
                        'ticket_firstname'  => form_error('ticket[firstname]['.$i.']'),
                        'ticket_lastname'   => form_error('ticket[lastname]['.$i.']'),
                        'ticket_email'      => form_error('ticket[email]['.$i.']'),
                        'ticket_mobile'     => form_error('ticket[mobile]['.$i.']'),
                        'ticket_terms'      => form_error('ticket[terms]['.$i.']'),
                    );
                }

                $captcha = array(
                    'captcha' => form_error('captcha')
                );

                $response['errors'] = $errors;
                $response['captcha'] = form_error('captcha');

            } else {
                
                if (isset($captcha_response['success']) AND $captcha_response['success'] === true) // Valid captcha
                {
                    $ticket_reference_no = strtotime('now');

                    // Insert to tickets
                    for($i=1;$i<=$ticket_qty;$i++)
                    {
                        $data = array(
                            'ticket_id'             => uniqid(),
                            'ticket_event_id' 		=> $post_data['event_id'],
                            'ticket_type_id' 		=> $this->session->userdata('ticket_type_id'),
                            'ticket_coupon_id'      => ($this->session->userdata('coupon_id') != '') ? $this->session->userdata('coupon_id') : '', // Ticket Coupon ID
                            'ticket_schedule_id' 	=> NULL,
                            'ticket_reference_no' 	=> $ticket_reference_no,
                            'ticket_fullname' 		=> isset($post_data['ticket']['fullname'][$i]) ? $post_data['ticket']['fullname'][$i] : '',
                            'ticket_firstname' 		=> isset($post_data['ticket']['firstname'][$i]) ? $post_data['ticket']['firstname'][$i] : '',
                            'ticket_middlename' 	=> isset($post_data['ticket']['middlename'][$i]) ? $post_data['ticket']['middlename'][$i] : '',
                            'ticket_middle_initial' => isset($post_data['ticket']['middle_initial'][$i]) ? $post_data['ticket']['middle_initial'][$i] : '',
                            'ticket_lastname' 		=> isset($post_data['ticket']['lastname'][$i]) ? $post_data['ticket']['lastname'][$i] : '',
                            'ticket_nickname' 		=> isset($post_data['ticket']['nickname'][$i]) ? $post_data['ticket']['nickname'][$i] : '',
                            'ticket_email' 			=> isset($post_data['ticket']['email'][$i]) ?  $post_data['ticket']['email'][$i] : '',
                            'ticket_mobile' 		=> isset($post_data['ticket']['mobile'][$i]) ?  $post_data['ticket']['mobile'][$i] : '',
                            'ticket_phone' 			=> isset($post_data['ticket']['phone'][$i]) ?  $post_data['ticket']['phone'][$i] : '',
                            'ticket_gender' 		=> isset($post_data['ticket']['gender'][$i]) ?  $post_data['ticket']['gender'][$i] : '',
                            'ticket_birthday' 		=> isset($post_data['ticket']['birthday'][$i]) ?  $post_data['ticket']['birthday'][$i] : '',
                            'ticket_mstatus' 		=> isset($post_data['ticket']['mstatus'][$i]) ?  $post_data['ticket']['mstatus'][$i] : '',
                            'ticket_address' 		=> isset($post_data['ticket']['address'][$i]) ?  $post_data['ticket']['address'][$i] : '',
                            'ticket_city' 			=> isset($post_data['ticket']['city'][$i]) ?  $post_data['ticket']['city'][$i] : '',
                            'ticket_zipcode' 		=> isset($post_data['ticket']['zipcode'][$i]) ?  $post_data['ticket']['zipcode'][$i] : '',
                            'ticket_country' 		=> isset($post_data['ticket']['country'][$i]) ?  $post_data['ticket']['country'][$i] : '',
                            'ticket_company' 		=> isset($post_data['ticket']['company'][$i]) ?  $post_data['ticket']['company'][$i] : '',
                            'ticket_work_address' 	=> isset($post_data['ticket']['work_address'][$i]) ?  $post_data['ticket']['work_address'][$i] : '',
                            'ticket_occupation' 	=> isset($post_data['ticket']['occupation'][$i]) ?  $post_data['ticket']['occupation'][$i] : '',
                            'ticket_work_phone' 	=> isset($post_data['ticket']['work_phone'][$i]) ?  $post_data['ticket']['work_phone'][$i] : '',

                            'ticket_medical_conditions'		=> isset($post_data['ticket']['medical_conditions'][$i]) ?  $post_data['ticket']['medical_conditions'][$i] : '',
                            'ticket_is_pwd' 				=> isset($post_data['ticket']['is_pwd'][$i]) ?  $post_data['ticket']['is_pwd'][$i] : '',
                            'ticket_is_senior' 				=> isset($post_data['ticket']['is_senior'][$i]) ?  $post_data['ticket']['is_senior'][$i] : '',
                            'ticket_emergency_contact' 		=> isset($post_data['ticket']['emergency_contact'][$i]) ?  $post_data['ticket']['emergency_contact'][$i] : '',
                            'ticket_emergency_number' 		=> isset($post_data['ticket']['emergency_number'][$i]) ?  $post_data['ticket']['emergency_number'][$i] : '',
                            'ticket_emergency_relationship' => isset($post_data['ticket']['emergency_relationship'][$i]) ?  $post_data['ticket']['emergency_relationship'][$i] : '',
                            'ticket_newsletter' 		    => isset($post_data['ticket']['newsletter'][$i]) ? $post_data['ticket']['newsletter'][$i] : '',
                            'ticket_bag_check'              => isset($post_data['ticket']['bag_check'][$i]) ? $post_data['ticket']['bag_check'][$i] : 'No', // Separate as addons
                            'ticket_custom_select_4'	=> isset($post_data['ticket']['tshirt_size'][$i])   ?  $post_data['ticket']['tshirt_size'][$i] : '',
                            'ticket_custom_text_7' 		=> isset($post_data['ticket']['newsletter'][$i])    ?  $post_data['ticket']['newsletter'][$i] : '',
                            'ticket_custom_text_8' 		=> isset($post_data['ticket']['terms'][$i])         ?  $post_data['ticket']['terms'][$i] : '',

                            'ticket_custom_text_1' 		=> isset($post_data['ticket']['custom_text_1'][$i]) ?  $post_data['ticket']['custom_text_1'][$i] : '',
                            'ticket_custom_text_2' 		=> isset($post_data['ticket']['custom_text_2'][$i]) ?  $post_data['ticket']['custom_text_2'][$i] : '',
                            'ticket_custom_text_3' 		=> isset($post_data['ticket']['custom_text_3'][$i]) ?  $post_data['ticket']['custom_text_3'][$i] : '',
                            'ticket_custom_text_4' 		=> isset($post_data['ticket']['custom_text_4'][$i]) ?  $post_data['ticket']['custom_text_4'][$i] : '',
                            'ticket_custom_text_5' 		=> isset($post_data['ticket']['custom_text_5'][$i]) ?  $post_data['ticket']['custom_text_5'][$i] : '',
                            'ticket_custom_text_6' 		=> isset($post_data['ticket']['custom_text_6'][$i]) ?  $post_data['ticket']['custom_text_6'][$i] : '',

                            'ticket_custom_select_1' 	=> isset($post_data['ticket']['custom_select_1'][$i]) ?  $post_data['ticket']['custom_select_1'][$i] : '',
                            'ticket_custom_select_2' 	=> isset($post_data['ticket']['custom_select_2'][$i]) ?  $post_data['ticket']['custom_select_2'][$i] : '',
                            'ticket_custom_select_3' 	=> isset($post_data['ticket']['custom_select_3'][$i]) ?  $post_data['ticket']['custom_select_3'][$i] : '',
                            'ticket_custom_select_4' 	=> isset($post_data['ticket']['custom_select_4'][$i]) ?  $post_data['ticket']['custom_select_4'][$i] : '',
                            'ticket_custom_select_5' 	=> isset($post_data['ticket']['custom_select_5'][$i]) ?  $post_data['ticket']['custom_select_5'][$i] : '',
                            'ticket_custom_select_6' 	=> isset($post_data['ticket']['custom_select_6'][$i]) ?  $post_data['ticket']['custom_select_6'][$i] : '',
                            'ticket_status'             => 'Registered' // Change when going public
                        );

                        // insert record
                        $this->tickets_model->insert($data);
                    }

                    // Add to session
                    $this->session->set_userdata(array('ticket_reference_no' => $ticket_reference_no));
                    
                    $order_total = str_replace(',', '', $this->session->userdata('ticket_total'));
                    
//                    $bag_check_total = '0.00';
//                    if(isset($post_data['ticket']['bag_check']))
//                    {
//                        $bag_check_total = (count($post_data['ticket']['bag_check']) * 100);
//                        $order_total     = $order_total + $bag_check_total; // Addons make this dynamic
//                    }

                    // Add to order table
                    $order_data = array(
                        'order_user_id'                   => '',
                        'order_event_id'                  => $post_data['event_id'],
                        'order_ticket_type_id'            => $this->session->userdata('ticket_type_id'),
                        'order_ticket_reference_no'       => $ticket_reference_no,
                        'order_ticket_qty'                => $ticket_qty,
                        'order_ticket_amount'             => str_replace(',', '', $this->session->userdata('ticket_type_amount')),
                        'order_ticket_discount'           => str_replace(',', '', $this->session->userdata('ticket_discount')),
                        'order_ticket_addon'              => '',
                        'order_ticket_total'              => $order_total,
                        'order_ticket_status'             => 'Registered'
                    );

                    $this->orders_model->insert($order_data);

                    if($order_total > 0)
                    {
                        // Process Paypal                
                        $gateway = Omnipay::create('PayPal_Express');
                        $gateway->setUsername($this->paypal_api_user);
                        $gateway->setPassword($this->paypal_api_pass);
                        $gateway->setSignature($this->paypal_api_signature);
                        $gateway->setTestMode($this->paypal_test_mode);

                        $params = array(
                            'amount'               => $order_total,
                            'currency'             => 'PHP',
                            'description'          => $this->session->userdata('ticket_type_name'),
                            'transactionId'        => random_string('numeric', 20),
                            'transactionReference' => $ticket_reference_no,
                            'returnUrl'            => site_url('event/success/legion-run-manila-2017'),
                            'cancelUrl'            => site_url('event/cancel/legion-run-manila-2017')
                         );

                        $paypal = $gateway->purchase($params)->send();
                        if ($paypal->isRedirect()) {
                            //$paypal->redirect();

                            $response = array(
                                'success'  => TRUE,
                                'message'  => 'Successful Registration.',
                                'redirect' => $paypal->getRedirectUrl()
                            );
                        } else {

                            $this->load->library('email');
                            $this->email->from('robert.obias@google.com');
                            $this->email->to('robert.obias@google.com');
                            $this->email->subject('Paypal erro');

                            $this->email->message('FAiled');

                            $this->email->send();

                            $response = array(
                                'success' => FALSE,
                                'message' => 'Unsuccessful Registration.'
                            );
                        }
                    }
                    else // Payment is 0 redirect t0 success page
                    {
                        // Mark order as paid
                        
                        $response = array(
                            'success'  => TRUE,
                            'message'  => 'Successful Registration.',
                            'redirect' => site_url('event/register/legion-run-manila-2017/success') 
                        );
                    }
                }
                else
                {
                    $response = array(
                        'success'  => FALSE,
                        'message'  => 'Unsuccessful Registration.',
                    );
                }
            }
        }
        else
        {
            $response = array(
                'success' => FALSE,
                'message' => 'Unsuccessful Registration.'
            );
        }

        echo json_encode($response);
    }

    public function paypal()
    {
        $gateway = Omnipay::create('PayPal_Express');
        $gateway->setUsername('rchristian_obias_api1.yahoo.com');
        $gateway->setPassword('HL33NHEAXYN7FAH6');
        $gateway->setSignature('AtFV-RI49GeyOALuF62Upy1pZ8fRAf3zOnB1GNbCIH16QCkEqcjsSJOD');
        $gateway->setTestMode(TRUE);

        $response = $gateway->purchase(
            array(
                'cancelUrl' => site_url('event/cancel'),
                'returnUrl' => site_url('event/confirm'),
                'amount'    =>  '200.00',
                'currency'  => 'PHP'
            )
        )->send();

        $response->redirect();
    }

    public function order($slug, $ticket_reference_no)
    {

        $microsite = $this->microsites_model->get_microsite($slug);
        
        $this->session->set_userdata('ticket_reference_no', $ticket_reference_no);
        
        $order = $this->orders_model->where('order_ticket_status', 'Pending')
                                    ->or_where('order_ticket_status', 'Failed Payment')
                                    ->or_where('order_ticket_status', 'Cancelled')
                                    ->join('ticket_types', 'ticket_type_id=order_ticket_type_id', 'LEFT')
                                    ->find_by(array('order_ticket_reference_no' => $ticket_reference_no));
        
        $data['header']       = 'Order';
		$data['page_heading'] = 'Order';
        $data['microsite']    = $microsite;
        $data['order']        = $order;
        
        if($this->input->post('submit'))
        {
            $gateway = Omnipay::create('PayPal_Express');
            $gateway->setUsername($this->paypal_api_user);
            $gateway->setPassword($this->paypal_api_pass);
            $gateway->setSignature($this->paypal_api_signature);
            $gateway->setTestMode($this->paypal_test_mode);

            $params = array(
                'amount'               => $order->order_ticket_total,
                'currency'             => 'PHP',
                'description'          => $order->ticket_type_name,
                'transactionId'        => random_string('numeric', 20),
                'transactionReference' => random_string('alnum', 10),
                'returnUrl'            => site_url('event/success/'.$slug),
                'cancelUrl'            => site_url('event/cancel/'.$slug)
             );

            $paypal = $gateway->purchase($params)->send();
            if ($paypal->isRedirect()) 
            {
                $paypal->redirect();
            } else {

            }
        }
        
		$this->template->set_template($microsite->microsite_theme_code);
        $this->template->add_js('assets/plugins/blockUI/jquery.blockUI.js');
        $this->template->add_js(module_js('frontend', 'event_order'), 'embed');
        $this->template->write_view('content', 'event_order', $data);
		$this->template->render();
    }

    public function success($slug)
    {
        $this->load->library('email');

        $ticket_reference_no = $this->session->userdata('ticket_reference_no');
        $microsite  = $this->microsites_model->get_microsite($slug);
    
        $data['header']       = 'Order';
		$data['page_heading'] = 'Order';
        $data['microsite']    = $microsite;

        $gateway = Omnipay::create('PayPal_Express');
        $gateway->setUsername($this->paypal_api_user);
        $gateway->setPassword($this->paypal_api_pass);
        $gateway->setSignature($this->paypal_api_signature);
        $gateway->setTestMode($this->paypal_test_mode);
            
        $formData = array(
                        'accessCode' => $this->input->get('token'),
                        'amount'     => str_replace(',', '', $this->session->userdata('ticket_total')), // Get from session
                        'currency'   => 'PHP'
                    );
        
        $response     = $gateway->completePurchase($formData)->send();
        $paypal_data  = $response->getData(); // this is the raw response object
        
       
        $data['ack'] = strtolower($paypal_data['ACK']);
        
        switch(strtolower($paypal_data['ACK']))
        {
            case 'success':
                $ticket_status = 'Paid';
                $ticket_payment_transaction_id = $paypal_data['PAYMENTINFO_0_TRANSACTIONID'];
            break;
            case 'successwithwarning':
                $ticket_status = 'Paid';
                $ticket_payment_transaction_id = $paypal_data['PAYMENTINFO_0_TRANSACTIONID'];
            case 'failure':
                $ticket_status = 'Failed Payment';
                $ticket_payment_transaction_id = '';
            break;
            default:
                $ticket_status = 'Pending';
                $ticket_payment_transaction_id = $paypal_data['PAYMENTINFO_0_TRANSACTIONID'];
        }
        
        if(!in_array(strtolower($paypal_data['ACK']), array('failure', 'successwithwarning'))) // Prevent resending of email
        {
            $tickets = $this->tickets_model->select('ticket_id')
                                           ->find_all_by(array('ticket_reference_no' => $ticket_reference_no));

            if($tickets)
            {
                foreach($tickets as $key=>$val)
                {
                    $ticket = $this->tickets_model->find($val);

                    // Update ticket status
                    if(strtolower($paypal_data['ACK']) == 'success')
                    {
                        $tickets_data = array(
                            'ticket_payment_method'         => 'Paypal', // make this dynamic
                            'ticket_payment_transaction_id' => $ticket_payment_transaction_id,
                            'ticket_status'                 => $ticket_status, // make this dynamic
                        );

                        $update_ticket = $this->tickets_model->update(array('ticket_id'=>$val), $tickets_data);
                    }
                }

                if(strtolower($paypal_data['ACK']) == 'success')
                {
                    $coupon_id = $this->session->userdata('coupon_id');

                    if($coupon_id != '') // Update only those with coupon
                    {
                        // Update coupon
                        $coupon = $this->coupons_model->find($coupon_id);
                        $this->coupons_model->update($coupon_id, array('coupon_used' => $coupon->coupon_used + count($tickets)));
                    }

                    // Update order
                    $this->orders_model->update(array('order_ticket_reference_no'=>$ticket_reference_no), array('order_ticket_status' => $ticket_status));
                }
                
                // Send email after status change
                foreach($tickets as $key=>$val)
                {
                    $ticket = $this->tickets_model->find($val);
                    
                    $this->email->clear(TRUE);
                    
                    //$this->email->initialize($config);
                    $this->email->from($microsite->event_reg_email_from, $microsite->event_name);
                    $this->email->to($ticket->ticket_email); // Add to session
                    $this->email->bcc($microsite->event_reg_email_from);
                    $this->email->subject($microsite->event_reg_email_subject . ' - Reference No: ' . $ticket->ticket_reference_no);

                    $this->generate_pdf($val);
                    $this->email->attach('assets/uploads/pdf/' . $val . '.pdf');

                    $this->email->message(
                        $this->_email(
                            $microsite->microsite_slug, // microsite slug
                            $microsite->event_reg_email_content, // microsite email template
                            $ticket, // user information
                            ($this->session->userdata('coupon_code') ? $this->session->userdata('coupon_code') : '') // coupon code
                        )
                    );

                    $this->email->send();
                }
                
            }
        }
        

        $this->template->set_template($microsite->microsite_theme_code);
        $this->template->write_view('content', 'event_order_success', $data);
		$this->template->render();
     }

    public function cancel($slug)
    {
        $gateway = Omnipay::create('PayPal_Express');
        $gateway->setUsername($this->paypal_api_user);
        $gateway->setPassword($this->paypal_api_pass);
        $gateway->setSignature($this->paypal_api_signature);
        $gateway->setTestMode($this->paypal_test_mode);
            
        $formData = array(
                        'accessCode' => $this->input->get('token'),
                        'amount'     => str_replace(',', '', $this->session->userdata('ticket_total')), // Get from session
                        'currency'   => 'PHP'
                    );
        
        $response     = $gateway->completePurchase($formData)->send();
        $paypal_data  = $response->getData(); // this is the raw response object 
        
        if(strtolower($paypal_data['ACK']) == 'failure') 
        {
            $this->load->library('email');

            $ticket_reference_no = $this->session->userdata('ticket_reference_no');
            $microsite           = $this->microsites_model->get_microsite($slug);
            
            // Get tickets with the same reference no
            $tickets = $this->tickets_model->select('ticket_id')
                                           ->find_all_by(array('ticket_reference_no' => $ticket_reference_no));

            $data['header']       = 'Order';
            $data['page_heading'] = 'Order';
            $data['microsite']    = $microsite;

            if($tickets)
            {
                foreach($tickets as $key=>$val)
                {
                    $this->email->clear();

                    $ticket = $this->tickets_model->find($val);
                    //$order  = $this->orders_model->find_by('order_ticket_reference_no', $tickets['0']);

                    //$this->email->initialize($config);
                    $this->email->from($microsite->event_reg_email_from, $microsite->event_name);
                    $this->email->to($ticket->ticket_email); // Add to session
                    $this->email->bcc($microsite->event_reg_email_from);
                    $this->email->subject('Cancelled Order with Reference No: ' . $ticket->ticket_reference_no);

                    $this->email->message(
                        $this->_email(
                            $microsite->microsite_slug, // microsite slug
                            $microsite->event_reg_email_content, // microsite email template
                            $ticket, // user information
                            ($this->session->userdata('coupon_code') ? $this->session->userdata('coupon_code') : ''), // coupon code
                            $val, 
                            'cancelled'
                        )
                    );

                    $this->email->send();
                    
                    $tickets_data = array(
                        'ticket_status' => 'Cancelled', // make this dynamic
                    );

                    $this->tickets_model->update(array('ticket_id'=>$val), $tickets_data);
                }
                
                $this->orders_model->update(array('order_ticket_reference_no' => $ticket_reference_no), array('order_ticket_status' => 'Cancelled'));
            }
        }

        $this->template->set_template($microsite->microsite_theme_code);
        $this->template->write_view('content', 'event_order_cancel', $data);
		$this->template->render();

    }

    public function ipn()
    {
        
        $this->load->library('email');
        if(function_exists('curl_init'))
        {
            echo 'exists';
        }
        else
        {
            echo 'not';
        }
        
        echo '1';
        $raw_post_data  = file_get_contents('php://input');
        $raw_post_array = explode('&', $raw_post_data);
        $myPost = array();
        foreach ($raw_post_array as $keyval)
        {
            $keyval = explode ('=', $keyval);
            if (count($keyval) == 2)
              $myPost[$keyval[0]] = urldecode($keyval[1]);
        }
        
        echo '2';
        
        // read the IPN message sent from PayPal and prepend 'cmd=_notify-validate'
        $req = 'cmd=_notify-validate';
        if (function_exists('get_magic_quotes_gpc')) {
            $get_magic_quotes_exists = true;
        }
        
        echo '3';
        foreach ($myPost as $key => $value) {
            if ($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1) {
                $value = urlencode(stripslashes($value));
            } else {
                $value = urlencode($value);
            }
            $req .= "&$key=$value";
        }
        
        echo '4';
        // Step 2: POST IPN data back to PayPal to validate
        $ch = curl_init($this->paypal_url);
        
        echo $this->paypal_url;
        curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch,  CURLOPT_HTTPHEADER, array('Connection: Close'));
        curl_setopt($curl, CURLOPT_SSLVERSION, 3); // For staging test

        // In wamp-like environments that do not come bundled with root authority certificates,
        // please download 'cacert.pem' from "http://curl.haxx.se/docs/caextract.html" and set
        // the directory path of the certificate as shown below:
        // curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__) . '/cacert.pem');

        if ( !($res = curl_exec($ch)) ) {
            echo '<pre>';
            var_dump($res);
            
            echo "Got " . curl_error($ch) . " when processing IPN data <br />";
            echo curl_errno($ch);
            
            if(OPENSSL_VERSION_NUMBER < 0x009080bf) {
                echo "OpenSSL Version Out-of-Date";
            } else {
                echo "OpenSSL Version OK";
            }

            
          curl_close($ch);
          exit;
        }
        curl_close($ch);
        
        echo '5';
        echo '<pre>';
        var_dump($res);
        
        if (strcmp ($res, "VERIFIED") == 0) {
            
            // Update 
            $paypal_post_data = $_POST;
            if($paypal_post_data['payment_status'] == 'Completed')
            {
                $this->email->from('rchristian_obias@yahoo.com', 'IPN Success');
                $this->email->to('robert.obias@google.com');
                #$this->email->bcc('them@their-example.com');

                $this->email->subject('Email Test');
                $this->email->message($this->paypal_url . ' - ' . $abc);

                $this->email->send();
            }
            
            $abc = '';
            // The IPN is verified, process it
            foreach($_POST as $key => $value) {
                $abc .= $key . " = " . $value . "<br>";
            }
            
            

        } else if (strcmp ($res, "INVALID") == 0) {
            
          // IPN invalid, log for manual investigation
            $abc = "The response from IPN was: <b>" .$res ."</b>";
            
            echo $abc;

        }
        
        $this->email->from('rchristian_obias@yahoo.com', 'IPN Test');
        $this->email->to('robert.obias@google.com');
        #$this->email->bcc('them@their-example.com');

        $this->email->subject('Email Test');
        $this->email->message($this->paypal_url . ' - ' . $abc);

        $this->email->send();


    }

	public function generate_pdf($ticket_id)
	{
		$ticket_info = $this->tickets_model->get_ticket($ticket_id);
        $fullname    = $ticket_info->ticket_firstname  . ' ' . ($ticket_info->ticket_middlename ? $ticket_info->ticket_middlename . ' ': '') . $ticket_info->ticket_lastname;

		$this->load->library('Pdf');

		// create new PDF document
		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetTitle($ticket_info->event_name);
		$pdf->SetSubject($ticket_info->event_name);

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// set font
		$pdf->SetFont('helvetica', '', 10);
		$pdf->SetCellPadding(10);

		// add a page
		$pdf->AddPage();

		// define some HTML content with style
		$html = '
		<style>
			.tables tr, .tables td  {
				border:6px solid #ccc;
				padding:10px;
			}
			small {
				color:#ccc;
				font-size: 12px;
                margin-left: -10px;
			}
		</style>
        <table cellpadding="8" class="tables" width="100%">
            <tr>
                <td rowspan="4" width="10%"></td>
                <td colspan="2" width="70%">
                    <small>Event</small>
					<h1>' . $ticket_info->event_name . '</h1>
                </td>
                <td rowspan="4" width="20%" valign="top">
                    <small>Name</small><br />
					' .$fullname . '
					<br /><br />
                    <small>Payment Status</small><br />
                    ' . $ticket_info->ticket_status . '
                </td>
            </tr>
            <tr>
                <td>
                    <small>Date+Time</small>
                    <h3>' . date('F d, Y g:i a', strtotime($ticket_info->event_starts_on)) . '</h3>
                </td>
                <td>
                    <small>Location</small>
					<h3>' . $ticket_info->event_venue_name . '</h3>
                    <p>' . $ticket_info->event_venue_address . '</p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <small>Order Info</small>
					<p><b>Order No.</b> ' . $ticket_info->ticket_reference_no . '. <br /><b>Ordered By</b> ' . $fullname . ' on ' . date('F d, Y g:i a', strtotime($ticket_info->ticket_created_on)) . '</p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <small>Type</small>
                    <p>' . $ticket_info->ticket_type_name . '</p>
                </td>
            </tr>
        </table>
        
		<br><br>
		<table cellpadding="10">
			<tr>
				<td colspan="2"  style="border:1px solid #ccc; font-size:11px;">
				Message from Legion Run!<br><br>

				Please read the following. You need to print and sign your ticket in order to be allowed to participate on event day !!!<br><br>

				I understand that participating in the Legion Run event is a physically challenging and
				potentially dangerous activity and involves the risk of serious injury and/or death.
				I assert that I am in good health and in proper physical condition to safely participate in the
				Legion Run event. I certify that I have no known or knowable medical, physical or mental
				conditions that would affect my ability to safely participate in the Legion Run event, or that
				would result in my participation creating a risk of danger to myself or to others.
				I am aware and informed of the inherent risks inparticipating in the Event and that my participation in an Event is entirely voluntary.
				I acknowledge that Legion Run recommends and encourages each client to get medical
				clearance from his/her personal physician prior to participation.
				I assert that I have not been advised or cautioned against participating by a medical practitioner.
				I understand that it is my responsibility to continuously monitor my own physical and mental
				condition during the Course, and I agree to withdraw immediately and notify appropriate
				personnel if at any point my continued participation would create a risk of danger to myself.<br><br>

				Have a question? Contact us at philippines@legionrun.com

				</td>
				<td>

				</td>
			</tr>
		</table>
		';
    
		// output the HTML content
		$pdf->writeHTML($html, true, false, true, false, '');

		//Close and output PDF document
		$pdf->Output($_SERVER['DOCUMENT_ROOT'] . "public/assets/uploads/pdf/" . $ticket_info->ticket_id . '.pdf', 'F');
  
	}
    
    public function process_addon()
    {
        $action          = $this->input->post('action');
        $bag_check_total = $this->input->post('bag_check') * 100;
      
        if($action == 'add')
        {
            $ticket_total    = str_replace(',', '', $this->session->userdata('ticket_total')) + $bag_check_total;
            if($this->session->userdata('addon_ticket_total'))
            {
                $ticket_total    = str_replace(',', '', $this->session->userdata('addon_ticket_total')) + 100;
            }
        }
        else
        {
            $ticket_total    = str_replace(',', '', $this->session->userdata('addon_ticket_total')) - 100;
        }
        

        $response = array(
            'bag_check_total' => number_format($bag_check_total, 2),
            'ticket_total'    => number_format($ticket_total, 2)
        );
        
        $addon_data = array(
            'bag_check'          => $bag_check_total, 
            'addon_ticket_total' => $ticket_total, 
        );
        
        $this->session->set_userdata($addon_data);
        
        echo json_encode($response);
    }
}

/* End of file Event.php */
/* Location: ./application/modules/frontend/controllers/Event.php */

<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Account Class
 *
 * @package		rcmediaph
 * @version		1.1
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2014-2016, Robert Christian Obias
 * @link		rchristian_obias@yahoo.com
 */
class Account extends CI_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->library(array('ion_auth', 'form_validation', 'facebook_ion_auth'));
		$this->load->config('ion_auth', TRUE); // overrides the email template
		$this->load->helper(array('url','language'));

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');
		$this->lang->load('account');

		$this->load->model('website/navigations_model');
		$this->load->model('events/events_model');
		$this->load->model('events/tickets_model');
        $this->load->model('events/coupons_model');
        
		// $this->load->model('users/groups_model');
		// $this->load->model('users/users_model');
		// $this->load->language('users/users');
	}

	// --------------------------------------------------------------------

	/**
	 * dashboard
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function dashboard()
	{
		$data['page_heading'] = 'My Events';
		$data['page_layout'] = 'full_width';

		if (!$this->ion_auth->logged_in())
		{
			redirect('account/login', 'refresh');
		}

		// get the user's events
		$data['events'] = $this->events_model
			->join('accounts', 'account_id = event_account_id', 'LEFT')
			->join('users', 'id = account_user_id', 'LEFT')
			->where('id', $_SESSION['user_id'])
			->limit(12)
			->find_all();

		$this->template->set_template(config_item('website_theme'));
		$this->template->add_css(module_css('frontend', 'account_dashboard'), 'embed');
		$this->template->add_js(module_js('frontend', 'account_dashboard'), 'embed');
		$this->template->write_view('content', 'account_dashboard', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * event
	 *
	 * @access	public
	 * @param	string $code
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function event($code = FALSE)
	{
		if (!$code) show_404();

		if (!$this->ion_auth->logged_in())
		{
			redirect('account/login', 'refresh');
		}

		// get the user's event
		$event = $this->events_model
			->join('accounts', 'account_id = event_account_id', 'LEFT')
			->join('users', 'id = account_user_id', 'LEFT')
			->find_by(array(
				'id' => $_SESSION['user_id'],
				'event_code' => $code
			));

		if (! $event)
		{
			show_404();
		}

		$data['page_heading'] = $event->event_name;
		$data['page_layout'] = 'full_width';
		$data['event'] = $event;

		$this->template->set_template(config_item('website_theme'));

		// datatables
		$this->template->add_css('assets/plugins/DataTables/datatables.min.css');
		$this->template->add_js('assets/plugins/DataTables/datatables.min.js');
        $this->template->add_js("var app_url= '".site_url()."'; ", 'embed');
        
		$this->template->add_css(module_css('frontend', 'account_event'), 'embed');
		$this->template->add_js(module_js('frontend', 'account_event'), 'embed');
		$this->template->write_view('content', 'account_event', $data);
		$this->template->render();
	}
    
    public function export($code)
    {
        $this->load->dbutil();
        $this->load->helper('download');
        
        if(!$code) 
        {
            show_404();
        }

        // Get the event
//        $event = $this->events_model->join('accounts', 'account_id = event_account_id', 'LEFT')
//                                    ->join('users', 'id = account_user_id', 'LEFT')
//                                    ->join('tickets', 'ticket_event_id = event_id', 'LEFT')
//                                    ->find_by(array(
//                                        'id' => $_SESSION['user_id'],
//                                        'event_code' => $code
//                                    ));
        
        $fields = [ 'ticket_id', 'ticket_firstname', 'ticket_middlename', 'ticket_lastname', 'ticket_email', 'ticket_mobile', 
                    'ticket_phone', 'ticket_gender', 'ticket_mstatus', 'ticket_birthday', 
        			'ticket_address', 'ticket_city', 'ticket_zipcode', 'ticket_country', 
                    'ticket_company', 'ticket_occupation', 'ticket_work_address', 'ticket_work_phone', 
                    'ticket_medical_conditions', 
        			'ticket_emergency_contact', 'ticket_emergency_number', 'ticket_emergency_relationship',
                    'ticket_custom_text_1', 'ticket_custom_select_1',
        			'ticket_newsletter', 'ticket_status', 'ticket_payment_method', 'ticket_payment_transaction_id'];

        // process condition
        $condition = '';
        
        if ($this->input->get('q'))
        {
        	$search = $this->input->get('q');

        	$likes = array();
	        foreach ($fields as $field)
	        {
	        	$likes[] = "{$field} LIKE '%{$this->db->escape_like_str($search)}%'";
	        }

	        $condition = ' AND (' . implode(' OR ', $likes) . ')';
	    }

        $event = $this->db->query("SELECT SQL_CALC_FOUND_ROWS 
                                    ticket_id, ticket_firstname, ticket_middlename, ticket_lastname, ticket_email, ticket_mobile, 
                                    ticket_phone, ticket_gender, ticket_mstatus AS ticket_marital_status, ticket_birthday, 
                                    ticket_address, ticket_city, ticket_zipcode, ticket_country, 
                                    ticket_company, ticket_occupation, ticket_work_address, ticket_work_phone, 
                                    ticket_medical_conditions, 
                                    ticket_emergency_contact, ticket_emergency_number, ticket_emergency_relationship,
                                    ticket_custom_text_1 AS ticket_group_code, ticket_custom_select_1 AS ticket_tshirt,
                                    ticket_newsletter, ticket_status, ticket_payment_method, ticket_payment_transaction_id
                                    FROM `events` 
                                    LEFT JOIN `accounts` ON `account_id` = `event_account_id` 
                                    LEFT JOIN `users` ON `id` = `account_user_id` 
                                    LEFT JOIN `tickets` ON `ticket_event_id` = `event_id` 
                                    WHERE `id` = '".$_SESSION['user_id']."' 
                                    AND `ticket_deleted` = '0' 
                                    AND `event_code` = '".$code."'" . $condition);
        
        
        $event_csv = $this->dbutil->csv_from_result($event);

        $file = 'assets/uploads/event_' . random_string('alnum', '8'). '.csv';
        
        // write to a file
        write_file($file, $event_csv);
        
        // Force download file
        force_download($file, NULL);

    }

	// --------------------------------------------------------------------

	/**
	 * ticketsdt
	 *
	 * @access	public
	 * @param	mixed datatables parameters (datatables.net)
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function ticketsdt($code)
	{
		if (!$code) show_404();

		if (!$this->ion_auth->logged_in())
		{
			echo json_encode(array()); // blank array
		}

		$fields = array(
            'ticket_reference_no',
            'CONCAT(ticket_firstname, " ", ticket_lastname)',
			'ticket_email',
			'ticket_phone',
			'ticket_firstname',
			'ticket_middlename',
			'ticket_lastname',
			'ticket_mobile',
			'ticket_gender',
            'ticket_custom_text_1',
            'coupon_code',
            'ticket_payment_method',
            'ticket_payment_transaction_id',
            'ticket_status',
			'ticket_mstatus',
			'ticket_is_pwd',
			'ticket_is_senior',
			'ticket_company',
			'ticket_occupation',
			'ticket_address',
			'ticket_city',
			'ticket_zipcode',
			'ticket_country',
			'ticket_created_on',
            'ticket_id'
		);

		$data = $this->tickets_model->join('events', 'event_id = ticket_event_id', 'LEFT')
                                    ->join('accounts', 'account_id = event_account_id', 'LEFT')
                                    ->join('coupons', 'coupon_id = ticket_coupon_id', 'LEFT')
                                    ->join('users', 'id = account_user_id', 'LEFT')
                                    ->where('id', $_SESSION['user_id'])
                                    ->where('event_code', $code)
                                    ->where('ticket_deleted', '0')
                                    ->order_by('ticket_created_on', 'DESC')
                                    ->datatables($fields);

		echo $data;
	}

	// --------------------------------------------------------------------

	/**
	 * register
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	// public function register()
	// {
	// 	$data['page_title'] = 'Register';
	// 	$data['page_layout'] = 'narrow_width';

	// 	if ($this->input->post())
	// 	{
	// 		$tables = $this->config->item('tables','ion_auth');

	// 		//validate form input
	// 		$this->form_validation->set_rules('first_name', lang('first_name'), 'required');
	// 		$this->form_validation->set_rules('last_name', lang('last_name'), 'required');
	// 		$this->form_validation->set_rules('email', lang('email'), 'required|valid_email|is_unique['.$tables['users'].'.email]');
	// 		$this->form_validation->set_rules('company', lang('company'), 'min_length[3]');
	// 		$this->form_validation->set_rules('phone', lang('phone'), 'min_length[7]');
	// 		// $this->form_validation->set_rules('username', lang('username'), 'required|is_unique['.$tables['users'].'.username]');
	// 		$this->form_validation->set_rules('password', lang('password'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[password_confirm]');
	// 		$this->form_validation->set_rules('password_confirm', lang('password_confirm'), 'required');
	// 		$this->form_validation->set_rules('terms', lang('terms'), 'callback__terms_check');
	// 		$this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

	// 		$this->form_validation->set_message('is_unique', '{field} is not available for registration');

	// 		if ($this->form_validation->run($this) == TRUE)
	// 		{
	// 			// $username = $this->input->post('username');
	// 			$password = $this->input->post('password');
	// 			$email = $this->input->post('email');
	// 			$additional = array(
	// 				'first_name' => $this->input->post('first_name'),
	// 				'last_name' => $this->input->post('last_name'),
	// 				'company' => $this->input->post('company'),
	// 				'phone' => $this->input->post('phone'),
	// 			); 

	// 			if ($this->ion_auth->register($email, $password, $email, $additional))
	// 			{
	// 				// if the registration is successful
	// 				$this->session->set_flashdata('message', $this->ion_auth->messages());
	// 				redirect('account/login', 'refresh');
	// 			}
	// 		}
	// 	}
	
	// 	$this->template->set_template(config_item('website_theme'));
	// 	$this->template->add_css(module_css('frontend', 'account_register'), 'embed');
	// 	// $this->template->add_js(module_js('frontend', 'user_register'), 'embed');
	// 	$this->template->write_view('content', 'account_register', $data);
	// 	$this->template->render();
	// }

	// --------------------------------------------------------------------

	/**
	 * register_fb
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	// public function register_fb()
	// {
	// 	$data['page_title'] = "Register with Facebook";

	// 	if ($this->input->post())
	// 	{
	// 		// validate form input
	// 		$this->form_validation->set_rules('terms', lang('terms'), 'callback__terms_check');
	// 		$this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

	// 		if ($this->form_validation->run($this) == TRUE)
	// 		{
	// 			// redirect to FB
	// 			redirect(site_url('account/facebook_login?return=' . current_url()), 'refresh');
	// 		}
	// 	}
	
	// 	$this->template->add_css(module_css('frontend', 'account_register'), 'embed');
	// 	// $this->template->add_js(module_js('frontend', 'account_register'), 'embed');
	// 	$this->template->write_view('content', 'account_register_fb', $data);
	// 	$this->template->render();
	// }


	// --------------------------------------------------------------------

	/**
	 * login
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	function login()
	{
		$data['page_heading'] = "Login";
		$data['page_layout'] = 'narrow_width';

		//validate form input
		$this->form_validation->set_rules('identity', lang('identity'), 'required');
		$this->form_validation->set_rules('password', lang('password'), 'required');
		$this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

		if ($this->form_validation->run($this) == true)
		{
			//check to see if the user is logging in
			//check for "remember me"
			$remember = (bool) $this->input->post('remember');

			if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
			{
				//if the login is successful
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect('account/dashboard', 'refresh');
			}
			else
			{
				//if the login was un-successful
				//redirect them back to the login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('account/login', 'refresh'); //use redirects instead of loading views for compatibility with MY_Controller libraries
			}
		}
		else
		{
			//the user is not logging in so display the login page
			//set the flash data error message if there is one
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$data['identity'] = array('name' => 'identity',
				'id' => 'identity',
				'type' => 'text',
				'value' => $this->form_validation->set_value('identity'),
			);
			$data['password'] = array('name' => 'password',
				'id' => 'password',
				'type' => 'password',
			);

			// template
			$this->template->set_template(config_item('website_theme').'_index');
			$this->template->add_css(module_css('frontend', 'account_login'), 'embed');
			$this->template->write_view('content', 'account_login', $data);
			$this->template->render();
		}
	}

	// --------------------------------------------------------------------

	/**
	 * facebook_login
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	// function facebook_login()
	// {
	// 	// first load
	// 	if (isset($_GET['return']))
	// 	{
	// 		$this->session->set_userdata('return', $_GET['return']);

	// 		// redirect the user to FB
	// 		$this->facebook_ion_auth->login();
	// 	}

	// 	// 2nd load, after getting back from FB
	// 	else if (isset($_GET['code']))
	// 	{
	// 		// register and login the user
	// 		if ($this->facebook_ion_auth->login())
	// 		{
	// 			redirect('account/facebook_login', 'refresh');
	// 		}

	// 		// header('Location:/');
	// 		redirect('', 'refresh');
	// 	}

	// 	// 3rd load
	// 	else if (!$this->ion_auth->logged_in())
	// 	{
	// 		$this->session->set_flashdata('flash_message', 'You have successfully registered through Facebook.  Please check your mailbox to verify your account.');

	// 		redirect('', 'refresh');
	// 	}
	// 	else
	// 	{
	// 		$this->session->set_flashdata('flash_message', 'You have successfully logged in through Facebook');

	// 		// return to the previous url before logging in
	// 		// $return = ($this->session->userdata('return')) ? $this->session->userdata('return') : '';
	// 		// if ($return != '')
	// 		// {
	// 		// 	header('Location: ' . $return);
	// 		// 	exit();
	// 		// }
	// 		// else
	// 		// {
	// 			redirect('account/dashboard', 'refresh');
	// 		// }
	// 	}		
	// }

	// --------------------------------------------------------------------

	/**
	 * logout
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	function logout()
	{
		//log the user out
		$this->ion_auth->logout();

		//redirect them to the login page
		$this->session->set_flashdata('message', $this->ion_auth->messages());
		$this->session->set_flashdata('flash_message', '');
		redirect('account/login', 'refresh');
	}

	// --------------------------------------------------------------------

	/**
	 * change_password
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	function change_password()
	{
		$this->acl->restrict('frontend.account.password');

		$this->form_validation->set_rules('old', lang('change_password_validation_old_password_label'), 'required');
		$this->form_validation->set_rules('new', lang('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
		$this->form_validation->set_rules('new_confirm', lang('change_password_validation_new_password_confirm_label'), 'required');
		$this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

		if (!$this->ion_auth->logged_in())
		{
			redirect('account/login', 'refresh');
		}

		$user = $this->ion_auth->user()->row();

		if ($this->form_validation->run() == false)
		{
			//display the form
			//set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
			$this->data['old_password'] = array(
				'name' => 'old',
				'id'   => 'old',
				'type' => 'password',
				'autocomplete' 	=> 'off'
			);
			$this->data['new_password'] = array(
				'name' => 'new',
				'id'   => 'new',
				'type' => 'password',
				'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				'autocomplete' 	=> 'off'
			);
			$this->data['new_password_confirm'] = array(
				'name' => 'new_confirm',
				'id'   => 'new_confirm',
				'type' => 'password',
				'pattern' => '^.{'.$this->data['min_password_length'].'}.*$',
				'autocomplete' 	=> 'off'
			);
			$this->data['user_id'] = array(
				'name'  => 'user_id',
				'id'    => 'user_id',
				'type'  => 'hidden',
				'value' => $user->id,
			);

			//render
			$this->_render_page('account/change_password', $this->data);
		}
		else
		{
			$identity = $this->session->userdata('identity');

			$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

			if ($change)
			{
				//if the password was successfully changed
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				$this->logout();
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('account/change_password', 'refresh');
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	 * forgot_password
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	function forgot_password()
	{
		$data['page_heading'] = "Forgot Password";
		$data['page_subhead'] = "Please enter your Email so we can send you an email to reset your password.";
		$data['page_layout'] = 'narrow_width';

		//setting validation rules by checking wheather identity is username or email
		if($this->config->item('identity', 'ion_auth') == 'username' )
		{
		   $this->form_validation->set_rules('email', lang('forgot_password_username_identity_label'), 'required');
		}
		else
		{
		   $this->form_validation->set_rules('email', lang('forgot_password_validation_email_label'), 'required|valid_email');
		}
		$this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');


		if ($this->form_validation->run() == false)
		{
			//setup the input
			$data['email'] = array('name' => 'email',
				'id' => 'email', 'class' => 'form-control'
			);

			if ( $this->config->item('identity', 'ion_auth') == 'username' ){
				$data['identity_label'] = lang('forgot_password_username_identity_label');
			}
			else
			{
				$data['identity_label'] = lang('forgot_password_email_identity_label');
			}

			//set any errors and display the form
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->template->set_template(config_item('website_theme'));
			$this->template->add_css(module_css('frontend', 'account_forgot_password'), 'embed');
			$this->template->write_view('content', 'account_forgot_password', $data);
			$this->template->render();
		}
		else
		{
			// get identity from username or email
			if ( $this->config->item('identity', 'ion_auth') == 'username' ){
				$identity = $this->ion_auth->where('username', strtolower($this->input->post('email')))->users()->row();
			}
			else
			{
				$identity = $this->ion_auth->where('email', strtolower($this->input->post('email')))->users()->row();
			}
				if(empty($identity)) {

					if($this->config->item('identity', 'ion_auth') == 'username')
					{
						$this->ion_auth->set_message('forgot_password_username_not_found');
					}
					else
					{
						$this->ion_auth->set_message('forgot_password_email_not_found');
					}

					$this->session->set_flashdata('message', $this->ion_auth->messages());
					redirect("account/forgot_password", 'refresh');
				}

			//run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

			if ($forgotten)
			{
				//if there were no errors
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("account/login", 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("account/forgot_password", 'refresh');
			}
		}
	}

	// --------------------------------------------------------------------

	/**
	 * reset_password
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function reset_password($code = NULL)
	{
		$data['page_heading'] = "Reset Password";
		$data['page_layout'] = 'narrow_width';

		if (!$code)
		{
			show_404();
		}

		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user)
		{
			//if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', lang('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', lang('reset_password_validation_new_password_confirm_label'), 'required');
			$this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

			if ($this->form_validation->run() == false)
			{
				//display the form

				//set the flash data error message if there is one
				$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$data['new_password'] = array(
					'name' => 'new',
					'id'   => 'new',
					'type' => 'password',
					'pattern' => '^.{'.$data['min_password_length'].'}.*$',
					'class' => 'form-control',
					'autocomplete' 	=> 'off'
				);
				$data['new_password_confirm'] = array(
					'name' => 'new_confirm',
					'id'   => 'new_confirm',
					'type' => 'password',
					'pattern' => '^.{'.$data['min_password_length'].'}.*$',
					'class' => 'form-control',
					'autocomplete' 	=> 'off'
				);
				$data['user_id'] = array(
					'name'  => 'user_id',
					'id'    => 'user_id',
					'type'  => 'hidden',
					'value' => $user->id,
				);
				$data['csrf'] = $this->_get_csrf_nonce();
				$data['code'] = $code;

				//render
				// $this->_render_page('account/reset_password', $this->data);
				$this->template->set_template(config_item('website_theme'));
				$this->template->add_css(module_css('frontend', 'account_reset_password'), 'embed');
				$this->template->write_view('content', 'account_reset_password', $data);
				$this->template->render();
			}
			else
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
				{

					//something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($code);

					show_error(lang('error_csrf'));

				}
				else
				{
					// finally change the password
					$identity = $user->{$this->config->item('identity', 'ion_auth')};

					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change)
					{
						//if the password was successfully changed
						$this->session->set_flashdata('message', $this->ion_auth->messages());
						redirect("account/login", 'refresh');
					}
					else
					{
						$this->session->set_flashdata('message', $this->ion_auth->errors());
						redirect('account/reset_password/' . $code, 'refresh');
					}
				}
			}
		}
		else
		{
			//if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("account/forgot_password", 'refresh');
		}
	}

	// --------------------------------------------------------------------

	/**
	 * activate
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	// function activate($id, $code=false)
	// {
	// 	if ($code !== false)
	// 	{
	// 		$activation = $this->ion_auth->activate($id, $code);
	// 	}
	// 	else if ($this->ion_auth->is_admin())
	// 	{
	// 		$activation = $this->ion_auth->activate($id);
	// 	}

	// 	if ($activation)
	// 	{
	// 		//redirect them to the auth page
	// 		$this->session->set_flashdata('message', $this->ion_auth->messages());
	// 		redirect("account/login", 'refresh');
	// 	}
	// 	else
	// 	{
	// 		//redirect them to the forgot password page
	// 		$this->session->set_flashdata('message', $this->ion_auth->errors());
	// 		redirect("account/forgot_password", 'refresh');
	// 	}
	// }

	// --------------------------------------------------------------------

	/**
	 * _terms_check
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function _terms_check($str)
	{
		if ($str == 1)
		{
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('_terms_check', 'You must agree to the Terms and Conditions');
			return FALSE;
		}
	}

	function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}

	function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE &&
			$this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}

	function _render_page($view, $data=null, $render=false)
	{

		$this->viewdata = (empty($data)) ? $this->data: $data;

		$view_html = $this->load->view($view, $this->viewdata, $render);

		if (!$render) return $view_html;
	}
    
    // --------------------------------------------------------------------

	/**
	 * form
	 *
	 * @access	public
	 * @param	$action string
	 * @param   $id integer
	 * @author 	Robert Christian Obias <robert.obias@google.com>
	 */
	public function form($action, $id = FALSE)
	{
        if (!$id) 
        { 
            show_404();
        }

		$this->acl->restrict('events.tickets.' . $action);

		$data['page_heading'] = lang($action . '_heading');
		$data['page_subhead'] = lang($action . '_subhead');
		$data['action']       = $action;
        
        $data['record']      = $this->tickets_model->find($id);
        
		if ($this->input->post())
		{
            
			if ($this->_save($action, $id, $data['record']))
			{
				echo json_encode(array('success' => true, 'message' => 'Successfuly updated the status.')); exit;
			}
			else
			{	
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors']  = array(
                    'ticket_status'    => form_error('ticket_status'),
				);
                
				echo json_encode($response);
				exit;
			}
		}
        
        


        $this->template->set_template('modal');

		$this->template->add_js(module_js('frontend', 'account_tickets_form'), 'embed');
		$this->template->write_view('content', 'account_tickets_form', $data);
		$this->template->render();
	}
    
    // --------------------------------------------------------------------

	/**
	 * _save
	 *
	 * @access	private
	 * @param	string $action
	 * @param 	integer $id
	 * @author 	Robert Christian Obias <robert.obias@google.com>
	 */
	private function _save($action = 'add', $id = 0, $ticket)
	{
		// validate inputs
        $this->form_validation->set_rules('ticket_status', 'Ticket Status', 'required');

		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

		if ($this->form_validation->run($this) == FALSE)
		{
			return FALSE;
		}
        else
        {
            $data = array(
                'ticket_status'  => $this->input->post('ticket_status'),
            );
            
            $this->tickets_model->update($id, $data);
            
            // Get all paid and pending tickets
            $tickets = $this->tickets_model->select('COUNT(ticket_id) AS cnt_tickets')
                                           ->where('(ticket_status = "Paid" OR ticket_status = "Pending" OR ticket_status = "Complimentary")')
                                           //->or_where('ticket_status', 'Pending')
                                           ->where('ticket_coupon_id', $ticket->ticket_coupon_id)
                                           ->find_all_by(array('ticket_event_id' => $ticket->ticket_event_id));
            
            $valid_tickets = $tickets['0']->cnt_tickets;

            // Update Coupon Used
            $return = $this->coupons_model->update(array(
                            'coupon_id' => $ticket->ticket_coupon_id
                        ), array(
                            'coupon_used' => $valid_tickets,
                        ));
        }

	
		return $return;
	}
}

/**
 * @package     rcmediaph
 * @version     1.0
 * @author      Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright   Copyright (c) 2014-2015, Robert Christian Obias
 * @link        rchristian_obias@yahoo.com
 */
$(function() {

    $("body").tooltip({ selector: '[tooltip-toggle=tooltip]' });
    
    var oTable = $('#datatables').dataTable({
        "bProcessing": true,
        "bServerSide": true,
        "sAjaxSource": "audittrails/datatables",
        "lengthMenu": [[10, 50, 100, 300, -1], [10, 50, 100, 300, "All"]],
        "pagingType": "full_numbers",
        "language": {
            "paginate": {
                "previous": 'Prev',
                "next": 'Next',
            }
        },
        "bAutoWidth": false,
        "aaSorting": [[ 0, "desc" ]],
        "aoColumnDefs": [
            
            {
                "aTargets": [0],
                "sClass": "col-md-1 text-center",
            },

            {
                "aTargets": [6],
                "bSortable": false,
                 "mRender": function (data, type, full) {
                    html = '<a href="audittrails/view/'+full[0]+'" data-toggle="modal" data-target="#audittrails_details" tooltip-toggle="tooltip" data-placement="top" title="View" class="btn btn-xs btn-success"><span class="fa fa-eye"></span> View</a>';

                    return html;
                },
                "sClass": "col-md-1 text-center",
            },
        ]
    });

    $('.btn-actions').prependTo('div.dataTables_filter');

    $('body').on('hidden.bs.modal', '.modal', function () {

    });

});
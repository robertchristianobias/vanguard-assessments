<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Audittrails Language File (English)
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2014-2015, Robert Christian Obias
 * @link		rchristian_obias@yahoo.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Reports';

// Labels
$lang['label_id']					= 'ID';
$lang['label_date']					= 'Date';
$lang['label_action']				= 'Action';
$lang['label_table']				= 'Table';
$lang['label_data']					= 'Data';
$lang['label_user_ip']				= 'IP Address';
$lang['label_user_agent']			= 'User Agent';
$lang['label_user']					= 'User';

// Headers
$lang['header_auditlog_info']		= 'Audit Trail Information';
$lang['header_form_data']			= 'Form Data';

// Index Function
$lang['index_heading']				= 'Audit Trails';
$lang['index_subhead']				= 'History of user\'s actions';

$lang['index_id']					= 'ID';
$lang['index_action']				= 'Action';
$lang['index_table']				= 'Table';
$lang['index_user']					= 'User';
$lang['index_ip']					= 'IP Address';
$lang['index_date']					= 'Date';


// View Function
$lang['view_heading']				= 'Audit Trail Details';
$lang['view_subhead']				= 'Audit Trail Details';

// Text
$lang['text_audittrail_details']	= 'Audit Trail Details';
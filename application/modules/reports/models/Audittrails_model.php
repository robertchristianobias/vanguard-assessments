<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Auditlogs_model Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2014-2015, Robert Christian Obias
 * @link		rchristian_obias@yahoo.com
 */
class Audittrails_model extends BF_Model 
{

	protected $table_name			= 'audittrails';
	protected $key					= 'audittrail_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'audittrail_created_on';
	protected $created_by_field		= 'audittrail_created_by';

	protected $set_modified			= FALSE;

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'audittrail_deleted';
	protected $deleted_by_field		= 'audittrail_deleted_by';

	public function get_datatables()
	{
		$fields = array(
			'audittrail_id', 
			'audittrail_action', 
			'audittrail_table', 
			'CONCAT(first_name, " ", last_name)', 
			'audittrail_user_ip', 
			'audittrail_created_on'
		);

		return $this->join('users', 'id = audittrail_created_by', 'LEFT')->datatables($fields);
	}
}
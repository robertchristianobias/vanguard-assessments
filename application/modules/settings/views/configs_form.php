<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span>
		<span class="sr-only"><?php echo lang('button_close')?></span>
	</button>
	<h4 class="modal-title" id="myModalLabel"><?php echo $page_heading?></h4>
</div>
<div class="modal-body">
	<?php echo form_open(current_url(), array('id' => 'form', 'class'=>'form-horizontalx'));?>
		<div class="form-group">
		<label class="control-label"><?php echo $config->config_label; ?>:</label>
			<?php if ($config->config_type == 'input' OR $config->config_type == 'text'): ?>
				<?php echo form_input(array('id'=>'config_value', 'name'=>'config_value', 'value'=>set_value('config_value', $config->config_value), 'class'=>'form-control')); ?>
			<?php elseif ($config->config_type == 'textarea'): ?>
				<?php echo form_textarea(array('id'=>'config_value', 'name'=>'config_value', 'rows'=>'10', 'value'=>set_value('config_value', isset($config->config_value) ? $config->config_value : '', TRUE), 'class'=>'form-control')); ?>
			<?php elseif ($config->config_type == 'dropdown'): ?>
				<?php $options = create_dropdown('array', $config->config_values); ?>
				<?php echo form_dropdown('config_value', $options, set_value('config_value', (isset($config->config_value)) ? $config->config_value: ''), 'id="' . 'config_value' . '" class="form-control"'); ?>
			<?php endif; ?>
			<div id="error-config_value"></div>
			<div class="help-block"><?php echo $config->config_notes; ?></div>
		</div>
	<?php echo form_close();?>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
		<i class="fa fa-times"></i> <?php echo lang('button_close')?>
	</button>
	<?php if($page_type == 'add'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_add')?>
		</button>
	<?php else: ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_update')?>
		</button>
	<?php endif; ?>
</div>
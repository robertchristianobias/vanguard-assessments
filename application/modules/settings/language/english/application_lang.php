<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Appliation Language File (English)
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2014-2015, Robert Christian Obias
 * @link		rchristian_obias@yahoo.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Settings';

// Index Function
$lang['index_heading']				= 'Application Settings';
$lang['index_subhead']				= 'This page shall contain various application settings';

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Settings Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <robert.obias@google.com>
 * @copyright 	Copyright (c) 2015, Google.
 * @link		http://www.google.com
 */
class Settings extends CI_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();

	}
	
	// --------------------------------------------------------------------
	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robert.obias@google.com>
	 */
	public function index() 
	{
		// this class was created just for the navigation menu
	}
}

/* End of file settings.php */
/* Location: ./application/modules/settings/controllers/settings.php */
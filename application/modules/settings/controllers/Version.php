<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Version Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2016, Robert Christian Obias
 * @link		rchristian_obias@yahoo.com
 */
class Version extends MX_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	function __construct()
	{
		parent::__construct();
	}

	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function index()
	{
		echo $this->config->item('app_version');
	}
}

/* End of file Version.php */
/* Location: ./application/modules/settings/controllers/Version.php */
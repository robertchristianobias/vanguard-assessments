<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Test_batteries Language File (English)
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Test Batteries';

// Labels
$lang['test_battery_company_id']	= 'Company';
$lang['test_battery_name']			= 'Name';
$lang['test_battery_tests']			= 'Tests';

// Buttons
$lang['button_add']					= 'Add Test battery';
$lang['button_update']				= 'Update Test battery';
$lang['button_delete']				= 'Delete Test battery';
$lang['button_edit_this']			= 'Edit This';


// Index Function
$lang['index_heading']				= 'Test Batteries';
$lang['index_subhead']				= 'List of all test batteries';
$lang['index_id']					= 'ID';
$lang['index_company_id']			= 'Company';
$lang['index_name']			= 'Name';
$lang['index_tests']			= 'Tests';

$lang['index_created_on']			= 'Created On';
$lang['index_created_by']			= 'Created By';
$lang['index_modified_on']			= 'Modified On';
$lang['index_modified_by']			= 'Modified By';
$lang['index_status']				= 'Status';
$lang['index_action']				= 'Action';

// View Function
$lang['view_heading']				= 'View Test battery';

// Add Function
$lang['add_heading']				= 'Add Test battery';
$lang['add_success']				= 'Test battery has been successfully added';

// Edit Function
$lang['edit_heading']				= 'Edit Test battery';
$lang['edit_success']				= 'Test battery has been successfully updated';

// Delete Function
$lang['delete_heading']				= 'Delete Test battery';
$lang['delete_confirm']				= 'Are you sure you want to delete this test battery?';
$lang['delete_success']				= 'Test battery has been successfully deleted';
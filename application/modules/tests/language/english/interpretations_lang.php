<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Interpretations Language File (English)
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Interpretations';

// Labels
$lang['interpretation_norm_to_use']			= 'Norm To Use';
$lang['interpretation_norm_value']			= 'Norm Value';
$lang['interpretation_gender']			= 'Gender';
$lang['interpretation_content']			= 'Content';

// Buttons
$lang['button_add']					= 'Add Interpretation';
$lang['button_update']				= 'Update Interpretation';
$lang['button_delete']				= 'Delete Interpretation';
$lang['button_edit_this']			= 'Edit This';


// Index Function
$lang['index_heading']				= 'Interpretations';
$lang['index_subhead']				= 'List of all interpretations';
$lang['index_id']					= 'ID';
$lang['index_norm_to_use']			= 'Norm To Use';
$lang['index_norm_value']			= 'Norm Value';
$lang['index_gender']			= 'Gender';
$lang['index_content']			= 'Content';

$lang['index_created_on']			= 'Created On';
$lang['index_created_by']			= 'Created By';
$lang['index_modified_on']			= 'Modified On';
$lang['index_modified_by']			= 'Modified By';
$lang['index_status']				= 'Status';
$lang['index_action']				= 'Action';

// View Function
$lang['view_heading']				= 'View Interpretation';

// Add Function
$lang['add_heading']				= 'Add Interpretation';
$lang['add_success']				= 'Interpretation has been successfully added';

// Edit Function
$lang['edit_heading']				= 'Edit Interpretation';
$lang['edit_success']				= 'Interpretation has been successfully updated';

// Delete Function
$lang['delete_heading']				= 'Delete Interpretation';
$lang['delete_confirm']				= 'Are you sure you want to delete this interpretation?';
$lang['delete_success']				= 'Interpretation has been successfully deleted';
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Examineess Language File (English)
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */

// Breadcrumbs
$lang['crumb_module']			= 'Examinees';

// Labels
$lang['examinee_count'] = 'No of examinees';
$lang['examinee_date'] = 'Date';

// Buttons
$lang['button_add']					= 'Add Examinees';
$lang['button_update']				= 'Update Examinees';
$lang['button_delete']				= 'Delete Examinees';
$lang['button_edit_this']			= 'Edit This';


// Index Function
$lang['index_heading']				= 'Examineess';
$lang['index_subhead']				= 'List of all Examineess';
$lang['index_id']					= 'ID';
$lang['index_name']			= 'Name';
$lang['index_slug']			= 'Slug';
$lang['index_type']			= 'Type';
$lang['index_instruction']			= 'Instruction';
$lang['index_parent_Examinees_id']			= 'Parent Examinees';
$lang['index_is_timed']			= 'Is Timed';
$lang['index_time_minute']			= 'Time Minute';
$lang['index_time_seconds']			= 'Time Seconds';
$lang['index_norm_type']			= 'Norm Type';

$lang['index_created_on']			= 'Created On';
$lang['index_created_by']			= 'Created By';
$lang['index_modified_on']			= 'Modified On';
$lang['index_modified_by']			= 'Modified By';
$lang['index_status']				= 'Status';
$lang['index_action']				= 'Action';

// View Function
$lang['view_heading']				= 'View Examinees';

// Add Function
$lang['add_heading']				= 'Add Examinees';
$lang['add_success']				= 'Examinees has been successfully added';

// Edit Function
$lang['edit_heading']				= 'Edit Examinees';
$lang['edit_success']				= 'Examinees has been successfully updated';

// Delete Function
$lang['delete_heading']				= 'Delete Examinees';
$lang['delete_confirm']				= 'Are you sure you want to delete this Examinees?';
$lang['delete_success']				= 'Examinees has been successfully deleted';

$lang['page_subhead'] = 'abc';
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Questions Language File (English)
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Questions';

// Labels
$lang['question_factor_id']			= 'Factor';
$lang['question_content']			= 'Content';
$lang['question_options']			= 'Options';

// Buttons
$lang['button_add']					= 'Add Question';
$lang['button_update']				= 'Update Question';
$lang['button_delete']				= 'Delete Question';
$lang['button_edit_this']			= 'Edit This';


// Index Function
$lang['index_heading']				= 'Questions';
$lang['index_subhead']				= 'List of all questions';
$lang['index_id']					= 'ID';
$lang['index_factor_id']			= 'Factor';
$lang['index_content']			= 'Content';
$lang['index_options']			= 'Options';

$lang['index_created_on']			= 'Created On';
$lang['index_created_by']			= 'Created By';
$lang['index_modified_on']			= 'Modified On';
$lang['index_modified_by']			= 'Modified By';
$lang['index_status']				= 'Status';
$lang['index_action']				= 'Action';

// View Function
$lang['view_heading']				= 'View Question';

// Add Function
$lang['add_heading']				= 'Add Question';
$lang['add_success']				= 'Question has been successfully added';

// Edit Function
$lang['edit_heading']				= 'Edit Question';
$lang['edit_success']				= 'Question has been successfully updated';

// Delete Function
$lang['delete_heading']				= 'Delete Question';
$lang['delete_confirm']				= 'Are you sure you want to delete this question?';
$lang['delete_success']				= 'Question has been successfully deleted';
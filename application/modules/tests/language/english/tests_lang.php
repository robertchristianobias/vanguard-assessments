<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Tests Language File (English)
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */

// Breadcrumbs
$lang['crumb_module']			= 'Tests';

// Labels
$lang['test_name']			    = 'Name';
$lang['test_slug']			    = 'Slug';
$lang['test_type']			    = 'Type';
$lang['test_instruction']		= 'Instruction';
$lang['test_parent_test_id']	= 'Parent Test';
$lang['test_is_timed']			= 'Is Timed?';
$lang['test_time_minute']		= 'Time Minute';
$lang['test_time_seconds']		= 'Time Seconds';
$lang['test_norm_type']			= 'Norm Type';
$lang['test_option_type']       = 'Option Type';
$lang['test_option_has_label']  = 'Has Label?';
$lang['test_option_no']         = 'No. of options';
$lang['test_age_range']         = 'Age Range';
$lang['test_category_range']    = 'Category Range';
$lang['test_tscore_range']      = 'TScore Range';
$lang['test_percentile_range']  = 'Percentile Range';


// Buttons
$lang['button_add']					= 'Add Test';
$lang['button_update']				= 'Update Test';
$lang['button_delete']				= 'Delete Test';
$lang['button_edit_this']			= 'Edit This';


// Index Function
$lang['index_heading']				= 'Tests';
$lang['index_subhead']				= 'List of all tests';
$lang['index_id']					= 'ID';
$lang['index_name']			= 'Name';
$lang['index_slug']			= 'Slug';
$lang['index_type']			= 'Type';
$lang['index_instruction']			= 'Instruction';
$lang['index_parent_test_id']			= 'Parent Test';
$lang['index_is_timed']			= 'Is Timed';
$lang['index_time_minute']			= 'Time Minute';
$lang['index_time_seconds']			= 'Time Seconds';
$lang['index_norm_type']			= 'Norm Type';

$lang['index_created_on']			= 'Created On';
$lang['index_created_by']			= 'Created By';
$lang['index_modified_on']			= 'Modified On';
$lang['index_modified_by']			= 'Modified By';
$lang['index_status']				= 'Status';
$lang['index_action']				= 'Action';

// View Function
$lang['view_heading']				= 'View Test';

// Add Function
$lang['add_heading']				= 'Add Test';
$lang['add_success']				= 'Test has been successfully added';

// Edit Function
$lang['edit_heading']				= 'Edit Test';
$lang['edit_success']				= 'Test has been successfully updated';

// Delete Function
$lang['delete_heading']				= 'Delete Test';
$lang['delete_confirm']				= 'Are you sure you want to delete this test?';
$lang['delete_success']				= 'Test has been successfully deleted';

$lang['page_subhead'] = 'abc';
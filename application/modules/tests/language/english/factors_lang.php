<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Factors Language File (English)
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Factors';

// Labels
$lang['factor_name']			= 'Name';
$lang['factor_norm_to_use_in_report']			= 'Norm To Use In Report';
$lang['factor_norm_type']			= 'Norm Type';
$lang['factor_left_description']			= 'Left Description';
$lang['factor_right_description']			= 'Right Description';
$lang['factor_norm_to_use']			= 'Norm To Use';
$lang['factor_norms']			= 'Norms';

// Buttons
$lang['button_add']					= 'Add Factor';
$lang['button_update']				= 'Update Factor';
$lang['button_delete']				= 'Delete Factor';
$lang['button_edit_this']			= 'Edit This';


// Index Function
$lang['index_heading']				= 'Factors';
$lang['index_subhead']				= 'List of all factors';
$lang['index_id']					= 'ID';
$lang['index_name']			= 'Name';
$lang['index_norm_to_use_in_report']			= 'Norm To Use In Report';
$lang['index_norm_type']			= 'Norm Type';
$lang['index_left_description']			= 'Left Description';
$lang['index_right_description']			= 'Right Description';
$lang['index_norm_to_use']			= 'Norm To Use';
$lang['index_norms']			= 'Norms';

$lang['index_created_on']			= 'Created On';
$lang['index_created_by']			= 'Created By';
$lang['index_modified_on']			= 'Modified On';
$lang['index_modified_by']			= 'Modified By';
$lang['index_status']				= 'Status';
$lang['index_action']				= 'Action';

// View Function
$lang['view_heading']				= 'View Factor';

// Add Function
$lang['add_heading']				= 'Add Factor';
$lang['add_success']				= 'Factor has been successfully added';

// Edit Function
$lang['edit_heading']				= 'Edit Factor';
$lang['edit_success']				= 'Factor has been successfully updated';

// Delete Function
$lang['delete_heading']				= 'Delete Factor';
$lang['delete_confirm']				= 'Are you sure you want to delete this factor?';
$lang['delete_success']				= 'Factor has been successfully deleted';
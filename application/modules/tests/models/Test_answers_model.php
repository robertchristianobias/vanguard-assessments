<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Tests_model Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Test_answers_model extends BF_Model {

	protected $table_name			= 'test_answers';
	protected $key					= 'test_answer_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'test_answer_created_on';
	protected $created_by_field		= 'test_answer_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'test_answer_modified_on';
	protected $modified_by_field	= 'test_answer_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'test_answer_deleted';
	protected $deleted_by_field		= 'test_answer_deleted_by';	
}


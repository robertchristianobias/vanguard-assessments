<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Tests_model Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Tests_model extends BF_Model {

	protected $table_name			= 'tests';
	protected $key					= 'test_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'test_created_on';
	protected $created_by_field		= 'test_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'test_modified_on';
	protected $modified_by_field	= 'test_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'test_deleted';
	protected $deleted_by_field		= 'test_deleted_by';

	// --------------------------------------------------------------------

	/**
	 * get_datatables
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function get_datatables()
	{
		$fields = array(
			'test_id', 
			'test_name',
			'test_type',
			'test_norm_type',

			'test_created_on', 
			'concat(creator.first_name, " ", creator.last_name)', 
			'test_modified_on', 
			'concat(modifier.first_name, " ", modifier.last_name)',
			'test_parent_test_id'
		);

		// $callback = array(
        //     array(
        //         'method'  => array('Callbacks', 'tests')
        //     )
        // );

		return $this->join('users as creator', 'creator.id = test_created_by', 'LEFT')
					->join('users as modifier', 'modifier.id = test_modified_by', 'LEFT')
					->where('test_parent_test_id', 0)
					->datatables($fields);
	}

	public function get_subtest_datatables($parent_test_id)
	{
		$fields = array(
			'test_id', 
			'test_name',
			'test_type',
			'test_norm_type',

			'test_created_on', 
			'concat(creator.first_name, " ", creator.last_name)', 
			'test_modified_on', 
			'concat(modifier.first_name, " ", modifier.last_name)',
			'test_parent_test_id'
		);

		// $callback = array(
        //     array(
        //         'method'  => array('Callbacks', 'tests')
        //     )
        // );

		return $this->join('users as creator', 'creator.id = test_created_by', 'LEFT')
					->join('users as modifier', 'modifier.id = test_modified_by', 'LEFT')
					->where('test_parent_test_id', $parent_test_id)
					->datatables($fields);
	}

	public function get_tests()
	{
		if(! $tests = $this->cache->get('tests'))
		{
			$tests = $this->where('test_deleted', 0)
					  	  ->order_by('test_name')
					  	  ->find_all();

			$this->cache->save('tests', $tests, 300); // TTL in seconds
		}
		
		return $tests;
	}

	public function get_test($test_id)
	{
		if(! $test = $this->cache->get('test_'.$test_id))
		{
			$test = $this->where('test_deleted', 0)
					  	  ->find($test_id);

			$this->cache->save('test_'.$test_id, $test, 300); // TTL in seconds
		}
		
		return $test;
	}

	public function tests_dropdown($key, $val, $blank = FALSE)
	{
		// get the accounts dropdown
		if (! $tests_dropdown = $this->cache->get('tests_dropdown'))
		{
			$tests_dropdown = $this->where('test_deleted', 0)
								   ->order_by('test_name')
								   ->format_dropdown($key, $val, $blank);
			
			$this->cache->save('tests_dropdown', $tests_dropdown, 300); // TTL in seconds
		}

		return $tests_dropdown;
	}
}


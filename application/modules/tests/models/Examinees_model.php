<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * examinees_model Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Examinees_model extends BF_Model {

	protected $table_name			= 'examinees';
	protected $key					= 'examinee_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'examinee_created_on';
	protected $created_by_field		= 'examinee_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'examinee_modified_on';
	protected $modified_by_field	= 'examinee_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'examinee_deleted';
	protected $deleted_by_field		= 'examinee_deleted_by';

	public function get_examinees($test_battery_id, $company_id)
	{
		$examinees = $this->join('users', 'id = examinee_user_id', 'LEFT')
						  ->where('examinee_test_battery_id', $test_battery_id)
						  ->where('examinee_company_id', $company_id)
						  ->where('examinee_deleted', 0)
						  ->find_all();

		return $examinees;
	}

	public function get_examinee_by_user($user_id)
	{
		$examinee = $this->where('examinee_deleted', 0)
						 ->find_by('examinee_user_id', $user_id);

		return $examinee;
	}
}
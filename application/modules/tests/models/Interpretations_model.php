<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Interpretations_model Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Interpretations_model extends BF_Model {

	protected $table_name			= 'interpretations';
	protected $key					= 'interpretation_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'interpretation_created_on';
	protected $created_by_field		= 'interpretation_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'interpretation_modified_on';
	protected $modified_by_field	= 'interpretation_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'interpretation_deleted';
	protected $deleted_by_field		= 'interpretation_deleted_by';

	// --------------------------------------------------------------------

	/**
	 * get_datatables
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function get_datatables($factor_id)
	{
		$fields = array(
			'interpretation_id', 
			'interpretation_norm_to_use',
			'interpretation_norm_value',
			'interpretation_gender',
			'interpretation_content',

			'interpretation_created_on', 
			'concat(creator.first_name, " ", creator.last_name)', 
			'interpretation_modified_on', 
			'concat(modifier.first_name, " ", modifier.last_name)'
		);

		return $this->join('users as creator', 'creator.id = interpretation_created_by', 'LEFT')
					->join('users as modifier', 'modifier.id = interpretation_modified_by', 'LEFT')
					->where('interpretation_factor_id', $factor_id)
					->datatables($fields);
	}
}
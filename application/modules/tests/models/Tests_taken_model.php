<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Tests_taken_model Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Tests_taken_model extends BF_Model {

	protected $table_name			= 'tests_taken';
	protected $key					= 'test_taken_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'test_taken_created_on';
	protected $created_by_field		= 'test_taken_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'test_taken_modified_on';
	protected $modified_by_field	= 'test_taken_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'test_taken_deleted';
	protected $deleted_by_field		= 'test_taken_deleted_by';


	public function get_tests_taken($user_id, $test_battery_id, $company_id)
	{
		$tests_taken = $this->where('test_taken_user_id', $user_id)
							->where('test_taken_test_battery_id', $test_battery_id)
							->where('test_taken_company_id', $company_id)
							->where('test_taken_deleted', 0)
							->where('test_taken_status', 'Pending')
							->find_all();

		return $tests_taken;
	}
}


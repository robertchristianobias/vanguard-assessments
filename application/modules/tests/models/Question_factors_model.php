<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Question_factors_model Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Question_factors_model extends BF_Model {

	protected $table_name			= 'question_factors';
	protected $key					= 'qf_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'qf_created_on';
	protected $created_by_field		= 'qf_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'qf_modified_on';
	protected $modified_by_field	= 'qf_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'qf_deleted';
	protected $deleted_by_field		= 'qf_deleted_by';

	public function get_question_factors($question_id)
	{
		$factors = $this->join('factors', 'factor_id = qf_factor_id', 'LEFT')
						->where('qf_deleted', 0)
						->where('qf_question_id', $question_id)
						->find_all();

		return $factors;
	}

}
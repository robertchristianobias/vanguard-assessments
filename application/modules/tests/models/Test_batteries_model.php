<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Test_batteries_model Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Test_batteries_model extends BF_Model {

	protected $table_name			= 'test_batteries';
	protected $key					= 'test_battery_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'test_battery_created_on';
	protected $created_by_field		= 'test_battery_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'test_battery_modified_on';
	protected $modified_by_field	= 'test_battery_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'test_battery_deleted';
	protected $deleted_by_field		= 'test_battery_deleted_by';

	// --------------------------------------------------------------------

	/**
	 * get_datatables
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function get_datatables()
	{
		$fields = array(
			'test_battery_id', 
			'company_name',
			'test_battery_name',
			'test_battery_tests',

			'test_battery_created_on', 
			'concat(creator.first_name, " ", creator.last_name)', 
			'test_battery_modified_on', 
			'concat(modifier.first_name, " ", modifier.last_name)',
			'company_id'
		);

		$callback = array(
            array(
                'method'  => array('Callbacks', 'test_batteries')
            )
        );

		return $this->join('companies', 'company_id=test_battery_company_id', 'LEFT')
					->join('users as creator', 'creator.id = test_battery_created_by', 'LEFT')
					->join('users as modifier', 'modifier.id = test_battery_modified_by', 'LEFT')
					->datatables($fields, $callback);
	}

	public function get_test_battery($id)
	{
		if (! $test_battery = $this->cache->get('test_battery'))
		{
			$test_battery = $this->where('test_battery_deleted', 0)
								 ->find($id);

			$this->cache->save('test_battery', $test_battery, 300); // TTL in seconds
		}

		return $test_battery;
	}

	
}
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Factors_model Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Factors_model extends BF_Model {

	protected $table_name			= 'factors';
	protected $key					= 'factor_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'factor_created_on';
	protected $created_by_field		= 'factor_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'factor_modified_on';
	protected $modified_by_field	= 'factor_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'factor_deleted';
	protected $deleted_by_field		= 'factor_deleted_by';

	// --------------------------------------------------------------------

	/**
	 * get_datatables
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function get_datatables()
	{
		$fields = array(
			'factor_id', 
			'factor_name',

			'factor_created_on', 
			'concat(creator.first_name, " ", creator.last_name)', 
			'factor_modified_on', 
			'concat(modifier.first_name, " ", modifier.last_name)'
		);

		return $this->join('users as creator', 'creator.id = factor_created_by', 'LEFT')
					->join('users as modifier', 'modifier.id = factor_modified_by', 'LEFT')
					->datatables($fields);
	}

	public function factors_dropdown($key, $val, $test_id, $blank = FALSE)
	{
		// get the accounts dropdown
		if (! $factors_dropdown = $this->cache->get('factors_dropdown'))
		{
			$factors_dropdown = $this->where('factor_deleted', 0)
								     ->where('factor_test_id', $test_id)
								     ->order_by('factor_name')
								     ->format_dropdown($key, $val, $blank);
			
			$this->cache->save('factors_dropdown', $factors_dropdown, 300); // TTL in seconds
		}

		return $factors_dropdown;
	}
}
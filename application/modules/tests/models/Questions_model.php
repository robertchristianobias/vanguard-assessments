<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Questions_model Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Questions_model extends BF_Model {

	protected $table_name			= 'questions';
	protected $key					= 'question_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'question_created_on';
	protected $created_by_field		= 'question_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'question_modified_on';
	protected $modified_by_field	= 'question_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'question_deleted';
	protected $deleted_by_field		= 'question_deleted_by';

	// --------------------------------------------------------------------

	/**
	 * get_datatables
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function get_datatables($test_id)
	{
		$fields = array(
			'question_id', 
			'question_factor_id',
			'question_content',
			'question_created_on', 
			'concat(creator.first_name, " ", creator.last_name)', 
			'question_modified_on', 
			'concat(modifier.first_name, " ", modifier.last_name)'
		);

		$callback = array(
            array(
                'method'  => array('Callbacks', 'questions')
            )
        );

		return $this->join('users as creator', 'creator.id = question_created_by', 'LEFT')
					->join('users as modifier', 'modifier.id = question_modified_by', 'LEFT')
					->where('question_test_id', $test_id)
					->datatables($fields, $callback);
	}

	public function get_questions($test_id, $row_per_page = '', $row_no = '')
	{
		if (!empty($row_per_page) || !empty($row_no))
		{
			$this->limit($row_per_page, $row_no);
		}

		$questions = $this->where('question_deleted', 0)
						  ->where('question_test_id', $test_id)
						  ->find_all();

		return $questions;
	}
}
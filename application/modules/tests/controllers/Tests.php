<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Tests Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Tests extends MX_Controller {
	
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model('tests_model');
		$this->load->language('tests');
	}
	
	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function index()
	{
		$this->acl->restrict('tests.tests.list');
		
		// page title
		$data['page_heading'] = lang('index_heading');
		$data['page_subhead'] = lang('index_subhead');
		
		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('tests'));
		
		// session breadcrumb
		$this->session->set_userdata('redirect', current_url());
		
		// add plugins
		$this->template->add_css('assets/plugins/DataTables/datatables.min.css');
		$this->template->add_js('assets/plugins/DataTables/datatables.min.js');
		
		// render the page
		$this->template->add_css(module_css('tests', 'tests_index'), 'embed');
		$this->template->add_js(module_js('tests', 'tests_index'), 'embed');
		$this->template->write_view('content', 'tests_index', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * datatables
	 *
	 * @access	public
	 * @param	mixed datatables parameters (datatables.net)
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function datatables()
	{
		$this->acl->restrict('tests.tests.list');

		echo $this->tests_model->get_datatables();
	}

	// --------------------------------------------------------------------

	/**
	 * form
	 *
	 * @access	public
	 * @param	$action string
	 * @param   $id integer
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	function form($action = 'add', $id = FALSE)
	{
		$this->acl->restrict('tests.tests.' . $action, 'modal');

		$data['page_heading'] = lang($action . '_heading');
		$data['action'] = $action;
		$data['tests_dropdown'] = $this->tests_model->tests_dropdown('test_id', 'test_name', TRUE);

		if ($this->input->post())
		{
			if ($this->_save($action, $id))
			{
				echo json_encode(array('success' => true, 'message' => lang($action . '_success'))); exit;
			}
			else
			{	
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(					
					'test_name'		=> form_error('test_name'),
					'test_slug'		=> form_error('test_slug'),
					'test_type'		=> form_error('test_type'),
					'test_instruction'		=> form_error('test_instruction'),
					//'test_is_timed'		=> form_error('test_is_timed'),
					'test_norm_type'		=> form_error('test_norm_type'),
					'test_option_type' => form_error('test_option_type'),
					'test_option_no' => form_error('test_option_no')
				);
				echo json_encode($response);
				exit;
			}
		}

		if ($action != 'add') $data['record'] = $this->tests_model->find($id);

		// render the page
		$this->template->set_template('modal');

		$this->template->add_js('assets/plugins/tinymce/tinymce.min.js');

		$this->template->add_css(module_css('tests', 'tests_form'), 'embed');
		$this->template->add_js(module_js('tests', 'tests_form'), 'embed');
		$this->template->write_view('content', 'tests_form', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * delete
	 *
	 * @access	public
	 * @param	integer $id
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	function delete($id)
	{
		$this->acl->restrict('tests.tests.delete', 'modal');

		$data['page_heading'] = lang('delete_heading');
		$data['page_confirm'] = lang('delete_confirm');
		$data['page_button'] = lang('button_delete');
		$data['datatables_id'] = '#datatables';

		if ($this->input->post())
		{
			$this->tests_model->delete($id);

			echo json_encode(array('success' => true, 'message' => lang('delete_success'))); exit;
		}

		$this->load->view('../../../views/confirm', $data);
	}


	// --------------------------------------------------------------------

	/**
	 * _save
	 *
	 * @access	private
	 * @param	string $action
	 * @param 	integer $id
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	private function _save($action = 'add', $id = 0)
	{
		// validate inputs
		$this->form_validation->set_rules('test_name', lang('test_name'), 'required');
		$this->form_validation->set_rules('test_slug', lang('test_slug'), 'required');
		$this->form_validation->set_rules('test_type', lang('test_type'), 'required');
		$this->form_validation->set_rules('test_instruction', lang('test_instruction'), 'required');
		//$this->form_validation->set_rules('test_is_timed', lang('test_is_timed'), 'required');
		$this->form_validation->set_rules('test_norm_type', lang('test_norm_type'), 'required');
		$this->form_validation->set_rules('test_option_type', lang('test_option_type'), 'required');
		$this->form_validation->set_rules('test_option_no', lang('test_option_no'), 'required');

		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
		
		if ($this->form_validation->run($this) == FALSE)
		{
			return FALSE;
		}
		
		$test_option_has_label = $this->input->post('test_option_has_label');
		if ( $test_option_has_label )
		{
			$test_option_has_label = 'on';
		}
		else
		{
			$test_option_has_label = 'off';
		}
		$data = array(
			'test_name'		=> $this->input->post('test_name'),
			'test_slug'		=> $this->input->post('test_slug'),
			'test_type'		=> $this->input->post('test_type'),
			'test_instruction'		=> $this->input->post('test_instruction'),
			'test_parent_test_id'		=> $this->input->post('test_parent_test_id'),
			'test_is_timed'		=> $this->input->post('test_is_timed'),
			'test_time_minute'		=> $this->input->post('test_time_minute'),
			'test_time_seconds'		=> $this->input->post('test_time_seconds'),
			'test_norm_type'		=> $this->input->post('test_norm_type'),
			'test_option_type' => $this->input->post('test_option_type'),
			'test_option_no' => $this->input->post('test_option_no'),
			'test_option_has_label' => $test_option_has_label,
			'test_option_label' => json_encode($this->input->post('test_option_label')),
			'test_age_range' => $this->input->post('test_age_range'),
			'test_category_range' => $this->input->post('test_category_range'),
			'test_tscore_range' => $this->input->post('test_tscore_range'),
			'test_percentile_range' => $this->input->post('test_percentile_range')
		);
		
		if ($action == 'add')
		{
			$insert_id = $this->tests_model->insert($data);
			$return = (is_numeric($insert_id)) ? $insert_id : FALSE;
		}
		else if ($action == 'edit')
		{
			$return = $this->tests_model->update($id, $data);
			$this->cache->delete('test_'.$id);
		}

		$this->cache->delete('tests');
		$this->cache->delete('tests_dropdown');
		

		return $return;

	}

	public function generate_labels()
	{
		$data['options'] = $this->input->post('options');

		$this->template->write_view('content', 'tests_generate_labels', $data);
		echo $this->template->render('content', TRUE);
	}

	public function subtests($parent_test_id)
	{
		$this->acl->restrict('tests.tests.list');

		$test = $this->tests_model->get_test($parent_test_id);
		
		// page title
		$data['page_heading']   = 'Subtests';
		$data['page_subhead']   = $test->test_name;
		$data['parent_test_id'] = $parent_test_id;
		
		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push($test->test_name, site_url('tests'));
		
		// session breadcrumb
		$this->session->set_userdata('redirect', current_url());
		
		// add plugins
		$this->template->add_css('assets/plugins/DataTables/datatables.min.css');
		$this->template->add_js('assets/plugins/DataTables/datatables.min.js');
		
		// render the page
		$this->template->add_js(module_js('tests', 'tests_subtest_index'), 'embed');
		$this->template->write_view('content', 'tests_subtest_index', $data);
		$this->template->render();
	}

	public function subtest_datatables($parent_test_id)
	{
		$this->acl->restrict('tests.tests.list');

		echo $this->tests_model->get_subtest_datatables($parent_test_id);
	}
}

/* End of file Tests.php */
/* Location: ./application/modules/tests/controllers/Tests.php */
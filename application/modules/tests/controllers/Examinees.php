<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Examinees Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */

class Examinees extends MX_Controller {
	
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model('examinees_model');
		$this->load->model('companies/companies_model');

		$this->load->language('examinees');
	}
	
	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function index()
	{
		$this->acl->restrict('companies.examinees.list');
		
		// page title
		$data['page_heading'] = lang('index_heading');
		$data['page_subhead'] = lang('index_subhead');
		
		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('examinees'));
		
		// session breadcrumb
		$this->session->set_userdata('redirect', current_url());
		
		// add plugins
		$this->template->add_css('assets/plugins/DataTables/datatables.min.css');
		$this->template->add_js('assets/plugins/DataTables/datatables.min.js');
		
		// render the page
		$this->template->add_css(module_css('examinees', 'examinees_index'), 'embed');
		$this->template->add_js(module_js('examinees', 'examinees_index'), 'embed');
		$this->template->write_view('content', 'examinees_index', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * datatables
	 *
	 * @access	public
	 * @param	mixed datatables parameters (datatables.net)
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function datatables()
	{
		$this->acl->restrict('companies.examinees.list');

		echo $this->examinees_model->get_datatables();
	}

	// --------------------------------------------------------------------

	/**
	 * form
	 *
	 * @access	public
	 * @param	$action string
	 * @param   $id integer
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	function form($action = 'add', $test_battery_id = 0, $company_id = 0,  $id = FALSE)
	{
		$this->acl->restrict('companies.examinees.' . $action, 'modal');

		$data['page_heading'] = lang($action . '_heading');
		$data['action'] = $action;

		if ($this->input->post())
		{
			if ($this->_save($action, $test_battery_id, $company_id, $id))
			{
				echo json_encode(array(
					'success'  => true, 
					'message'  => lang($action . '_success'), 
					'redirect' => site_url('tests/examinees/download/'. $test_battery_id . '/' . $company_id) 
				)); exit;
			}
			else
			{	
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(					
					'examinee_count'	=> form_error('examinee_count'),
					'examinee_date'		=> form_error('examinee_date'),
				);
				echo json_encode($response);
				exit;
			}
		}

		if ($action != 'add') $data['record'] = $this->examinees_model->find($id);

		// render the page
		$this->template->set_template('modal');
		
		$this->template->add_js('assets/plugins/bootstrap-daterangepicker/moment.min.js');
		$this->template->add_js('assets/plugins/bootstrap-daterangepicker/daterangepicker.js');
		$this->template->add_css('assets/plugins/bootstrap-daterangepicker/daterangepicker.css');
		
		$this->template->add_css(module_css('tests', 'examinees_form'), 'embed');
		$this->template->add_js(module_js('tests', 'examinees_form'), 'embed');
		$this->template->write_view('content', 'examinees_form', $data);
		$this->template->render();
	}	// --------------------------------------------------------------------

	/**
	 * _save
	 *
	 * @access	private
	 * @param	string $action
	 * @param 	integer $id
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	private function _save($action = 'add', $test_battery_id = 0, $company_id = 0, $id = 0)
	{
		// validate inputs
		$this->form_validation->set_rules('examinee_count', lang('examinee_count'), 'required');
		$this->form_validation->set_rules('examinee_date', lang('examinee_date'), 'required');

		$this->form_validation->set_message('required', 'This field is required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
		
		if ($this->form_validation->run($this) == FALSE)
		{
			return FALSE;
		}

		$company = $this->companies_model->get_company($company_id);
		list($start_date, $end_date) = explode(' - ', $this->input->post('examinee_date'));

		$examinee_count = $this->input->post('examinee_count');
		$data = array();
		if ( $examinee_count )
		{
			for ($i = 1; $i <= $examinee_count; $i++)
			{
				// create the user
				$username 	= $company->company_prefix . '_' . random_string('alnum', 4);
				$email    	= '';
				$password 	= random_string('alnum', 8);
				$groups 	= array('2');
				$additional_data = array();
				$user_id = $this->ion_auth->register($username, $password, $email, $additional_data, $groups);

				if ( $user_id )
				{
					$data[] = array(
						'examinee_raw_password' => $password,
						'examinee_user_id'	=> $user_id,
						'examinee_test_battery_id' => $test_battery_id,
						'examinee_company_id' => $company_id,
						'examinee_start_date' => date('Y-m-d', strtotime($start_date)),
						'examinee_end_date'	  => date('Y-m-d', strtotime($end_date))
					);
				}
			}

			$this->examinees_model->insert_batch($data);
		}

		return TRUE;
	}

	public function download($test_battery_id, $company_id)
	{
		$this->load->helper('download');

		// file name 
		$filename = 'assets/uplaods/csv/csv_' . date('Ymd') . '.csv';

		header("Content-Description: File Transfer"); 
		header("Content-Disposition: attachment; filename=$filename"); 
		header("Content-Type: application/csv; ");
		 
		// get data 
		$examinees = $this->examinees_model->get_examinees($test_battery_id, $company_id);
		$csv_data = array();
		if ( $examinees)
		{
			foreach ( $examinees as $examinee )
			{
				$csv_data[] = array($examinee->username, $examinee->examinee_raw_password, date('F d, Y', strtotime($examinee->examinee_start_date)), date('F d, Y', strtotime($examinee->examinee_end_date)));
			}
		}
	  
		// file creation 
		$file = fopen('php://output', 'w');
	   
		$header = array("Username","Password","Start Date","End Date"); 
		fputcsv($file, $header);
		foreach ($csv_data as $key=>$line){ 
			fputcsv($file, $line); 
		}
		fclose($file); 

		force_download($filename, NULL);
		exit; 
		
	}
}

/* End of file examinees.php */
/* Location: ./application/modules/examinees/controllers/examinees.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Test_batteries Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Test_batteries extends MX_Controller {
	
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	function __construct()
	{
		parent::__construct();

		$this->load->model('companies/companies_model');

		$this->load->model('test_batteries_model');
		$this->load->model('tests_model');
		$this->load->language('test_batteries');
	}
	
	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function index()
	{
		$this->acl->restrict('tests.test_batteries.list');
		
		// page title
		$data['page_heading'] = lang('index_heading');
		$data['page_subhead'] = lang('index_subhead');
		
		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('test_batteries'));
		
		// session breadcrumb
		$this->session->set_userdata('redirect', current_url());
		
		// add plugins
		$this->template->add_css('assets/plugins/DataTables/datatables.min.css');
		$this->template->add_js('assets/plugins/DataTables/datatables.min.js');
		
		// render the page
		$this->template->add_css(module_css('tests', 'test_batteries_index'), 'embed');
		$this->template->add_js(module_js('tests', 'test_batteries_index'), 'embed');
		$this->template->write_view('content', 'test_batteries_index', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * datatables
	 *
	 * @access	public
	 * @param	mixed datatables parameters (datatables.net)
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function datatables()
	{
		$this->acl->restrict('tests.test_batteries.list');

		echo $this->test_batteries_model->get_datatables();
	}

	// --------------------------------------------------------------------

	/**
	 * form
	 *
	 * @access	public
	 * @param	$action string
	 * @param   $id integer
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	function form($action = 'add', $id = FALSE)
	{
		$this->acl->restrict('tests.test_batteries.' . $action);

		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('test_batteries'));

		$data['page_heading'] = lang($action . '_heading');
		$data['page_subhead'] = '';
		$data['action'] = $action;
		$data['companies_dropdown'] = $this->companies_model->get_companies_dropdown('company_id', 'company_name', TRUE);
		$data['tests'] = $this->tests_model->get_tests();
		
		if ($this->input->post())
		{
			if ($this->_save($action, $id))
			{
				echo json_encode(array(
					'success'  => true, 
					'message'  => lang($action . '_success'),
					'redirect' => site_url('tests/test_batteries')
				)); exit;
			}
			else
			{	
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors']  = array(					
					'test_battery_company_id' => form_error('test_battery_company_id'),
					'test_battery_name'		  => form_error('test_battery_name'),
					'test_battery_tests'	  => form_error('test_battery_tests[]'),
				);
				echo json_encode($response);
				exit;
			}
		}

		if ($action != 'add')
		{
			$data['record'] = $this->test_batteries_model->find($id);
		}

		// render the page
		// $this->template->set_template('modal');

		$this->template->add_css('assets/plugins/bootstrap-duallistbox/bootstrap-duallistbox.min.css');
		$this->template->add_js('assets/plugins/bootstrap-duallistbox/jquery.bootstrap-duallistbox.min.js');
		$this->template->add_css('assets/plugins/select2/css/select2.min.css');
		$this->template->add_js('assets/plugins/select2/js/select2.min.js');

		$this->template->add_css(module_css('tests', 'test_batteries_form'), 'embed');
		$this->template->add_js(module_js('tests', 'test_batteries_form'), 'embed');
		$this->template->write_view('content', 'test_batteries_form', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * delete
	 *
	 * @access	public
	 * @param	integer $id
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	function delete($id)
	{
		$this->acl->restrict('tests.test_batteries.delete', 'modal');

		$data['page_heading'] = lang('delete_heading');
		$data['page_confirm'] = lang('delete_confirm');
		$data['page_button'] = lang('button_delete');
		$data['datatables_id'] = '#datatables';

		if ($this->input->post())
		{
			$this->test_batteries_model->delete($id);

			echo json_encode(array('success' => true, 'message' => lang('delete_success'))); exit;
		}

		$this->load->view('../../../views/confirm', $data);
	}


	// --------------------------------------------------------------------

	/**
	 * _save
	 *
	 * @access	private
	 * @param	string $action
	 * @param 	integer $id
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	private function _save($action = 'add', $id = 0)
	{
		// validate inputs
		$this->form_validation->set_rules('test_battery_company_id', lang('test_battery_company_id'), 'required');
		$this->form_validation->set_rules('test_battery_name', lang('test_battery_name'), 'required');
		$this->form_validation->set_rules('test_battery_tests[]', lang('test_battery_tests'), 'required');

		$this->form_validation->set_message('required', 'This field is required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
		
		if ($this->form_validation->run($this) == FALSE)
		{
			return FALSE;
		}

		$data = array(
			'test_battery_company_id'	=> $this->input->post('test_battery_company_id'),
			'test_battery_name'			=> $this->input->post('test_battery_name'),
			'test_battery_tests'		=> json_encode($this->input->post('test_battery_tests')),
		);

		if ($action == 'add')
		{
			$insert_id = $this->test_batteries_model->insert($data);
			$return = (is_numeric($insert_id)) ? $insert_id : FALSE;
		}
		else if ($action == 'edit')
		{
			$return = $this->test_batteries_model->update($id, $data);
		}

		return $return;
	}

	public function examinees($test_battery_id)
	{
		$this->acl->restrict('tests.test_batteries.add', 'modal');

		$companies    = $this->companies_model->get_companies();
		$test_battery = $this->test_batteries_model->get_test_battery($test_battery_id);

		$company_arr = array();
		if($companies)
		{
			foreach($companies as $company)
			{
				$company_arr[$company->company_id] = array(
					'company_id'	 => $company->company_id,
					'company_prefix' => $company->company_prefix
				);
			}
		}

		$total_examinees      = $this->accounts_model->count_by('account_test_battery_id', $test_battery->test_battery_id);
		
		$data['page_heading']    = 'Add Examinees';
		$data['action'] 	     = 'add';
		$data['total_examinees'] = $total_examinees;
		$data['test_battery']    = $test_battery;

		if($this->input->post())
		{
			$this->form_validation->set_rules('account_no_examinees', 'No. of examinees', 'required');
			$this->form_validation->set_rules('account_start_date', 'Start Date', 'required');
			$this->form_validation->set_rules('account_end_date', 'End Date', 'required');

			$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');

			if ($this->form_validation->run($this) == FALSE)
			{
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(					
					'account_no_examinees' => 'No. of examinees',
					'account_start_date'   => 'Start Date',
					'account_end_date'	   => 'End Date'
				);

				echo json_encode($response);
				exit;
			}
			else
			{
				
				$account_no_examinees = $this->input->post('account_no_examinees');
				$account_start_date   = date('Y-m-d', strtotime($this->input->post('account_start_date')));
				$account_end_date     = date('Y-m-d', strtotime($this->input->post('account_end_date')));

				$company_prefix = $company_arr[$test_battery->test_battery_company_id]['company_prefix'];
				for($i=1;$i<=$account_no_examinees;$i++)
				{
					$password = random_string('alnum', 8);
					$username = strtoupper($company_prefix.'-'.  str_pad($total_examinees+$i, 4, '0', STR_PAD_LEFT).'-'.random_string('alpha', 3));
					$data = array(
						'account_username' 	 	  => $username,
						'account_password' 	 	  => $password,
						'account_test_battery_id' => $test_battery->test_battery_id,
						'account_company_id'	  => $company_arr[$test_battery->test_battery_company_id]['company_id'],
						'account_start_date' 	  => $account_start_date,
						'account_end_date'   	  => $account_end_date
 					);

					 $this->accounts_model->insert($data);
				}

				echo json_encode(array('success' => TRUE, 'message' => 'Successfully added the examinees')); exit;
				
			}
		}

		// render the page
		$this->template->set_template('modal');
		
		$this->template->add_js('assets/plugins/datetimepicker/jquery.datetimepicker.js');
		$this->template->add_css('assets/plugins/datetimepicker/jquery.datetimepicker.css');

		$this->template->add_css(module_css('tests', 'test_examinees_form'), 'embed');
		$this->template->add_js(module_js('tests', 'test_examinees_form'), 'embed');

		$this->template->write_view('content', 'test_examinees_form', $data);
		$this->template->render();
	}

	public function download_examinees($test_battery_id)
	{

		$test_battery = $this->test_batteries_model->get_test_battery($test_battery_id);
		$accounts 	  = $this->accounts_model->get_account_by_test_battery($test_battery_id);
		
		$filename 	  = url_title($test_battery->test_battery_name, 'underscore', TRUE) . '_' . strtotime(date('Y-m-d')) . '.csv';

		if($accounts) 
		{
            $arr[] = array("Username", "Password", "Start Date", "End Date", "Status");
            foreach($accounts as $account) 
			{
                $arr[] = array(
					$account->account_username,
					$account->account_password,
					date('F d, Y', strtotime($account->account_start_date)),
					date('F d, Y', strtotime($account->account_end_date)),
					$account->examinee_status
				);
            }
            
            echo array_to_csv($arr, $filename); exit;

        } else {
            show_error('No Examiness yet', 500);
        }
	}

}

/* End of file Test_batteries.php */
/* Location: ./application/modules/tests/controllers/Test_batteries.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Interpretations Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Interpretations extends MX_Controller {
	
	public $factor_id;
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model('interpretations_model');
		$this->load->model('tests_model');

		$this->load->language('interpretations');

		$this->factor_id = ($this->session->userdata('factor_id') ? $this->session->userdata('factor_id') : 0) ;
	}
	
	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function index()
	{
		$this->acl->restrict('tests.interpretations.list');
		
		// page title
		$data['page_heading'] = lang('index_heading');
		$data['page_subhead'] = lang('index_subhead');
		
		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('interpretations'));
		
		// session breadcrumb
		$this->session->set_userdata('redirect', current_url());
		
		// add plugins
		$this->template->add_css('assets/plugins/DataTables/datatables.min.css');
		$this->template->add_js('assets/plugins/DataTables/datatables.min.js');
		
		// render the page
		$this->template->add_css(module_css('tests', 'interpretations_index'), 'embed');
		$this->template->add_js(module_js('tests', 'interpretations_index'), 'embed');
		$this->template->write_view('content', 'interpretations_index', $data);
		$this->template->render();
	}

	public function view($factor_id)
	{
		$this->acl->restrict('tests.interpretations.list');
		
		// page title
		$data['page_heading'] = lang('index_heading');
		$data['page_subhead'] = lang('index_subhead');
		$data['factor_id'] 	  = $factor_id;
		
		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('interpretations'));
		
		// session breadcrumb
		$this->session->set_userdata('redirect', current_url());
		
		// add plugins
		$this->template->add_css('assets/plugins/DataTables/datatables.min.css');
		$this->template->add_js('assets/plugins/DataTables/datatables.min.js');
		$this->template->add_js('var factor_id='.$factor_id.';', 'embed');
		
		// render the page
		$this->template->add_css(module_css('tests', 'interpretations_index'), 'embed');
		$this->template->add_js(module_js('tests', 'interpretations_index'), 'embed');
		$this->template->write_view('content', 'interpretations_index', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * datatables
	 *
	 * @access	public
	 * @param	mixed datatables parameters (datatables.net)
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function datatables($factor_id)
	{
		$this->acl->restrict('tests.interpretations.list');

		echo $this->interpretations_model->get_datatables($factor_id);
	}

	// --------------------------------------------------------------------

	/**
	 * form
	 *
	 * @access	public
	 * @param	$action string
	 * @param   $id integer
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	function form($action = 'add', $factor_id = 0, $id = FALSE)
	{
		$this->acl->restrict('tests.interpretations.' . $action, 'modal');

		$data['page_heading'] = lang($action . '_heading');
		$data['action'] = $action;

		if ($this->input->post())
		{
			if ($this->_save($action, $factor_id, $id))
			{
				echo json_encode(array('success' => true, 'message' => lang($action . '_success'))); exit;
			}
			else
			{	
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(					
					'interpretation_norm_to_use'		=> form_error('interpretation_norm_to_use'),
					'interpretation_norm_value'		=> form_error('interpretation_norm_value'),
					'interpretation_gender'		=> form_error('interpretation_gender'),
					'interpretation_content'		=> form_error('interpretation_content'),
				);
				echo json_encode($response);
				exit;
			}
		}

		if ($action != 'add') $data['record'] = $this->interpretations_model->find($id);

		// render the page
		$this->template->set_template('modal');
		$this->template->add_css(module_css('tests', 'interpretations_form'), 'embed');
		$this->template->add_js(module_js('tests', 'interpretations_form'), 'embed');
		$this->template->write_view('content', 'interpretations_form', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * delete
	 *
	 * @access	public
	 * @param	integer $id
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	function delete($id)
	{
		$this->acl->restrict('tests.interpretations.delete', 'modal');

		$data['page_heading'] = lang('delete_heading');
		$data['page_confirm'] = lang('delete_confirm');
		$data['page_button'] = lang('button_delete');
		$data['datatables_id'] = '#datatables';

		if ($this->input->post())
		{
			$this->interpretations_model->delete($id);

			echo json_encode(array('success' => true, 'message' => lang('delete_success'))); exit;
		}

		$this->load->view('../../../views/confirm', $data);
	}


	// --------------------------------------------------------------------

	/**
	 * _save
	 *
	 * @access	private
	 * @param	string $action
	 * @param 	integer $id
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	private function _save($action = 'add', $factor_id = 0, $id = 0)
	{
		// validate inputs
		$this->form_validation->set_rules('interpretation_norm_to_use', lang('interpretation_norm_to_use'), 'required');
		$this->form_validation->set_rules('interpretation_norm_value', lang('interpretation_norm_value'), 'required');
		$this->form_validation->set_rules('interpretation_gender', lang('interpretation_gender'), 'required');
		$this->form_validation->set_rules('interpretation_content', lang('interpretation_content'), 'required');

		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
		
		if ($this->form_validation->run($this) == FALSE)
		{
			return FALSE;
		}

		$data = array(
			'interpretation_factor_id' 		=> $factor_id,
			'interpretation_norm_to_use'	=> $this->input->post('interpretation_norm_to_use'),
			'interpretation_norm_value'		=> $this->input->post('interpretation_norm_value'),
			'interpretation_gender'			=> $this->input->post('interpretation_gender'),
			'interpretation_content'		=> $this->input->post('interpretation_content'),
		);
		

		if ($action == 'add')
		{
			$insert_id = $this->interpretations_model->insert($data);
			$return = (is_numeric($insert_id)) ? $insert_id : FALSE;
		}
		else if ($action == 'edit')
		{
			$return = $this->interpretations_model->update($id, $data);
		}

		return $return;

	}

	public function norm_value()
	{
		is_ajax();

		$norm_to_use = $this->input->post('norm_to_use');
		$test_id 	 = $this->session->userdata('test_id');
		$test_data = $this->tests_model->get_test($test_id);

		switch ( $norm_to_use )
		{
			case 'Category':
				$label   = 'Category';
				$dropdown = array();
				if ( isset($test_data->test_category_range) )
				{
					$array = explode("\n", $test_data->test_category_range);
					foreach ($array as $v)
					{
						$dropdown[$v] = $v;
					}
				}
			break;
			case 'T-Score':
				$label   = 'T-Score';
				$dropdown = array();
				if ( isset($test_data->test_tscore_range) )
				{
					$array = explode("\n", $test_data->test_tscore_range);
					foreach ($array as $v)
					{
						$dropdown[$v] = $v;
					}
				}
			break;
			case 'Sten':
				$label    = 'Sten';
				$dropdown = create_dropdown('array', '1,2,3,4,5,6,7,8,9,10');
			break;
			case 'Percentile':
				$label    = 'Percentile';
				$dropdown = array();
				if ( isset($test_data->test_percentile_range) )
				{
					$array = explode("\n", $test_data->test_percentile_range);
					foreach ($array as $v)
					{
						$dropdown[$v] = $v;
					}
				}
			break;
			default:
				$label    = 'Stanine';
				$dropdown = create_dropdown('array', '1,2,3,4,5,6,7,8,9');
		}

		$data['label']    = $label;
		$data['dropdown'] = $dropdown;

		$this->template->write_view('content', 'interpretations_norm_value', $data);

		echo $this->template->render('content', TRUE);
	}
}

/* End of file Interpretations.php */
/* Location: ./application/modules/tests/controllers/Interpretations.php */
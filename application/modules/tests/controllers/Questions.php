<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Questions Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Questions extends MX_Controller {
	
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model('factors_model');
		$this->load->model('questions_model');
		$this->load->model('question_factors_model');
		$this->load->model('tests_model');

		$this->load->language('questions');
	}
	
	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function index()
	{
		$this->acl->restrict('tests.questions.list');
		
		// page title
		$data['page_heading'] = lang('index_heading');
		$data['page_subhead'] = lang('index_subhead');
		
		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('questions'));
		
		// session breadcrumb
		$this->session->set_userdata('redirect', current_url());
		
		// add plugins
		$this->template->add_css('assets/plugins/DataTables/datatables.min.css');
		$this->template->add_js('assets/plugins/DataTables/datatables.min.js');
		
		// render the page
		$this->template->add_css(module_css('tests', 'questions_index'), 'embed');
		$this->template->add_js(module_js('tests', 'questions_index'), 'embed');
		$this->template->write_view('content', 'questions_index', $data);
		$this->template->render();
	}

	public function view($test_id)
	{
		$this->acl->restrict('tests.questions.list');

		$test = $this->tests_model->get_test($test_id);
		
		// page title
		$data['page_heading'] = lang('index_heading');
		$data['page_subhead'] = $test->test_name;
		
		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('questions'));
		
		// session breadcrumb
		$this->session->set_userdata('redirect', current_url());
		
		// add plugins
		$this->template->add_css('assets/plugins/DataTables/datatables.min.css');
		$this->template->add_js('assets/plugins/DataTables/datatables.min.js');
		$this->template->add_js('var test_id='.$test_id.';', 'embed');

		// render the page
		$this->template->add_css(module_css('tests', 'questions_index'), 'embed');
		$this->template->add_js(module_js('tests', 'questions_index'), 'embed');
		$this->template->write_view('content', 'questions_index', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * datatables
	 *
	 * @access	public
	 * @param	mixed datatables parameters (datatables.net)
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function datatables($test_id)
	{
		$this->acl->restrict('tests.questions.list');

		echo $this->questions_model->get_datatables($test_id);
	}

	// --------------------------------------------------------------------

	/**
	 * form
	 *
	 * @access	public
	 * @param	$action string
	 * @param   $id integer
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	function form($action = 'add', $test_id, $id = FALSE)
	{
		$this->acl->restrict('tests.questions.' . $action);

		$test = $this->tests_model->get_test($test_id);

		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('tests'));
		
		$data['page_heading'] = lang($action . '_heading');
		$data['page_subhead'] = $test->test_name	;

		$data['action'] 		  = $action;
		$data['factors_dropdown'] = $this->factors_model->factors_dropdown('factor_id', 'factor_name', $test_id, FALSE);
		$data['test'] 			  = $test;

		// custom tpl
		switch ( $test->test_option_type )
		{
			case 'Image':
				$tpl = 'questions_image';
			break;
			case 'Text':
				$tpl = 'questions_text';
			break;
			case 'Radio':
				$tpl = 'questions_radio';
			break;
			default:
				$tpl = 'questions_checkbox';
		}
		
		$tpl_data['test'] = $test;
		$tpl_data['record'] = $this->questions_model->find($id);
		$data['tpl'] 	  = $this->load->view($tpl, $tpl_data, TRUE);

		if ($this->input->post())
		{
			if ($this->_save($action, $test_id, $id))
			{
				$response['success'] = TRUE;
				$response['message'] = lang($action . '_success');
				$response['redirect']  = site_url('tests/questions/view/' . $test_id);
			}
			else
			{	
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(					
					'question_factor_id'	=> form_error('question_factor_id[]'),
					'question_content'		=> form_error('question_content'),
				);
			}

			echo json_encode($response);
			exit;
		}

		if ($action != 'add') 
		{
			$data['record'] 	= $this->questions_model->find($id);
			$data['factors']    = $this->question_factors_model->get_question_factors($id);
		}

		// render the page
		// $this->template->add_js('assets/plugins/tinymce/tinymce.min.js');
		$this->template->add_js('assets/plugins/ckeditor/ckeditor.js');
		$this->template->add_css('assets/plugins/select2/css/select2.min.css');
		$this->template->add_js('assets/plugins/select2/js/select2.min.js');

		$this->template->add_js('assets/plugins/dropzone/dropzone.js');
		$this->template->add_css('assets/plugins/dropzone/dropzone.css');

		$this->template->add_css(module_css('tests', 'questions_form'), 'embed');
		$this->template->add_js(module_js('tests', 'questions_form'), 'embed');
		$this->template->write_view('content', 'questions_form', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * delete
	 *
	 * @access	public
	 * @param	integer $id
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	function delete($id)
	{
		$this->acl->restrict('tests.questions.delete', 'modal');

		$data['page_heading'] = lang('delete_heading');
		$data['page_confirm'] = lang('delete_confirm');
		$data['page_button'] = lang('button_delete');
		$data['datatables_id'] = '#datatables';

		if ($this->input->post())
		{
			$this->questions_model->delete($id);

			echo json_encode(array('success' => true, 'message' => lang('delete_success'))); exit;
		}

		$this->load->view('../../../views/confirm', $data);
	}


	// --------------------------------------------------------------------

	/**
	 * _save
	 *
	 * @access	private
	 * @param	string $action
	 * @param 	integer $id
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	private function _save($action = 'add', $test_id, $id = 0)
	{
		// validate inputs
		$this->form_validation->set_rules('question_factor_id[]', lang('question_factor_id'), 'required');
		$this->form_validation->set_rules('question_content', lang('question_content'), 'required');

		$this->form_validation->set_message('required', 'This field is required');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
		
		if ( $this->form_validation->run($this) == FALSE)
		{
			return FALSE;
		}

		$data = array(
			'question_test_id'		=> $test_id,
			'question_factor_id'	=> 0,
			'question_content'		=> $this->input->post('question_content'),
			'question_options'		=> json_encode($this->input->post('question_options')),
		);
		
		if ($action == 'add')
		{
			$insert_id = $this->questions_model->insert($data);
			$return    = (is_numeric($insert_id)) ? $insert_id : FALSE;
			$id 	   = $insert_id;
		}
		else if ($action == 'edit')
		{
			$return = $this->questions_model->update($id, $data);
		}

		// question_factor_id
		$question_factor_id = $this->input->post('question_factor_id');
		$qf_data = array();
		if ($question_factor_id)
		{
			$this->question_factors_model->delete_where(array('qf_question_id' => $id));

			foreach ( $question_factor_id as $val )
			{
				$qf_data[] = array(
					'qf_question_id' => $id,
					'qf_factor_id'	 => $val
				);
			}

			$this->question_factors_model->insert_batch($qf_data);
		}

		return $return;

	}
}

/* End of file Questions.php */
/* Location: ./application/modules/tests/controllers/Questions.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Factors Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Factors extends MX_Controller {
	
	public $test_id;

	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model('factors_model');
		$this->load->language('factors');

		$this->test_id = $this->session->userdata('test_id');
	}
	
	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function index()
	{
		$this->acl->restrict('tests.factors.list');
		
		// page title
		$data['page_heading'] = lang('index_heading');
		$data['page_subhead'] = lang('index_subhead');
		
		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('factors'));
		
		// session breadcrumb
		$this->session->set_userdata('redirect', current_url());
		
		// add plugins
		$this->template->add_css('assets/plugins/DataTables/datatables.min.css');
		$this->template->add_js('assets/plugins/DataTables/datatables.min.js');
		
		// render the page
		$this->template->add_css(module_css('tests', 'factors_index'), 'embed');
		$this->template->add_js(module_js('tests', 'factors_index'), 'embed');
		$this->template->write_view('content', 'factors_index', $data);
		$this->template->render();
	}

	public function view($test_id)
	{
		$this->acl->restrict('tests.factors.list');
		
		$test_id = (int) $test_id;

		$this->session->set_userdata(array(
			'test_id' => $test_id
		));

		// page title
		$data['page_heading'] = lang('index_heading');
		$data['page_subhead'] = lang('index_subhead');
		
		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('factors'));
		
		// session breadcrumb
		$this->session->set_userdata('redirect', current_url());
		
		// add plugins
		$this->template->add_css('assets/plugins/DataTables/datatables.min.css');
		$this->template->add_js('assets/plugins/DataTables/datatables.min.js');
		$this->template->add_js('var test_id='.$test_id.';', 'embed');
		// render the page
		$this->template->add_css(module_css('tests', 'factors_index'), 'embed');
		$this->template->add_js(module_js('tests', 'factors_index'), 'embed');
		
		$this->template->write_view('content', 'factors_index', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * datatables
	 *
	 * @access	public
	 * @param	mixed datatables parameters (datatables.net)
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function datatables($test_id)
	{
		$this->acl->restrict('tests.factors.list');

		echo $this->factors_model->where('factor_test_id', $test_id)
								 ->get_datatables();
	}

	// --------------------------------------------------------------------

	/**
	 * form
	 *
	 * @access	public
	 * @param	$action string
	 * @param   $id integer
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	function form($action = 'add', $id = FALSE)
	{
		$this->acl->restrict('tests.factors.' . $action, 'modal');

		$data['page_heading'] = lang($action . '_heading');
		$data['action'] = $action;

		if ($this->input->post())
		{
			if ($this->_save($action, $id))
			{
				echo json_encode(array('success' => true, 'message' => lang($action . '_success'))); exit;
			}
			else
			{	
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(					
					'factor_name'		=> form_error('factor_name'),
					'factor_norm_to_use_in_report'		=> form_error('factor_norm_to_use_in_report'),
					'factor_norm_type'		=> form_error('factor_norm_type'),
					'factor_norm_to_use'		=> form_error('factor_norm_to_use'),
					'factor_norms'		=> form_error('factor_norms'),
				);
				echo json_encode($response);
				exit;
			}
		}

		if ($action != 'add') $data['record'] = $this->factors_model->find($id);

		// render the page
		$this->template->set_template('modal');
		$this->template->add_css(module_css('tests', 'factors_form'), 'embed');
		$this->template->add_js(module_js('tests', 'factors_form'), 'embed');
		$this->template->write_view('content', 'factors_form', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * delete
	 *
	 * @access	public
	 * @param	integer $id
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	function delete($id)
	{
		$this->acl->restrict('tests.factors.delete', 'modal');

		$data['page_heading'] = lang('delete_heading');
		$data['page_confirm'] = lang('delete_confirm');
		$data['page_button'] = lang('button_delete');
		$data['datatables_id'] = '#datatables';

		if ($this->input->post())
		{
			$this->factors_model->delete($id);

			echo json_encode(array('success' => true, 'message' => lang('delete_success'))); exit;
		}

		$this->load->view('../../../views/confirm', $data);
	}


	// --------------------------------------------------------------------

	/**
	 * _save
	 *
	 * @access	private
	 * @param	string $action
	 * @param 	integer $id
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	private function _save($action = 'add', $id = 0)
	{
		// validate inputs
		$this->form_validation->set_rules('factor_name', lang('factor_name'), 'required');
		$this->form_validation->set_rules('factor_norm_to_use_in_report', lang('factor_norm_to_use_in_report'), 'required');
		$this->form_validation->set_rules('factor_norm_type', lang('factor_norm_type'), 'required');
		$this->form_validation->set_rules('factor_norm_to_use', lang('factor_norm_to_use'), 'required');
		$this->form_validation->set_rules('factor_norms', lang('factor_norms'), 'required');

		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
		
		if ($this->form_validation->run($this) == FALSE)
		{
			return FALSE;
		}

		$data = array(
			'factor_test_id' 				=> $this->test_id,
			'factor_name'					=> $this->input->post('factor_name'),
			'factor_norm_to_use_in_report'	=> $this->input->post('factor_norm_to_use_in_report'),
			'factor_norm_type'				=> $this->input->post('factor_norm_type'),
			'factor_left_description'		=> $this->input->post('factor_left_description'),
			'factor_right_description'		=> $this->input->post('factor_right_description'),
			'factor_norm_to_use'			=> $this->input->post('factor_norm_to_use'),
			'factor_norms'					=> $this->input->post('factor_norms'),
		);
		
		if ( $action == 'add')
		{
			$insert_id = $this->factors_model->insert($data);
			$return = (is_numeric($insert_id)) ? $insert_id : FALSE;
		}
		else if ( $action == 'edit')
		{
			$return = $this->factors_model->update($id, $data);
		}

		return $return;

	}
}

/* End of file Factors.php */
/* Location: ./application/modules/tests/controllers/Factors.php */
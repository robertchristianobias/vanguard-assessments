<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span>
		<span class="sr-only"><?php echo lang('button_close')?></span>
	</button>
	<h4 class="modal-title" id="myModalLabel"><?php echo $page_heading?></h4>
</div>

<div class="modal-body">
	<?php echo form_open(current_url(), 'id="examinees-form" class="form-horizontal"'); ?>
		<div class="form-group">
			<label class="col-sm-3 control-label" for="examinee_count"><?php echo lang('examinee_count')?>:</label>
			<div class="col-sm-8">
				<?php echo form_input(array('id'=>'examinee_count', 'name'=>'examinee_count', 'value'=>set_value('examinee_count', isset($record->examinee_count) ? $record->examinee_count : ''), 'class'=>'form-control'));?>
				<div id="error-examinee_count"></div>
			</div>
		</div>
		<div class="form-group">
			<label class="col-sm-3 control-label" for="examinee_date"><?php echo lang('examinee_date')?>:</label>
			<div class="col-sm-8">
				<?php echo form_input(array('id'=>'examinee_date', 'name'=>'examinee_date', 'value'=>set_value('examinee_date', isset($record->examinee_date) ? $record->examinee_date : ''), 'class'=>'form-control datepicker'));?>
				<div id="error-examinee_date"></div>
			</div>
		</div>
	<?php echo form_close(); ?>
</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
		<i class="fa fa-times"></i> <?php echo lang('button_close')?>
	</button>
	<?php if ($action == 'add'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_add')?>
		</button>
	<?php elseif ($action == 'edit'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_update')?>
		</button>
	<?php else: ?>
		<script>$(".modal-body :input").attr("disabled", true);</script>
	<?php endif; ?>
</div>
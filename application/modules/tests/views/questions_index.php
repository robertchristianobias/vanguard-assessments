<span class="btn-actions">
	<?php if ($this->acl->restrict('tests.questions.add', 'return')): ?>
		<a href="<?php echo site_url('tests/questions/form/add/' . $this->uri->segment('4'))?>" class="btn btn-sm btn-success btn-add" id="btn_add"><span class="fa fa-plus"></span> <?php echo lang('button_add')?></a>
	<?php endif; ?>
</span>
	
<table class="table table-striped table-bordered table-hover dt-responsive" id="datatables" data-test-id="<?php echo $this->uri->segment('4'); ?>">
	<thead>
		<tr>
			<th class="all"><?php echo lang('index_id')?></th>
			<th class="min-desktop"><?php echo lang('index_factor_id'); ?></th>
			<th class="min-desktop"><?php echo lang('index_content'); ?></th>

			<th class="none"><?php echo lang('index_created_on')?></th>
			<th class="none"><?php echo lang('index_created_by')?></th>
			<th class="none"><?php echo lang('index_modified_on')?></th>
			<th class="none"><?php echo lang('index_modified_by')?></th>
			<th class="min-tablet"><?php echo lang('index_action')?></th>
		</tr>
	</thead>
</table>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span>
		<span class="sr-only"><?php echo lang('button_close')?></span>
	</button>
	<h4 class="modal-title" id="myModalLabel"><?php echo $page_heading?></h4>
</div>

<div class="modal-body">

	<div class="form-horizontal">

		<div class="form-group">
			<label class="col-sm-3 control-label" for="interpretation_norm_to_use"><?php echo lang('interpretation_norm_to_use')?>:</label>
			<div class="col-sm-8">
				<?php echo form_dropdown('interpretation_norm_to_use', norm_type(), isset($record->interpretation_norm_to_use) ? $record->interpretation_norm_to_use : '', 'class="form-control" id="interpretation_norm_to_use"'); ?>
				<div id="error-interpretation_norm_to_use"></div>
			</div>
		</div>

		<div id="norm-value-wrapper"></div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="interpretation_gender"><?php echo lang('interpretation_gender')?>:</label>
			<div class="col-sm-8">
				<?php echo form_dropdown('interpretation_gender', gender(), isset($record->interpretation_gender) ? $record->interpretation_gender : '', 'class="form-control" id="interpretation_gender"'); ?>			
				<div id="error-interpretation_gender"></div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="interpretation_content"><?php echo lang('interpretation_content')?>:</label>
			<div class="col-sm-8">
				<?php echo form_textarea(array('id'=>'interpretation_content', 'name'=>'interpretation_content', 'value'=>set_value('interpretation_content', isset($record->interpretation_content) ? $record->interpretation_content : ''), 'class'=>'form-control'));?>
				<div id="error-interpretation_content"></div>
			</div>
		</div>

	</div>

</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
		<i class="fa fa-times"></i> <?php echo lang('button_close')?>
	</button>
	<?php if ($action == 'add'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_add')?>
		</button>
	<?php elseif ($action == 'edit'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_update')?>
		</button>
	<?php else: ?>
		<script>$(".modal-body :input").attr("disabled", true);</script>
	<?php endif; ?>
</div>
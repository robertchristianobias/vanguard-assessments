
<?php echo form_open(current_url(), 'id="question-form"'); ?>
	<div class="nav-tabs-custom bottom-margin">
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#tab_page" data-toggle="tab">
					<span class="fa fa-file-text"></span> Content
				</a>
			</li>
			<li class="pull-right"></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_page">
				<div class="row">
					<div class="col-sm-9">
						
						<div class="form-group">
							<label class="control-label" for="question_content"><?php echo lang('question_content')?>:</label>
							<?php echo form_textarea(array('id'=>'question_content', 'name'=>'question_content', 'value'=>set_value('question_content', isset($record->question_content) ? $record->question_content : '', FALSE), 'class'=>'form-control ckeditor'));?>
							<div id="error-question_content"></div>
						</div>
						
						<div class="form-group">
							<label class="control-label" for="question_options"><?php echo lang('question_options')?>:</label>
							<?php echo $tpl; ?>
						</div>
							
					</div>
					<div class="col-sm-3">
						<div class="form-group">
							<?php
								$factor_arr = array();
								if ( isset($factors) )
								{
									foreach ( $factors as $factor)
									{        
										$factor_arr[] = $factor->qf_factor_id;
									}
								}
							?>
							<label class="control-label" for="question_factor_id"><?php echo lang('question_factor_id'); ?>: </label>
							<select name="question_factor_id[]" class="form-control select2" id="question_factor_id" multiple="multiple">
								<?php foreach ( $factors_dropdown as $key=>$val) { ?>
									<option <?php echo in_array($key, $factor_arr) ? 'selected="selected"' : ''; ?> value="<?php echo $key; ?>"><?php echo $val; ?></option>
								<?php } ?>
							</select>
							<div id="error-question_factor_id"></div>
						</div>
						<div class="top-margin5">
							<?php if ($action == 'add'): ?>
								<button id="submit-question" class="btn btn-default btn-lg btn-block" type="submit" data-loading-text="<?php echo lang('processing')?>">
									<i class="fa fa-plus"></i> <?php echo lang('button_add')?>
								</button>
							<?php elseif ($action == 'edit'): ?>
								<button id="submit-question" class="btn btn-default btn-lg btn-block" type="submit" data-loading-text="<?php echo lang('processing')?>">
									<i class="fa fa-save"></i> <?php echo lang('button_update')?>
								</button>
							<?php endif; ?>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
<?php echo form_close(); ?>
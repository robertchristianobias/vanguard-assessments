<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span>
		<span class="sr-only"><?php echo lang('button_close')?></span>
	</button>
	<h4 class="modal-title" id="myModalLabel"><?php echo $page_heading?></h4>
</div>

<div class="modal-body">

	<form name="" method="POST" action="" id="test-examinee-form">
		<div class="form-horizontal">
            <div class="form-group">
                <div class="col-sm-12">
                    <?php if($total_examinees > 0) { ?>
                        <div id="download-link" style="<?php echo $total_examinees == 0 ? 'display: none;' : 'display: block;'; ?>">
                            <a href="<?php echo site_url('tests/test_batteries/download_examinees/'.$test_battery->test_battery_id); ?>">Download Examinees Accounts</a>
                        </div>
                    <?php } ?>
                </div>
            </div>
			<div class="form-group">
				<label class="col-sm-3 control-label" for="account_no_examinees"># of Examinees:</label>
				<div class="col-sm-8">
					<?php echo form_input(array('id'=>'account_no_examinees', 'name'=>'account_no_examinees', 'value'=>set_value('account_no_examinees', isset($record->account_no_examinees) ? $record->account_no_examinees : ''), 'class'=>'form-control'));?>
					<div id="error-account_no_examinees"></div>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-3 control-label" for="account_start_date">Start Date:</label>
				<div class="col-sm-8">
					<?php echo form_input(array('id'=>'account_start_date', 'name'=>'account_start_date', 'value'=>set_value('account_start_date', isset($record->account_start_date) ? $record->account_start_date : ''), 'class'=>'form-control datetimepicker'));?>
					<div id="error-account_start_date"></div>
				</div>
			</div>
            <div class="form-group">
				<label class="col-sm-3 control-label" for="account_end_date">End Date:</label>
				<div class="col-sm-8">
					<?php echo form_input(array('id'=>'account_end_date', 'name'=>'account_end_date', 'value'=>set_value('account_end_date', isset($record->account_end_date) ? $record->account_end_date : ''), 'class'=>'form-control datetimepicker'));?>
					<div id="error-account_end_date"></div>
				</div>
			</div>
			
		</div>
	</form>
</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
		<i class="fa fa-times"></i> <?php echo lang('button_close')?>
	</button>
	<?php if ($action == 'add'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_add')?>
		</button>
	<?php elseif ($action == 'edit'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_update')?>
		</button>
	<?php else: ?>
		<script>$(".modal-body :input").attr("disabled", true);</script>
	<?php endif; ?>
</div>
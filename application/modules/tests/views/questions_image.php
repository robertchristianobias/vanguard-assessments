
<?php
   $question_options = array();
   if ( isset($record->question_options))
   {
      $question_options = json_decode($record->question_options);
      $question_options = (array) $question_options;
   }
?>
<div class="row">
   <div class="col-md-12" id="option-img">
   <?php for ($i=1; $i<=$test->test_option_no; $i++) { ?>
      <?php
         $val      = isset($question_options['option_' . $i]->image)    ? $question_options['option_' . $i]->image    : '';
         $no_image = isset($question_options['option_' . $i]->no_image) ? $question_options['option_' . $i]->no_image : '';
         $answer   = isset($question_options['option_' . $i]->answer)   ? $question_options['option_' . $i]->answer   : '';
      ?>
      <div class="row">
         <div class="form-group">
            <label class="control-label col-md-2"><b>Image #<?php echo $i; ?></b> </label>
            <div class="col-md-5">
               <input type="hidden" name="question_options[option_<?php echo $i; ?>][image]" value="<?php echo $val; ?>" id="question_options_img_<?php echo $i; ?>" />
               <div class="dropzone" id="question_options_img_<?php echo $i; ?>_dropzone"></div>
               <br />
               <?php echo form_input('question_options[option_'.$i.'][no_image]', set_value('question_options[no_image_'.$i.']', $no_image), 'class="form-control" placeholder="Enter choice instead of image"'); ?>
               <br />
               <?php echo form_input('question_options[option_'.$i.'][answer]', set_value('question_options[answer_'.$i.']', $answer), 'class="form-control" placeholder="Enter answer value"'); ?>
            </div>
            <?php if ( $val ) { ?>
            <div class="col-md-3">
               <img src="<?php echo site_url($val); ?>" class="img-responsive" />
            </div>
            <?php } ?>
         </div>
      </div>
      <hr />
   <?php } ?>
   </div>
</div>

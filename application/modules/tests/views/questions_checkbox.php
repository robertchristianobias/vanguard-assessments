
<?php
   $question_options = array();
   if ( isset($record->question_options))
   {
      $question_options = json_decode($record->question_options);
      $question_options = (array) $question_options;
   }
?>
<div class="row">
   <div class="col-md-12" id="option-img">
   <?php for ($i=1; $i<=$test->test_option_no; $i++) { ?>
      <?php
         $option = isset($question_options['option_' . $i]) ? $question_options['option_' . $i] : '';
         $answer = isset($question_options['answer_'. $i]) ? $question_options['answer_'. $i] : '';
      ?>
      <div class="row">
         <div class="form-group">
            <label class="control-label col-md-2">&nbsp;</label>
            <div class="col-md-10">
               <div class="row">
                  <div class="col-md-6">
                     <?php echo form_input('question_options[option_'.$i.']', set_value('question_options[option_'.$i.']', $option), 'class="form-control" placeholder="Enter option here"'); ?>
                  </div>
                  <div class="col-md-6">
                     <?php echo form_input('question_options[answer_'.$i.']', set_value('question_options[answer_'.$i.']', $answer), 'class="form-control" placeholder="Enter answer value"'); ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <hr />
   <?php } ?>
   </div>
</div>

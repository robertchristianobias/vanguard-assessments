<?php for($i=1;$i<=$options;$i++) { ?>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="test_option_label">Label #<?php echo $i; ?>:</label>					
        <div class="col-sm-8">
            <?php echo form_input(array('name'=>'test_option_label[]', 'value'=>set_value('test_option_label[]'), 'class'=>'form-control'));?>
        </div>
    </div>
<?php } ?>
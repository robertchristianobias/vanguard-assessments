<div class="form-group">
    <label class="col-sm-3 control-label" for="interpretation_norm_value"><?php echo $label; ?>:</label>
    <div class="col-sm-8">
        <?php echo form_dropdown('interpretation_norm_value', $dropdown, isset($record->interpretation_norm_value) ? $record->interpretation_norm_value : '', 'class="form-control" id="interpretation_norm_value"'); ?>
        <div id="error-interpretation_norm_value"></div>
    </div>
</div>
/**
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */

$(function() {

	var duallist = $('#duallist').bootstrapDualListbox({infoTextFiltered: '<span class="label label-purple label-lg">Filtered</span>'});
	var container1 = duallist.bootstrapDualListbox('getContainer');
	container1.find('.btn').addClass('btn-white btn-info btn-bold');

	$('.select2').select2();

	// handles the submit action
	$('#submit').click(function(e){
		// change the button to loading state
		var btn = $(this);
		btn.button('loading');

		var frm = $('#test-battery-form');
			ajax_url = frm.attr('action');

		// prevents a submit button from submitting a form
		e.preventDefault();

		// submits the data to the backend
		$.post(ajax_url, frm.serializeArray(),
			function(data, status){
				// handles the returned data
				var o = jQuery.parseJSON(data);
				if (o.success === false) {
					// reset the button
					btn.button('reset');
					
					// shows the error message
					alertify.error(o.message);

					// displays individual error messages
					if (o.errors) {
						for (var form_name in o.errors) {
							$('#error-' + form_name).html(o.errors[form_name]);
						}
					}
				} else {
					// // refreshes the datatables
					// $('#datatables').dataTable().fnDraw();

					// // closes the modal
					// $('#modal').modal('hide'); 

					// // restores the modal content to loading state
					// restore_modal(); 

					// shows the success message
					alertify.success(o.message); 

					window.location.replace(o.redirect);
				}
			});
	});

	// disables the enter key
	$('form input').keydown(function(event){
		if(event.keyCode == 13) {
			event.preventDefault();
			return false;
		}
	});
});
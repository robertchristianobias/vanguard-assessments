/**
 * @package     rcmediaph
 * @version     1.0
 * @author      Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright   Copyright (c) 2017, Google.
 * @link        http://www.google.com
 */
$(function() {

	// renders the datatables (datatables.net)
	var oTable = $('#datatables').dataTable({
		"bProcessing": true,
		"bServerSide": true,
		"sAjaxSource": "test_batteries/datatables",
		"lengthMenu": [[10, 20, 50, 100, 300, -1], [10, 20, 50, 100, 300, "All"]],
		"pagingType": "full_numbers",
		"language": {
			"paginate": {
				"previous": 'Prev',
				"next": 'Next',
			}
		},
		"bAutoWidth": false,
		"aaSorting": [[ 0, "desc" ]],
		"aoColumnDefs": [
			{
				"aTargets": [0],
				"sClass": "col-md-1 text-center",
			},
		
			{
				"aTargets": [8],
				"bSortable": false,
				"mRender": function (data, type, full) {
					html  = '<a href="'+app_url+'tests/examinees/form/add/'+full[0]+'/'+full[8]+'" data-toggle="modal" data-target="#modal" tooltip-toggle="tooltip" data-placement="top" title="Examinees" class="btn btn-xs btn-info"><span class="fa fa-user"></span></a> ';
					html += '<a href="'+app_url+'tests/test_batteries/form/edit/'+full[0]+'" title="Edit" class="btn btn-xs btn-warning"><span class="fa fa-pencil"></span></a> ';
					html += '<a href="'+app_url+'tests/test_batteries/delete/'+full[0]+'" data-toggle="modal" data-target="#modal" tooltip-toggle="tooltip" data-placement="top" title="Delete" class="btn btn-xs btn-danger"><span class="fa fa-trash-o"></span></a>';

					return html;
				},
				"sClass": "col-md-2 text-center",
			},
		]
	});

	// positions the button next to searchbox
	$('.btn-actions').prependTo('div.dataTables_filter');

	// executes functions when the modal closes
	$('body').on('hidden.bs.modal', '.modal', function () {        
		// eg. destroys the wysiwyg editor
	});

});
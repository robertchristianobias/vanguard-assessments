/**
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */

$(function() {

	Dropzone.autoDiscover = false;
	
	$('.select2').select2();

	if ( $('#option-img').length ) {
		for (i = 1; i <= 20; i++) {
			handle_single_image_dropzone('question_options_img_' + i, app_url + 'files/images/upload');
		}
	}

	// handles the submit action
	$('#submit-question').click(function(e){
		// change the button to loading state
		var btn = $(this);
		btn.button('loading');

		// prevents a submit button from submitting a form
		e.preventDefault();
		var question_form = $('#question-form').serializeArray();
			question_form.push({
				name:  'question_content',
				value:  CKEDITOR.instances['question_content'].getData()
			});
			
			ajax_url = $('#question-form').attr('action');

		// submits the data to the backend
		$.post(ajax_url, question_form,
		function(data, status){
			// handles the returned data
			var o = jQuery.parseJSON(data);
			if (o.success === false) {
				// reset the button
				btn.button('reset');
				
				// shows the error message
				alertify.error(o.message);

				// displays individual error messages
				if (o.errors) {
					for (var form_name in o.errors) {
						$('#error-' + form_name).html(o.errors[form_name]);
					}
				}
			} else {
				alertify.alert(o.message, function() {
					window.location.replace(o.redirect);
				});
			}
		});
	});

	// disables the enter key
	$('form input').keydown(function(event){
		if(event.keyCode == 13) {
			event.preventDefault();
			return false;
		}
	});

	function handle_single_image_dropzone(element, endpoint) {
		$('#'+element+'_dropzone').dropzone({
			url: endpoint,
			clickable: true,
			addRemoveLinks: true,
			maxFilesize: 2,
			maxFiles: 1,
			init: function() {
				this.on('success', function(file, data) {
					var response = $.parseJSON(data);

					if(response.status == 'success') {
						$('#'+element).val(response.image);
					} else {
						$('#error-'+element).html('<span class="text-danger">'+response.error+'</span>');
					}
				});
				
				this.on("removedfile", function(file) {});
			}
		});
	}
});
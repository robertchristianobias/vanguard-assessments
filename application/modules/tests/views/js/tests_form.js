/**
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */

$(function() {

	handle_tiny_mce('test_instruction');

	$('#test_name').blur(function() {
		$('#test_slug').val(convertToSlug($('#test_name').val()));
	});

	$('#test_option_has_label').change(function() {
		var has_label = $(this).val();

	});

	$('#generate-labels').click(function() {
		var options = $('#test_option_no').val();

		$.post(app_url+'tests/generate_labels', {
			options: options
		}, function(data) {
			$('#options-wrapper').html(data);
		});
	});

	// handles the submit action
	$('#submit').click(function(e){
		// change the button to loading state
		var btn = $(this);
			
		btn.button('loading');

		// prevents a submit button from submitting a form
		e.preventDefault();

		var test_form = $('#test-form').serializeArray();
			test_form.push({
				name: 'test_instruction',
				value: tinyMCE.get('test_instruction').getContent(),
			});

		// submits the data to the backend
		$.post(ajax_url, test_form,
		function(data, status){
			// handles the returned data
			var o = jQuery.parseJSON(data);
			if (o.success === false) {
				// reset the button
				btn.button('reset');
				
				// shows the error message
				alertify.error(o.message);

				// displays individual error messages
				if (o.errors) {
					for (var form_name in o.errors) {
						$('#error-' + form_name).html(o.errors[form_name]);
					}
				}
			} else {
				// refreshes the datatables
				$('#datatables').dataTable().fnDraw();

				// closes the modal
				$('#modal').modal('hide'); 

				// restores the modal content to loading state
				restore_modal(); 

				// shows the success message
				alertify.success(o.message); 
			}
		});
	});

	// disables the enter key
	$('form input').keydown(function(event){
		if(event.keyCode == 13) {
			event.preventDefault();
			return false;
		}
	});

	function handle_tiny_mce(element) {
    
		// initialize tinymce
		tinymce.init({
			selector: "#" + element,
			theme: "modern",
			statusbar: true,
			menubar: false,
			relative_urls: false,
			remove_script_host : false,
			convert_urls : true,
			plugins: [
				'advlist autolink lists link charmap print preview hr anchor pagebreak',
				'searchreplace wordcount visualblocks visualchars code',
				'insertdatetime media nonbreaking save table contextmenu directionality',
				'emoticons template paste textcolor colorpicker textpattern'
			],
			toolbar1: 'styleselect forecolor backcolor | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | table',
			image_advtab: true,
			setup: function (editor) {
				editor.addButton('mybutton', {
					text: '',
						icon: 'image',
						onclick: function () {
							$('#modal').modal({
								remote: app_url + 'files/images/rte/mce'
							})
						}
					});
			},
			content_css: app_url + 'themes/backend/aceadmin/css/tinymce.css'
		});
	}

	function convertToSlug(Text) {
		return Text
			.toLowerCase()
			.replace(/ /g,'_')
			.replace(/[^\w-]+/g,'');
	}
});
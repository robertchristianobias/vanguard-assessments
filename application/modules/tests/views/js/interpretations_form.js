/**
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */

$(function() {
	var norm_to_use = $('#interpretation_norm_to_use').val();
		ntu(norm_to_use);

	$('#interpretation_norm_to_use').change(function() {
		var norm_to_use = $(this).val();
		ntu(norm_to_use);
		// $('#norm-value-wrapper').html('loading...');
		// $.post(app_url + 'tests/interpretations/norm_value', {
		// 	norm_to_use: norm_to_use
		// }, function(data) {
		// 	$('#norm-value-wrapper').html(data);
		// });
	});

	function ntu(norm_to_use)
	{
		$('#norm-value-wrapper').html('loading...');
		$.post(app_url + 'tests/interpretations/norm_value', {
			norm_to_use: norm_to_use
		}, function(data) {
			$('#norm-value-wrapper').html(data);
		});
	}

	// handles the submit action
	$('#submit').click(function(e){
		// change the button to loading state
		var btn = $(this);
		btn.button('loading');

		// prevents a submit button from submitting a form
		e.preventDefault();

		// submits the data to the backend
		$.post(ajax_url, {
			interpretation_norm_to_use: $('#interpretation_norm_to_use').val(),
			interpretation_norm_value: $('#interpretation_norm_value').val(),
			interpretation_gender: $('#interpretation_gender').val(),
			interpretation_content: $('#interpretation_content').val(),

		},
		function(data, status){
			// handles the returned data
			var o = jQuery.parseJSON(data);
			if (o.success === false) {
				// reset the button
				btn.button('reset');
				
				// shows the error message
				alertify.error(o.message);

				// displays individual error messages
				if (o.errors) {
					for (var form_name in o.errors) {
						$('#error-' + form_name).html(o.errors[form_name]);
					}
				}
			} else {
				// refreshes the datatables
				$('#datatables').dataTable().fnDraw();

				// closes the modal
				$('#modal').modal('hide'); 

				// restores the modal content to loading state
				restore_modal(); 

				// shows the success message
				alertify.success(o.message); 
			}
		});
	});

	// disables the enter key
	$('form input').keydown(function(event){
		if(event.keyCode == 13) {
			event.preventDefault();
			return false;
		}
	});
});
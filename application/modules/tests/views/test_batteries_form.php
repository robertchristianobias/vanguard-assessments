
<?php echo form_open(current_url(), 'id="test-battery-form"'); ?>
	<div class="nav-tabs-custom bottom-margin">
		<ul class="nav nav-tabs">
			<li class="active">
				<a href="#tab_page" data-toggle="tab">
					<span class="fa fa-file-text"></span> Content
				</a>
			</li>
			<li class="pull-right"></li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane active" id="tab_page">
				<div class="row">
					<div class="col-sm-9">
						<div class="form-group">
							<label class="control-label" for="test_battery_name"><?php echo lang('test_battery_name')?>:</label>
							<?php echo form_input(array('id'=>'test_battery_name', 'name'=>'test_battery_name', 'value'=>set_value('test_battery_name', isset($record->test_battery_name) ? $record->test_battery_name : ''), 'class'=>'form-control'));?>
							<div id="error-test_battery_name"></div>
						</div>
						<div class="form-group">
							<label class="control-label" for="test_battery_tests"><?php echo lang('test_battery_tests')?>:</label>
							<?php
								$tests_included = isset($record->test_battery_tests) ? json_decode($record->test_battery_tests) : array();
							?>
							<select multiple="multiple" size="10" name="test_battery_tests[]" id="duallist">
								<?php if($tests) { ?>
									<?php foreach($tests as $test) { ?>
										<option <?php echo (in_array($test->test_id, $tests_included) ? 'selected="selected"' : ''); ?> value="<?php echo $test->test_id; ?>"><?php echo $test->test_name; ?></option>
									<?php } ?>
								<?php } ?>
							</select>
							<div id="error-test_battery_tests"></div>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="form-group">
							<label class="control-label" for="test_battery_company_id"><?php echo lang('test_battery_company_id')?>:</label>
							<?php echo form_dropdown('test_battery_company_id', $companies_dropdown, isset($record->test_battery_company_id) ? $record->test_battery_company_id : '', 'class="form-control select2" id="test_battery_company_id"'); ?>
							<div id="error-test_battery_company_id"></div>
						</div>
						<div class="top-margin5">
							<button type="button" class="btn btn-default" data-dismiss="modal">
								<i class="fa fa-times"></i> <?php echo lang('button_close')?>
							</button>
							<?php if ($action == 'add'): ?>
								<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
									<i class="fa fa-save"></i> <?php echo lang('button_add')?>
								</button>
							<?php elseif ($action == 'edit'): ?>
								<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
									<i class="fa fa-save"></i> <?php echo lang('button_update')?>
								</button>
							<?php else: ?>
								<script>$(".modal-body :input").attr("disabled", true);</script>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	

<?php echo form_close(); ?>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span>
		<span class="sr-only"><?php echo lang('button_close')?></span>
	</button>
	<h4 class="modal-title" id="myModalLabel"><?php echo $page_heading?></h4>
</div>

<div class="modal-body">
	<form name="" method="POST" action="" id="test-form">
		<div class="form-horizontal">
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#info">Information</a></li>
				<li><a data-toggle="tab" href="#instruction">Instructions</a></li>
				<li><a data-toggle="tab" href="#settings">Settings</a></li>
				<li><a data-toggle="tab" href="#questionoptions">Options</a></li>
				<li><a data-toggle="tab" href="#ranges">Ranges</a></li>
			</ul>
			<div class="tab-content">
				<div id="info" class="tab-pane fade in active">
					<div class="form-group">
						<label class="col-sm-3 control-label" for="test_name"><?php echo lang('test_name')?>:</label>
						<div class="col-sm-8">
							<?php echo form_input(array('id'=>'test_name', 'name'=>'test_name', 'value'=>set_value('test_name', isset($record->test_name) ? $record->test_name : ''), 'class'=>'form-control'));?>
							<div id="error-test_name"></div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="test_slug"><?php echo lang('test_slug')?>:</label>
						<div class="col-sm-8">
							<?php echo form_input(array('id'=>'test_slug', 'name'=>'test_slug', 'value'=>set_value('test_slug', isset($record->test_slug) ? $record->test_slug : ''), 'class'=>'form-control'));?>
							<div id="error-test_slug"></div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="test_type"><?php echo lang('test_type')?>:</label>
						<div class="col-sm-4">
							<?php
								$test_type_arr = create_dropdown('array', ',Personality,Intellectual');
								
								echo form_dropdown('test_type', $test_type_arr, isset($record->test_type) ? $record->test_type : '', 'class="form-control" id="test_type"');
							?>
							<div id="error-test_type"></div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="test_parent_test_id"><?php echo lang('test_parent_test_id')?>:</label>
						<div class="col-sm-8">
							<?php echo form_dropdown('test_parent_test_id', $tests_dropdown, isset($record->test_parent_test_id) ? $record->test_parent_test_id : '', 'class="form-control" id="test_parent_test_id"'); ?>
							<div id="error-test_parent_test_id"></div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="test_is_timed"><?php echo lang('test_is_timed')?>:</label>
						<div class="col-sm-8">
							<input name="test_is_timed" class="ace ace-switch ace-switch-5" id="test_is_timed" type="checkbox" />
							<span class="lbl"></span>

							<div id="error-test_is_timed"></div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="test_time_minute">Time:</label>
						<div class="col-sm-4">
							<?php echo form_input(array('id'=>'test_time_minute', 'name'=>'test_time_minute', 'value'=>set_value('test_time_minute', isset($record->test_time_minute) ? $record->test_time_minute : ''), 'class'=>'form-control', 'placeholder'=>'Minute'));?>
							<div id="error-test_time_minute"></div>
						</div>
						<div class="col-sm-4">
							<?php echo form_input(array('id'=>'test_time_seconds', 'name'=>'test_time_seconds', 'value'=>set_value('test_time_seconds', isset($record->test_time_seconds) ? $record->test_time_seconds : ''), 'class'=>'form-control', 'placeholder'=>'Seconds'));?>
							<div id="error-test_time_seconds"></div>
						</div>
					</div>
				</div>
				<div id="instruction" class="tab-pane fade">
					<div class="form-group">
						<div class="col-sm-12">
							<?php echo form_input(array('id'=>'test_instruction', 'name'=>'test_instruction', 'value'=>set_value('test_instruction', isset($record->test_instruction) ? $record->test_instruction : '', FALSE), 'class'=>'form-control'));?>
							<div id="error-test_instruction"></div>
						</div>
					</div>
				</div>
				<div id="settings" class="tab-pane fade">
					<div class="form-group">
						<label class="col-sm-3 control-label" for="test_norm_type"><?php echo lang('test_norm_type')?>:</label>
						<div class="col-sm-8">
							<?php
								$norm_type_arr = create_dropdown('array', ',Stanine,Percentile,Sten,T-Score,Category');
								echo form_dropdown('test_norm_type', $norm_type_arr, isset($record->test_norm_type) ? $record->test_norm_type : '', 'class="form-control" id="test_norm_type"');
							?>
							<div id="error-test_norm_type"></div>
						</div>
					</div>
				</div>
				<div id="questionoptions" class="tab-pane fade">
					<div class="form-group">
						<label class="col-sm-3 control-label" for="test_option_type"><?php echo lang('test_option_type')?>:</label>
						<div class="col-sm-8">
							<?php
								$norm_type_arr = create_dropdown('array', ',Text,Radio,Checkbox,Image');
								echo form_dropdown('test_option_type', $norm_type_arr, isset($record->test_option_type) ? $record->test_option_type : '', 'class="form-control" id="test_option_type"');
							?>
							<div id="error-test_option_type"></div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="test_option_has_label"><?php echo lang('test_option_has_label')?>:</label>
						<div class="col-sm-4">
							<input name="test_option_has_label" <?php echo isset($record->test_option_has_label) ? ($record->test_option_has_label == 'on' ? 'checked="checked"' : '') : ''; ?> class="ace ace-switch ace-switch-5 input-switch" id="test_option_has_label" type="checkbox" />
							<span class="lbl"></span>
							<?php
								#$norm_type_arr = create_dropdown('array', ',Yes,No');
								#echo form_dropdown('test_option_has_label', $norm_type_arr, isset($record->test_option_has_label) ? $record->test_option_has_label : '', 'class="form-control" id="test_option_has_label"');
							?>
							<div id="error-test_option_has_label"></div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="test_option_no"><?php echo lang('test_option_no')?>:</label>					
						<div class="col-sm-4">
							<?php echo form_input(array('id'=>'test_option_no', 'name'=>'test_option_no', 'value'=>set_value('test_option_no', isset($record->test_option_no) ? $record->test_option_no : '', FALSE), 'class'=>'form-control'));?>
							<div id="error-test_option_no"></div>
						</div>
						<div class="col-sm-4">
							<a href="javascript:;" id="generate-labels" class="btn btn-info btn-sm">Generate Labels</a>
						</div>
					</div>
					<?php if ( $record->test_option_has_label == 'on' ) { ?>
						<div id="options-wrapper">
							<?php if(isset($record->test_option_label)) { ?>
								<?php
									$option_label = json_decode($record->test_option_label);
								?>
							
								<?php for($i=1;$i<=$record->test_option_no;$i++) { ?>
									<div class="form-group">
										<label class="col-sm-3 control-label" for="test_option_label">Label #<?php echo $i; ?>:</label>					
										<div class="col-sm-8">
											<?php echo form_input(array('name'=>'test_option_label[]', 'value'=>set_value('test_option_label[]', $option_label[$i-1]), 'class'=>'form-control'));?>
										</div>
									</div>
								<?php } ?>
							<?php } ?>
						</div>
					<?php } ?>
				</div>
				<div id="ranges" class="tab-pane fade">
					<div class="form-group">
						<label class="col-sm-3 control-label" for="test_age_range"><?php echo lang('test_age_range')?>:</label>
						<div class="col-sm-8">
							<?php echo form_textarea(array('id'=>'test_age_range', 'name'=>'test_age_range', 'value'=>set_value('test_age_range', isset($record->test_age_range) ? $record->test_age_range : ''), 'class'=>'form-control'));?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="test_category_range"><?php echo lang('test_category_range')?>:</label>
						<div class="col-sm-8">
							<?php echo form_textarea(array('id'=>'test_category_range', 'name'=>'test_category_range', 'value'=>set_value('test_category_range', isset($record->test_category_range) ? $record->test_category_range : ''), 'class'=>'form-control'));?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="test_tscore_range"><?php echo lang('test_tscore_range')?>:</label>
						<div class="col-sm-8">
							<?php echo form_textarea(array('id'=>'test_tscore_range', 'name'=>'test_tscore_range', 'value'=>set_value('test_tscore_range', isset($record->test_tscore_range) ? $record->test_tscore_range : ''), 'class'=>'form-control'));?>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label" for="test_percentile_range"><?php echo lang('test_percentile_range')?>:</label>
						<div class="col-sm-8">
							<?php echo form_textarea(array('id'=>'test_percentile_range', 'name'=>'test_percentile_range', 'value'=>set_value('test_percentile_range', isset($record->test_percentile_range) ? $record->test_percentile_range : ''), 'class'=>'form-control'));?>
						</div>
					</div>
				
				</div>
			</div>
		</div>
	</form>
</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
		<i class="fa fa-times"></i> <?php echo lang('button_close')?>
	</button>
	<?php if ($action == 'add'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_add')?>
		</button>
	<?php elseif ($action == 'edit'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_update')?>
		</button>
	<?php else: ?>
		<script>$(".modal-body :input").attr("disabled", true);</script>
	<?php endif; ?>
</div>
<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span>
		<span class="sr-only"><?php echo lang('button_close')?></span>
	</button>
	<h4 class="modal-title" id="myModalLabel"><?php echo $page_heading?></h4>
</div>

<div class="modal-body">

	<div class="form-horizontal">

		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#info">Information</a></li>
			<li><a data-toggle="tab" href="#description">Descriptions</a></li>
			<li><a data-toggle="tab" href="#norms">Norms</a></li>
		</ul>

		<div class="tab-content">
			<div id="info" class="tab-pane fade in active">
				<div class="form-group">
					<label class="col-sm-3 control-label" for="factor_name"><?php echo lang('factor_name')?>:</label>
					<div class="col-sm-8">
						<?php echo form_input(array('id'=>'factor_name', 'name'=>'factor_name', 'value'=>set_value('factor_name', isset($record->factor_name) ? $record->factor_name : ''), 'class'=>'form-control'));?>
						<div id="error-factor_name"></div>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label" for="factor_norm_to_use_in_report"><?php echo lang('factor_norm_to_use_in_report')?>:</label>
					<div class="col-sm-8">
						<?php echo form_dropdown('factor_norm_to_use_in_report', norm_type(), isset($record->factor_norm_to_use_in_report) ? $record->factor_norm_to_use_in_report : '', 'class="form-control" id="factor_norm_to_use_in_report"'); ?>
						<span class="help-block">
							Set this to use the correct norm to pull the interpretation to. 
						</span>
						<div id="error-factor_norm_to_use_in_report"></div>						
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label" for="factor_norm_type"><?php echo lang('factor_norm_type')?>:</label>
					<div class="col-sm-8">
						<?php echo form_dropdown('factor_norm_type', norm_type(), isset($record->factor_norm_type) ? $record->factor_norm_type : '', 'class="form-control" id="factor_norm_type"'); ?>
						<div id="error-factor_norm_type"></div>
					</div>
				</div>
			</div>
			<div id="description" class="tab-pane fade">
				<div class="form-group">
					<label class="col-sm-3 control-label" for="factor_left_description"><?php echo lang('factor_left_description')?>:</label>
					<div class="col-sm-8">
						<?php echo form_textarea(array('id'=>'factor_left_description', 'name'=>'factor_left_description', 'value'=>set_value('factor_left_description', isset($record->factor_left_description) ? $record->factor_left_description : ''), 'class'=>'form-control'));?>
						<div id="error-factor_left_description"></div>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label" for="factor_right_description"><?php echo lang('factor_right_description')?>:</label>
					<div class="col-sm-8">
						<?php echo form_textarea(array('id'=>'factor_right_description', 'name'=>'factor_right_description', 'value'=>set_value('factor_right_description', isset($record->factor_right_description) ? $record->factor_right_description : ''), 'class'=>'form-control'));?>
						<div id="error-factor_right_description"></div>
					</div>
				</div>
			</div>
			<div id="norms" class="tab-pane fade">
				<div class="form-group">
					<label class="col-sm-3 control-label" for="factor_norm_to_use"><?php echo lang('factor_norm_to_use')?>:</label>
					<div class="col-sm-8">
						<?php echo form_dropdown('factor_norm_to_use', norm_to_use(), isset($record->factor_norm_to_use) ? $record->factor_norm_to_use : '', 'class="form-control" id="factor_norm_to_use"'); ?>
						<div id="error-factor_norm_to_use"></div>
					</div>
				</div>

				<div class="form-group">
					<label class="col-sm-3 control-label" for="factor_norms"><?php echo lang('factor_norms')?>:</label>
					<div class="col-sm-8">
						<?php echo form_textarea(array('id'=>'factor_norms', 'name'=>'factor_norms', 'value'=>set_value('factor_norms', isset($record->factor_norms) ? $record->factor_norms : ''), 'class'=>'form-control'));?>
						<span class="help-block">
							<p class="bold">Note:</p>
							<p>Make sure the norms format is correct. Add 0 if stanine/%tile/sten is not applicable.</p>
							<p>RawScore - Stanine - %ILE - STEN - TScore - Category</p>
						</span>
						<div id="error-factor_norms"></div>
					</div>
				</div>
			</div>

		</div>

		

		

		



	</div>

</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
		<i class="fa fa-times"></i> <?php echo lang('button_close')?>
	</button>
	<?php if ($action == 'add'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_add')?>
		</button>
	<?php elseif ($action == 'edit'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_update')?>
		</button>
	<?php else: ?>
		<script>$(".modal-body :input").attr("disabled", true);</script>
	<?php endif; ?>
</div>
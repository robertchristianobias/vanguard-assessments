<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Migration_Create_questions extends CI_Migration {

	private $_table = 'questions';

	private $_permissions = array(
		array('Questions Link', 'tests.questions.link'),
		array('Questions List', 'tests.questions.list'),
		array('View Question', 'tests.questions.view'),
		array('Add Question', 'tests.questions.add'),
		array('Edit Question', 'tests.questions.edit'),
		array('Delete Question', 'tests.questions.delete'),
	);

	function __construct()
	{
		parent::__construct();

		$this->load->model('migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'question_id' 			=> array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE, 'null' => FALSE),
			'question_factor_id'		=> array('type' => 'INT', 'constraint' => 10, 'null' => FALSE),
			'question_content'		=> array('type' => 'TEXT', 'null' => FALSE),
			'question_options'		=> array('type' => 'TEXT', 'null' => FALSE),

			'question_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'question_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'question_modified_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'question_modified_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'question_deleted' 		=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'question_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('question_id', TRUE);
		$this->dbforge->add_key('question_factor_id');

		$this->dbforge->add_key('question_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

		// add the module permissions
		$this->migrations_model->add_permissions($this->_permissions);

	}

	public function down()
	{
		// drop the table
		$this->dbforge->drop_table($this->_table, TRUE);

		// delete the permissions
		$this->migrations_model->delete_permissions($this->_permissions);

	}
}
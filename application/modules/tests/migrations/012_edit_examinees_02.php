<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link		http://www.google.com
 */
class Migration_Edit_examinees_02 extends CI_Migration 
{
	private $_table = 'examinees';

	public function __construct() 
	{
		parent::__construct();

		$this->load->model('migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'examinee_status' => array('type' => 'VARCHAR', 'constraint' => 20, 'null' => TRUE, 'after' => 'examinee_end_date', 'default' => 'Available'),
		);
		
		$this->dbforge->add_column($this->_table, $fields);
	}

	public function down()
	{
		$this->dbforge->drop_column($this->_table, 'examinee_status');
	}
}
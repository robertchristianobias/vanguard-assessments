<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Migration_Create_interpretations extends CI_Migration {

	private $_table = 'interpretations';

	private $_permissions = array(
		array('Interpretations Link', 'tests.interpretations.link'),
		array('Interpretations List', 'tests.interpretations.list'),
		array('View Interpretation', 'tests.interpretations.view'),
		array('Add Interpretation', 'tests.interpretations.add'),
		array('Edit Interpretation', 'tests.interpretations.edit'),
		array('Delete Interpretation', 'tests.interpretations.delete'),
	);

	function __construct()
	{
		parent::__construct();

		$this->load->model('migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'interpretation_id' 			=> array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE, 'null' => FALSE),
			'interpretation_factor_id'	=> array('type' => 'INT', 'constraint' => 11, 'null' => FALSE),
			'interpretation_norm_to_use'		=> array('type' => 'SET("Stanine","Percentile","Sten","T-Score","Category")', 'null' => FALSE),
			'interpretation_norm_value'		=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			'interpretation_gender'		=> array('type' => 'SET("Male","Female")', 'null' => FALSE),
			'interpretation_content'		=> array('type' => 'TEXT', 'null' => FALSE),

			'interpretation_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'interpretation_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'interpretation_modified_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'interpretation_modified_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'interpretation_deleted' 		=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'interpretation_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('interpretation_id', TRUE);
		$this->dbforge->add_key('interpretation_factor_id');
		$this->dbforge->add_key('interpretation_norm_to_use');
		$this->dbforge->add_key('interpretation_gender');

		$this->dbforge->add_key('interpretation_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

		// add the module permissions
		$this->migrations_model->add_permissions($this->_permissions);
	}

	public function down()
	{
		// drop the table
		$this->dbforge->drop_table($this->_table, TRUE);

		// delete the permissions
		$this->migrations_model->delete_permissions($this->_permissions);
	}
}
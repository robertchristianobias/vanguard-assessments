<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Migration_Create_question_factors extends CI_Migration {

	private $_table = 'question_factors';

	public function __construct()
	{
		parent::__construct();

		$this->load->model('migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'qf_id' 			=> array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE, 'null' => FALSE),
			'qf_question_id'	=> array('type' => 'INT', 'constraint' => 10, 'null' => FALSE),
			'qf_factor_id'		=> array('type' => 'INT', 'constraint' => 10, 'null' => FALSE),

			'qf_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'qf_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'qf_modified_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'qf_modified_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'qf_deleted' 		=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'qf_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('qf_id', TRUE);
		$this->dbforge->add_key('qf_question_id');
		$this->dbforge->add_key('qf_factor_id');

		$this->dbforge->add_key('qf_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

	}

	public function down()
	{
		// drop the table
		$this->dbforge->drop_table($this->_table, TRUE);
	}
}
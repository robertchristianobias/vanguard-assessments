<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Migration_Rollback_tests extends CI_Migration {

	function __construct()
	{
		parent::__construct();
	}
	
	public function up()
	{

	}

	public function down()
	{
		
	}
}
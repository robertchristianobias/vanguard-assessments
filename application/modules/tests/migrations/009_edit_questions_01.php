<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link		http://www.google.com
 */
class Migration_Edit_questions_01 extends CI_Migration 
{
	private $_table = 'questions';

	public function __construct() 
	{
		parent::__construct();

		$this->load->model('migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'question_test_id' 	=> array('type' => 'INT', 'constraint' => 10, 'null' => FALSE, 'after' => 'question_id'),
		);
		
		$this->dbforge->add_column($this->_table, $fields);
	}

	public function down()
	{
		$this->dbforge->drop_column($this->_table, 'question_test_id');
	}
}
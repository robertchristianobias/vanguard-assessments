<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Migration_Create_factors extends CI_Migration {

	private $_table = 'factors';

	private $_permissions = array(
		array('Factors Link', 'tests.factors.link'),
		array('Factors List', 'tests.factors.list'),
		array('View Factor', 'tests.factors.view'),
		array('Add Factor', 'tests.factors.add'),
		array('Edit Factor', 'tests.factors.edit'),
		array('Delete Factor', 'tests.factors.delete'),
	);

	function __construct()
	{
		parent::__construct();

		$this->load->model('migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'factor_id' 			=> array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE, 'null' => FALSE),
			'factor_test_id'	=> array('type' => 'INT', 'constraint' => 11, 'null' => FALSE),
			'factor_name'		=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			'factor_norm_to_use_in_report'		=> array('type' => 'SET("Stanine","Percentile","Sten","T-Score","Category")', 'null' => FALSE),
			'factor_norm_type'		=> array('type' => 'SET("Stanine","Percentile","Sten","T-Score","Category")', 'null' => FALSE),
			'factor_left_description'		=> array('type' => 'TEXT', 'null' => FALSE),
			'factor_right_description'		=> array('type' => 'TEXT', 'null' => FALSE),
			'factor_norm_to_use'		=> array('type' => 'SET("Normal","Gender","Position","Age")', 'null' => FALSE),
			'factor_norms'		=> array('type' => 'TEXT', 'null' => FALSE),

			'factor_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'factor_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'factor_modified_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'factor_modified_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'factor_deleted' 		=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'factor_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('factor_id', TRUE);
		$this->dbforge->add_key('factor_name');
		$this->dbforge->add_key('factor_norm_to_use_in_report');
		$this->dbforge->add_key('factor_norm_type');
		$this->dbforge->add_key('factor_norm_to_use');

		$this->dbforge->add_key('factor_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

		// add the module permissions
		$this->migrations_model->add_permissions($this->_permissions);
	}

	public function down()
	{
		// drop the table
		$this->dbforge->drop_table($this->_table, TRUE);

		// delete the permissions
		$this->migrations_model->delete_permissions($this->_permissions);
	}
}
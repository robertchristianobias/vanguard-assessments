<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Migration_Create_tests_answers extends CI_Migration {

	private $_table = 'test_answers';

	public function __construct()
	{
		parent::__construct();

		$this->load->model('migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'test_answer_id' 			 => array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE, 'null' => FALSE),
			'test_answer_user_id'		 => array('type' => 'INT', 'constraint' => 10, 'null' => FALSE),
			'test_answer_test_id'		 => array('type' => 'INT', 'constraint' => 10, 'null' => FALSE),
			'test_answer_question_id'	 => array('type' => 'INT', 'constraint' => 10, 'null' => FALSE ),
			'test_answer_answer' 		 => array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			
			'test_answer_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'test_answer_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'test_answer_modified_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'test_answer_modified_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'test_answer_deleted' 		=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'test_answer_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('test_answer_id', TRUE);
		$this->dbforge->add_key('test_answer_user_id');
		$this->dbforge->add_key('test_answer_test_id');
		$this->dbforge->add_key('test_answer_question_id');
		$this->dbforge->add_key('test_answer_answer');

		$this->dbforge->add_key('test_answer_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

	}

	public function down()
	{
		// drop the table
		$this->dbforge->drop_table($this->_table, TRUE);

	}
}
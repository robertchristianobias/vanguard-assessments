<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Migration_Create_test_batteries extends CI_Migration {

	private $_table = 'test_batteries';

	private $_permissions = array(
		array('Test_batteries Link', 'tests.test_batteries.link'),
		array('Test_batteries List', 'tests.test_batteries.list'),
		array('View Test_battery', 'tests.test_batteries.view'),
		array('Add Test_battery', 'tests.test_batteries.add'),
		array('Edit Test_battery', 'tests.test_batteries.edit'),
		array('Delete Test_battery', 'tests.test_batteries.delete'),
	);

	private $_menus = array(
		array(
			'menu_parent'		=> 'tests',
			'menu_text' 		=> 'Test_batteries', 
			'menu_link' 		=> 'tests/test_batteries', 
			'menu_perm' 		=> 'tests.test_batteries.link', 
			'menu_icon' 		=> 'fa fa-microchip', 
			'menu_order' 		=> 2, 
			'menu_active' 		=> 1
		),
	);

	function __construct()
	{
		parent::__construct();

		$this->load->model('migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'test_battery_id' 			=> array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE, 'null' => FALSE),
			'test_battery_company_id'		=> array('type' => 'INT', 'constraint' => 10, 'null' => FALSE),
			'test_battery_name'		=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			'test_battery_tests'		=> array('type' => 'TEXT', 'null' => FALSE),

			'test_battery_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'test_battery_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'test_battery_modified_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'test_battery_modified_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'test_battery_deleted' 		=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'test_battery_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('test_battery_id', TRUE);
		$this->dbforge->add_key('test_battery_company_id');
		$this->dbforge->add_key('test_battery_name');

		$this->dbforge->add_key('test_battery_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

		// add the module permissions
		$this->migrations_model->add_permissions($this->_permissions);

		// add the module menu
		$this->migrations_model->add_menus($this->_menus);
	}

	public function down()
	{
		// drop the table
		$this->dbforge->drop_table($this->_table, TRUE);

		// delete the permissions
		$this->migrations_model->delete_permissions($this->_permissions);

		// delete the menu
		$this->migrations_model->delete_menus($this->_menus);
	}
}
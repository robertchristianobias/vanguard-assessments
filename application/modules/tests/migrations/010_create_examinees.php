<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Migration_Create_examinees extends CI_Migration {

	private $_table = 'examinees';

	private $_permissions = array(
		array('Examinees Link', 'companies.examinees.link'),
		array('Examinees List', 'companies.examinees.list'),
		array('View Examinee', 'companies.examinees.view'),
		array('Add Examinee', 'companies.examinees.add'),
		array('Edit Examinee', 'companies.examinees.edit'),
		array('Delete Examinee', 'companies.examinees.delete'),
	);

	public function __construct()
	{
		parent::__construct();

		$this->load->model('migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'examinee_id' 				=> array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE, 'null' => FALSE),
			'examinee_raw_password'		=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			'examinee_user_id'			=> array('type' => 'INT', 'constraint' => 10, 'null' => FALSE),
			'examinee_test_battery_id'	=> array('type' => 'INT', 'constraint' => 10, 'null' => FALSE ),
			'examinee_company_id'		=> array('type' => 'INT', 'constraint' => 10, 'null' => FALSE),
			'examinee_start_date'		=> array('type' => 'DATE', 'null' => FALSE),
			'examinee_end_date'			=> array('type' => 'DATE', 'null' => FALSE),

			'examinee_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'examinee_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'examinee_modified_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'examinee_modified_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'examinee_deleted' 		=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'examinee_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('examinee_id', TRUE);
		$this->dbforge->add_key('examinee_user_id');
		$this->dbforge->add_key('examinee_test_battery_id');
		$this->dbforge->add_key('examinee_company_id');

		$this->dbforge->add_key('examinee_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

		// add the module permissions
		$this->migrations_model->add_permissions($this->_permissions);
	}

	public function down()
	{
		// drop the table
		$this->dbforge->drop_table($this->_table, TRUE);

		// delete the permissions
		$this->migrations_model->delete_permissions($this->_permissions);
	}
}
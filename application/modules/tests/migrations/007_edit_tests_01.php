<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link		http://www.google.com
 */
class Migration_Edit_tests_01 extends CI_Migration 
{
	private $_table = 'tests';

	public function __construct() 
	{
		parent::__construct();

		$this->load->model('migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'test_age_range' 		=> array('type' => 'TEXT', 'null' => TRUE),
			'test_category_range'	=> array('type' => 'TEXT', 'null' => TRUE),
			'test_tscore_range'		=> array('type' => 'TEXT', 'null' => TRUE),
			'test_percentile_range' => array('type' => 'TEXT', 'null' => TRUE)
		);
		
		$this->dbforge->add_column($this->_table, $fields);
	}

	public function down()
	{
		$this->dbforge->drop_column($this->_table, 'test_age_range');
		$this->dbforge->drop_column($this->_table, 'test_category_range');
		$this->dbforge->drop_column($this->_table, 'test_tscore_range');
		$this->dbforge->drop_column($this->_table, 'test_percentile_range');
	}
}
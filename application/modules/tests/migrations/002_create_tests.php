<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Migration_Create_tests extends CI_Migration {

	private $_table = 'tests';

	private $_permissions = array(
		array('Tests Link', 'tests.tests.link'),
		array('Tests List', 'tests.tests.list'),
		array('View Test', 'tests.tests.view'),
		array('Add Test', 'tests.tests.add'),
		array('Edit Test', 'tests.tests.edit'),
		array('Delete Test', 'tests.tests.delete'),
	);

	private $_menus = array(
		array(
			'menu_parent'		=> '',
			'menu_text' 		=> 'Tests', 
			'menu_link' 		=> 'tests', 
			'menu_perm' 		=> 'tests.tests.link', 
			'menu_icon' 		=> 'fa fa-lightbulb-o', 
			'menu_order' 		=> 2, 
			'menu_active' 		=> 1
		),
		array(
			'menu_parent'		=> 'tests',
			'menu_text' 		=> 'Tests', 
			'menu_link' 		=> 'tests/tests', 
			'menu_perm' 		=> 'tests.tests.link', 
			'menu_icon' 		=> 'fa fa-lightbulb-o', 
			'menu_order' 		=> 1, 
			'menu_active' 		=> 1
		),
	);

	function __construct()
	{
		parent::__construct();

		$this->load->model('migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'test_id' 			=> array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE, 'null' => FALSE),
			'test_name'		=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			'test_slug'		=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			'test_type'		=> array('type' => 'SET("Personality","Intellectual")', 'null' => FALSE),
			'test_instruction'		=> array('type' => 'TEXT', 'null' => FALSE),
			'test_parent_test_id'		=> array('type' => 'INT', 'constraint' => 10, 'null' => FALSE),
			'test_is_timed'		=> array('type' => 'SET("Yes","No")', 'null' => TRUE),
			'test_time_minute'		=> array('type' => 'INT', 'constraint' => 10, 'null' => TRUE),
			'test_time_seconds'		=> array('type' => 'INT', 'constraint' => 10, 'null' => TRUE),
			'test_norm_type'		=> array('type' => 'SET("Stanine","Percentile","Sten","T-Score","Category")', 'null' => FALSE),
			'test_option_type' => array('type' => 'VARCHAR', 'constraint' => 50, 'null' => FALSE),
			'test_option_no' 	=> array('type' => 'INT', 'constraint' => 11, 'null' => FALSE),			
			'test_option_has_label'  => array('type' => 'VARCHAR', 'constraint' => 50, 'null' => TRUE),
			'test_option_label' => array('type' => 'TEXT', 'null' => TRUE),
			'test_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'test_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'test_modified_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'test_modified_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'test_deleted' 		=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'test_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('test_id', TRUE);
		$this->dbforge->add_key('test_name');
		$this->dbforge->add_key('test_slug');
		$this->dbforge->add_key('test_type');
		$this->dbforge->add_key('test_parent_test_id');
		$this->dbforge->add_key('test_norm_type');

		$this->dbforge->add_key('test_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

		// add the module permissions
		$this->migrations_model->add_permissions($this->_permissions);

		// add the module menu
		$this->migrations_model->add_menus($this->_menus);
	}

	public function down()
	{
		// drop the table
		$this->dbforge->drop_table($this->_table, TRUE);

		// delete the permissions
		$this->migrations_model->delete_permissions($this->_permissions);

		// delete the menu
		$this->migrations_model->delete_menus($this->_menus);
	}
}
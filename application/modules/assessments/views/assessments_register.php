<div id="content">
    <h3 class="main_question">Please fill with your details</h3>

    <?php echo form_open(current_url(), 'id="register-form"'); ?>
        <h4>Personal Information</h4>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_input('first_name', set_value('first_name'), 'class="form-control" placeholder="First Name"'); ?>
                    <div id="error-first_name"></div>
                </div>
                
                <div class="form-group">
                    <?php echo form_input('bday', set_value('bday'), 'class="form-control" placeholder="Birthdate"'); ?>
                    <div id="error-bday"></div>
                </div>

                <div class="form-group">
                    <div class="styled-select">
                        <select class="required" name="gender">
                            <option value="" selected="">Gender</option>
                            <option value="Male">Male</option>
                            <option value="Female">Female</option>
                        </select>
                    </div>
                </div>
                
                <div class="form-group">
                    <button type="submit" name="submit" class="submit" id="submit">Submit</button>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <?php echo form_input('email', set_value('email'), 'class="form-control" placeholder="Email"'); ?>
                    <div id="error-email"></div>
                </div>
                <div class="form-group">
                    <?php echo form_input('job_title', set_value('job_title'), 'class="form-control" placeholder="Last/Current Job Title"'); ?>
                    <div id="error-job_title"></div>
                </div>
                <div class="form-group">
                    <?php echo form_input('industry', set_value('industry'), 'class="form-control" placeholder="Industry Employed"'); ?>
                    <div id="error-industry"></div>
                </div>
            </div>
        </div>
    <?php echo form_close(); ?>
</div>
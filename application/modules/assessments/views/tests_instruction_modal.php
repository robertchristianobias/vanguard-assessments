<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span>
		<span class="sr-only"><?php echo lang('button_close')?></span>
	</button>
	<h4 class="modal-title" id="myModalLabel"><?php echo $page_heading?></h4>
</div>
<div class="modal-body">
    <h2><?php echo $test->test_name ?></h2>
    <?php echo $test->test_instruction; ?>
</div>
<div class="modal-footer">
	<button type="button" class="btn btn-info" data-dismiss="modal">
		<i class="fa fa-times"></i> <?php echo lang('button_close')?>
	</button>

</div>


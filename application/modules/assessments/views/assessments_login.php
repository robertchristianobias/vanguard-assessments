<div id="content">
    <?php echo form_open(current_url(), 'id="login-form"'); ?>
        <h3><?php echo $page_heading; ?></h3>
        <div class="form-group">
            <?php echo form_input('username', set_value('username'), 'class="form-control styled" placeholder="Username"'); ?>
            <div id="error-username"></div>
        </div>
        <div class="form-group">
            <?php echo form_input('password', set_value('password'), 'class="form-control styled" placeholder="Password"'); ?>
            <div id="error-password"></div>
        </div>
        <div class="form-group">
            <button type="submit" name="submit" class="submit" id="submit">Submit</button>
        </div>
    <?php echo form_close(); ?>
</div>
$(document).ready(function() {
    
    var $test_id = $('#assessment_form').data('test-id');

    $('#paging').on('click', 'a',function(e){
        e.preventDefault();
        
        var $ajax_url = website_url + 'assessments/tests/save_answer/' + $test_id;
            $data  = $('#assessment_form').serializeArray();

        // save to db
        $.post($ajax_url, $data, function(data) {
            var $response = $.parseJSON(data);

            if($response.success === false) {
                alertify.error($response.message);

                // displays individual error messages
                if ($response.errors) {
                    for (var form_name in $response.errors) {
                        $('.error-' + form_name).html($response.errors[form_name]);
                    }
                }
            }
            else
            {
                var pageno = $(this).attr('data-ci-pagination-page');
                load_paging(pageno);
            }
        });
        
        
    });

    load_paging(1);

    function load_paging(pageno) {

        var $ajax_url = website_url + 'assessments/tests/paging/' + pageno;

        $.post($ajax_url, {
            test_id: $test_id
        }, function(data) {
            var $response = $.parseJSON(data);

            if ($response.total_page == pageno) // show the submit button
            {
                $('#submit_test').removeClass('hidden');
            }
            else
            {

            }

            $('#results').html($response.result);
            $('#paging').html($response.pagination);
        });

   
    }

});
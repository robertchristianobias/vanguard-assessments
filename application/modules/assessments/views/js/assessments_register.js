/**
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */

$(function() {

	// handles the submit action
	$('#submit').click(function(e){

		// change the button to loading state
		var btn = $(this);
		btn.html('loading');

		var frm = $('#register-form');
			ajax_url = frm.attr('action');

		// prevents a submit button from submitting a form
		e.preventDefault();

		// submits the data to the backend
		$.post(ajax_url, frm.serializeArray(),

			function(data, status) {

				// handles the returned data
				var o = jQuery.parseJSON(data);
                if (o.success === false) {
					// reset the button
					btn.html('Submit');
					
					// shows the error message
					alertify.error(o.message);

					// displays individual error messages
					if (o.errors) {
						for (var form_name in o.errors) {
							$('#error-' + form_name).html(o.errors[form_name]);
						}
					}
				} else {

					alertify.alert('Success', o.message, function() {

						window.location.replace(o.redirect);
					});

				}
			});
	});

});
<?php if ( $record ) { ?>
    <?php $counter = 1; ?>
        <?php foreach ( $record as $question ) { ?>
            <?php
                $options = json_decode($question->question_options);
                $options = (array) $options;
            ?>
            <div class="question_wrapper">
                <div class="row clearfix">
                    <div class="col-md-12">
                        <div class="clearfix">
                            <p class="pull-left"><strong><?php echo $counter; ?>.</strong> </p>
                            <div class="pull-left">
                                <?php echo $question->question_content; ?>
                            </div>
                        </div>
                        
                        <div class="question_options">
                            <div class="row">
                                <?php if ( $options ) { ?>
                                    <?php $counter_2 = 1; ?>
                                    <?php foreach ( $options as $option ) { ?>
                                        <div class="col-xs-4 col-md-4 text-center">
                                            <div class="radio">
                                                <label class="text-center">
                                                    <img src="<?php echo site_url($option->image); ?>" />
                                                </label>
                                            </div>
                                            <input type="radio" name="answers[<?php echo $question->question_id; ?>]" value="<?php echo $option->answer; ?>">
                                        </div>
                                    <?php $counter_2++; ?>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="error-answers"></div>

                    </div>
                </div>
                <hr />
            </div>
        <?php $counter++; ?>
        <?php } ?>
    <?php } ?> 
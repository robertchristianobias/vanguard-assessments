<div id="content">
    <h2><?php echo $test->test_name ?></h2>
    <small><a href="<?php echo site_url('assessments/tests/view_instruction/' . encode($test->test_id)); ?>" data-toggle="modal" data-target="#modal">View instructions</a></small>
  
    <?php echo form_open(current_url(), 'id="assessment_form" data-test-id="'.encode($test->test_id).'"'); ?>
        <div id="results"></div>
        <div id="paging" class="text-left"></div>

        <br />
        <div class="form-group hidden" id="submit_test">
            <button type="submit" name="submit" class="submit btn-block" id="submit">Submit</button>
        </div>
    <?php echo form_close(); ?>
</div>
<div id="content">
    <h2><?php echo $test->test_name ?></h2>

    <?php echo $test->test_instruction; ?>

    <div class="text-center">
        <a href="<?php echo site_url('assessments/tests/start/' . encode($test->test_id)); ?>" class="btn btn-info">I HAVE READ AND UNDERSTOOD THE INSTRUCTION. START THE TEST</a>
    </div>
    
</div>
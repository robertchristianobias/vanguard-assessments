<div id="content">
    <h2><?php echo $page->page_title; ?></h2>

    <?php echo $page->page_content; ?>

    <div class="text-center">
        <a href="<?php echo site_url('assessments/logout'); ?>" class="btn btn-danger">I DISAGREE</a>
        <a href="<?php echo site_url('assessments/register'); ?>" class="btn btn-info">CLICK HERE TO PROCEED TO REGISTRATION PAGE</a>
    </div>
    
</div>
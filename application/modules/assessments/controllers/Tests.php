<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Companies Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Tests extends CI_Controller {
	
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */

	public $user;
	public function __construct()
	{
		parent::__construct();

		$this->user = $this->ion_auth->user()->row();
        $this->load->model('tests/examinees_model');
        $this->load->model('tests/tests_model');
        $this->load->model('tests/test_batteries_model');
        $this->load->model('tests/tests_taken_model');
        $this->load->model('tests/questions_model');
        $this->load->model('tests/test_answers_model');

        $this->load->library('pagination');
	}
	
	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function index()
	{
        if ( ! $this->user )
		{
			redirect('assessments/login');
        }
        
        // define the tests that will be taken
        $examinee = $this->examinees_model->get_examinee_by_user($this->user->id);
        $test_battery = $this->test_batteries_model->get_test_battery($examinee->examinee_test_battery_id);
        $tests = json_decode($test_battery->test_battery_tests);
        $data = array();

        if ( $tests )
        {
            // get completed tests
            $tests_taken = $this->tests_taken_model->get_tests_taken($examinee->examinee_user_id, $examinee->examinee_test_battery_id, $examinee->examinee_company_id);
            $tests_arr = array();
            if ( $tests_taken )
            {
                foreach ( $tests_taken as $test )
                {
                    $tests_arr[] = $test->test_taken_test_id;
                }
            }
            else
            {
                $tests_arr = $tests;
                foreach ( $tests_arr as $test )
                {
                    // save the test and redirect to the test page
                    $data[] = array(
                        'test_taken_user_id' => $this->user->id,
                        'test_taken_test_id' => $test,
                        'test_taken_status'  => 'Pending',
                        'test_taken_test_battery_id' => $examinee->examinee_test_battery_id,
                        'test_taken_company_id' => $examinee->examinee_company_id
                    );
                }

                $this->tests_taken_model->insert_batch($data);
            }

            redirect('assessments/tests/instruction/' . encode($tests_arr['0']));

        }

       

       // redirect('assessments/login');
		// $this->acl->restrict('companies.companies.list');
		
		// // page title
		// $data['page_heading'] = lang('index_heading');
		// $data['page_subhead'] = lang('index_subhead');
		
		// // breadcrumbs
		// $this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		// $this->breadcrumbs->push(lang('crumb_module'), site_url('companies'));
		
		// // session breadcrumb
		// $this->session->set_userdata('redirect', current_url());
		
		// // add plugins
		// $this->template->add_css('assets/plugins/DataTables/datatables.min.css');
		// $this->template->add_js('assets/plugins/DataTables/datatables.min.js');
		
		// // render the page
		// $this->template->add_css(module_css('companies', 'companies_index'), 'embed');
		// $this->template->add_js(module_js('companies', 'companies_index'), 'embed');
		// $this->template->write_view('content', 'companies_index', $data);
		// $this->template->render();
    }
    
    public function instruction($test_id)
    {
        if ( ! $this->user )
		{
			redirect('assessments/login');
        }
        
        $data['test'] = $this->tests_model->get_test(decode($test_id));
        $data['page_heading'] = 'Instruction';

        $this->template->set_template(config_item('website_theme').'_index');

		$this->template->add_js(module_js('assessments', 'tests_instruction'), 'embed');
		$this->template->write_view('content', 'tests_instruction', $data);
		$this->template->render();
    }

    public function view_instruction($test_id)
    {
        if ( ! $this->user )
		{
			redirect('assessments/login');
        }

        $test_id = decode($test_id);
        $test = $this->tests_model->get_test($test_id);
        
        $data['test'] = $test;
        $data['page_heading'] = 'View Instruction';

        $this->template->set_template(config_item('website_theme').'_modal');
        $this->template->write_view('content', 'tests_instruction_modal', $data);
		$this->template->render();
    }

    public function start($test_id)
    {
        if ( ! $this->user )
		{
			redirect('assessments/login');
        }
        
        $test_id = decode($test_id);
        $test = $this->tests_model->get_test($test_id);
       // $questions = $this->questions_model->get_questions($test_id);

        $data['test']         = $test;
        $data['page_heading'] = 'Start ' . $test->test_name;
        $data['questions']    = array();

        switch ( $test->test_option_type )
        {
            case 'Image':
                $tpl = 'tests_start_image';
            break;
            case 'Text':
            break;
            default:
        }

        $this->template->set_template(config_item('website_theme').'_index');

        $this->template->add_js(module_js('assessments', 'tests_start'), 'embed');

        $this->template->write_view('content', 'tests_start', $data);
		$this->template->render();
    }

    public function paging($row_no = 1)
    {
        $row_per_page = 10;

        if ( $row_no != 1 ) {
            $row_no = ( $row_no - 1) * $row_per_page;
        }

        $test_id   = decode($this->input->post('test_id'));
        $test      = $this->tests_model->get_test($test_id);
        $questions = $this->questions_model->get_questions($test_id);

        switch ( $test->test_option_type )
        {
            case 'Image':
                $tpl = 'tests_start_image';
            break;
            case 'Text':
            break;
            default:
        }
        
        $allcount = count($questions);

 
        $users_record = $this->questions_model->get_questions($test_id, $row_per_page, $row_no);

        $paging_config = array(
            'base_url'         => site_url('assessments/tests/paging'),
            'use_page_numbers' => TRUE,
            'total_rows'       => $allcount,
            'per_page'         => $row_per_page,
            'next_link'        => 'Next',
            'prev_link'        => 'Previous',
            'last_link'        => FALSE,
            'first_link'       => FALSE,
            'display_pages'    => FALSE,
            'attributes'       => array(
                'class' => 'btn btn-success'
            )
        );

        $this->pagination->initialize($paging_config);

        $data['pagination'] = $this->pagination->create_links();
        $data['result']     = $this->load->view($tpl, array('record'=>$users_record), TRUE);
        $data['row']        = $row_no;
        $data['total_page'] = ceil($allcount / $row_per_page);

        echo json_encode($data);
    }

    public function save_answer($test_id)
    {
        $test_id = decode($test_id);

        if ($_POST)
        {
            $answers = $this->input->post('answers');
            $answer_data = array();
            foreach ( $answers as $question_id=>$answer )
            {
                $answer_data[] = array(
                    'test_answer_user_id'       => $this->user->id,
                    'test_answer_test_id'       => $test_id,
                    'test_answer_question_id'   => $question_id,
                    'test_answer_answer'        => $answer
                );
            }

            $this->test_answers_model->insert_batch($answer_data);

            $response['success'] = TRUE;
            $response['message'] = '';
            $response['redirect'] = '';

            echo json_encode($response); exit;            
        }

        // $this->form_validation->set_rules('answers[]', lang('answers'), 'required');

        // $this->form_validation->set_message('required', 'Answer is required');
        // $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
        
        // if ($this->form_validation->run($this) == FALSE)
        // {
        //     $response['success'] = FALSE;
        //     $response['message'] = 'One or more question are not yet answered';
        //     $response['errors'] = array(					
        //         'answers' => form_error('answers[]'),
        //     );
        // }
        // else
        // {

        // }

    }

}

/* End of file Companies.php */
/* Location: ./application/modules/companies/controllers/Companies.php */
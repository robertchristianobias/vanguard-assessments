<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * Companies Class
 *
 * @package        rcmediaph
 * @version        1.0
 * @author         Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright     Copyright (c) 2017, Google.
 * @link        http://www.google.com
 */
class Assessments extends CI_Controller
{
    
    /**
     * Constructor
     *
     * @access    public
     *
     */
    
    public $user;
    public function __construct()
    {
        parent::__construct();
        
        $this->user = $this->ion_auth->user()->row();
        
        $this->load->model('website/pages_model');
        $this->load->model('tests/examinees_model');
        $this->load->model('users/users_model');
    }
    
    // --------------------------------------------------------------------
    
    /**
     * index
     *
     * @access    public
     * @param    none
     * @author     Robert Christian Obias <rchristian_obias@yahoo.com>
     */
    public function index()
    {
        // redirect('assessments/login');
        // $this->acl->restrict('companies.companies.list');
        
        // // page title
        // $data['page_heading'] = lang('index_heading');
        // $data['page_subhead'] = lang('index_subhead');
        
        // // breadcrumbs
        // $this->breadcrumbs->push(lang('crumb_home'), site_url(''));
        // $this->breadcrumbs->push(lang('crumb_module'), site_url('companies'));
        
        // // session breadcrumb
        // $this->session->set_userdata('redirect', current_url());
        
        // // add plugins
        // $this->template->add_css('assets/plugins/DataTables/datatables.min.css');
        // $this->template->add_js('assets/plugins/DataTables/datatables.min.js');
        
        // // render the page
        // $this->template->add_css(module_css('companies', 'companies_index'), 'embed');
        // $this->template->add_js(module_js('companies', 'companies_index'), 'embed');
        // $this->template->write_view('content', 'companies_index', $data);
        // $this->template->render();
    }
    
    public function login()
    {
        $data['page_heading'] = 'Examinees Login';
        
        if ($_POST) {
            $this->form_validation->set_rules('username', lang('username'), 'required');
            $this->form_validation->set_rules('password', lang('password'), 'required');
            
            $this->form_validation->set_message('required', 'This field is required');
            $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
            
            if ($this->form_validation->run($this) == FALSE) {
                $response['success'] = FALSE;
                $response['message'] = lang('validation_error');
                $response['errors']  = array(
                    'username' => form_error('username'),
                    'password' => form_error('password')
                );
            } else {
                $username = $this->input->post('username');
                $password = $this->input->post('password');
                
                $this->ion_auth_model->identity_column = 'username';
                
                if ($this->ion_auth->login($username, $password, FALSE)) {
                    $examinee = $this->examinees_model->find_by('examinee_raw_password', $password);
                    
                    if (isset($examinee->examinee_status)) {
                        if ($examinee->examinee_status == 'Available') {
                            $response['success']  = TRUE;
                            $response['message']  = 'Successfulyl login';
                            $response['redirect'] = site_url('assessments/consent');
                        } else // redirect to verify identity page
                            {
                            $response['success']  = TRUE;
                            $response['message']  = 'Please verify your account on the next page';
                            $response['redirect'] = site_url('assessments/verify');
                        }
                    }
                } else {
                    $response['success'] = FALSE;
                    $response['message'] = 'Login incorrect';
                }
            }
            
            echo json_encode($response);
            exit;
        }
        
        $this->template->set_template(config_item('website_theme') . '_index');
        
        $this->template->add_js(module_js('assessments', 'assessments_login'), 'embed');
        $this->template->write_view('content', 'assessments_login', $data);
        $this->template->render();
        
    }
    
    public function logout()
    {
        $logout = $this->ion_auth->logout();
        
        if ($logout) {
            redirect('assessments/login');
        }
    }
    
    public function consent()
    {
        if (!$this->user) {
            redirect('assessments/login');
        }
        
        $page = $this->pages_model->get_page('2');
        
        $data['page_heading'] = $page->page_title;
        $data['page']         = $page;
        
        $this->template->set_template(config_item('website_theme') . '_index');
        
        //$this->template->add_js(module_js('assessments', 'assessments_login'), 'embed');
        $this->template->write_view('content', 'assessments_consent', $data);
        $this->template->render();
    }
    
    public function register()
    {
        if (!$this->user) {
            redirect('assessments/login');
        }
        
        $data['page_heading'] = 'Register';
        
        if ($_POST) {
            $this->form_validation->set_rules('first_name', lang('first_name'), 'required');
            $this->form_validation->set_rules('bday', lang('bday'), 'required');
            $this->form_validation->set_rules('gender', lang('gender'), 'required');
            $this->form_validation->set_rules('email', lang('email'), 'required|valid_email');
            $this->form_validation->set_rules('job_title', lang('job_title'), 'required');
            $this->form_validation->set_rules('industry', lang('industry'), 'required');
            
            $this->form_validation->set_message('required', 'This field is required');
            $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
            
            if ($this->form_validation->run($this) == FALSE) {
                $response['success'] = FALSE;
                $response['message'] = lang('validation_error');
                $response['errors']  = array(
                    'first_name' => form_error('first_name'),
										'bday' => form_error('bday'),
										'gender' => form_error('gender'),
                    'email' => form_error('email'),
										'job_title' => form_error('job_title'),
										'industry' => form_error('industry')
                );
            } else {

                $user_data = array(
										'first_name' => $this->input->post('first_name'),
										'bday'			 => date('Y-m-d', strtotime($this->input->post('bday'))),
										'gender'		 => $this->input->post('gender'),
                    'email' 	   => $this->input->post('email'),
										'job_title'  => $this->input->post('job_title'),
										'industry'	 => $this->input->post('industry'),
                    'last_name'  => '',
										
                );
                
                $user = $this->users_model->update($this->user->id, $user_data);
                
                if ($user) {
                    // Update the examinee status
                    $this->examinees_model->update(array(
                        'examinee_user_id' => $this->user->id
                    ), array(
                        'examinee_status' => 'Taken'
                    ));
                    
                    $response['success']  = TRUE;
                    $response['message']  = 'Successfully added your details';
                    $response['redirect'] = site_url('assessments/tests');
                } else {
                    $response['success'] = FALSe;
                    $response['message'] = 'Try again later';
                }
            }
            
            echo json_encode($response);
            exit;
        }
        
        $this->template->set_template(config_item('website_theme') . '_index');
        
        $this->template->add_js(module_js('assessments', 'assessments_register'), 'embed');
        $this->template->write_view('content', 'assessments_register', $data);
        $this->template->render();
    }
}

/* End of file Companies.php */
/* Location: ./application/modules/companies/controllers/Companies.php */
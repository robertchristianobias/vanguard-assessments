<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span>
		<span class="sr-only"><?php echo lang('button_close')?></span>
	</button>
	<h4 class="modal-title" id="myModalLabel"><?php echo $page_heading?></h4>
</div>

<div class="modal-body">

	<div class="form-horizontal">


		<div class="nav-tabs-custom bottom-margin">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#meta_tab" data-toggle="tab"><span class="fa fa-code"></span> <?php echo lang('title_meta_tags'); ?></a></li>
				<li><a href="#twitter_tab" data-toggle="tab"><span class="fa fa-twitter"></span> <?php echo lang('title_twitter_tags'); ?></a></li>
				<li><a href="#facebook_tab" data-toggle="tab"><span class="fa fa-facebook"></span> <?php echo lang('title_facebook_tags'); ?></a></li>
				<li class="hide"><a href="#image_tab" data-toggle="tab"><span class="fa fa-image"></span></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="meta_tab">

					<div class="form-group">
						<label class="col-sm-2 control-label" for="metatag_title"><?php echo lang('metatag_title')?>:</label>
						<div class="col-sm-10">
							<?php echo form_input(array('id'=>'metatag_title', 'name'=>'metatag_title', 'value'=>set_value('metatag_title', isset($record->metatag_title) ? $record->metatag_title : ''), 'class'=>'form-control'));?>
							<div id="error-metatag_title"></div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="metatag_keywords"><?php echo lang('metatag_keywords')?>:</label>
						<div class="col-sm-10">
							<?php echo form_textarea(array('id'=>'metatag_keywords', 'name'=>'metatag_keywords', 'rows'=>'2', 'value'=>set_value('metatag_keywords', isset($record->metatag_keywords) ? $record->metatag_keywords : ''), 'class'=>'form-control')); ?>
							<div id="error-metatag_keywords"></div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="metatag_description"><?php echo lang('metatag_description')?>:</label>
						<div class="col-sm-10">
							<?php echo form_textarea(array('id'=>'metatag_description', 'name'=>'metatag_description', 'rows'=>'4', 'value'=>set_value('metatag_description', isset($record->metatag_description) ? $record->metatag_description : ''), 'class'=>'form-control')); ?>
							<div id="error-metatag_description"></div>
						</div>
					</div>
				</div>

				<div class="tab-pane" id="twitter_tab">

					<div class="form-group">
						<label class="col-sm-2 control-label" for="metatag_twitter_title"><?php echo lang('metatag_twitter_title')?>:</label>
						<div class="col-sm-10">
							<?php echo form_input(array('id'=>'metatag_twitter_title', 'name'=>'metatag_twitter_title', 'value'=>set_value('metatag_twitter_title', isset($record->metatag_twitter_title) ? $record->metatag_twitter_title : ''), 'class'=>'form-control'));?>
							<div id="error-metatag_twitter_title"></div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="metatag_twitter_description"><?php echo lang('metatag_twitter_description')?>:</label>
						<div class="col-sm-10">
							<?php echo form_textarea(array('id'=>'metatag_twitter_description', 'name'=>'metatag_twitter_description', 'rows'=>'5', 'value'=>set_value('metatag_twitter_description', isset($record->metatag_twitter_description) ? $record->metatag_twitter_description : ''), 'class'=>'form-control')); ?>
							<div id="error-metatag_twitter_description"></div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="metatag_twitter_image"><?php echo lang('metatag_twitter_image')?>:</label>
						<div class="col-sm-10">
							<a href="javascript:;" id="metatag_twitter_image_link" class="metatag_image" data-target="metatag_twitter_image"><img id="metatag_twitter_image_thumb" class="metatag_image_thumb" src="<?php echo (isset($record->metatag_twitter_image) && $record->metatag_twitter_image) ? site_url($record->metatag_twitter_image) : site_url('assets/images/transparent.png'); ?>" height="100" />
							<?php echo form_input(array('id'=>'metatag_twitter_image', 'name'=>'metatag_twitter_image', 'value'=>set_value('metatag_twitter_image', isset($record->metatag_twitter_image) ? $record->metatag_twitter_image : ''), 'class'=>'form-control metatag_image_path hide'));?></a>
							<div id="error-metatag_twitter_image"></div>
						</div>
					</div>
				</div>

				<div class="tab-pane" id="facebook_tab">

					<div class="form-group">
						<label class="col-sm-2 control-label" for="metatag_og_title"><?php echo lang('metatag_og_title')?>:</label>
						<div class="col-sm-10">
							<?php echo form_input(array('id'=>'metatag_og_title', 'name'=>'metatag_og_title', 'value'=>set_value('metatag_og_title', isset($record->metatag_og_title) ? $record->metatag_og_title : ''), 'class'=>'form-control'));?>
							<div id="error-metatag_og_title"></div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="metatag_og_description"><?php echo lang('metatag_og_description')?>:</label>
						<div class="col-sm-10">
							<?php echo form_textarea(array('id'=>'metatag_og_description', 'name'=>'metatag_og_description', 'rows'=>'5', 'value'=>set_value('metatag_og_description', isset($record->metatag_og_description) ? $record->metatag_og_description : ''), 'class'=>'form-control')); ?>
							<div id="error-metatag_og_description"></div>
						</div>
					</div>

					<div class="form-group">
						<label class="col-sm-2 control-label" for="metatag_og_image"><?php echo lang('metatag_og_image')?>:</label>
						<div class="col-sm-10">
							<a href="javascript:;" id="metatag_og_image_link" class="metatag_image" data-target="metatag_og_image"><img id="metatag_og_image_thumb" class="metatag_image_thumb" src="<?php echo (isset($record->metatag_og_image) && $record->metatag_og_image) ? site_url($record->metatag_og_image) : site_url('assets/images/transparent.png'); ?>" height="100" />
							<?php echo form_input(array('id'=>'metatag_og_image', 'name'=>'metatag_og_image', 'value'=>set_value('metatag_og_image', isset($record->metatag_og_image) ? $record->metatag_og_image : ''), 'class'=>'form-control metatag_image_path hide'));?></a>
							<div id="error-metatag_og_image"></div>
						</div>
					</div>

				</div>


				<div class="tab-pane" id="image_tab">
					<div class="nav-tabs-custom bottom-margin">
						<ul class="nav nav-tabs">
							
							<li class="active"><a href="#tab_1" data-toggle="tab">Add Existing Image</a></li>
							<li><a href="#tab_2" data-toggle="tab">Upload Image</a></li>
						</ul>
						<div class="tab-content" data-target="">
							

							<div class="tab-pane active" id="tab_1">
								<table class="table table-striped table-bordered table-hover dt-responsive" id="dt-images">
									<thead>
										<tr>
											<th class="all"><?php echo lang('index_id')?></th>
											<th class="min-desktop"><?php echo lang('index_width'); ?></th>
											<th class="min-desktop"><?php echo lang('index_height'); ?></th>
											<th class="min-desktop"><?php echo lang('index_name'); ?></th>
											<th class="min-desktop"><?php echo lang('index_file'); ?></th>
											<th class="min-desktop"><?php echo lang('index_thumb'); ?></th>

											<th class="none"><?php echo lang('index_created_on')?></th>
											<th class="none"><?php echo lang('index_created_by')?></th>
											<th class="none"><?php echo lang('index_modified_on')?></th>
											<th class="none"><?php echo lang('index_modified_by')?></th>
											<th class="min-tablet"><?php echo lang('index_action')?></th>
										</tr>
									</thead>
								</table>
								<div id="thumbnails" class="row text-center"></div>
							</div>

							<div class="tab-pane" id="tab_2">
								<div class="form-horizontal top-margin3">

									<div class="col-sm-4">

										<div class="form-group">

											<?php echo form_open(site_url('files/images/upload'), array('class'=>'dropzone', 'id'=>'dropzone'));?>
											<div class="fallback">
												<input name="file" type="file" class="hide" />
											</div>
											<?php echo form_close();?>

										</div>

									</div>

									<div class="col-sm-6 text-center">
										<div id="image_sizes"></div>
									</div>

								</div>

								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>


			</div>

		</div>

	</div>

</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
		<i class="fa fa-times"></i> <?php echo lang('button_close')?>
	</button>
	<?php if ($action == 'add'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_add')?>
		</button>
	<?php elseif ($action == 'edit'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_update')?>
		</button>
	<?php else: ?>
		<script>$(".modal-body :input").attr("disabled", true);</script>
	<?php endif; ?>
</div>
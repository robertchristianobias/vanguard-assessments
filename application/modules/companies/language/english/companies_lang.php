<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Companies Language File (English)
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */

// Breadcrumbs
$lang['crumb_module']				= 'Companies';

// Labels
$lang['company_name']			= 'Name';
$lang['company_prefix']			= 'Prefix';
$lang['company_logo']			= 'Logo';

// Buttons
$lang['button_add']					= 'Add Company';
$lang['button_update']				= 'Update Company';
$lang['button_delete']				= 'Delete Company';
$lang['button_edit_this']			= 'Edit This';


// Index Function
$lang['index_heading']				= 'Companies';
$lang['index_subhead']				= 'List of all companies';
$lang['index_id']					= 'ID';
$lang['index_name']			= 'Name';
$lang['index_prefix']			= 'Prefix';
$lang['index_logo']			= 'Logo';

$lang['index_created_on']			= 'Created On';
$lang['index_created_by']			= 'Created By';
$lang['index_modified_on']			= 'Modified On';
$lang['index_modified_by']			= 'Modified By';
$lang['index_status']				= 'Status';
$lang['index_action']				= 'Action';

// View Function
$lang['view_heading']				= 'View Company';

// Add Function
$lang['add_heading']				= 'Add Company';
$lang['add_success']				= 'Company has been successfully added';

// Edit Function
$lang['edit_heading']				= 'Edit Company';
$lang['edit_success']				= 'Company has been successfully updated';

// Delete Function
$lang['delete_heading']				= 'Delete Company';
$lang['delete_confirm']				= 'Are you sure you want to delete this company?';
$lang['delete_success']				= 'Company has been successfully deleted';
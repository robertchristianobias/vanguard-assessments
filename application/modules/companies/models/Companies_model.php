<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Companies_model Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Companies_model extends BF_Model {

	protected $table_name			= 'companies';
	protected $key					= 'company_id';
	protected $date_format			= 'datetime';
	protected $log_user				= TRUE;

	protected $set_created			= TRUE;
	protected $created_field		= 'company_created_on';
	protected $created_by_field		= 'company_created_by';

	protected $set_modified			= TRUE;
	protected $modified_field		= 'company_modified_on';
	protected $modified_by_field	= 'company_modified_by';

	protected $soft_deletes			= TRUE;
	protected $deleted_field		= 'company_deleted';
	protected $deleted_by_field		= 'company_deleted_by';

	// --------------------------------------------------------------------

	/**
	 * get_datatables
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function get_datatables()
	{
		$fields = array(
			'company_id', 
			'company_name',
			'company_prefix',

			'company_created_on', 
			'concat(creator.first_name, " ", creator.last_name)', 
			'company_modified_on', 
			'concat(modifier.first_name, " ", modifier.last_name)'
		);

		return $this->join('users as creator', 'creator.id = company_created_by', 'LEFT')
					->join('users as modifier', 'modifier.id = company_modified_by', 'LEFT')
					->datatables($fields);
	}

	public function get_companies()
	{
		if (! $companies = $this->cache->get('companies'))
		{
			$companies = $this->where('company_deleted', 0)
							  ->order_by('company_name')
							  ->find_all();

			$this->cache->save('companies', $companies, 300); // TTL in seconds	
		}

		return $companies;
	}

	public function get_company($id)
	{
		if (! $company = $this->cache->get('company'))
		{
			$company = $this->where('company_deleted', 0)
							->find($id);

			$this->cache->save('company', $company, 300); // TTL in seconds	
		}

		return $company;
	}

	public function get_companies_dropdown($key, $val, $blank = FALSE)
	{
		// get the companies_dropdown
		if (! $companies_dropdown = $this->cache->get('companies_dropdown'))
		{
			$companies_dropdown = $this->where('company_deleted', 0)
									   ->order_by('company_name')
									   ->format_dropdown($key, $val, $blank);
			
			$this->cache->save('companies_dropdown', $companies_dropdown, 300); // TTL in seconds
		}

		return $companies_dropdown;
	}
}
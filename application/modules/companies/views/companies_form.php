<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span>
		<span class="sr-only"><?php echo lang('button_close')?></span>
	</button>
	<h4 class="modal-title" id="myModalLabel"><?php echo $page_heading?></h4>
</div>

<div class="modal-body">

	<div class="form-horizontal">

		<div class="form-group">
			<label class="col-sm-3 control-label" for="company_name"><?php echo lang('company_name')?>:</label>
			<div class="col-sm-8">
				<?php echo form_input(array('id'=>'company_name', 'name'=>'company_name', 'value'=>set_value('company_name', isset($record->company_name) ? $record->company_name : ''), 'class'=>'form-control'));?>
				<div id="error-company_name"></div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="company_prefix"><?php echo lang('company_prefix')?>:</label>
			<div class="col-sm-8">
				<?php echo form_input(array('id'=>'company_prefix', 'name'=>'company_prefix', 'value'=>set_value('company_prefix', isset($record->company_prefix) ? $record->company_prefix : ''), 'class'=>'form-control'));?>
				<div id="error-company_prefix"></div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="company_logo"><?php echo lang('company_logo')?>:</label>
			<div class="col-sm-4">
				<input type="hidden" name="company_logo" value="" id="company_logo" />
				<div class="dropzone" id="company_logo_dropzone"></div>
				<div id="error-company_logo"></div>
			</div>
			<?php if(isset($record->company_logo)) { ?>
				<div class="col-sm-4">
					<img src="<?php echo display_image($record->company_logo, 'thumb'); ?>" class="img-responsive" />	
				</div>
			<?php } ?>
		</div>



	</div>

</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
		<i class="fa fa-times"></i> <?php echo lang('button_close')?>
	</button>
	<?php if ($action == 'add'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_add')?>
		</button>
	<?php elseif ($action == 'edit'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_update')?>
		</button>
	<?php else: ?>
		<script>$(".modal-body :input").attr("disabled", true);</script>
	<?php endif; ?>
</div>
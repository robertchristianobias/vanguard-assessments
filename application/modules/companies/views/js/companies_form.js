/**
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */

$(function() {

	$('#company_name').blur(function() {
		$('#company_prefix').val(convertToSlug($('#company_name').val()));
	});

	handle_single_image_dropzone('company_logo', app_url + 'files/images/upload');

	// handles the submit action
	$('#submit').click(function(e){
		// change the button to loading state
		var btn = $(this);
		btn.button('loading');

		// prevents a submit button from submitting a form
		e.preventDefault();

		// submits the data to the backend
		$.post(ajax_url, {
			company_name: 	$('#company_name').val(),
			company_prefix: $('#company_prefix').val(),
			company_logo: 	$('#company_logo').val(),

		},
		function(data, status){
			// handles the returned data
			var o = jQuery.parseJSON(data);
			if (o.success === false) {
				// reset the button
				btn.button('reset');
				
				// shows the error message
				alertify.error(o.message);

				// displays individual error messages
				if (o.errors) {
					for (var form_name in o.errors) {
						$('#error-' + form_name).html(o.errors[form_name]);
					}
				}
			} else {
				// refreshes the datatables
				$('#datatables').dataTable().fnDraw();

				// closes the modal
				$('#modal').modal('hide'); 

				// restores the modal content to loading state
				restore_modal(); 

				// shows the success message
				alertify.success(o.message); 
			}
		});
	});

	// disables the enter key
	$('form input').keydown(function(event){
		if(event.keyCode == 13) {
			event.preventDefault();
			return false;
		}
	});

	function handle_single_image_dropzone(element, endpoint) {
		$('#'+element+'_dropzone').dropzone({
			url: endpoint,
			clickable: true,
			addRemoveLinks: true,
			maxFilesize: 2,
			maxFiles: 1,
			init: function() {
				this.on('success', function(file, data) {
					var response = $.parseJSON(data);

					if(response.status == 'success') {
						$('#'+element).val(response.image);
					} else {
						$('#error-'+element).html('<span class="text-danger">'+response.error+'</span>');
					}
				});
				
				this.on("removedfile", function(file) {});
			}
		});
	}

	function convertToSlug(Text) {
		return Text
			.toLowerCase()
			.replace(/ /g,'_')
			.replace(/[^\w-]+/g,'');
	}
});
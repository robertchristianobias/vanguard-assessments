<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Migration_Create_companies extends CI_Migration {

	private $_table = 'companies';

	private $_permissions = array(
		array('Companies Link', 'companies.companies.link'),
		array('Companies List', 'companies.companies.list'),
		array('View Company', 'companies.companies.view'),
		array('Add Company', 'companies.companies.add'),
		array('Edit Company', 'companies.companies.edit'),
		array('Delete Company', 'companies.companies.delete'),
	);

	private $_menus = array(
		array(
			'menu_parent'		=> 'companies',
			'menu_text' 		=> 'Companies', 
			'menu_link' 		=> 'companies/companies', 
			'menu_perm' 		=> 'companies.companies.link', 
			'menu_icon' 		=> 'fa fa-university', 
			'menu_order' 		=> 2, 
			'menu_active' 		=> 1
		),
	);

	function __construct()
	{
		parent::__construct();

		$this->load->model('migrations_model');
	}
	
	public function up()
	{
		$fields = array(
			'company_id' 			=> array('type' => 'INT', 'unsigned' => TRUE, 'auto_increment' => TRUE, 'null' => FALSE),
			'company_name'		=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			'company_prefix'		=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),
			'company_logo'		=> array('type' => 'VARCHAR', 'constraint' => 255, 'null' => FALSE),

			'company_created_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'company_created_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'company_modified_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
			'company_modified_on' 	=> array('type' => 'DATETIME', 'null' => TRUE),
			'company_deleted' 		=> array('type' => 'TINYINT', 'constraint' => 1, 'unsigned' => TRUE, 'null' => FALSE),
			'company_deleted_by' 	=> array('type' => 'MEDIUMINT', 'unsigned' => TRUE, 'null' => TRUE),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('company_id', TRUE);
		$this->dbforge->add_key('company_name');
		$this->dbforge->add_key('company_prefix');

		$this->dbforge->add_key('company_deleted');
		$this->dbforge->create_table($this->_table, TRUE);

		// add the module permissions
		$this->migrations_model->add_permissions($this->_permissions);

		// add the module menu
		$this->migrations_model->add_menus($this->_menus);
	}

	public function down()
	{
		// drop the table
		$this->dbforge->drop_table($this->_table, TRUE);

		// delete the permissions
		$this->migrations_model->delete_permissions($this->_permissions);

		// delete the menu
		$this->migrations_model->delete_menus($this->_menus);
	}
}
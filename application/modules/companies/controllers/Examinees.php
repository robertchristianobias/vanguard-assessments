<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Examinees Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2017, Google.
 * @link		http://www.google.com
 */
class Examinees extends MX_Controller {
	
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		parent::__construct();

		$this->load->model('examinees_model');
		$this->load->language('examinees');
	}
	
	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function index()
	{
		$this->acl->restrict('examinees.examinees.list');
		
		// page title
		$data['page_heading'] = lang('index_heading');
		$data['page_subhead'] = lang('index_subhead');
		
		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('examinees'));
		
		// session breadcrumb
		$this->session->set_userdata('redirect', current_url());
		
		// add plugins
		$this->template->add_css('assets/plugins/DataTables/datatables.min.css');
		$this->template->add_js('assets/plugins/DataTables/datatables.min.js');
		
		// render the page
		$this->template->add_css(module_css('examinees', 'examinees_index'), 'embed');
		$this->template->add_js(module_js('examinees', 'examinees_index'), 'embed');
		$this->template->write_view('content', 'examinees_index', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * datatables
	 *
	 * @access	public
	 * @param	mixed datatables parameters (datatables.net)
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function datatables()
	{
		$this->acl->restrict('examinees.examinees.list');

		echo $this->examinees_model->get_datatables();
	}

	// --------------------------------------------------------------------

	/**
	 * form
	 *
	 * @access	public
	 * @param	$action string
	 * @param   $id integer
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	function form($action = 'add', $id = FALSE)
	{
		$this->acl->restrict('examinees.examinees.' . $action, 'modal');

		$data['page_heading'] = lang($action . '_heading');
		$data['action'] = $action;

		if ($this->input->post())
		{
			if ($this->_save($action, $id))
			{
				echo json_encode(array('success' => true, 'message' => lang($action . '_success'))); exit;
			}
			else
			{	
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(					
					'company_name'		=> form_error('company_name'),
					'company_prefix'		=> form_error('company_prefix'),
					'company_logo'		=> form_error('company_logo'),
				);
				echo json_encode($response);
				exit;
			}
		}

		if ($action != 'add') $data['record'] = $this->examinees_model->find($id);

		// render the page
		$this->template->set_template('modal');
		$this->template->add_js('assets/plugins/dropzone/dropzone.js');
		$this->template->add_css('assets/plugins/dropzone/dropzone.css');
		
		$this->template->add_css(module_css('examinees', 'examinees_form'), 'embed');
		$this->template->add_js(module_js('examinees', 'examinees_form'), 'embed');
		$this->template->write_view('content', 'examinees_form', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * delete
	 *
	 * @access	public
	 * @param	integer $id
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	function delete($id)
	{
		$this->acl->restrict('examinees.examinees.delete', 'modal');

		$data['page_heading'] = lang('delete_heading');
		$data['page_confirm'] = lang('delete_confirm');
		$data['page_button'] = lang('button_delete');
		$data['datatables_id'] = '#datatables';

		if ($this->input->post())
		{
			$this->examinees_model->delete($id);

			echo json_encode(array('success' => true, 'message' => lang('delete_success'))); exit;
		}

		$this->load->view('../../../views/confirm', $data);
	}


	// --------------------------------------------------------------------

	/**
	 * _save
	 *
	 * @access	private
	 * @param	string $action
	 * @param 	integer $id
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	private function _save($action = 'add', $id = 0)
	{
		// validate inputs
		$this->form_validation->set_rules('company_name', lang('company_name'), 'required');
		$this->form_validation->set_rules('company_prefix', lang('company_prefix'), 'required');
		$this->form_validation->set_rules('company_logo', lang('company_logo'), 'required');

		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
		
		if ($this->form_validation->run($this) == FALSE)
		{
			return FALSE;
		}

		$data = array(
			'company_name'		=> $this->input->post('company_name'),
			'company_prefix'		=> $this->input->post('company_prefix'),
			'company_logo'		=> $this->input->post('company_logo'),
		);
		

		if ($action == 'add')
		{
			$insert_id = $this->examinees_model->insert($data);
			$return = (is_numeric($insert_id)) ? $insert_id : FALSE;
		}
		else if ($action == 'edit')
		{
			$return = $this->examinees_model->update($id, $data);
		}

		return $return;

	}
}

/* End of file examinees.php */
/* Location: ./application/modules/examinees/controllers/examinees.php */
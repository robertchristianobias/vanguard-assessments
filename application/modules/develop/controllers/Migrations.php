<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migrations Class
 *
 * @package		rcmediaph
 * @version		1.1
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2014-2016, Robert Christian Obias
 * @link		rchristian_obias@yahoo.com
 */
class Migrations extends MX_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('migration');
		$this->load->language('migrations');
	}

	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function index()
	{
		// $this->acl->restrict('develop.migrations.list');
	
		// $data['page_heading'] = lang('index_heading');
		// $data['page_subhead'] = lang('index_subhead');

		// // breadcrumbs
		// $this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		// $this->breadcrumbs->push(lang('crumb_module'), site_url('develop/migrations'));
		// $this->breadcrumbs->push(lang('index_heading'), site_url('develop/migrations'));
		
		// // session breadcrumb
		// $this->session->set_userdata('redirect', current_url());
		
		// // get all migration files
		// $data['migrations'] = $this->migration->display_all_migrations();
		// ksort($data['migrations']);

		// $data['core_migrations'] = array(
		// 	'001_rollback_develop.php',
		// 	'001_rollback_reports.php',
		// 	'001_rollback_settings.php',
		// 	'002_create_menus.php',
		// 	'001_rollback_users.php',
		// 	'002_create_users.php',
		// 	'003_create_grants.php',
		// 	'004_create_permissions.php',
		// );
		
		// // get the current versions
		// $migrations = $this->db->get('migrations')->result();
		// $versions = array();

		// foreach ($migrations as $migration)
		// {
		// 	$versions[$migration->module] = $migration->version;
		// }
		// $data['versions'] = $versions;

		// // datatables
		// $this->template->add_css('assets/plugins/DataTables/datatables.min.css');
		// $this->template->add_js('assets/plugins/DataTables/datatables.min.js');

		// // $this->template->add_css(module_css('develop', 'menus_index'), 'embed');
		// $this->template->add_js(module_js('develop', 'migrations_index'), 'embed');
		// $this->template->write_view('content', 'migrations_index', $data);
		// $this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * rollback
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function rollback($module = FALSE, $version = 1)
	{
		if (! $module) show_404();

		$this->acl->restrict('develop.migrations.rollback', 'modal');

		$data['page_heading'] = lang('rollback_heading');
		$data['page_confirm'] = lang('rollback_confirm');
		$data['page_button'] = lang('button_rollback');

		if ($this->input->post())
		{
			if ($this->migration->init_module($module)) 
			{
				if (! $this->migration->version($version)) 
				{
					echo json_encode(array('success' => FALSE, 'message' => $this->migration->error_string())); exit;
				}
				else 
				{
					$this->cache->delete(site_url() . 'app_menu');
					
					$this->session->set_flashdata('flash_message', lang('rollback_success'));
					echo json_encode(array('success' => TRUE)); exit;
				}
			}
			else
			{
				echo json_encode(array('success' => FALSE, 'message' => 'There was an error with the rollback.')); exit;
			}
		}

		$this->load->view('../../../views/confirm', $data);	
	}

	// --------------------------------------------------------------------

	/**
	 * migrate
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function migrate($module = FALSE, $action = 'install')
	{
		if (! $module) show_404();

		$this->acl->restrict('develop.migrations.migrate', 'modal');

		$data['page_heading'] = ($action == 'install') ? lang('install_heading') : lang('migrate_heading');
		$data['page_confirm'] = ($action == 'install') ? lang('install_confirm') : lang('migrate_confirm');
		$data['page_button'] = ($action == 'install') ? lang('button_install') : lang('button_upgrade');

		if ($this->input->post())
		{
			if ($this->migration->init_module($module)) 
			{
				if (! $this->migration->current()) 
				{
					echo json_encode(array('success' => FALSE, 'message' => $this->migration->error_string())); exit;
				}
				else 
				{
					$this->cache->delete(site_url() . 'app_menu');

					$this->session->set_flashdata('flash_message', ($action == 'install') ? lang('install_success') :lang('migrate_success'));
					echo json_encode(array('success' => TRUE)); exit;
				}
			}
			else
			{
				echo json_encode(array('success' => FALSE, 'message' => 'There was an error.')); exit;
			}
		}

		$this->load->view('../../../views/confirm', $data);	
	}
}

/* End of file Migrations.php */
/* Location: ./application/modules/develop/controllers/Migrations.php */
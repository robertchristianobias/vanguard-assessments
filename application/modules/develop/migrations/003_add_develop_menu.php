<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Migration Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2015, Google.
 * @link		http://www.google.com
 */
class Migration_Add_develop_menu extends CI_Migration 
{

	private $_permissions = array(
		array('Develop Link', 'develop.develop.link'),

		array('Modules Link', 'develop.modules.link'),
		array('List Modules', 'develop.modules.list'),
		array('Add Modules', 'develop.modules.add'),
		array('Delete Modules', 'develop.modules.delete'),
		array('Migrate Databases', 'develop.migrations.migrate'),
		array('Rollback Databases', 'develop.migrations.rollback'),
	);

	private $_menus = array(		
		array(
			'menu_parent'		=> 'develop',
			'menu_text' 		=> 'Modules', 
			'menu_link' 		=> 'develop/modules', 
			'menu_perm' 		=> 'develop.modules.link', 
			'menu_icon' 		=> 'fa fa-sitemap', 
			'menu_order' 		=> 1, 
			'menu_active' 		=> 1
		),
	);

	function __construct()
	{
		parent::__construct();

		$this->load->model('migrations_model');
	}
	
	public function up()
	{
		// add the module permissions
		$this->migrations_model->add_permissions($this->_permissions);

		// add the module menu
		$this->migrations_model->add_menus($this->_menus);
	}

	public function down()
	{
		// delete the permissions
		$this->migrations_model->delete_permissions($this->_permissions);

		// delete the menu
		$this->migrations_model->delete_menus($this->_menus);
	}
}
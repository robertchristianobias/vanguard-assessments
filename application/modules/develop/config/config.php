<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['tpl_migration'] 		= APPPATH . 'modules/develop/views/template/config/migration.php';
$config['tpl_controller'] 		= APPPATH . 'modules/develop/views/template/controllers/Template.php';
$config['tpl_language'] 		= APPPATH . 'modules/develop/views/template/language/english/template_lang.php';
$config['tpl_migration1'] 		= APPPATH . 'modules/develop/views/template/migrations/001_rollback_template.php';
$config['tpl_migration2'] 		= APPPATH . 'modules/develop/views/template/migrations/002_create_template.php';
$config['tpl_model'] 			= APPPATH . 'modules/develop/views/template/models/Template_model.php';
$config['tpl_html_index'] 		= APPPATH . 'modules/develop/views/template/views/template_index.php';
$config['tpl_html_form'] 		= APPPATH . 'modules/develop/views/template/views/template_form.php';
$config['tpl_html_input'] 		= APPPATH . 'modules/develop/views/template/views/template_input.php';
$config['tpl_html_select'] 		= APPPATH . 'modules/develop/views/template/views/template_select.php';
$config['tpl_html_textarea'] 	= APPPATH . 'modules/develop/views/template/views/template_textarea.php';
$config['tpl_html_checkbox'] 	= APPPATH . 'modules/develop/views/template/views/template_checkbox.php';
$config['tpl_html_radio'] 		= APPPATH . 'modules/develop/views/template/views/template_radio.php';
$config['tpl_css_index'] 		= APPPATH . 'modules/develop/views/template/views/css/template_index.css';
$config['tpl_css_form'] 		= APPPATH . 'modules/develop/views/template/views/css/template_form.css';
$config['tpl_js_index'] 		= APPPATH . 'modules/develop/views/template/views/js/template_index.js';
$config['tpl_js_form'] 			= APPPATH . 'modules/develop/views/template/views/js/template_form.js';
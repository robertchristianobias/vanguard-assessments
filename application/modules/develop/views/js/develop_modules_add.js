/**
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2014-2015, Robert Christian Obias
 * @link		rchristian_obias@yahoo.com
 */
 
$(function() {
	// handles the submit action
	$('#submit').click(function(e){
		// change the button to loading state
		var btn = $(this);
		btn.button('loading');
	});

	$('#module_name_singular').keyup(function() {
		var table_name = $(this).val().replace(/\s+/g, '_').toLowerCase() + '_';
		$('.table-name').text(table_name);
	});

	$('#clone').click(function(){
		$('#row_template').clone(true).insertAfter($('.table_column').last());
	});

	// $("body").addClass('sidebar-collapse');
});

$('.column_type').change(function() {
	var target = $(this).parent().siblings().children('.column_length');
	switch($(this).val()) {
		case 'CHAR': target.val(255); break;
		case 'VARCHAR': target.val(255); break;
		case 'BIGINT': target.val(20); break;
		case 'INT': target.val(10); break;
		case 'MEDIUMINT': target.val(8); break;
		case 'SMALLINT': target.val(5); break;
		case 'TINYINT': target.val(3); break;
		case 'DECIMAL': target.val('10,2'); break;
		case 'SET': target.val('("one","two")'); break;
		default: target.val('--'); break;
	}
});
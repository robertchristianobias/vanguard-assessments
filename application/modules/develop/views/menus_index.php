<table class="table table-striped table-bordered table-hover dt-responsive" id="datatables">
	<thead>
		<tr>
			<th class="all"><?php echo lang('index_id')?></th>
			<th class="all"><?php echo lang('index_text')?></th>
			<th class="min-desktop"><?php echo lang('index_link')?></th>
			<th class="min-desktop"><?php echo lang('index_permission')?></th>
			<th class="min-desktop"><?php echo lang('index_icon')?></th>
			<th class="min-desktop"><?php echo lang('index_order')?></th>
			<th class="min-tablet"><?php echo lang('index_active')?></th>
			<th class="none"><?php echo lang('index_created_on')?></th>
			<th class="none"><?php echo lang('index_created_by')?></th>
			<th class="none"><?php echo lang('index_modified_on')?></th>
			<th class="none"><?php echo lang('index_modified_by')?></th>
			<th class="min-tablet"><?php echo lang('index_action')?></th>
		</tr>	
	</thead>
</table>

<div class="modal-header">
	<button type="button" class="close" data-dismiss="modal">
		<span aria-hidden="true">&times;</span>
		<span class="sr-only"><?php echo lang('button_close')?></span>
	</button>
	<h4 class="modal-title" id="myModalLabel"><?php echo $page_heading?></h4>
</div>

<div class="modal-body">

	<div class="form-horizontal">

		<div class="form-group">
			<label class="col-sm-3 control-label" for="username"><?php echo lang('username')?>:</label>
			<div class="col-sm-8">
				<?php echo form_input(array('id'=>'username', 'name'=>'username', 'value'=>set_value('username', isset($record->username) ? $record->username : ''), 'class'=>'form-control'));?>
				<div id="error-username"></div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="password"><?php echo lang('password')?>:</label>
			<div class="col-sm-8">
				<?php echo form_input(array('id'=>'password', 'name'=>'password', 'value'=>set_value('password', isset($record->password) ? $record->password : $new_password), 'class'=>'form-control'));?>
				<div id="error-password"></div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="api_key"><?php echo lang('api_key')?>:</label>
			<div class="col-sm-8">
				<?php echo form_input(array('id'=>'api_key', 'name'=>'api_key', 'value'=>set_value('api_key', isset($record->api_key) ? $record->api_key : $new_api_key), 'class'=>'form-control'));?>
				<div id="error-api_key"></div>
			</div>
		</div>

		<div class="form-group">
			<label class="col-sm-3 control-label" for="status"><?php echo lang('status')?>:</label>
			<div class="col-sm-8">
				<?php $options = create_dropdown('array', 'Active,Disabled'); ?>
				<?php echo form_dropdown('status', $options, set_value('status', (isset($record->status)) ? $record->status : ''), 'id="status" class="form-control"'); ?>
				<div id="error-status"></div>
			</div>
		</div>

	</div>

</div>

<div class="modal-footer">
	<button type="button" class="btn btn-default" data-dismiss="modal">
		<i class="fa fa-times"></i> <?php echo lang('button_close')?>
	</button>
	<?php if ($action == 'add'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_add')?>
		</button>
	<?php elseif ($action == 'edit'): ?>
		<button id="submit" class="btn btn-success" type="submit" data-loading-text="<?php echo lang('processing')?>">
			<i class="fa fa-save"></i> <?php echo lang('button_update')?>
		</button>
	<?php else: ?>
		<script>$(".modal-body :input").attr("disabled", true);</script>
	<?php endif; ?>
</div>
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Api_model Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2014-2015, Robert Christian Obias
 * @link		rchristian_obias@yahoo.com
 */
class Api_model extends CI_Model 
{

	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	function __construct()
	{
		parent::__construct();

		$this->load->driver('cache', $this->config->item('cache_drivers'));
	}

	// --------------------------------------------------------------------

	/**
	 * get_configs
	 * This is a sample function that gets data from the database (or cache)
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function get_configs()
	{
		$this->load->model('settings/configs_model');

		// get from cache
		if (!$configs = $this->cache->get(site_url() . 'api_configs'))
		{
			// get from db
			$configs = $this->configs_model
				->select('config_id as id, config_name as name, config_value as value')
				->where('config_deleted', 0)
				->find_all();

			// write to cache
			$this->cache->save(site_url() . 'api_configs', $configs, 300); // cache for 5 mins
		}

		return $configs;
	}

	// --------------------------------------------------------------------

	/**
	 * success
	 *
	 * @access	public
	 * @param	object $rest
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function success($rest, $data = '')
	{
		$rest->response(array(
            'status'  => 'success',
            'data'    => $data
        ), 200);
	}

	// --------------------------------------------------------------------

	/**
	 * validation_error
	 *
	 * @access	public
	 * @param	object $rest
	 * @param	string $error
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function validation_error($rest, $error)
	{
		$rest->response(array('error' => 'validation_error', 'description' => $error), 400);
	}

	// --------------------------------------------------------------------

	/**
	 * error
	 *
	 * @access	public
	 * @param	object $rest
	 * @param	string $error_code
	 * @param	string $description
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function error($rest, $error_code, $description = FALSE)
	{
		// get the error info
		$error = $this->_get_error($error_code, $description);

		// get the error code
		$error_code = $error['error_code'];

		// remove the error code
		unset($error['error_code']);

		// return the error
		$rest->response($error, $error_code);
	}

	// --------------------------------------------------------------------

	/**
	 * _get_error
	 *
	 * @access	public
	 * @param	string $error
	 * @param 	sring $description
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	private function _get_error($error, $description = FALSE)
	{
		$errors = array(
			'no_token' 				=> array('code' => 404, 'description' => 'Token is missing'),
			'no_user' 				=> array('code' => 404, 'description' => 'User not found'),
			'no_record' 			=> array('code' => 404, 'description' => 'Record/s not found'),
			'registered' 			=> array('code' => 400, 'description' => 'User is already registered'),
			'ticket_invalid'		=> array('code' => 400, 'description' => 'Ticket is already used'),
			'param_missing' 		=> array('code' => 400, 'description' => 'One of the required parameters is missing'),
			'param_invalid' 		=> array('code' => 400, 'description' => 'One of the required parameters is invalid'),
			'add_failed' 			=> array('code' => 500, 'description' => 'Failed to add the record'),
			'update_failed' 		=> array('code' => 500, 'description' => 'Failed to update the record'),
			'format_invalid' 		=> array('code' => 406, 'description' => 'One of the inputs has invalid format'),
			'duplicate' 			=> array('code' => 400, 'description' => 'Duplicate entry'),
			'login_invalid' 		=> array('code' => 400, 'description' => 'The login information is incorrect'),
			'reset_code_invalid' 	=> array('code' => 400, 'description' => 'The reset code is incorrect'),
			'reset_code_expired' 	=> array('code' => 400, 'description' => 'The reset code has expired'),
            'session_code_expired'  => array('code' => 400, 'description' => 'Session code has expired'),
			'invalid_request' 		=> array('code' => 400, 'description' => 'Invalid request'),
			'api_request_failed' 	=> array('code' => 400, 'description' => 'API request failed'),
			'unknown' 				=> array('code' => 500, 'description' => 'Unknown error'),
		);

		$error_desc = ($description) ? $description : $errors[$error]['description'];

		$return = array(
			'status' => 'failed', 
			'error' => $error, 
			'error_description' => $error_desc,
			'error_code' => $errors[$error]['code']
		);

		return $return;
	}
    
    // --------------------------------------------------------------------
    
    /**
	 * session
	 * 
	 * Check if session exist and is valid
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robert.obias@google.com>
	 */
    public function session($session_code)
    {
        $event_session = $this->event_sessions_model->select('event_session_id, event_session_event_id, event_session_event_user_id')
                                                    ->find_by(array(
                                                        'event_session_code' => $session_code
                                                    ));
        // Session Code is correct
        if($event_session)
        {
            // Check if expired
            $session = $this->event_sessions_model->select('event_session_id, event_session_event_id, event_session_event_user_id')
                                                             ->where('event_session_expire >=', date('Y-m-d h:i:s'))
                                                             ->find_by(array(
                                                                'event_session_code' => $session_code
                                                             ));
            if($session)
            {
                $return = array(
                    'error_code' => '',
                    'data' => $session
                );
            }
            else
            {
                $return = array(
                    'error_code' => 'session_code_expired'
                );
            }
        }
        else
        {
            $return = array(
                'error_code' => 'api_request_failed'
            );
        }
        
        return $return;
    }
}
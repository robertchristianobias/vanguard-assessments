<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Event Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <robert.obias@google.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link		http://www.google.com
 */
require APPPATH.'modules/api/libraries/REST_Controller.php';

class Event extends REST_Controller
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	public function __construct()
	{
		// Construct our parent class
		parent::__construct();

        $this->load->add_package_path(APPPATH.'/third_party/IonAuth/');
        
        $this->load->model('events/events_model');
        $this->load->model('events/schedules_model');
        $this->load->model('events/event_users_model');
        $this->load->model('events/event_sessions_model');
        $this->load->model('events/ticket_types_model');
        $this->load->model('events/event_gates_model');
        $this->load->model('events/event_attendances_model');
        $this->load->model('events/transactions_model');
        $this->load->model('events/tickets_model');
        
		$this->load->model('api_model');
	}
    
    // --------------------------------------------------------------------

	/**
	 * event_login_post
	 * 
	 * This is a sample API that outputs data from the database.
	 *
	 * @access	public
	 * @param	event_code
     *          event_username
     *          event_password
	 * @author 	Robert Christian Obias <robert.obias@google.com>
	 */
	public function login_post()
	{

        $this->load->model('ion_auth_model');
      
        $event_code          = $this->input->post('event_code');
        $event_user_username = $this->input->post('username');
        $event_user_password = $this->input->post('password');
        
        // Verify Event Code
        $event = $this->events_model->select('event_id')
                                    ->find_by(array('event_code'=>$event_code));
        
        // Verify Username
        $event_user = $this->event_users_model->select('event_user_id, event_user_password')
                                              ->find_by(
                                                    array(
                                                        'event_user_event_id' => $event->event_id,
                                                        'event_user_username' => $event_user_username,
                                                        'event_user_status'   => 'Active'
                                                    ));
        
        $this->form_validation->set_rules('event_code', 'Event Code', 'required');
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_error_delimiters('', '');
		
        if($this->form_validation->run($this) == FALSE)
        {
            $this->api_model->validation_error($this, validation_errors());
        }
        else
        {
            // Code and username exist
            if($event AND $event_user)
            {
                // Check if correct password
                if ($this->ion_auth_model->bcrypt->verify($event_user_password, $event_user->event_user_password))
                {
                    // Check if user exists in session
                    // and not expired
                    $event_session = $this->event_sessions_model->select('event_session_id, event_session_event_user_id, event_session_code')
                                                                ->find_by(
                                                                    array(
                                                                        'event_session_event_user_id' => $event_user->event_user_id,
                                                                ));
                    
                    // Session exist, update session and session expiration
                    if($event_session)
                    {
                        $event_session_data = array(
                            'event_session_code'  => random_string('sha1'),
                            'event_session_expire' => date('Y-m-d h:i:s', strtotime('+30 minutes'))
                        );
                        
                        $success = $this->event_sessions_model->update(array('event_session_event_user_id' => $event_user->event_user_id), $event_session_data);
                        
                        if($success)
                        {
                            $data = array(
                                'session_code'  => $event_session_data['event_session_code']
                            );
                            
                            $return = $this->api_model->success($this, $data);
                        }
                        else
                        {
                            $return = $this->api_model->error($this, 'update_failed');
                        }
                    }
                    else
                    {
                        $event_session_data = array(
                            'event_session_event_id'        => $event->event_id,
                            'event_session_event_user_id'   => $event_user->event_user_id,
                            'event_session_code'            => random_string('sha1'),
                            'event_session_expire'          => date('Y-m-d h:i:s', strtotime('+30 minutes'))
                        );

                        $success = $this->event_sessions_model->insert($event_session_data);

                        if($success)
                        {
                            $data = array(
                                //'event_user_id' => $event_user->event_user_id,
                                'session_code'  => $event_session_data['event_session_code']
                            );

                            $return = $this->api_model->success($this, $data);
                        }
                        else
                        {
                            $return = $this->api_model->error($this, 'add_failed');
                        }
                    }
                }
                else
                {
                    $return = $this->api_model->error($this, 'login_invalid');
                }
            }
            else
            {
                $return = $this->api_model->error($this, 'no_record');
            }
        }

        return $return;
	}

    // --------------------------------------------------------------------

	/**
	 * info_post
	 * 
	 * Output Event Information
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robert.obias@google.com>
	 */
	public function info_post()
	{
        
        $session_code = $this->input->post('session_code');
        
        $this->form_validation->set_rules('session_code', 'Session Code', 'required');
        $this->form_validation->set_error_delimiters('', '');
		
        if($this->form_validation->run($this) == FALSE)
        {
            $return = $this->api_model->validation_error($this, validation_errors());
        }
        else
        {
            $session      = $this->api_model->session($session_code);
            $session      = (object)$session;

            switch($session->error_code)
            {
                case 'session_code_expired':
                    $return = $this->api_model->error($this, 'session_code_expired');
                break;
                case 'api_request_failed':
                    $return = $this->api_model->error($this, 'api_request_failed');
                break;
                default:
                    $event       = $this->events_model
                                                      ->join('accounts', 'account_id = event_account_id', 'LEFT')
                                                      ->join('event_types', 'event_types.event_type_id = events.event_type_id', 'LEFT')
                                                      ->join('event_categories', 'event_categories.event_category_id = events.event_category_id', 'LEFT')
                                                      ->join('mobile_themes', 'mobile_theme_id = event_mobile_theme_id', 'LEFT')
                                                      ->where('account_deleted', 0)
                                                      ->where('event_type_deleted', 0)
                                                      ->where('event_category_deleted', 0)
                                                      ->where('event_deleted', 0)
                                                      ->find_by(array('event_id' => $session->data->event_session_event_id));

                    $schedules   = $this->schedules_model->select('schedule_id AS id, schedule_name AS name, schedule_location AS location, schedule_address AS address, schedule_starts_on AS starts_on')
                                                         ->select('schedule_ends_on AS ends_on, schedule_status AS status')
                                                         ->where('schedule_event_id',$session->data->event_session_event_id)
                                                         ->where('schedule_status', 'Active')
                                                         ->where('schedule_deleted', 0)
                                                         ->find_all();


                    $ticket_types = $this->ticket_types_model->select('ticket_type_id AS id, ticket_type_name AS name, ticket_type_description AS description, ticket_type_currency AS currency')
                                                             ->select('ticket_type_amount AS amount, ticket_type_capacity AS capacity, ticket_type_available AS available')
                                                             ->select('ticket_type_sells_on AS sells_on, ticket_type_ends_on AS ends_on, ticket_type_min_qty AS min_qty')
                                                             ->select('ticket_type_max_qty AS max_qty, ticket_type_type AS type, ticket_type_status AS status')
                                                             ->where('ticket_type_event_id', $session->data->event_session_event_id)
                                                             ->where('ticket_type_status', 'Posted')
                                                             ->where('ticket_type_deleted', 0)
                                                             ->find_all();

                    $gates = $this->event_gates_model->select('event_gate_id AS id, event_gate_name AS gate_name, event_gate_ticket_type_ids AS ticket_type_ids')
                                                     ->select('event_gate_status AS status')
                                                     ->where('event_gate_event_id', $session->data->event_session_event_id)
                                                     ->where('event_gate_status', 'Active')
                                                     ->where('event_gate_deleted', 0)
                                                     ->find_all();

                    if($event)
                    {
                        $data = array(
                            'event' => array(
                                'id' => $event->event_id,
                                'account' => $event->account_name,
                                'type' => $event->event_type_name,
                                'category' => $event->event_category_name,
                                'name' => $event->event_name,
                                'description' => $event->event_description,
                                'rules' => $event->event_rules,
                                'organizer' => $event->event_organizer,
                                'starts_on' => date('F d, Y', strtotime($event->event_starts_on)),
                                'ends_on' => date('F d, Y', strtotime($event->event_ends_on)),
                                'contacts' => $event->event_contacts,
                                'photo' => site_url($event->event_photo),
                                'website' => $event->event_website,
                                'facebook' => $event->event_facebook,
                                'instagram' => $event->event_instagram,
                                'twitter' => $event->event_twitter,
                                'youtube' => $event->event_youtube,
                                'sponsors' => $event->event_sponsors,
                                'mobile_theme' => array(
                                    'name' => $event->mobile_theme_name,
                                    'code' => $event->mobile_theme_code,
                                    'screenshot' => site_url($event->mobile_theme_screenshot),
                                    'orientation' => $event->mobile_theme_orientation
                                ),
                                'mobile_logo' => site_url($event->event_mobile_logo),
                                'mobile_bg' => site_url($event->event_mobile_bg),
                                'code' => $event->event_code,
                                'has_microsite' => $event->event_has_microsite,
                                'has_multiple_schedules' => $event->event_has_multiple_schedules,
                                'has_ticket_levels' => $event->event_has_ticket_levels,
                                'has_attendance_tracking' => $event->event_has_attendance_tracking,
                                'has_sessions' => $event->event_has_sessions,
                                'has_sessions_tracking' => $event->event_has_sessions_tracking,
                                'has_feedback_module' => $event->event_has_feedback_module,
                                'has_survey_module' => $event->event_has_survey_module,
                                'has_raffle_module' => $event->event_has_raffle_module,
                                'has_badge_module' => $event->event_has_badge_module,
                                'status' =>$event->event_status,
                            ),
                            'schedules' => $schedules,
                            'ticket_types' => $ticket_types,
                            'gates' => $gates,
                        );

                        $return = $this->api_model->success($this, $data);
                    }
                    else
                    {
                        $return = $this->api_model->error($this, 'no_record');
                    }
            }
        }
        
        
        return $return;
        
	}
    
     // --------------------------------------------------------------------

	/**
	 * ticket_post
	 * 
	 * Display Tickets
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robert.obias@google.com>
	 */
	public function tickets_post()
	{
        
        $session_code = $this->input->post('session_code');
        
        $this->form_validation->set_rules('session_code', 'Session Code', 'required');
        $this->form_validation->set_error_delimiters('', '');
		
        if($this->form_validation->run($this) == FALSE)
        {
            $return = $this->api_model->validation_error($this, validation_errors());
        }
        else
        {
            $session  = $this->api_model->session($session_code);
            $session  = (object)$session;

            switch($session->error_code)
            {
                case 'session_code_expired':
                    $return = $this->api_model->error($this, 'session_code_expired');
                break;
                case 'api_request_failed':
                    $return = $this->api_model->error($this, 'api_request_failed');
                break;
                default:
                    $tickets = $this->tickets_model->select('ticket_seat_number AS seat_number, ticket_is_pwd AS is_pwd, ticket_is_senior AS is_senior, ticket_company AS company')
                                                   ->select('ticket_registered_on AS registered_on, ticket_qrcode AS qrcode, ticket_qr_status AS qr_status')
                                                   ->select('ticket_rfid AS rfid, ticket_status AS status')
                                                   ->where('ticket_event_id', $session->data->event_session_event_id)
                                                   ->where('ticket_deleted', 0)
                                                   ->find_all();
                    if($tickets)
                    {
                        $data = $tickets;
                        $return = $this->api_model->success($this, $data);
                    }
                    else
                    {
                        $return = $this->api_model->error($this, 'no_record');
                    }
            }
        }
        
        return $return;
	}

    // --------------------------------------------------------------------
    
    /**
	 * Refresh Session
	 * 
	 * Check if session exist and is valid
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <robert.obias@google.com>
	 */
    public function refresh_session_post()
    {
        $old_session_code = $this->input->post('old_session_code');
        
        $this->form_validation->set_rules('old_session_code', 'Session Code', 'required');
        $this->form_validation->set_error_delimiters('', '');
		
        if($this->form_validation->run($this) == FALSE)
        {
            $return = $this->api_model->validation_error($this, validation_errors());
        }
        else
        {
            $session      = $this->api_model->session($old_session_code);
            $session      = (object)$session;
            
            if($session->error_code == 'session_code_expired')
            {
                $event_session_data = array(
                    'event_session_expire' => date('Y-m-d h:i:s' ,strtotime('+30 minutes')),
                    'event_session_code' => random_string('sha1')
                );

                $update = $this->event_sessions_model->update(array('event_session_code' => $old_session_code), $event_session_data);

                if($update)
                {
                    $return = $this->api_model->success($this, array('session_code' => $event_session_data['event_session_code']));
                }
                else
                {
                    $return = $this->api_model->error($this, 'update_failed');
                }
            }
            else
            {
                $return = $this->api_model->error($this, 'api_request_failed');
            }
            
        }
        
        return $return;
    }
}
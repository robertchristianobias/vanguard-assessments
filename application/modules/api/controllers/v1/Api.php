<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Api Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2014-2015, Robert Christian Obias
 * @link		rchristian_obias@yahoo.com
 */
require APPPATH.'modules/api/libraries/REST_Controller.php';

class Api extends REST_Controller
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	function __construct()
	{
		// Construct our parent class
		parent::__construct();

		$this->load->model('api_model');
	}

	// --------------------------------------------------------------------

	/**
	 * configs_post
	 * 
	 * This is a sample API that outputs data from the database.
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function configs_post()
	{
		// get the configs
		$configs = $this->api_model->get_configs();

		if ($configs)
		{
			$this->response($configs, 200); // 200 being the HTTP response code
		}
		else
		{
			$this->api_model->error($this, 'no_record');
		}
	}
}
<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Ticket Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2016, Google.
 * @link		http://www.google.com
 */
require APPPATH.'modules/api/libraries/REST_Controller.php';

class Ticket extends REST_Controller
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	function __construct()
	{
		// Construct our parent class
		parent::__construct();

		$this->load->model('events/events_model');
		$this->load->model('events/event_users_model');
		$this->load->model('events/event_attendances_model');
		$this->load->model('events/event_sessions_model');
		$this->load->model('events/event_gates_model');
		$this->load->model('events/schedules_model');
		$this->load->model('events/tickets_model');
		$this->load->model('events/ticket_types_model');
		$this->lang->load('events/tickets');
		$this->load->model('api_model');
	}

	// --------------------------------------------------------------------

	/**
	 * scan_post
	 * 
	 * This is a sample API that outputs data from the database.
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */

	public function scan_post()
	{
		// validate inputs
		$this->form_validation->set_rules('session_code', lang('session_code'), 'required|max_length[100]');
		$this->form_validation->set_rules('qrcode', lang('qrcode'), 'required');
		$this->form_validation->set_rules('gate_id', lang('gate_id'), 'required|max_length[100]');
		$this->form_validation->set_error_delimiters('', '');
		
		if ($this->form_validation->run($this) == FALSE)
		{
			$return = $this->api_model->validation_error($this, validation_errors());
		}
		else
		{
			// check user session 
			$event_user = $this->event_sessions_model->find_by(array("event_session_code" => $this->post('session_code')));

			if (! $event_user )
			{
				$return = $this->api_model->error($this, 'api_request_failed');
			}
			else
			{
				$return = $this->_validate_post($event_user);				
			}
		}

		return $return;
	}

	private function _validate_post($user_data)
	{

		// validate QR Code
		$ticket = $this->tickets_model->find_by(array('ticket_qrcode' => $this->post('qrcode')));
		if (! $ticket) 
		{
			$this->api_model->error($this, 'no_record');
		}

		// validate ticket if already used
		$ticket_used = $this->tickets_model->find_by(array('ticket_id' => $ticket->ticket_id , 'ticket_status' => 'Attended'));
		if ($ticket_used)
		{
			$return = $this->api_model->error($this, 'ticket_invalid');
		}

		// check if 'Event' exists
		$this->db->where("DATE(NOW()) BETWEEN DATE(event_starts_on) AND DATE(event_ends_on)"); // check event date end
		$event = $this->events_model->find_by(array("event_id" => $ticket->ticket_event_id , "event_status" => 'Active'));
		if (! $event)
		{
			$return = $this->api_model->error($this, 'no_record');
		}

		// change ticket status to 'Attended'
		$this->tickets_model->update($ticket->ticket_id, array("ticket_status" => "Attended"));

		// check if events has attendance tracking
		if ($event->event_has_attendance_tracking)
		{
			// prepare attendance data
			$data = array(
					"event_attendance_event_id" 	=> (int)$event->event_id,
					"event_attendance_ticket_id" 	=> (int)$ticket->ticket_event_id,
					"event_attendance_gate_id" 		=> (int)$this->post("gate_id"),
					"event_attendance_user_id" 		=> (int)$user_data->event_session_event_user_id,
				);

			// insert event attendance
			$this->event_attendances_model->insert($data);
		}

		// get event schedules
        $schedules   = $this->schedules_model->select('schedule_id AS id, schedule_name AS name, schedule_location AS location, schedule_address AS address, schedule_starts_on AS starts_on')
                                             ->select('schedule_ends_on AS ends_on, schedule_status AS status')
                                             ->where('schedule_event_id', $event->event_id)
                                             ->where('schedule_status', 'Active')
                                             ->where('schedule_deleted', 0)
                                             ->find_all();

	    // get gates
	    $gates = $this->event_gates_model->select('event_gate_id AS id, event_gate_name AS gate_name, event_gate_ticket_type_ids AS ticket_type_ids')
                                             ->select('event_gate_status AS status')
                                             ->where('event_gate_event_id', $event->event_id)
                                             ->where('event_gate_id', $this->post("gate_id"))
                                             ->where('event_gate_status', 'Active')
                                             ->where('event_gate_deleted', 0)
                                             ->find_all();

        // get ticket type
        $ticket_types = $this->ticket_types_model->select('ticket_type_id AS id, ticket_type_name AS name, ticket_type_description AS description, ticket_type_currency AS currency')
	                                         ->select('ticket_type_amount AS amount, ticket_type_capacity AS capacity, ticket_type_available AS available')
	                                         ->select('ticket_type_sells_on AS sells_on, ticket_type_ends_on AS ends_on, ticket_type_min_qty AS min_qty')
	                                         ->select('ticket_type_max_qty AS max_qty, ticket_type_type AS type, ticket_type_status AS status')
	                                         ->where_in('ticket_type_id', json_decode($gates[0]->ticket_type_ids))
	                                         ->where('ticket_type_status', 'Posted')
	                                         ->where('ticket_type_deleted', 0)
	                                         ->find_all();

		$data = array(
				"event" => array(
	                    'id' 			=> $event->event_id,
	                    'name' 			=> $event->event_name,
	                    'description' 	=> $event->event_description,
	                    'rules' 		=> $event->event_rules,
	                    'organizer' 	=> $event->event_organizer,
	                    'starts_on' 	=> date('F d, Y', strtotime($event->event_starts_on)),
	                    'ends_on' 		=> date('F d, Y', strtotime($event->event_ends_on)),
	                    'contacts' 		=> $event->event_contacts,
	                    'photo' 		=> site_url($event->event_photo),
	                    'website' 		=> $event->event_website,
	                    'facebook' 		=> $event->event_facebook,
	                    'instagram' 	=> $event->event_instagram,
	                    'twitter' 		=> $event->event_twitter,
	                    'youtube' 		=> $event->event_youtube,
	                    'sponsors' 		=> $event->event_sponsors,
	                    'code' 			=> $event->event_code,
	                    'status' 		=>$event->event_status,
					),
	            "tickets" => array(
	            		"id" 			=> $ticket->ticket_id,
	            		"seat_number" 	=> $ticket->ticket_seat_number,
	            		"firstname" 	=> $ticket->ticket_firstname,
	            		"middlename" 	=> $ticket->ticket_middlename,
	            		"lastname" 		=> $ticket->ticket_lastname,
	            		"mobile" 		=> $ticket->ticket_mobile,
	            		"phone" 		=> $ticket->ticket_phone,
	            		"email" 		=> $ticket->ticket_email,
	            		"gender" 		=> $ticket->ticket_gender,
	            		"birthday" 		=> $ticket->ticket_birthday,
	            		"is_pwd" 		=> $ticket->ticket_is_pwd,
	            		"is_senior" 	=> $ticket->ticket_is_senior,
	            		"company" 		=> $ticket->ticket_company,
	            		"occupation" 	=> $ticket->ticket_occupation,
	            		"address1" 		=> $ticket->ticket_address1,
	            		"address2" 		=> $ticket->ticket_address2,
	            		"city" 			=> $ticket->ticket_city,
	            		"zipcode" 		=> $ticket->ticket_zipcode,
	            		"country" 		=> $ticket->ticket_country,
	            	),
	            "schedules" 	=> $schedules,
	            "ticket_types" 	=> $ticket_types,
	            "gates" 		=> $gates
			);

		$return = $this->api_model->success($this, $data);

		return $return;
	}
}
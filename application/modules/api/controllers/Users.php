<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * User Class
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2015, Google.
 * @link		http://www.google.com
 */
class Users extends MX_Controller 
{
	/**
	 * Constructor
	 *
	 * @access	public
	 *
	 */
	function __construct()
	{
		parent::__construct();

		$this->load->model('api_users_model');
		$this->load->language('users');
	}
	
	// --------------------------------------------------------------------

	/**
	 * index
	 *
	 * @access	public
	 * @param	none
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function index()
	{
		$this->acl->restrict('api.users.list');
		
		// page title
		$data['page_heading'] = lang('index_heading');
		$data['page_subhead'] = lang('index_subhead');
		
		// breadcrumbs
		$this->breadcrumbs->push(lang('crumb_home'), site_url(''));
		$this->breadcrumbs->push(lang('crumb_module'), site_url('develop/builder'));
		$this->breadcrumbs->push(lang('index_heading'), site_url('users'));
		
		// datatables
		$this->template->add_css('assets/plugins/DataTables/datatables.min.css');
		$this->template->add_js('assets/plugins/DataTables/datatables.min.js');
		
		// render the page
		$this->template->add_css(module_css('api', 'users_index'), 'embed');
		$this->template->add_js(module_js('api', 'users_index'), 'embed');
		$this->template->write_view('content', 'users_index', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * datatables
	 *
	 * @access	public
	 * @param	mixed datatables parameters (datatables.net)
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	public function datatables()
	{
		$this->acl->restrict('api.users.list');

		echo $this->api_users_model->get_datatables();
	}

	// --------------------------------------------------------------------

	/**
	 * form
	 *
	 * @access	public
	 * @param	$action string
	 * @param   $id integer
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	function form($action = 'add', $id = FALSE)
	{
		$this->acl->restrict('api.users.' . $action, 'modal');

		$data['page_heading'] = lang($action . '_heading');
		$data['action'] = $action;

		if ($this->input->post())
		{
			if ($this->_save($action, $id))
			{
				echo json_encode(array('success' => true, 'message' => lang($action . '_success'))); exit;
			}
			else
			{	
				$response['success'] = FALSE;
				$response['message'] = lang('validation_error');
				$response['errors'] = array(					
					'username'		=> form_error('username'),
					'password'		=> form_error('password'),
					'api_key'		=> form_error('api_key'),
					'status'		=> form_error('status'),
				);
				echo json_encode($response);
				exit;
			}
		}

		// random password and api key
		$data['new_password'] = random_string('sha1');
		$data['new_api_key'] = random_string('sha1');

		if ($action != 'add') $data['record'] = $this->api_users_model->find($id);

		// render the page
		$this->template->set_template('modal');
		$this->template->add_css(module_css('api', 'users_form'), 'embed');
		$this->template->add_js(module_js('api', 'users_form'), 'embed');
		$this->template->write_view('content', 'users_form', $data);
		$this->template->render();
	}

	// --------------------------------------------------------------------

	/**
	 * delete
	 *
	 * @access	public
	 * @param	integer $id
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	function delete($id)
	{
		$this->acl->restrict('api.users.delete', 'modal');

		$data['page_heading'] = lang('delete_heading');
		$data['page_confirm'] = lang('delete_confirm');
		$data['page_button'] = lang('button_delete');
		$data['datatables_id'] = '#datatables';

		if ($this->input->post())
		{
			$this->api_users_model->delete($id);

			echo json_encode(array('success' => true, 'message' => lang('delete_success'))); exit;
		}

		$this->load->view('../../../views/confirm', $data);
	}


	// --------------------------------------------------------------------

	/**
	 * _save
	 *
	 * @access	private
	 * @param	string $action
	 * @param 	integer $id
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
	private function _save($action = 'add', $id = 0)
	{
		// validate inputs
		$this->form_validation->set_rules('username', lang('username'), 'required');
		$this->form_validation->set_rules('password', lang('password'), 'required');
		$this->form_validation->set_rules('api_key', lang('api_key'), 'required');
		$this->form_validation->set_rules('status', lang('status'), 'required');

		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
		
		if ($this->form_validation->run($this) == FALSE)
		{
			return FALSE;
		}

		$data = array(
			'username'		=> $this->input->post('username'),
			'password'		=> $this->input->post('password'),
			'api_key'		=> $this->input->post('api_key'),
			'status'		=> $this->input->post('status'),
		);
		

		if ($action == 'add')
		{
			$insert_id = $this->api_users_model->insert($data);
			$return = (is_numeric($insert_id)) ? $insert_id : FALSE;
		}
		else if ($action == 'edit')
		{
			$return = $this->api_users_model->update($id, $data);
		}

		return $return;

	}
}

/* End of file Users.php */
/* Location: ./application/modules/api/controllers/Users.php */
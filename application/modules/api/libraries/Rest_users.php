<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Rest_users Class
 *
 * This class loads the users from api_users table
 *
 * @package		rcmediaph
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 * @copyright 	Copyright (c) 2015, Google
 * @link		
 */
class Rest_users {
	
	 /**
	  * Constructor
	  *
	  * @access	public
	  *
	  */
	public function __construct()
	{	
		$this->CI =& get_instance();

		$this->CI->load->driver('cache', $this->CI->config->item('cache_drivers'));

		// check if api_users table exists
		if ($this->CI->db->table_exists('api_users'))
		{
			if (! $api_users = $this->CI->cache->get('api_users'))
			{
				$api_users = $this->CI->db
					->select('username, password')
					->where('au_deleted', 0)
					->where('status', 'Active')
					->get('api_users');

				if ($api_users = $api_users->result())
				{
					// then save to cache
					$this->CI->cache->save('api_users', $api_users, 300); // cache for 5 mins
				}
			}
			
			$valid_logins = array();
			foreach ($api_users as $user)
			{
				$valid_logins[$user->username] = $user->password;
			}

			$this->CI->config->set_item('rest_valid_logins', $valid_logins);
		 
			log_message('debug', "Rest_users Class Initialized");
		}
		else
		{
			// die('Configs table does not exists');
		}
	}
}

/* End of file Rest_users.php */
/* Location: ./application/libraries/Rest_users.php */
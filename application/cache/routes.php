<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$route['home'] = 'frontend/page/view/home';
$route['examinees-consent'] = 'frontend/page/view/examinees-consent';
$route['our-products'] = 'frontend/page/view/our-products';
$route['contact-us'] = 'frontend/page/view/contact-us';
$route['test'] = 'frontend/page/view/test';
$route['sdfsdf'] = 'frontend/page/view/sdfsdf';
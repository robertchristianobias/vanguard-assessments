<?php  if ( ! defined('BASEPATH')) { exit('No direct script access allowed'); }
/**
 * Callbacks Class
 *
 *
 * @package		Callback
 * @version		1.0
 * @author 		Robert Christian Obias <rchristian_obias@yahoo.com>
 */
class Callbacks {

    public function __construct()
    {
        
    }

    // --------------------------------------------------------------------
	/**
	 * Tests
	 *
	 * @access	public
	 * @param	array $params
	 * @author 	Robert Christian Obias <rchristian_obias@yahoo.com>
	 */
    public static function questions($params)
    {
        $CI =& get_instance();
        $data = array();
        $CI->load->model('tests/question_factors_model');

        if ( $params )
        {
            $counter = 0;
            $arr = array();
            foreach ( $params as $key => $val )
            {
                $factors = $CI->question_factors_model->get_question_factors($val['question_id']);
                $factor_str = '';
                if ( $factors )
                {
                    foreach ( $factors as $factor )
                    {
                        $factor_str .= $factor->factor_name . ', ';
                    }
                }

                $arr = array(
                    'question_factor_id' => substr($factor_str, 0, -2)
                );
                
                $data[$counter] = array_merge($params[$counter], $arr);
                $counter++;
            }
        }
        return $data;
    }

    public static function test_batteries($params)
    {
        $CI =& get_instance();
        $CI->load->model('tests/tests_model');

        $data = array();

        if ( $params )
        {
            $counter = 0;
            $arr = array();
            foreach ( $params as $key => $val )
            {
                $test_battery_tests = json_decode($val['test_battery_tests']);
                $test_str = '';
                foreach ( $test_battery_tests as $v )
                {
                    $test = $CI->tests_model->get_test($v);
                    $test_str .= '- '.$test->test_name . '<br />';
                }

                $arr = array(
                    'test_battery_tests' => $test_str
                );
                
                $data[$counter] = array_merge($params[$counter], $arr);
                $counter++;
            }
        }
        return $data;
    }
}